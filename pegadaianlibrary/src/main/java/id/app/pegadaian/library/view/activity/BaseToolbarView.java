package id.app.pegadaian.library.view.activity;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;

/**
 * Created by Dell on 3/5/2018.
 */

public interface BaseToolbarView {
    AppCompatTextView getToolbarTitle();
}
