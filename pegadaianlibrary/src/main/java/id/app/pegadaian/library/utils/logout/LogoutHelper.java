package id.app.pegadaian.library.utils.logout;

import android.app.Activity;

import id.app.pegadaian.library.widget.colordialog.PromptDialog;

/**
 * Created by Dell on 3/15/2018.
 */

public class LogoutHelper {

    private Activity activity;
    private String title;
    private String message;
    private String ok;
    private PromptDialog.OnPositiveListener onPositiveListener;

    public LogoutHelper(Activity activity) {
        this.activity = activity;
    }

    public LogoutHelper setTitle(String title) {
        this.title = title;
        return this;
    }

    public LogoutHelper setMessage(String message) {
        this.message = message;
        return this;
    }

    public LogoutHelper setOk(String ok) {
        this.ok = ok;
        return this;
    }

    public LogoutHelper setOnPositiveListener(PromptDialog.OnPositiveListener onPositiveListener) {
        this.onPositiveListener = onPositiveListener;
        return this;
    }

    public void showDialogError() {
        PromptDialog promptDialog = new PromptDialog(activity)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(title)
                .setContentText(message)
                .setPositiveListener(ok, onPositiveListener);
        promptDialog.setCancelable(false);
        promptDialog.show();
    }
}