package id.app.pegadaian.library;

import android.app.Application;

import dagger.android.HasActivityInjector;

/**
 * Created by Dell on 2/27/2018.
 */

public abstract class BaseApplication extends Application implements HasActivityInjector {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
