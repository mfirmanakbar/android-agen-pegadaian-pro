package id.app.pegadaian.library.callback;

import java.util.logging.Level;
import java.util.logging.Logger;

import id.app.pegadaian.library.common.LogMode;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.functions.Consumer;

/**
 * Created by user on 6/6/2017.
 */

public class ConsumerThrowableAdapter implements Consumer<Throwable> {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void accept(Throwable throwable) throws Exception {
        if (ConfigurationUtils.IS_RUSH_B_MODE.equals(LogMode.TAG_LOG_MODE))
            logger.log(Level.SEVERE, this.getClass().getName(), throwable);
    }
}