package id.app.pegadaian.library.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


/**
 * Response holder provided to the UI
 */
public class Response {

    public Status status;

    @Nullable
    public Object result;

    @Nullable
    public Throwable error;

    public String type;

    private Response(Status status, @Nullable Throwable error) {
        this.status = status;
        this.error = error;
    }

    public Response(Status status, Object result) {
        this.status = status;
        this.result = result;
        this.type = null;
    }

    public Response(Status status, Object result, String type) {
        this.status = status;
        this.result = result;
        this.type = type;
    }

    public static Response loading() {
        return new Response(Status.LOADING, "");
    }

    public static Response loading(String type) {
        return new Response(Status.LOADING, "", type);
    }

    public static Response success(@NonNull Object data) {
        return new Response(Status.SUCCESS, data);
    }

    public static Response success(@NonNull Object data, String type) {
        return new Response(Status.SUCCESS, data, type);
    }

    public static Response error(@NonNull Object error) {
        return new Response(Status.ERROR, error);
    }

    public static Response error(@NonNull Object error, String type) {
        return new Response(Status.ERROR, error, type);
    }

    public static Response error(@NonNull Throwable error) {
        return new Response(Status.ERROR, error);
    }
}