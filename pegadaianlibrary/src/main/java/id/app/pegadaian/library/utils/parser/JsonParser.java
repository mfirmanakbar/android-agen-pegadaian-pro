package id.app.pegadaian.library.utils.parser;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;

import javax.inject.Inject;

/**
 * Created by Dell on 2/27/2018.
 */

public class JsonParser {

    private Gson gson;

    @Inject
    public JsonParser(Gson gson) {
        this.gson = gson;
    }

    public <T> T getObject(Object object, Class<T> classOfT) {
        JsonObject jsonObject = gson.toJsonTree(object).getAsJsonObject();
        return gson.fromJson(jsonObject.toString(), classOfT);
    }

    public <T> T getObjects(Object object, Type type) {
        JsonArray jsonArray = gson.toJsonTree(object).getAsJsonArray();
        return gson.fromJson(jsonArray, type);
    }

    public <T> T getObject(String value, Class<T> classOfT) {
        return gson.fromJson(value, classOfT);
    }

    public String getObjectToString(Object object) {
        return gson.toJson(object);
    }
}