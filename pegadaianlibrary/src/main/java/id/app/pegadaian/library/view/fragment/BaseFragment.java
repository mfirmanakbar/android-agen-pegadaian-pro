package id.app.pegadaian.library.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.mikepenz.fastadapter.listeners.EventHook;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import id.app.pegadaian.library.R;


/**
 * Created by Dell on 8/21/2017.
 */

public abstract class BaseFragment extends Fragment implements Validator.ValidationListener {

    private Validator validator;

    protected void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    protected void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }

    protected void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view.isFocused() || imm.isActive())
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    protected void showSnackBar(Activity activity, String message) {
        Snackbar.make(activity.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG).show();
    }

    protected Intent getIntent(Context packageContext, Class<?> cls) {
        return new Intent(packageContext, cls);
    }

    protected void showActivityAndFinishCurrentActivity(Intent intent) {
        showActivity(intent);
        getActivity().finish();
    }

    protected void showActivity(Intent intent) {
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    public Typeface getLatoRegular() {
        return Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");
    }

    public Typeface getLatoBold() {
        return Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Bold.ttf");
    }

    public Typeface getRonniaRegular() {
        return Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/RonniaReg.otf");
    }

    public Validator getValidator() {
        return validator;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof AppCompatEditText) {
                ((AppCompatEditText) view).setError(message);
            } else {
                showSnackBar(getActivity(), message);
            }
        }
    }

    protected void attachSpinner(Activity activity, Spinner spinner, List<String> values) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(activity,
                id.app.pegadaian.library.R.layout.view_spinner_type, values);
        dataAdapter.setDropDownViewResource(id.app.pegadaian.library.R.layout.view_spinner_item_type);
        spinner.setAdapter(dataAdapter);
    }

    protected void attachSpinner(Activity activity, Spinner spinner, List<String> values, AdapterView.OnItemSelectedListener listener) {
        attachSpinner(activity, spinner, values);
        spinner.setOnItemSelectedListener(listener);
    }

    protected void configureAdapter(FastItemAdapter<?> adapter,
                                    RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        if (!adapter.isSelectable())
            adapter.withSelectable(true);
    }
}