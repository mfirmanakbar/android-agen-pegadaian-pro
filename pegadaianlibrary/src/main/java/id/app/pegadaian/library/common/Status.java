package id.app.pegadaian.library.common;

/**
 * Possible status types of a response provided to the UI
 */
public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}
