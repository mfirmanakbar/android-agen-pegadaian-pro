package id.app.pegadaian.library.utils.file;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Response;

/**
 * Created by Dell on 1/22/2018.
 */

public class DownloadHelper {

    private static String pathname = "/agent";

    public static Function<Response<ResponseBody>, Observable<File>> processResponse(String filename) {
        return responseBodyResponse -> saveToDisk(responseBodyResponse, filename);
    }

    private static Observable<File> saveToDisk(final Response<ResponseBody> response, String filename) {
        return Observable.create(subscriber -> {
            try {
                String urlPath = Environment.getExternalStorageDirectory().getAbsolutePath() + pathname;
                new File(urlPath).mkdir();
                File destinationFile = new File(urlPath + "/" + filename);
                destinationFile.createNewFile();
                BufferedSink bufferedSink = Okio.buffer(Okio.sink(destinationFile));
                bufferedSink.writeAll(response.body().source());
                bufferedSink.close();

                subscriber.onNext(destinationFile);
                subscriber.onComplete();
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }
}
