package id.app.pegadaian.library.utils.share;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;

/**
 * Created by Dell on 8/22/2017.
 */

public class ShareFileHelper {

    public static void shareStruk(File file,
                                  String subject,
                                  String message,
                                  Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.setType("application/pdf");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(intent, subject)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}