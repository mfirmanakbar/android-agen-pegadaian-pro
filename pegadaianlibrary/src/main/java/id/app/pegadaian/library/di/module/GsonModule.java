package id.app.pegadaian.library.di.module;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Dell on 2/27/2018.
 */

@Module
public class GsonModule {

    @Named("Gson")
    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }
}
