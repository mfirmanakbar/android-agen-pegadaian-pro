package id.app.pegadaian.library.utils.string;

import android.support.annotation.NonNull;
import android.util.Patterns;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by user on 5/30/2017.
 */

public class StringHelper {

    public static String getStringBuilderToString(String... items) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : items) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }

    public static String getStringBuilderToStringFromList(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = list.size() - 1;
        for (int x = 0; x < list.size(); x++) {
            stringBuilder.append(list.get(x));
            if (x != i) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    @NonNull
    public static String getStringBuilderToStringFromList(List<String> list, String divider) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = list.size() - 1;
        for (int x = 0; x < list.size(); x++) {
            stringBuilder.append(list.get(x));
            if (x != i) {
                stringBuilder.append(divider);
            }
        }
        return stringBuilder.toString();
    }

    public static String getSimplePriceFormatter(Double price) {
        return String.format("%,.0f", price).replace(",", ".");
    }

    public static String getSimplePriceFormatter(String price) {
        return String.format("%,.0f", Double.valueOf(price)).replace(",", ".");
    }

    public static String getSimplePriceWithoutFormatter(Double price) {
        return String.format("%,.0f", price).replace(",", "");
    }

    public static String getPriceInRp(Double price) {
        return getStringBuilderToString("Rp ", getSimplePriceFormatter(price));
    }

    public static String getPriceInRp(BigDecimal price) {
        return getStringBuilderToString("Rp ", getSimplePriceFormatter(price.doubleValue()));
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static String[] splitString(String divider, String value) {
        return value.split(divider);
    }

    public static boolean isUrl(String url) {
        return StringUtils.isNotBlank(url) && (url.contains("http://") || url.contains("https://"));
    }

    public static boolean isEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPhone(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }
}