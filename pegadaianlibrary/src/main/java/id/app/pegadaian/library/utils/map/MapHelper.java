package id.app.pegadaian.library.utils.map;

import java.util.Map;

/**
 * Created by Dell on 3/9/2018.
 */

public class MapHelper {

    public static Object getKeyFromValue(Map hm, Object value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }
}
