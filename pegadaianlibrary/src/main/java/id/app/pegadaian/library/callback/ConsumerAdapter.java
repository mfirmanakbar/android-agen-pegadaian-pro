package id.app.pegadaian.library.callback;

import java.util.logging.Level;
import java.util.logging.Logger;

import id.app.pegadaian.library.common.LogMode;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import io.reactivex.functions.Consumer;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by user on 6/6/2017.
 */

public class ConsumerAdapter<T> implements Consumer<T> {
    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void accept(T t) throws Exception {
        if (ConfigurationUtils.IS_RUSH_B_MODE.equals(LogMode.TAG_LOG_MODE))
            logger.log(Level.INFO, this.getClass().getName(), StringHelper.getStringBuilderToString("subscribe onNext : ", t.toString()));
    }
}