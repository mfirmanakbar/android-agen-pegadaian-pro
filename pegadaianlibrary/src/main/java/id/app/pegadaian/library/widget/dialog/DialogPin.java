package id.app.pegadaian.library.widget.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import id.app.pegadaian.library.R;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public class DialogPin extends DialogFragment{

    private Button buttonCancel;
    private Button buttonOk;
    private EditText pinTransaksi;

    private View.OnClickListener positiveListener;
    private View.OnClickListener negativeListener;
    int containerWidth;
    private WindowManager.LayoutParams dialogLayoutParams = new WindowManager.LayoutParams();

    public static DialogPin newInstance() {
        return new DialogPin();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        containerWidth = getResources().getDimensionPixelSize
                (R.dimen.dialog_container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View customView = inflater.inflate(R.layout.dialog_pin, null);
        buttonCancel = customView.findViewById(R.id.button_cancel);
        buttonOk = customView.findViewById(R.id.button_ok);
        pinTransaksi = customView.findViewById(R.id.input_pin);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(customView);

        Dialog dialog = dialogBuilder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnKeyListener((dialog1, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK);

        if (getPositiveListener() != null) {
            buttonOk.setOnClickListener(getPositiveListener());
        }
        if (getNegativeListener() != null) {
            buttonCancel.setOnClickListener(getNegativeListener());
        }
        return dialog;
    }

    public void onStart() {
        super.onStart();
        if (getDialog() != null) {
            dialogLayoutParams.copyFrom(getDialog().getWindow().getAttributes());
            dialogLayoutParams.width = containerWidth;
            getDialog().getWindow().setAttributes(dialogLayoutParams);
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public View.OnClickListener getPositiveListener() {
        return positiveListener;
    }

    public void setPositiveListener(View.OnClickListener positiveListener) {
        this.positiveListener = positiveListener;
    }

    public void dismissDialog() {
        dismissAllowingStateLoss();
    }

    public String getPinTransaksi() {
        return pinTransaksi.getText().toString();
    }

    public View.OnClickListener getNegativeListener() {
        return view -> DialogPin.this.dismissAllowingStateLoss();
    }

    public void setNegativeListener(View.OnClickListener negativeListener) {
        this.negativeListener = negativeListener;
    }

}
