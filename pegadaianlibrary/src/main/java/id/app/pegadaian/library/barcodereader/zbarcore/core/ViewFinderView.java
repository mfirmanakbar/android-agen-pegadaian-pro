package id.app.pegadaian.library.barcodereader.zbarcore.core;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import id.app.pegadaian.library.R;

public class ViewFinderView extends View implements IViewFinder {
    private static final String TAG = "ViewFinderView";

    private Rect mFramingRect;

    private static final float PORTRAIT_WIDTH_RATIO = 6f / 8;
    private static final float PORTRAIT_WIDTH_HEIGHT_RATIO = 0.75f;
    private static final float SQUARE_DIMENSION_RATIO = 0.625F;
    private static final float SQUARE_DIMENSION_RATIO_MINIMUM = 0.425F;
    private static final float LANDSCAPE_HEIGHT_RATIO = 5f / 8;
    private static final float LANDSCAPE_WIDTH_HEIGHT_RATIO = 1.4f;
    private static final int MIN_DIMENSION_DIFF = 50;

    private float defaultSquareDimensionRatio = 5f / 8;

    private static final int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192, 128, 64};
    private int scannerAlpha;
    private static final int POINT_SIZE = 10;
    private static final long ANIMATION_DELAY = 80l;

    private final int mDefaultLaserColor = getResources().getColor(R.color.viewfinder_laser);
    private int mDefaultMaskColor = getResources().getColor(R.color.viewfinder_mask);
    private final int mDefaultBorderColor = getResources().getColor(R.color.viewfinder_border);
    private final int mDefaultBorderStrokeWidth = getResources().getInteger(R.integer.viewfinder_border_width);
    private final int mDefaultBorderLineLength = getResources().getInteger(R.integer.viewfinder_border_length);
    private int cornerRadius = getResources().getDimensionPixelSize(R.dimen.masking_border_radius);
    ;

    protected Paint mLaserPaint;
    protected Paint mFinderMaskPaint;
    protected Paint mBorderPaint;
    protected int mBorderLineLength;
    protected boolean mSquareViewFinder;
    private boolean mIsLaserEnabled;
    private float mBordersAlpha;
    private int mViewFinderOffset = 0;
    private Paint areaTradeMarkPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private float squareTradeMarkRatio;

    public ViewFinderView(Context context) {
        super(context);
        init();
        initRatio();
    }

    public ViewFinderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
        initRatio();
    }

    public ViewFinderView(Context context, boolean isWithBorder, int maskColor) {
        super(context);
        mDefaultMaskColor = maskColor;
        init(isWithBorder);
        initRatio();
    }

    public ViewFinderView(Context context, boolean isWithBorder) {
        super(context);
        init(isWithBorder);
        initRatio();
    }

    private void initRatio() {
        int densityDpi = getResources().getDisplayMetrics().densityDpi;

        if (densityDpi == DisplayMetrics.DENSITY_LOW || densityDpi == DisplayMetrics.DENSITY_MEDIUM) {
            squareTradeMarkRatio = SQUARE_DIMENSION_RATIO_MINIMUM;
        } else {
            squareTradeMarkRatio = SQUARE_DIMENSION_RATIO;
        }
    }

    private void init(boolean isWithBorder) {
        //set up laser paint
        mLaserPaint = new Paint();
        mLaserPaint.setColor(mDefaultLaserColor);
        mLaserPaint.setStyle(Paint.Style.FILL);

        //finder mask paint
        mFinderMaskPaint = new Paint();
        mFinderMaskPaint.setColor(mDefaultMaskColor);

        if (isWithBorder) {
            areaTradeMarkPaint.setColor(Color.RED);
            areaTradeMarkPaint.setDither(true);
            areaTradeMarkPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            //border paint
            mBorderPaint = new Paint();
            mBorderPaint.setColor(mDefaultBorderColor);
            mBorderPaint.setStyle(Paint.Style.STROKE);
            mBorderPaint.setStrokeWidth(mDefaultBorderStrokeWidth);
            mBorderPaint.setStrokeJoin(Paint.Join.ROUND);
            mBorderPaint.setDither(true);
            mBorderPaint.setPathEffect(new CornerPathEffect(10));

            mBorderLineLength = mDefaultBorderLineLength;
        }
    }

    private void init() {
        //set up laser paint
        mLaserPaint = new Paint();
        mLaserPaint.setColor(mDefaultLaserColor);
        mLaserPaint.setStyle(Paint.Style.FILL);

        //finder mask paint
        mFinderMaskPaint = new Paint();
        mFinderMaskPaint.setColor(mDefaultMaskColor);

        //border paint
        mBorderPaint = new Paint();
        mBorderPaint.setColor(mDefaultBorderColor);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(mDefaultBorderStrokeWidth);
        mBorderPaint.setAntiAlias(true);

        mBorderLineLength = mDefaultBorderLineLength;
    }

    @Override
    public void setLaserColor(int laserColor) {
        mLaserPaint.setColor(laserColor);
    }

    @Override
    public void setMaskColor(int maskColor) {
        mFinderMaskPaint.setColor(maskColor);
    }

    @Override
    public void setBorderColor(int borderColor) {
        if (mBorderPaint != null)
            mBorderPaint.setColor(borderColor);
    }

    @Override
    public void setBorderStrokeWidth(int borderStrokeWidth) {
        if (mBorderPaint != null)
            mBorderPaint.setStrokeWidth(borderStrokeWidth);
    }

    @Override
    public void setBorderLineLength(int borderLineLength) {
        mBorderLineLength = borderLineLength;
    }

    @Override
    public void setLaserEnabled(boolean isLaserEnabled) {
        mIsLaserEnabled = isLaserEnabled;
    }

    @Override
    public void setBorderCornerRounded(boolean isBorderCornersRounded) {
        if (mBorderPaint != null) {
            if (isBorderCornersRounded) {
                mBorderPaint.setStrokeJoin(Paint.Join.ROUND);
            } else {
                mBorderPaint.setStrokeJoin(Paint.Join.BEVEL);
            }
        }
    }

    @Override
    public void setBorderAlpha(float alpha) {
        if (mBorderPaint != null) {
            int colorAlpha = (int) (255 * alpha);
            mBordersAlpha = alpha;
            mBorderPaint.setAlpha(colorAlpha);
        }
    }

    @Override
    public void setBorderCornerRadius(int borderCornersRadius) {
        if (mBorderPaint != null)
            mBorderPaint.setPathEffect(new CornerPathEffect(borderCornersRadius));
    }

    @Override
    public void setViewFinderOffset(int offset) {
        mViewFinderOffset = offset;
    }

    // TODO: Need a better way to configure this. Revisit when working on 2.0
    @Override
    public void setSquareViewFinder(boolean set) {
        mSquareViewFinder = set;
    }

    public void setupViewFinder() {
        updateFramingRect();
        invalidate();
    }

    public Rect getFramingRect() {
        return mFramingRect;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (getFramingRect() == null) {
            return;
        }

        drawViewFinderMask(canvas);
        drawViewFinderBorder(canvas);
        cropViewFinderArea(canvas);

        if (mIsLaserEnabled) {
            drawLaser(canvas);
        }
    }

    private void drawViewFinderMask(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        Rect framingRect = new Rect(0, 0, width, height);
        canvas.drawRect(framingRect, mFinderMaskPaint);
    }

    public void drawViewFinderBorder(Canvas canvas) {
        if (mBorderPaint != null) {
            Rect framingRect = getFramingRect();

            float size = Math.min(framingRect.height(), framingRect.width());
            float endLine = framingRect.width() / 4;
            float startLine = (framingRect.width() / 4) * 3;
            doDrawCornerBottomLeft(canvas, size, framingRect, endLine, startLine);
            doDrawCornerBottomRight(canvas, size, framingRect, startLine);
            doDrawCornerTopLeft(canvas, framingRect, endLine);
            doDrawCornerTopRight(canvas, size, framingRect, endLine, startLine);
        }
    }

    public void drawLaser(Canvas canvas) {
        Rect framingRect = getFramingRect();

        // Draw a red "laser scanner" line through the middle to show decoding is active
        mLaserPaint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
        scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
        int middle = framingRect.height() / 2 + framingRect.top;
        canvas.drawRect(framingRect.left + 2, middle - 1, framingRect.right - 1, middle + 2, mLaserPaint);

        postInvalidateDelayed(ANIMATION_DELAY,
                framingRect.left - POINT_SIZE,
                framingRect.top - POINT_SIZE,
                framingRect.right + POINT_SIZE,
                framingRect.bottom + POINT_SIZE);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        updateFramingRect();
    }

    public synchronized void updateFramingRect() {
        Point viewResolution = new Point(getWidth(), getHeight());
        int width;
        int height;
        int orientation = DisplayUtils.getScreenOrientation(getContext());

        if (mSquareViewFinder) {
            if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                height = (int) ((float) viewResolution.y * squareTradeMarkRatio);
                width = height;
            } else {
                width = (int) ((float) viewResolution.x * squareTradeMarkRatio);
                height = width;
            }
        } else {
            if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                height = (int) (getHeight() * LANDSCAPE_HEIGHT_RATIO);
                width = (int) (LANDSCAPE_WIDTH_HEIGHT_RATIO * height);
            } else {
                width = (int) (getWidth() * PORTRAIT_WIDTH_RATIO);
                height = (int) (PORTRAIT_WIDTH_HEIGHT_RATIO * width);
            }
        }

        if (width > getWidth()) {
            width = getWidth() - MIN_DIMENSION_DIFF;
        }

        if (height > getHeight()) {
            height = getHeight() - MIN_DIMENSION_DIFF;
        }

        int leftOffset = (viewResolution.x - width) / 2;
        int topOffset = (viewResolution.y - height) / 2;
        mFramingRect = new Rect(leftOffset + mViewFinderOffset, topOffset + mViewFinderOffset,
                leftOffset + width - mViewFinderOffset, topOffset + height - mViewFinderOffset);
    }

    public float getDefaultSquareDimensionRatio() {
        return defaultSquareDimensionRatio;
    }

    public void setDefaultSquareDimensionRatio(float defaultSquareDimensionRatio) {
        this.defaultSquareDimensionRatio = defaultSquareDimensionRatio;
    }

    private void cropViewFinderArea(Canvas canvas) {
        canvas.drawRoundRect(new RectF(getFramingRect()), cornerRadius, cornerRadius, areaTradeMarkPaint);
    }

    private void doDrawCornerBottomLeft(Canvas canvas, float size, Rect framingRect,
                                        float endLine, float startLine) {
        canvas.drawLine(framingRect.left + cornerRadius, framingRect.top + size,
                framingRect.left + endLine, framingRect.top + size, mBorderPaint);
        canvas.drawLine(framingRect.left, framingRect.top + startLine,
                framingRect.left, framingRect.bottom - cornerRadius, mBorderPaint);
        RectF rBottomLeft = new RectF(framingRect.left,
                framingRect.top + size - 2 * cornerRadius, framingRect.left + 2 * cornerRadius,
                framingRect.top + size);
        canvas.drawArc(rBottomLeft, 90, 90, false, mBorderPaint);
    }

    private void doDrawCornerBottomRight(Canvas canvas, float size,
                                         Rect framingRect, float startLine) {
        canvas.drawLine(framingRect.left + startLine, framingRect.top + size,
                framingRect.right - cornerRadius, framingRect.top + size, mBorderPaint);
        canvas.drawLine(framingRect.left + size, framingRect.top + startLine,
                framingRect.left + size, framingRect.bottom - cornerRadius, mBorderPaint);
        RectF rBottomRight = new RectF(framingRect.left + size - 2 * cornerRadius,
                framingRect.top + size - 2 * cornerRadius, framingRect.left + size
                , framingRect.top + size);
        canvas.drawArc(rBottomRight, 0, 90, false, mBorderPaint);
    }

    private void doDrawCornerTopLeft(Canvas canvas, Rect framingRect, float endLine) {
        canvas.drawLine(framingRect.left + cornerRadius, framingRect.top,
                framingRect.left + endLine, framingRect.top, mBorderPaint);
        canvas.drawLine(framingRect.left, framingRect.top + cornerRadius,
                framingRect.left, framingRect.top + endLine, mBorderPaint);
        RectF rTopLeft = new RectF(framingRect.left, framingRect.top,
                framingRect.left + 2 * cornerRadius, framingRect.top + 2 * cornerRadius);
        canvas.drawArc(rTopLeft, 180, 90, false, mBorderPaint);
    }

    private void doDrawCornerTopRight(Canvas canvas, float size, Rect framingRect,
                                      float endLine, float startLine) {
        canvas.drawLine(framingRect.left + startLine, framingRect.top,
                framingRect.right - cornerRadius, framingRect.top, mBorderPaint);
        canvas.drawLine(framingRect.left + size, framingRect.top + cornerRadius,
                framingRect.left + size, framingRect.top + endLine, mBorderPaint);
        RectF rTopRight = new RectF(framingRect.left + size - 2 * cornerRadius, framingRect.top,
                framingRect.left + size, framingRect.top + 2 * cornerRadius);
        canvas.drawArc(rTopRight, 270, 90, false, mBorderPaint);
    }
}