package id.app.pegadaian.library.common;

import com.google.gson.annotations.Expose;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class ResponseNostraAPI {
    @Expose
    private String message;
    @Expose
    private Object result;

    public String getMessage() {
        return message;
    }

    public Object getResult() {
        return result;
    }
}
