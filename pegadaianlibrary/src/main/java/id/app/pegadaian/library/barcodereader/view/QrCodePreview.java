package id.app.pegadaian.library.barcodereader.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Vibrator;
import android.util.AttributeSet;

import id.app.pegadaian.library.barcodereader.mobilevision.camera.CameraSourcePreview;
import id.app.pegadaian.library.barcodereader.zbarcore.core.ViewFinderView;

/**
 * Created by Dell on 3/18/2018.
 */

public class QrCodePreview extends CameraSourcePreview {

    private Vibrator vibrator;
    private ViewFinderView viewFinder;

    public QrCodePreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        viewFinder = new ViewFinderView(context, true);
        viewFinder.setDefaultSquareDimensionRatio(4f / 5);
        viewFinder.setSquareViewFinder(true);
        viewFinder.setLaserEnabled(false);
        viewFinder.setLaserColor(Color.RED);
        addView(viewFinder);
    }

    public void handlerOnRetrieved() {
        vibrator.vibrate(new long[]{0, 250, 50, 250}, -1);
    }
}
