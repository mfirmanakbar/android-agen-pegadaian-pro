package id.app.pegadaian.library.utils.time;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 8/19/2017.
 */

public class TimeUtils {

    public static String getFormattedTime(long seconds) {
        String time;
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
        if (day != 0) {
            time = StringHelper.getStringBuilderToString(String.format("%02d", day), ":",
                    String.format("%02d", hours), ":",
                    String.format("%02d", minute), ":",
                    String.format("%02d", second));
        } else {
            time = StringHelper.getStringBuilderToString(String.format("%02d", hours), ":",
                    String.format("%02d", minute), ":",
                    String.format("%02d", second));
        }
        return time;
    }

    public static String getDateFormated(String format, long time) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(time));
    }

    public static String getDateFormated(String time) {
        String reformattedDate = null;
        SimpleDateFormat inputFormat = new SimpleDateFormat("MM-yyyy");
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM/yyyy");
        try {

            reformattedDate = outputFormat.format(inputFormat.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return reformattedDate;
    }

    public static String EpochToDate1(long epoch, String formatDate) {
        // Date date = new Date(epoch * 1000L);
        Date date = new Date(epoch);
        DateFormat format = new SimpleDateFormat(formatDate);
        format.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        String formatted = format.format(date);
        return formatted;
    }

    public static String EpochToDate2(long epoch, String formatDate) {
        // Date date = new Date(epoch * 1000L);
        Date date = new Date(epoch);
        DateFormat format = new SimpleDateFormat(formatDate);
        Calendar calendar = new GregorianCalendar();
        TimeZone timeZone = calendar.getTimeZone();
        format.setTimeZone(timeZone);
        String formatted = format.format(date);
        return formatted;
    }

    public static String getDateFormated2(String format, long time) {
        DateTime dt = new DateTime(time);
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        String dtStr = fmt.print(dt);
        return dtStr;
    }
}