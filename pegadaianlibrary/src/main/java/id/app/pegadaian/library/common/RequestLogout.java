package id.app.pegadaian.library.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/12/2018.
 */

public class RequestLogout {

    @SerializedName("accessToken")
    @Expose
    private String accessToken;

    public RequestLogout(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
