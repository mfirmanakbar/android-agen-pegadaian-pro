package id.app.pegadaian.library.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import id.app.pegadaian.library.R;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.ErrorContent;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.logout.LogoutHelper;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import retrofit2.HttpException;

/**
 * Created by Dell on 2/28/2018.
 */

public class BaseViewModel extends ViewModel {

    private final SchedulersFacade schedulersFacade;
    private final JsonParser jsonParser;
    private final RxBus rxBus;
    private final PreferenceManager preferenceManager;
    private final BaseUseCase baseUseCase;
    private CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<Response> response = new MutableLiveData<>();
    private Observable<Location> locationUpdatesObservable;
    private ReactiveLocationProvider locationProvider;
    private final static int REQUEST_CHECK_SETTINGS = 0;
    private Activity activity;
    private LocationRequest locationRequest;

    public BaseViewModel(SchedulersFacade schedulersFacade,
                         JsonParser jsonParser,
                         RxBus rxBus,
                         PreferenceManager preferenceManager,
                         BaseUseCase baseUseCase) {
        this.schedulersFacade = schedulersFacade;
        this.jsonParser = jsonParser;
        this.rxBus = rxBus;
        this.preferenceManager = preferenceManager;
        this.baseUseCase = baseUseCase;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected void onCleared() {
        if (disposables != null)
            disposables.clear();
    }

    public void onStart(String... request) {
        if (activity != null)
            for (String req : request) {
                if (Manifest.permission.ACCESS_FINE_LOCATION.equals(req)) {
                    getDisposables().add(new RxPermissions(activity)
                            .request(Manifest.permission.ACCESS_FINE_LOCATION)
                            .subscribeOn(getSchedulersFacade().io())
                            .observeOn(getSchedulersFacade().ui())
                            .subscribe(new ConsumerAdapter<Boolean>() {
                                @Override
                                public void accept(Boolean granted) throws Exception {
                                    super.accept(granted);
                                    if (granted) {
                                        updateCurrentLocation();
                                    } else {
                                        onPermissionDenied();
                                    }
                                }
                            }));
                } else if (Manifest.permission.CAMERA.equals(req)) {
                    getDisposables().add(new RxPermissions(activity)
                            .request(Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE)
                            .subscribeOn(getSchedulersFacade().io())
                            .observeOn(getSchedulersFacade().ui())
                            .subscribe(new ConsumerAdapter<Boolean>() {
                                @Override
                                public void accept(Boolean granted) throws Exception {
                                    super.accept(granted);
                                    if (granted) {
                                        onCameraPermissionGranted();
                                    } else {
                                        onPermissionDenied();
                                    }
                                }
                            }));
                } else if (Manifest.permission.RECEIVE_SMS.equals(req)) {
                    getDisposables().add(new RxPermissions(activity)
                            .request(Manifest.permission.RECEIVE_SMS)
                            .subscribeOn(getSchedulersFacade().io())
                            .observeOn(getSchedulersFacade().ui())
                            .subscribe(new ConsumerAdapter<Boolean>() {
                                @Override
                                public void accept(Boolean granted) throws Exception {
                                    super.accept(granted);
                                    if (!granted) {
                                        onPermissionDenied();
                                    }
                                }
                            }));
                }
            }
    }

    public void updateLocation() {
        getDisposables().add(locationUpdatesObservable
                .map(location -> location)
                .doOnSubscribe(disposable -> {
                    if (response().hasActiveObservers())
                        response().postValue(Response.loading());
                })
                .subscribe(new ConsumerAdapter<Location>() {
                    @Override
                    public void accept(Location location) throws Exception {
                        super.accept(location);
                        baseUseCase.setCurrentLocation(location);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.success(location, ConfigurationUtils.TAG_UPDATE_LOCATION));
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers())
                            response().postValue(Response.error(throwable));
                    }
                }));
    }

    public void getCurrentLocation() {
        locationProvider = new ReactiveLocationProvider(activity);
        setLocationRequest();
        locationUpdatesObservable = locationProvider
                .checkLocationSettings(
                        new LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest)
                                .setAlwaysShow(true)
                                .build()
                )
                .doOnSubscribe(disposable -> {
                    if (response().hasActiveObservers())
                        response().postValue(Response.loading());
                })
                .doOnNext(new ConsumerAdapter<LocationSettingsResult>() {
                    @Override
                    public void accept(LocationSettingsResult locationSettingsResult) throws Exception {
                        super.accept(locationSettingsResult);
                        Status status = locationSettingsResult.getStatus();
                        if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                            try {
                                status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException th) {
                                Log.e(activity.getLocalClassName(), "Error opening settings activity.", th);
                            }
                        }
                    }
                }).flatMap(locationSettingsResult -> {
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        return locationProvider.getUpdatedLocation(locationRequest);
                    } else {
                        return null;
                    }
                });
    }

    private void setLocationRequest() {
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1)
                .setInterval(1000);
    }

    public void unBinding() {
        onCleared();
    }

    public SchedulersFacade getSchedulersFacade() {
        return schedulersFacade;
    }

    public JsonParser getJsonParser() {
        return jsonParser;
    }

    public RxBus getRxBus() {
        return rxBus;
    }

    public MutableLiveData<Response> response() {
        return response;
    }

    public CompositeDisposable getDisposables() {
        return disposables;
    }

    public PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }

    public BaseUseCase getBaseUseCase() {
        return baseUseCase;
    }

    public ReactiveLocationProvider getLocationProvider() {
        return locationProvider;
    }

    public Activity getActivity() {
        return activity;
    }

    @NonNull
    public LocationRequest getLocationRequest() {
        return locationRequest;
    }

    public void updateCurrentLocation() {
        //TODO override after location granted
    }

    public void onCameraPermissionGranted() {
        //TODO override after camera granted
    }

    public void onPermissionDenied() {
        if (response().hasActiveObservers())
            response().postValue(Response.error(false, ConfigurationUtils.TAG_PERMISSION_DENIED));
    }

    @NonNull
    public ConsumerThrowableAdapter handleError() {
        return new ConsumerThrowableAdapter() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                super.accept(throwable);
                String errorMessage;
                String errorType = ConfigurationUtils.TAG_ERROR;
                if (StringUtils.isNotBlank(throwable.getMessage())) {
                    if (throwable instanceof UnknownHostException) {
                        errorMessage = getActivity().getResources().getString(R.string.error_no_host);
                    } else if (throwable instanceof SocketException) {
                        errorMessage = getActivity().getResources().getString(R.string.error_service_unavailable);
                    } else if (throwable instanceof SocketTimeoutException) {
                        errorMessage = getActivity().getResources().getString(R.string.error_service_unavailable);
                    } else if (throwable instanceof FileNotFoundException) {
                        errorMessage = getActivity().getResources().getString(R.string.file_not_found);
                    } else {
                        if (ConfigurationUtils.TAG_ERROR_SERVICE_UNAVAILABLE == ((HttpException) throwable).response().code()) {
                            errorMessage = getActivity().getResources().getString(R.string.error_service_unavailable);
                        } else if (ConfigurationUtils.TAG_ERROR_GATEWAY_TIMEOUT == ((HttpException) throwable).response().code()) {
                            errorMessage = getActivity().getResources().getString(R.string.error_no_host);
                            errorType = ConfigurationUtils.TAG_GATEWAY_TIMEOUT;
                        } else if (ConfigurationUtils.TAG_ERROR_SERVICE_UNAUTHORIZED == ((HttpException) throwable).response().code()) {
                            errorMessage = getActivity().getResources().getString(R.string.error_unauthorized);
                            errorType = ConfigurationUtils.TAG_UNAUTHORIZED;
                            new LogoutHelper(getActivity())
                                    .setTitle(getActivity().getString(R.string.error))
                                    .setMessage(errorMessage)
                                    .setOk(getActivity().getString(R.string.ok))
                                    .setOnPositiveListener(dialog -> doLogoutAgent())
                                    .showDialogError();
                        } else {
                            ErrorContent errorContent = getJsonParser().getObject(((HttpException) throwable).response().errorBody().string(),
                                    ErrorContent.class);
                            errorMessage = (StringUtils.isNotBlank(errorContent.getErrorDescription())) ?
                                    errorContent.getErrorDescription() : errorContent.getError();
                        }
                    }
                } else {
                    errorMessage = throwable.fillInStackTrace().toString();
                }
                if (response().hasActiveObservers() && !ConfigurationUtils.TAG_UNAUTHORIZED.equals(errorType))
                    response().setValue(Response.error(errorMessage, errorType));
            }
        };
    }

    @NonNull
    public ConsumerThrowableAdapter handleError(String type) {
        return new ConsumerThrowableAdapter() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                super.accept(throwable);
                if (response().hasActiveObservers())
                    response().setValue(Response.error(null, type));
            }
        };
    }

    public void doLogoutAgent() {
        getDisposables().add(getBaseUseCase()
                .logout(getBaseUseCase().getToken())
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                getBaseUseCase().insertToken("");
                                getBaseUseCase().insertUID("");
                                response().postValue(Response.success("", ConfigurationUtils.TAG_LOGOUT));
                            } else {
                                response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_logout), ConfigurationUtils.TAG_LOGOUT_FAILED));
                            }
                        }
                    }
                }, handleError()));
    }
}