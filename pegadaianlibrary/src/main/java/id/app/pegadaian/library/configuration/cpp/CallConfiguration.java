package id.app.pegadaian.library.configuration.cpp;

/**
 * Created by user on 27/02/2018.
 */

public class CallConfiguration {

    static {
        System.loadLibrary("native-lib");
    }

    public static native String appName();
    public static native String preferenceName();
    public static native String ninjaMode();
    public static native String rushBMode();
    public static native String sampleBaseUrl();
    public static native String loginBaseUrl();
}
