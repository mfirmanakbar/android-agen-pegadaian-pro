package id.app.pegadaian.library.utils.restapi;

import id.app.pegadaian.library.common.LogMode;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 27/02/2018.
 */

public class BaseRestApiAdapter {

    protected static Retrofit restAdapter;
    protected static Retrofit restLogoutAdapter;
    protected static String logMode = LogMode.TAG_LOG_MODE;

    protected static Retrofit getRestAdapter(String serverURL) {
        OkHttpClientFactory okHttpClientFactory = OkHttpClientFactory.getInstance();
        HttpLoggingInterceptor logging = getLoggingLevel(logMode);
        okHttpClientFactory.getOkHttpClientForRestAdapter().addInterceptor(logging);
        return new Retrofit.Builder()
                .baseUrl(serverURL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClientFactory.getOkHttpClientForRestAdapter().build())
                .build();
    }

    private static HttpLoggingInterceptor getLoggingLevel(String logMode) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (ConfigurationUtils.IS_RUSH_B_MODE.equals(logMode)) {
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return logging;
    }
}