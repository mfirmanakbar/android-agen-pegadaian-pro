package id.app.pegadaian.library.view.activity;

/**
 * Created by Dell on 3/5/2018.
 */

public interface BaseView  {
    int getContentViewId();
}
