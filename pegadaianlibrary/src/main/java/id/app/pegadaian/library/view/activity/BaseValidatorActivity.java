package id.app.pegadaian.library.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

/**
 * Created by Dell on 3/11/2018.
 */

public abstract class BaseValidatorActivity extends BaseToolbarActivity implements Validator.ValidationListener {

    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initValidator();
    }

    protected void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof AppCompatEditText) {
                ((AppCompatEditText) view).setError(message);
            } else {
                showSnackBar(this, message);
            }
        }
    }

    public Validator getValidator() {
        return validator;
    }
}
