package id.app.pegadaian.library.di.module;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.bus.RxBus;

/**
 * Created by snyind on 1/18/18.
 */

@Module
public class RxBusModule {

    private RxBus rxBus;

    @Named("RxBus")
    @Provides
    @Singleton
    RxBus provideRxBus() {
        if (rxBus == null) {
            rxBus = new RxBus();
        }
        return rxBus;
    }
}
