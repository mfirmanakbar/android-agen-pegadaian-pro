package id.app.pegadaian.library.utils;

import android.content.Context;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/21/2018.
 */

public final class Utils {

    public static Class getActivity(String prefix, String activity) throws ClassNotFoundException {
        String className = StringHelper.getStringBuilderToString(prefix,
                activity.replace(" ", ""),
                "Activity");
        return Class.forName(className);
    }

    public static int getImageResource(String resName, Context context) {
        return context.getResources().getIdentifier(
                resName.toLowerCase().replace(" ", ""), "mipmap", context.getPackageName());
    }
}