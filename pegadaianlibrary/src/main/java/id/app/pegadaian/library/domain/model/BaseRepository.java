package id.app.pegadaian.library.domain.model;

import id.app.pegadaian.library.common.RequestLogout;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/4/2018.
 */

public class BaseRepository {

    private final PreferenceManager preferenceManager;
    private LogoutRestApi logoutRestApi;

    public BaseRepository(PreferenceManager preferenceManager,
                          LogoutRestApi logoutRestApi) {
        this.preferenceManager = preferenceManager;
        this.logoutRestApi = logoutRestApi;
    }

    public BaseRepository(PreferenceManager preferenceManager) {
        this.preferenceManager = preferenceManager;
    }

    public PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }

    public LogoutRestApi getLogoutRestApi() {
        return logoutRestApi;
    }

    public void setCurrentLocation(String currentLocation) {
        preferenceManager.setCurrentLocation(currentLocation);
    }

    public boolean setToken(String accessToken) {
        getPreferenceManager().setToken(accessToken);
        return true;
    }

    public boolean setUID(String id) {
        getPreferenceManager().setUID(id);
        return true;
    }

    public boolean setAgentOutletId(String id) {
        getPreferenceManager().setAgentOutletId(id);
        return true;
    }

    public String getToken() {
        return getPreferenceManager().getAccessToken();
    }

    public String getUID() {
        return getPreferenceManager().getUID();
    }

    public String getAgentOutletId() {
        return getPreferenceManager().getAgentOutletId();
    }

    public Observable<Object> logout(RequestLogout requestLogout) {
        return getLogoutRestApi().logout(requestLogout);
    }
}