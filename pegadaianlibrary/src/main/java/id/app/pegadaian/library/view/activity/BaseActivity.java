package id.app.pegadaian.library.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.List;

import id.app.pegadaian.library.R;
import id.app.pegadaian.library.widget.colordialog.ColorDialog;
import id.app.pegadaian.library.widget.colordialog.PromptDialog;

/**
 * Created by Dell on 2/28/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Typeface ronniaRegular;
    private Typeface ronniaSemiBold;
    private Typeface ronniaBold;
    private Typeface ronniaLight;
    private Typeface latoRegular;
    private Typeface latoBold;
    private Typeface latoLight;
    protected Handler handler;
    private boolean isUsingDefaultOnBackPressed = false;
    protected Snackbar snackbar;
    private PromptDialog promptDialog;
    private ColorDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        handler = new Handler();
        initTypefaces();
    }

    private void initTypefaces() {
        //Ronnia Font
        ronniaRegular = Typeface.createFromAsset(getAssets(),
                "fonts/RonniaReg.otf");
        ronniaSemiBold = Typeface.createFromAsset(getAssets(),
                "fonts/RonniaBold.otf");
        ronniaBold = Typeface.createFromAsset(getAssets(),
                "fonts/RonniaExtrabold.otf");
        ronniaLight = Typeface.createFromAsset(getAssets(),
                "fonts/RonniaLight.otf");

        //Lato Font
        latoRegular = Typeface.createFromAsset(getAssets(),
                "fonts/Lato-Regular.ttf");
        latoBold = Typeface.createFromAsset(getAssets(),
                "fonts/Lato-Bold.ttf");
        latoLight = Typeface.createFromAsset(getAssets(),
                "fonts/Lato-Light.ttf");
    }

    @Nullable
    protected Toolbar getToolbar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    protected void showActivityNoAnimation(Context packageContext, Class<?> cls) {
        Intent intent = getIntent(packageContext, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    protected void showSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);
    }

    protected boolean postDelayed(Runnable r, long delayMillis) {
        return handler.postDelayed(r, delayMillis);
    }

    protected void showActivity(Context packageContext, Class<?> cls) {
        Intent intent = getIntent(packageContext, cls);
        showActivity(intent);
    }

    protected void showActivity(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    protected void showActivityAndFinishCurrentActivity(Intent intent) {
        showActivity(intent);
        finish();
    }

    protected void showActivityAndFinishCurrentActivity(Context packageContext, Class<?> cls) {
        Intent intent = getIntent(packageContext, cls);
        showActivity(intent);
        finish();
    }

    protected Intent getIntent(Context packageContext, Class<?> cls) {
        return new Intent(packageContext, cls);
    }

    protected void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    protected void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view.isFocused() || imm.isActive())
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onBackPressed() {
        if (isUsingDefaultOnBackPressed()) {
            super.onBackPressed();
        } else {
            finishActivity();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (promptDialog != null && promptDialog.isShowing()) {
            promptDialog.dismiss();
        }

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void finishActivity() {
        finish();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finishActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showSnackBar(Activity activity, String message) {
        this.snackbar = Snackbar
                .make(activity.findViewById(android.R.id.content),
                        message,
                        Snackbar.LENGTH_LONG)
                .setAction(R.string.confirm_lbl, view -> snackbar.dismiss());
        this.snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        this.snackbar.show();
    }

    protected boolean isUsingDefaultOnBackPressed() {
        return isUsingDefaultOnBackPressed;
    }

    protected void setUsingDefaultOnBackPressed(boolean usingDefaultOnBackPressed) {
        isUsingDefaultOnBackPressed = usingDefaultOnBackPressed;
    }

    public Typeface getRonniaRegular() {
        return ronniaRegular;
    }

    public Typeface getRonniaSemiBold() {
        return ronniaSemiBold;
    }

    public Typeface getRonniaBold() {
        return ronniaBold;
    }

    public Typeface getRonniaLight() {
        return ronniaLight;
    }

    public Typeface getLatoRegular() {
        return latoRegular;
    }

    public Typeface getLatoBold() {
        return latoBold;
    }

    public Typeface getLatoLight() {
        return latoLight;
    }

    abstract protected int getContentViewId();

    public void showDialogSuccess(String message, PromptDialog.OnPositiveListener onPositiveListener) {
        promptDialog = new PromptDialog(this)
                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(getString(R.string.success))
                .setContentText(message)
                .setPositiveListener(getString(R.string.ok), onPositiveListener);
        promptDialog.show();
    }

    public void showDialogSuccessNoCancel(String message, PromptDialog.OnPositiveListener onPositiveListener) {
        promptDialog = new PromptDialog(this)
                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(getString(R.string.success))
                .setContentText(message)
                .setPositiveListener(getString(R.string.ok), onPositiveListener);
        promptDialog.setCancelable(false);
        promptDialog.setCanceledOnTouchOutside(false);
        promptDialog.setOnKeyListener((dialog1, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK);
        promptDialog.show();
    }

    public void showDialogError(String message) {
        promptDialog = new PromptDialog(this)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(getString(R.string.error))
                .setContentText(message)
                .setPositiveListener(getString(R.string.ok), dialog -> dialog.dismiss());
        promptDialog.show();
    }

    public void showDialogError(String message,
                                PromptDialog.OnPositiveListener onPositiveListener) {
        promptDialog = new PromptDialog(this)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(getString(R.string.error))
                .setContentText(message)
                .setPositiveListener(getString(R.string.ok), onPositiveListener);
        promptDialog.show();
    }

    public void showDialogError(String title,
                                String message,
                                PromptDialog.OnPositiveListener onPositiveListener) {
        promptDialog = new PromptDialog(this)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(title)
                .setContentText(message)
                .setPositiveListener(getString(R.string.ok), onPositiveListener);
        promptDialog.show();
    }

    public void showDialogError(String title,
                                String message,
                                ColorDialog.OnPositiveListener onPositiveListener) {
        dialog = new ColorDialog(this);
        dialog.setTitle(title);
        dialog.setContentText(message);
        dialog.setPositiveListener(getResources().getString(R.string.ok), onPositiveListener)
                .setNegativeListener(getResources().getString(R.string.cancel),
                        dialog12 -> dialog12.dismiss()).show();
    }

    protected void attachSpinner(Spinner spinner, List<String> values) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.view_spinner_type, values);
        dataAdapter.setDropDownViewResource(R.layout.view_spinner_item_type);
        spinner.setAdapter(dataAdapter);
    }

    protected void attachSpinner(Spinner spinner, List<String> values, AdapterView.OnItemSelectedListener listener) {
        attachSpinner(spinner, values);
        spinner.setOnItemSelectedListener(listener);
    }

    protected void configureAdapter(FastItemAdapter<?> adapter,
                                    RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }
}