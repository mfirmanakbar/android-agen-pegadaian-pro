package id.app.pegadaian.library.di.module;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 9/14/2017.
 */

@Module
public class PreferenceModule {

    @Named("PreferenceInfo")
    @Provides
    @Singleton
    String providePreferenceName() {
        return ConfigurationUtils.PREFERENCE_NAME;
    }

    @Provides
    @Singleton
    PreferenceManager providePreferenceManager(@Named("ApplicationContext") Context context,
                                               @Named("PreferenceInfo") String preferenceName) {
        return new PreferenceManager(context, preferenceName);
    }
}
