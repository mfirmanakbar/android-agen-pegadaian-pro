package id.app.pegadaian.library.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Dell on 3/5/2018.
 */

public abstract class BaseToolbarActivity extends BaseActivity implements BaseToolbarView {

    private AppCompatTextView toolbarTitle;
    private AppCompatTextView toolbarDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbarTitle = getToolbarTitle();
        toolbarDescription = getToolbarDescription();
    }

    protected void configureToolbar(String title) {
        Toolbar toolbar = getToolbar();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
            if (StringUtils.isNotBlank(title)) {
                toolbarTitle.setTypeface(getRonniaBold());
                toolbarTitle.setText(title);
            }
        }
    }

    protected void configureToolbarNoBack(String title){
        Toolbar toolbar = getToolbar();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
            if (StringUtils.isNotBlank(title)) {
                toolbarTitle.setTypeface(getRonniaBold());
                toolbarTitle.setText(title);
            }
        }
    }

    protected void configureToolbar(String title, String description) {
        configureToolbar(title);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (StringUtils.isNotBlank(description) && toolbarDescription != null) {
            toolbarDescription.setTypeface(getRonniaLight());
            toolbarDescription.setText(description);
        }
    }

    //TODO override for toolbar description
    protected AppCompatTextView getToolbarDescription() {
        return null;
    }
}