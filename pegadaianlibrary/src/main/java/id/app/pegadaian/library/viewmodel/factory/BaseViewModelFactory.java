package id.app.pegadaian.library.viewmodel.factory;

import android.arch.lifecycle.ViewModelProvider;

import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 2/28/2018.
 */

public abstract class BaseViewModelFactory implements ViewModelProvider.Factory {

    private final SchedulersFacade schedulersFacade;
    private final JsonParser jsonParser;
    private final RxBus rxBus;
    private final PreferenceManager preferenceManager;

    public BaseViewModelFactory(SchedulersFacade schedulersFacade,
                                JsonParser jsonParser,
                                RxBus rxBus,
                                PreferenceManager preferenceManager) {
        this.schedulersFacade = schedulersFacade;
        this.jsonParser = jsonParser;
        this.rxBus = rxBus;
        this.preferenceManager = preferenceManager;
    }

    public SchedulersFacade getSchedulersFacade() {
        return schedulersFacade;
    }

    public JsonParser getJsonParser() {
        return jsonParser;
    }

    public RxBus getRxBus() {
        return rxBus;
    }

    public PreferenceManager getPreferenceManager() {
        return preferenceManager;
    }
}