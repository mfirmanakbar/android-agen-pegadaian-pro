package id.app.pegadaian.library.domain.interactors;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;

import id.app.pegadaian.library.common.RequestLogout;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/4/2018.
 */

public class BaseUseCase {

    private BaseRepository repository;

    private Compressor compressor;

    public BaseUseCase(BaseRepository repository,
                       Compressor compressor) {
        this.repository = repository;
        this.compressor = compressor;
    }

    public BaseUseCase(BaseRepository repository){
        this.repository = repository;
    }

    public BaseRepository getRepository() {
        return repository;
    }

    public void setCurrentLocation(Location location) {
        LatLng latLang = new LatLng(location.getLatitude(), location.getLongitude());
        String latitude = String.valueOf(latLang.latitude);
        String longitude = String.valueOf(latLang.longitude);
        String currentLocation = StringHelper.getStringBuilderToString(latitude,
                ",",
                longitude);
        repository.setCurrentLocation(currentLocation);
    }

    public String getToken() {
        return repository.getToken();
    }

    public String getUID() {
        return repository.getUID();
    }

    public void insertToken(String accessToken) {
        repository.setToken(accessToken);
    }

    public void insertUID(String id) {
        repository.setUID(id);
    }

    public Observable<File> compressFile(File file) {
        return Observable.create(emitter -> {
            File fileCompressed = compressor
                    .setQuality(ConfigurationUtils.TAG_QUALITY_IMAGE)
                    .compressToFile(file);
            emitter.onNext(fileCompressed);
            emitter.onComplete();
        });
    }

    public Observable<Object> logout(String accessToken) {
        return getRepository().logout(new RequestLogout(accessToken));
    }

    public void insertAgentOutletid(String id) {
        repository.setAgentOutletId(id);
    }

    public String getAgentOutletId() {
        return repository.getAgentOutletId();
    }
}