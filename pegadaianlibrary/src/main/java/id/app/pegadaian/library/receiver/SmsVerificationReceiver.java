package id.app.pegadaian.library.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import id.app.pegadaian.library.utils.ConfigurationUtils;

public class SmsVerificationReceiver extends BroadcastReceiver {

    private static final String PREFIX_SMS_VERIFICATION = ConfigurationUtils.TAG_PREFIX_OTP;
    private static final String VERIFICATION_CODE_TAG = "Verification Code";
    public static final String SMS_RECEIVED_INTENT = "android.provider.Telephony.SMS_RECEIVED";
    private OtpView view;

    public SmsVerificationReceiver(OtpView view) {
        this.view = view;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < (pdusObj != null ? pdusObj.length : 0); i++) {
                    SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String message = sms.getDisplayMessageBody();
                    if (message.startsWith(PREFIX_SMS_VERIFICATION)) {
                        view.setSmsVerificationCode(message.substring(PREFIX_SMS_VERIFICATION.length(),
                                PREFIX_SMS_VERIFICATION.length() + 6));
                    }
                }
            }
        } catch (Exception e) {
            Log.e(VERIFICATION_CODE_TAG, e.getMessage(), e);
        }
    }
}
