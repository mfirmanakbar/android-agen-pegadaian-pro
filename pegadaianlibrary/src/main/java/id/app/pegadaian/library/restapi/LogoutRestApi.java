package id.app.pegadaian.library.restapi;

import id.app.pegadaian.library.common.RequestLogout;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Dell on 3/14/2018.
 */

public interface LogoutRestApi {

    @Headers("Content-Type: application/json")
    @POST("api/auth/logout")
    Observable<Object> logout(@Body RequestLogout requestLogout);
}
