package id.app.pegadaian.library.utils.sharedprefences;

import android.content.Context;
import android.content.SharedPreferences;

import id.app.pegadaian.library.utils.ConfigurationUtils;

public class PreferenceManager {

    private SharedPreferences sharedPreferences;

    public PreferenceManager(Context context,
                             String preferenceName) {
        sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
    }

    //TODO SET
    public void setSampleValue(Integer value) {
        sharedPreferences.edit()
                .putInt(ConfigurationUtils.TAG_SAMPLE_VALUE, value)
                .apply();
    }

    public void setCurrentLocation(String currentLocation) {
        sharedPreferences.edit()
                .putString(ConfigurationUtils.TAG_CURRENT_LOCATION, currentLocation)
                .apply();
    }

    public void setCurrentOtpCountdown(Long currentOtpCountdown) {
        sharedPreferences.edit()
                .putLong(ConfigurationUtils.TAG_CURRENT_OTP_COUNTDOWN, currentOtpCountdown)
                .apply();
    }

    public void setToken(String accessToken) {
        sharedPreferences.edit()
                .putString(ConfigurationUtils.TAG_ACCEESS_TOKEN, accessToken)
                .apply();
    }

    public void setPinOrPassword(String pinOrPassword) {
        sharedPreferences.edit()
                .putString(ConfigurationUtils.TAG_PIN_OR_PASSWORD, pinOrPassword)
                .apply();
    }

    public void setUID(String id) {
        sharedPreferences.edit()
                .putString(ConfigurationUtils.TAG_UID, id)
                .apply();
    }

    public void setAgentOutletId(String id) {
        sharedPreferences.edit()
                .putString(ConfigurationUtils.TAG_AGENT_OUTLET_ID, id)
                .apply();
    }

    //=========================================================================================================================

    //TODO GET

    public Integer getSampleValue() {
        return sharedPreferences.getInt(ConfigurationUtils.TAG_SAMPLE_VALUE, 0);
    }

    public String getCurrentLocation() {
        return sharedPreferences.getString(ConfigurationUtils.TAG_CURRENT_LOCATION, null);
    }

    public Long getCurrentOtpCountdown() {
        return sharedPreferences.getLong(ConfigurationUtils.TAG_CURRENT_OTP_COUNTDOWN, ConfigurationUtils.TAG_OTP_COUNTDOWN);
    }

    public String getAccessToken() {
        return sharedPreferences.getString(ConfigurationUtils.TAG_ACCEESS_TOKEN, null);
    }

    public String getPinOrPassword() {
        return sharedPreferences.getString(ConfigurationUtils.TAG_PIN_OR_PASSWORD, null);
    }

    public String getUID() {
        return sharedPreferences.getString(ConfigurationUtils.TAG_UID, null);
    }

    public String getAgentOutletId() {
        return sharedPreferences.getString(ConfigurationUtils.TAG_AGENT_OUTLET_ID, null);
    }
}