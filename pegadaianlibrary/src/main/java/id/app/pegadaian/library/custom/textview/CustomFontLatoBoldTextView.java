package id.app.pegadaian.library.custom.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Dell on 3/8/2018.
 */

public class CustomFontLatoBoldTextView extends AppCompatTextView {
    public CustomFontLatoBoldTextView(Context context) {
        super(context);
        init();
    }

    public CustomFontLatoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontLatoBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Bold.ttf");
        this.setTypeface(tf);
    }
}
