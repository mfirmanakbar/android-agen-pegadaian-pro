package id.app.pegadaian.library.di.module;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.utils.parser.JsonParser;

/**
 * Created by Dell on 2/27/2018.
 */

@Module
public class JsonParserModule {

    @Named("JsonParser")
    @Provides
    @Singleton
    JsonParser provideJsonParser(@Named("Gson") Gson gson) {
        return new JsonParser(gson);
    }
}