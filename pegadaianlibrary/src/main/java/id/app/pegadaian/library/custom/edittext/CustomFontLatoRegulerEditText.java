package id.app.pegadaian.library.custom.edittext;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class CustomFontLatoRegulerEditText extends AppCompatEditText {

    public CustomFontLatoRegulerEditText(Context context) {
        super(context);
        init();
    }

    public CustomFontLatoRegulerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontLatoRegulerEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        this.setTypeface(tf);
    }
}