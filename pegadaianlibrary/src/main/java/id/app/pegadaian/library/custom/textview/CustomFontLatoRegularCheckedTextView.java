package id.app.pegadaian.library.custom.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.util.AttributeSet;

/**
 * Created by Dell on 3/16/2018.
 */

public class CustomFontLatoRegularCheckedTextView extends AppCompatCheckedTextView {

    public CustomFontLatoRegularCheckedTextView(Context context) {
        super(context);
        init();
    }

    public CustomFontLatoRegularCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontLatoRegularCheckedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        this.setTypeface(tf);
    }
}