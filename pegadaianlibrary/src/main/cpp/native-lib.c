//
// Created by user on 6/6/2017.
//

#include <jni.h>

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_appName(JNIEnv *env) {
    return (*env)->NewStringUTF(env, "AgentApp");
}

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_ninjaMode(JNIEnv *env) {

    return (*env)->NewStringUTF(env, "agen_debug_mode_is_off_yeah");
}

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_rushBMode(JNIEnv *env) {
    return (*env)->NewStringUTF(env, "agen_debug_mode_is_on_yeah");
}

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_sampleBaseUrl(JNIEnv *env) {
    return (*env)->NewStringUTF(env, "https://devpds.pegadaian.co.id:9010/oauth/");
}

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_loginBaseUrl(JNIEnv *env) {
    return (*env)->NewStringUTF(env, "http://dev.nostratech.com:20021/");
}

JNIEXPORT jstring JNICALL
Java_id_app_pegadaian_library_configuration_cpp_CallConfiguration_preferenceName(JNIEnv *env) {
    return (*env)->NewStringUTF(env, "LocalPegadaianAgentSyariahPreference");
}