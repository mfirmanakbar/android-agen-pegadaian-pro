package co.pegadaian.syariah.agen.registration.view;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Dell on 3/7/2018.
 */

public class RegistrationMapsFragment extends BaseFragment implements OnMapReadyCallback,
        LocationSource.OnLocationChangedListener {

    @BindView(R.id.outlet_search_edit_text)
    AppCompatEditText outletSearchEditText;

    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;

    private static RegistrationActivity registrationActivity;
    private Unbinder unbinder;

    public static RegistrationMapsFragment newInstance(RegistrationActivity activity) {
        RegistrationMapsFragment registrationMapsFragment = new RegistrationMapsFragment();
        registrationActivity = activity;
        return registrationMapsFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_maps, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        configureMap();
        configureProgressBar();
        return view;
    }

    private void configureFont() {
        outletSearchEditText.setTypeface(getLatoRegular());
    }

    private void configureMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);
    }

    private void configureProgressBar() {
        progressBar.setProgress((100 / 6) * 4);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initMapsFragment(outletSearchEditText);
    }

    @Override
    public void onLocationChanged(Location location) {
        registrationActivity.initLocation(location);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        registrationActivity.initMap(googleMap);
    }

    @Override
    public void onValidationSucceeded() {

    }
}