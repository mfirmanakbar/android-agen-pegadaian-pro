package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.transaction.domain.interactors.TransactionUseCase;
import co.pegadaian.syariah.agen.transaction.domain.model.TransactionRepository;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/16/2018.
 */

@Module
public class GadaiModule {

    @Provides
    TransactionRepository provideGadaiRepository(PreferenceManager preferenceManager) {
        return new TransactionRepository(preferenceManager, RestApiAdapter.getRestApi(), RestApiAdapter.getLogoutRestApi());
    }

    @Provides
    TransactionViewModelFactory provideGadaiViewModelFactory(TransactionUseCase transactionUseCase,
                                                             SchedulersFacade schedulersFacade,
                                                             @Named("JsonParser") JsonParser jsonParser,
                                                             @Named("RxBus") RxBus rxBus,
                                                             PreferenceManager preferenceManager,
                                                             RealmConfigurator realmConfigurator) {
        return new TransactionViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, transactionUseCase, realmConfigurator);
    }
}