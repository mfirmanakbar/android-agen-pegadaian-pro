package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.LoginModule;
import co.pegadaian.syariah.agen.login.view.LoginActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 2/28/2018.
 */

@Module
public abstract class LoginBuilderModule {

    @ContributesAndroidInjector(modules = {LoginModule.class})
    abstract LoginActivity bindLoginActivity();
}
