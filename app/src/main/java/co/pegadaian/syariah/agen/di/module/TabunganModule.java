package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import co.pegadaian.syariah.agen.tabungan.domain.interactors.TabunganUseCase;
import co.pegadaian.syariah.agen.tabungan.domain.model.TabunganRepository;
import co.pegadaian.syariah.agen.tabungan.viewmodel.factory.TabunganViewModelFactory;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public class TabunganModule {

    @Provides
    TabunganRepository provideTabunganRepository(PreferenceManager preferenceManager) {
        return new TabunganRepository(preferenceManager, RestApiAdapter.getRestApi(),
                RestApiAdapter.getLogoutRestApi());
    }

    @Provides
    TabunganViewModelFactory provideTabunganViewModelFactory(TabunganUseCase tabunganUseCase,
                                                             SchedulersFacade schedulersFacade,
                                                             @Named("JsonParser") JsonParser jsonParser,
                                                             @Named("RxBus") RxBus rxBus,
                                                             PreferenceManager preferenceManager,
                                                             RealmConfigurator realmConfigurator) {
        return new TabunganViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager,
                tabunganUseCase, realmConfigurator);
    }
}
