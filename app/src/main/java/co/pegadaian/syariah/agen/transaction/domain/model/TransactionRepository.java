package co.pegadaian.syariah.agen.transaction.domain.model;

import co.pegadaian.syariah.agen.base.repository.BaseTransactionRepository;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiLogamMulia;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiNonPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/16/2018.
 */

public class TransactionRepository extends BaseTransactionRepository {

    public TransactionRepository(PreferenceManager preferenceManager,
                                 RestApi restApi,
                                 LogoutRestApi logoutRestApi) {
        super(preferenceManager, restApi, logoutRestApi);
    }

    public Observable<Object> getJenisPerhiasan() {
        return getRestApi().getJenisPerhiasan();
    }

    public Observable<Object> submitBukaTabungan(SubmitBE submitBE) {
        return getRestApi().submitBukaTabungan(getToken(), submitBE);
    }

    public Observable<Object> taksirPerhiasan(ItemGadai itemGadai) {
        return getRestApi().taksirPerhiasan(getToken(), itemGadai);
    }

    public Observable<Object> simulasiPerhiasan(RequestSimulasiPerhiasan requestSimulasiPerhiasan) {
        return getRestApi().simulasiPerhiasan(getToken(), requestSimulasiPerhiasan);
    }
    public Observable<Object> getJenisKendaraanMotor() {
        return getRestApi().getKendaraanMotor();
    }
    public Observable<Object> getBidangUsaha() {
        return getRestApi().getBidangUsaha();
    }
    public Observable<Object> getJenisTempatUsaha() {
        return getRestApi().getJenisTempatUsaha();
    }
    public Observable<Object> getStatusTempatUsaha() {
        return getRestApi().getStatusTempatUsaha();
    }
    public Observable<Object> getJenisKendaraanMobil() {
        return getRestApi().getKendaraanMobil();
    }

    public Observable<Object> getKepingLogam() {
        return getRestApi().getKepingLogam();
    }

    public Observable<Object> taksirLogamMulia(ItemGadai itemGadai) {
        return getRestApi().taksirLogamMulia(getToken(), itemGadai);
    }

    public Observable<Object> simulasiLogamMulia(RequestSimulasiLogamMulia requestSimulasiLogamMulia) {
        return getRestApi().simulasiLogamMulia(getToken(), requestSimulasiLogamMulia);
    }

    public Observable<Object> getMobil() {
        return getRestApi().getMobil();
    }

    public Observable<Object> getMotor() {
        return getRestApi().getMotor();
    }

    public Observable<Object> simulasiKendaraan(RequestSimulasiNonPerhiasan requestSimulasiNonPerhiasan) {
        return getRestApi().simulasiKendaraan(getToken(), requestSimulasiNonPerhiasan);
    }
}