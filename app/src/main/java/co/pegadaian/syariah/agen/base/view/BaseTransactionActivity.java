package co.pegadaian.syariah.agen.base.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.viewmodel.BaseTransactionViewModel;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.custom.GlideRequestListener;
import co.pegadaian.syariah.agen.custom.PositionedCropTransformation;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import co.pegadaian.syariah.agen.object.master.Domain;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.KonfirmasiGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSendOtpOpenGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.registration.view.DialogChooserFragment;
import co.pegadaian.syariah.agen.transaction.view.activity.ScanQrCodeActivity;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.easyimage.DefaultCallback;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.printbluetooth.cn.bluetooth.sdk.BluetoothService;
import id.app.pegadaian.library.printbluetooth.cn.bluetooth.sdk.DeviceListActivity;
import id.app.pegadaian.library.printbluetooth.command.sdk.Command;
import id.app.pegadaian.library.printbluetooth.command.sdk.PrintPicture;
import id.app.pegadaian.library.printbluetooth.command.sdk.PrinterCommand;
import id.app.pegadaian.library.receiver.OtpView;
import id.app.pegadaian.library.receiver.SmsVerificationReceiver;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.map.MapHelper;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.widget.dialog.DialogPin;

import static id.app.pegadaian.library.utils.ConfigurationUtils.DEVICE_NAME;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public abstract class BaseTransactionActivity extends BaseToolbarActivity implements BaseTransactionView,
        BaseDialogChooserListener, OtpView {

    protected FastItemAdapter<ItemGadaiAdapter> itemGadaiAdapter = new FastItemAdapter<>();
    private CustomFontLatoBoldTextView bodyNamaNasabah;
    private ImageView imageSearch;
    private AppCompatButton buttonDaftarkan;
    private RelativeLayout areaRegisterNasabah;
    private CustomFontLatoRegularTextView nomorCifNasabah;
    private CustomFontLatoRegularTextView nomorKtpNasabah;
    private AppCompatButton buttonSelanjutnya;
    private RelativeLayout areaNasabahDitemukan;
    private AppCompatEditText nomorHp;
    private BaseTransactionViewModel baseTransactionViewModel;
    private EditText searchNasabah;
    private ResponseNasabah responseNasabah;
    private AppCompatImageView ktpImage;
    private AVLoadingIndicatorView circleProgressBarKtpImage;
    private RelativeLayout areaKtpImage;
    private AppCompatEditText namaBankEditText;
    private AppCompatEditText namaPemilikEditText;
    private AppCompatEditText nomorRekeningEditText;
    private FragmentManager fragmentManagerBankAccount;
    private FragmentManager fragmentManagerTambahNasabah;
    private DialogChooserFragment dialogChooserFragment;
    private ArrayAdapter<String> simpleAdapter;
    private String idBank;
    private FastItemAdapter<ItemTransaksi> listDataNasabah = new FastItemAdapter<>();
    private FastItemAdapter<ItemTransaksi> listDataTransaksi = new FastItemAdapter<>();
    private RecyclerView dataNasabah;
    private RecyclerView dataTransaksi;
    private CustomFontLatoBoldTextView namaBank;
    private CustomFontLatoBoldTextView nomorRekening;
    private CustomFontLatoRegularTextView namaNasabah;
    private KonfirmasiGadai konfirmasiGadai;
    private AppCompatTextView otpCountDownText;
    private LinearLayout retryArea;
    private AppCompatEditText otpEditText;
    private Animation slideUp;
    private Animation slideDown;
    private boolean isAlreadySetTimer = false;
    private boolean isAlreadyVerified = false;
    private SmsVerificationReceiver smsVerificationReceiver;
    private FastItemAdapter<ListGadai> listItemAdapter;
    private Map<String, String> dataMap;
    private String idProvinsi;
    private String idKota;
    private String idKecamatan;
    private String idKelurahan;
    private EditText provinceEditText;
    private EditText cityEditText;
    private EditText kecamatanEditText;
    private EditText kelurahanEditText;
    private TextView tanggalLahir;
    private Spinner statusPerkawinanChooser;
    private TextView labelFotoKtp;
    private ProgressBar progressGambarKtp;
    private String pilihProvinsiLabel;
    private String cariProvinsiLabel;
    private String pilihKotaLabel;
    private String cariKotaLabel;
    private String pilihKecamatanLabel;
    private String cariKecamatanLabel;
    private String pilihKelurahanLabel;
    private String cariKelurahanLabel;
    private String statusPerkawinan;
    private Calendar calendarLahir;
    private ArrayList<String> statusPernikahanList;
    private String urlKtp;
    private FastItemAdapter<ListGadai> listPerhiasanFastItemAdapter;
    private String tenor;
    private RequestSubmitGadai requestSubmitGadai;
    private Domain domain;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    private BluetoothService mService = null;

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 101;
    private static final int REQUEST_ENABLE_BT = 102;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    private AppCompatImageView buktiTransaksi;
    private DialogPin dialogPin;
    private ListGadai listGadai;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            finishActivity();
        }
    }

    protected void initAnimation() {
        slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
    }

    protected void initlabelAlamat() {
        pilihProvinsiLabel = getResources().getString(R.string.pilih_provinsi_label);
        cariProvinsiLabel = getResources().getString(R.string.cari_provinsi_label);
        pilihKotaLabel = getResources().getString(R.string.pilih_kota_kabupaten_label);
        cariKotaLabel = getResources().getString(R.string.cari_kota_kabupaten_label);
        pilihKecamatanLabel = getResources().getString(R.string.pilih_kecamatan_label);
        cariKecamatanLabel = getResources().getString(R.string.cari_kecamatan_label);
        pilihKelurahanLabel = getResources().getString(R.string.pilih_kelurahan_label);
        cariKelurahanLabel = getResources().getString(R.string.cari_kelurahan_label);
    }


    @Override
    public void configViewModel(BaseTransactionViewModel baseTransactionViewModel) {
        this.baseTransactionViewModel = baseTransactionViewModel;
    }

    @Override
    public void initComponentCariPengguna(CustomFontLatoBoldTextView bodyNamaNasabah,
                                          ImageView imageSearch,
                                          AppCompatButton buttonDaftarkan,
                                          RelativeLayout areaRegisterNasabah,
                                          CustomFontLatoRegularTextView nomorCifNasabah,
                                          CustomFontLatoRegularTextView nomorKtpNasabah,
                                          AppCompatButton buttonSelanjutnya,
                                          RelativeLayout areaNasabahDitemukan,
                                          AppCompatEditText nomorHp,
                                          EditText searchNasabah,
                                          AppCompatImageView ktpImage,
                                          AVLoadingIndicatorView circleProgressBarKtpImage,
                                          RelativeLayout areaKtpImage) {
        this.bodyNamaNasabah = bodyNamaNasabah;
        this.imageSearch = imageSearch;
        this.buttonDaftarkan = buttonDaftarkan;
        this.areaRegisterNasabah = areaRegisterNasabah;
        this.nomorCifNasabah = nomorCifNasabah;
        this.nomorKtpNasabah = nomorKtpNasabah;
        this.buttonSelanjutnya = buttonSelanjutnya;
        this.areaNasabahDitemukan = areaNasabahDitemukan;
        this.nomorHp = nomorHp;
        this.searchNasabah = searchNasabah;
        this.ktpImage = ktpImage;
        this.circleProgressBarKtpImage = circleProgressBarKtpImage;
        this.areaKtpImage = areaKtpImage;
    }

    @Override
    public void checkKtpOrCif(String ktpOrCif, String type) {
        baseTransactionViewModel.checkKtpOrCif(ktpOrCif, type);
    }

    @Override
    public void sendKonfirmasiTabungan(String nominalPembukaan, String type) {
        baseTransactionViewModel.checkAmount(nominalPembukaan.toString(), type);
    }

    @Override
    public void addKtpImage() {
        EasyImage.openCamera(BaseTransactionActivity.this, 1);
    }

    @Override
    public void goToNextPage(String activity, Object value) {
        baseTransactionViewModel.routeToNextPage(activity, value);
    }

    @Override
    public void showScanQrCode() {
        showActivity(new Intent(this, ScanQrCodeActivity.class));
    }

    public EditText getSearchNasabah() {
        return searchNasabah;
    }

    public ResponseNasabah getResponseNasabah() {
        return responseNasabah;
    }

    public void setResponseNasabah(ResponseNasabah responseNasabah) {
        this.responseNasabah = responseNasabah;
    }

    protected void showNasabahResult() {
        areaNasabahDitemukan.setVisibility(View.VISIBLE);
        bodyNamaNasabah.setText(getResponseNasabah().getNamaNasabah());
        nomorCifNasabah.setText(getResponseNasabah().getCif());
        nomorKtpNasabah.setText(getResponseNasabah().getNoIdentitas());
        nomorHp.setText(getResponseNasabah().getNoHp());
        loadKtpImage(getResponseNasabah().getUrlKtp());
    }

    protected void loadKtpImage(String urlImage) {
        if (labelFotoKtp != null) {
            hidePlaceHolderKtp();
        }
        this.urlKtp = urlImage;
        GlideApp.with(this)
                .load(urlImage)
                .transform(new PositionedCropTransformation(this, 0.5f, 0.5f))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new GlideRequestListener(circleProgressBarKtpImage))
                .into(ktpImage);
    }

    protected void loadKtpImageCircle(String urlImage) {
        if (labelFotoKtp != null) {
            hidePlaceHolderKtp();
        }
        this.urlKtp = urlImage;
        GlideApp.with(this)
                .load(urlImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new GlideRequestListener(circleProgressBarKtpImage))
                .transform(new CircleTransform(this))
                .into(ktpImage);
    }

    protected void hidePlaceHolderKtp() {
        labelFotoKtp.setVisibility(View.GONE);
    }

    @Override
    public void initComponentRekeningTujuan(AppCompatEditText namaBankEditText,
                                            AppCompatEditText namaPemilikEditText,
                                            AppCompatEditText nomorRekeningEditText,
                                            FragmentManager fragmentManager) {
        this.namaBankEditText = namaBankEditText;
        this.namaPemilikEditText = namaPemilikEditText;
        this.nomorRekeningEditText = nomorRekeningEditText;
        this.fragmentManagerBankAccount = fragmentManager;
    }

    @Override
    public void showListOfBankDialog() {
        baseTransactionViewModel.getListOfBank();
    }

    protected void showDialogList(ArrayList<String> values, String title, String hint, String type,
                                  FragmentManager fragmentManager) {
        dismissKeyboard();
        if (dialogChooserFragment != null) {
            dialogChooserFragment.dismiss();
            dialogChooserFragment = null;
        }
        dialogChooserFragment = DialogChooserFragment.newInstance(this, values, title,
                hint, type, this);
        dialogChooserFragment.show(fragmentManager, DialogChooserFragment.TAG_SHOW_LIST);
    }

    protected void showDialogProvince(ArrayList<String> values) {
        showDialogList(values, pilihProvinsiLabel, cariProvinsiLabel, ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS,
                getFragmentTambahNasabah());
    }

    protected void showDialogCity(ArrayList<String> values) {
        showDialogList(values, pilihKotaLabel, cariKotaLabel, ConfigurationUtils.TAG_GET_KOTA_SUCCESS,
                getFragmentTambahNasabah());
    }

    protected void showDialogKecamatan(ArrayList<String> values) {
        showDialogList(values, pilihKecamatanLabel, cariKecamatanLabel, ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS,
                getFragmentTambahNasabah());
    }

    protected void showDialogKelurahan(ArrayList<String> values) {
        showDialogList(values, pilihKelurahanLabel, cariKelurahanLabel, ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS,
                getFragmentTambahNasabah());
    }

    public FragmentManager getFragmentManagerBankAccount() {
        return fragmentManagerBankAccount;
    }

    public FragmentManager getFragmentTambahNasabah() {
        return fragmentManagerTambahNasabah;
    }

    public Map<String, String> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
    }

    @Override
    public void initDialogChooserFragment(AppCompatEditText dialogSearchEditText, ArrayAdapter<String> adapter) {
        simpleAdapter = adapter;
        baseTransactionViewModel.bindingSearchEditText(dialogSearchEditText);
    }

    @Override
    public void handleOnItemSearchDialogClick(int index, String type) {
        dialogChooserFragment.dismiss();
        String item = simpleAdapter.getItem(index);
        String id = MapHelper.getKeyFromValue(dataMap, item).toString();
        if (ConfigurationUtils.TAG_GET_LIST_OF_BANK_SUCCESS.equals(type)) {
            idBank = id;
            namaBankEditText.setText(item);
        } else if (ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS.equals(type)) {
            idProvinsi = id;
            idKota = null;
            idKecamatan = null;
            idKelurahan = null;
            provinceEditText.setText(item);
            cityEditText.setText(null);
            kecamatanEditText.setText(null);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KOTA_SUCCESS.equals(type)) {
            idKota = id;
            idKecamatan = null;
            idKelurahan = null;
            cityEditText.setText(item);
            kecamatanEditText.setText(null);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS.equals(type)) {
            idKecamatan = id;
            idKelurahan = null;
            kecamatanEditText.setText(item);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS.equals(type)) {
            idKelurahan = id;
            kelurahanEditText.setText(item);
        }
    }

    public String getIdBank() {
        return idBank;
    }

    public String getNamaPemilik() {
        return namaPemilikEditText.getText().toString();
    }

    public String getNorekPemilik() {
        return nomorRekeningEditText.getText().toString();
    }

    public FastItemAdapter<ItemTransaksi> getListDataNasabah() {
        return listDataNasabah;
    }

    public FastItemAdapter<ItemTransaksi> getListDataTransaksi() {
        return listDataTransaksi;
    }

    private void configureMenuAdapter(RecyclerView.LayoutManager linearLayout,
                                      FastItemAdapter<ItemTransaksi> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void configureListDataNasabahAdapter(RecyclerView.LayoutManager linearLayout,
                                                 RecyclerView recyclerView) {
        configureMenuAdapter(linearLayout, getListDataNasabah(), recyclerView);
    }

    private void configureListDataTransaksiAdapter(RecyclerView.LayoutManager linearLayout,
                                                   RecyclerView recyclerView) {
        configureMenuAdapter(linearLayout, getListDataTransaksi(), recyclerView);
    }

    @Override
    public void initComponentKonfirmasi(RecyclerView dataNasabah,
                                        RecyclerView dataTransaksi,
                                        CustomFontLatoBoldTextView namaBank,
                                        CustomFontLatoBoldTextView nomorRekening,
                                        CustomFontLatoRegularTextView namaNasabah) {
        this.dataNasabah = dataNasabah;
        this.dataTransaksi = dataTransaksi;
        this.namaBank = namaBank;
        this.nomorRekening = nomorRekening;
        this.namaNasabah = namaNasabah;
        configureListDataNasabahAdapter(new LinearLayoutManager(this), dataNasabah);
        configureListDataTransaksiAdapter(new LinearLayoutManager(this), dataTransaksi);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                baseTransactionViewModel.compressFile(imageFiles.get(0), type);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(BaseTransactionActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
        handleResultBluetooth(requestCode, resultCode, data);
    }

    private void handleResultBluetooth(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE: {
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras().getString(
                            DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        BluetoothDevice device = mBluetoothAdapter
                                .getRemoteDevice(address);
                        // Attempt to connect to the device
                        mService.connect(device);
                    }
                }
                break;
            }
            case REQUEST_ENABLE_BT: {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    mService = new BluetoothService(this, mHandler);
                    Intent serverIntent = new Intent(BaseTransactionActivity.this, DeviceListActivity.class);
                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                } else {
                    // User did not enable Bluetooth or an error occured
                    showSnackBar(this, getString(R.string.bt_not_enabled_leaving));
                    finishActivity();
                }
                break;
            }
        }
    }

    protected void initConfirmationValue() {
        getListDataNasabah().add(new ItemTransaksi(getString(R.string.nama_nasabah),
                getKonfirmasiGadai().getNamaNasabah()));
        getListDataTransaksi().add(new ItemTransaksi(getString(R.string.barang_jaminan),
                getKonfirmasiGadai().getBarangJaminan()));
        getListDataTransaksi().add(new ItemTransaksi(getString(R.string.nilai_taksiran),
                getKonfirmasiGadai().getNilaiTaksiranFormatted()));
        getListDataTransaksi().add(new ItemTransaksi(getString(R.string.nilai_marhun_bih),
                getKonfirmasiGadai().getMarhunBihFormatted()));
        getListDataTransaksi().add(new ItemTransaksi(getString(R.string.tenor),
                getKonfirmasiGadai().getTenor()));
        namaBank.setText(getKonfirmasiGadai().getNamaBank());
        nomorRekening.setText(getKonfirmasiGadai().getNorekRekening());
        namaNasabah.setText(getKonfirmasiGadai().getNamaNasabah());
    }

    public ArrayAdapter<String> getSimpleAdapter() {
        return simpleAdapter;
    }

    public AppCompatEditText getNamaBankEditText() {
        return namaBankEditText;
    }

    public AppCompatEditText getNomorRekeningEditText() {
        return nomorRekeningEditText;
    }

    public AppCompatEditText getNamaPemilikEditText() {
        return namaPemilikEditText;
    }

    public KonfirmasiGadai getKonfirmasiGadai() {
        return konfirmasiGadai;
    }

    public void setKonfirmasiGadai(KonfirmasiGadai konfirmasiGadai) {
        this.konfirmasiGadai = konfirmasiGadai;
    }

    protected void handleNextToOtp() {
        isAlreadySetTimer = true;
        isAlreadyVerified = false;
        baseTransactionViewModel.resetOtpCountDown();
        otpCountDownText.setText(baseTransactionViewModel.getRessetedTimer());
        handleSetOtp();
    }

    protected void handleSetOtp() {
        RequestSendOtpOpenGadai requestSendOtpOpenGadai =
                new RequestSendOtpOpenGadai("081381355063", ConfigurationUtils.TAG_OPEN_GADAI_AGEN,
                        BigDecimal.valueOf(Double.valueOf(getKonfirmasiGadai().getMarhunBih())));
        baseTransactionViewModel.sendOtp(requestSendOtpOpenGadai, ConfigurationUtils.TAG_SEND_OTP);
    }

    @Override
    public void initOtpCountDownFragment(AppCompatTextView timerText,
                                         LinearLayout retryArea,
                                         AppCompatButton retryButton,
                                         AppCompatEditText otpEditText) {
        this.otpCountDownText = timerText;
        this.retryArea = retryArea;
        this.retryArea.setVisibility(View.GONE);
        this.otpEditText = otpEditText;
        retryButton.setOnClickListener(view -> {
            retryArea.setVisibility(View.GONE);
            retryArea.startAnimation(slideDown);
            isAlreadySetTimer = true;
            isAlreadyVerified = false;
            RequestSendOtpOpenGadai requestSendOtpOpenGadai =
                    new RequestSendOtpOpenGadai("081381355063", ConfigurationUtils.TAG_OPEN_GADAI_AGEN,
                            BigDecimal.valueOf(Double.valueOf(getKonfirmasiGadai().getMarhunBih())));
            baseTransactionViewModel.sendOtp(requestSendOtpOpenGadai, ConfigurationUtils.TAG_SEND_RETRY_OTP);
        });
    }

    protected void handleVerifyOtp() {
        baseTransactionViewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                "081381355063"));
    }

    protected void handleSetOtp(String value) {
        otpCountDownText.setText(value);
    }

    protected void handleExpiredOtpCountDownTimer(String value) {
        isAlreadySetTimer = false;
        isAlreadyVerified = false;
        otpCountDownText.setText(value);
        retryArea.setVisibility(View.VISIBLE);
        retryArea.startAnimation(slideUp);
    }

    protected void handleSendOtpSuccess() {
        baseTransactionViewModel.timer();
    }

    protected void handleVerifyOtpSuccess(String ressetedTime) {
        isAlreadySetTimer = false;
        isAlreadyVerified = true;
        baseTransactionViewModel.resetOtpCountDown();
        otpCountDownText.setText(ressetedTime);
        nextToSubmitGadai();
    }

    protected void handleFailedSendOtp() {
        isAlreadySetTimer = false;
        isAlreadyVerified = false;
    }

    public boolean isAlreadySetTimer() {
        return isAlreadySetTimer;
    }

    public boolean isAlreadyVerified() {
        return isAlreadyVerified;
    }

    @Override
    public void setSmsVerificationCode(String code) {
        otpEditText.setText(code);
        otpEditText.setSelection(code.length());
        if (code.length() == 6)
            baseTransactionViewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                    "081381355063"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        baseTransactionViewModel.onStart(Manifest.permission.CAMERA, Manifest.permission.RECEIVE_SMS);
        registerReceiverPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mService != null) {

            if (mService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService.start();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null)
            mService.stop();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(smsVerificationReceiver);
        super.onStop();
    }

    private void registerReceiverPresenter() {
        smsVerificationReceiver = new SmsVerificationReceiver(this);
        registerReceiver(smsVerificationReceiver, new IntentFilter(SmsVerificationReceiver.SMS_RECEIVED_INTENT));
    }

    @Override
    public void initComponentSubmitGadai(FastItemAdapter<ListGadai> listItemAdapter) {
        this.listItemAdapter = listItemAdapter;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public FastItemAdapter<ListGadai> getListItemAdapter() {
        return listItemAdapter;
    }

    protected void setRequestSubmitGadai() {
        baseTransactionViewModel.handleCounterUploadImageObservable();
        List<ListGadai> listGadai = new ArrayList<>();
        listGadai.addAll(baseTransactionViewModel.getListItemGadai(itemGadaiAdapter));
        requestSubmitGadai = new RequestSubmitGadai(getResponseNasabah().getCif(),
                getResponseNasabah().getIbuKandung(),
                getResponseNasabah().getKelurahan().getId(),
                getResponseNasabah().getJalan(),
                getResponseNasabah().getJenisKelamin(),
                getResponseNasabah().getKewarganegaraan(),
                getNorekPemilik(),
                ConfigurationUtils.TAG_AGENT,
                getIdBank(),
                ConfigurationUtils.TAG_KODE_PRODUK_RAHN,
                getResponseNasabah().getNoIdentitas(),
                getResponseNasabah().getNamaNasabah().toUpperCase(),
                getKonfirmasiGadai().getNamaPemilikRekening(),
                getResponseNasabah().getNoHp(),
                "12345",
                "",
                getResponseNasabah().getStatusKawin(),
                0L,
                getResponseNasabah().getTglLahir(),
                System.currentTimeMillis(),
                getResponseNasabah().getTempatLahir(),
                "10",
                listGadai,
                Integer.valueOf(getTenor()),
                BigDecimal.valueOf(Double.valueOf(getKonfirmasiGadai().getMarhunBih())),
                urlKtp);
        listItemAdapter.clear();
        listItemAdapter.add(requestSubmitGadai.getListGadai());
        baseTransactionViewModel.setRequestSubmitGadai(requestSubmitGadai);
        baseTransactionViewModel.handleSubmitGadai();

    }

    public void getDate() {
        final Calendar mcurrentDate = Calendar.getInstance();
        final int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker;
        mDatePicker = new DatePickerDialog(this, (datePicker, selectedyear, selectedmonth, selectedday) -> {
            selectedmonth = selectedmonth + 1;
            calendarLahir.set(selectedyear,selectedmonth,selectedday);
            tanggalLahir.setText(StringHelper.getStringBuilderToString(Integer.toString(selectedday)
                    , "-", Integer.toString(selectedmonth),
                    "-", Integer.toString(selectedyear)));
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Tanggal Lahir");
        mDatePicker.show();

    }

    protected void setImageSearch(boolean status) {
        imageSearch.setEnabled(status);
    }

    protected void setNasabahFound() {
        areaNasabahDitemukan.setVisibility(View.VISIBLE);
    }

    protected void setNasabahNotFound() {
        areaRegisterNasabah.setVisibility(View.VISIBLE);
    }

    public void initComponentTambahNasabah(ImageView fotoKtp,
                                           TextView labelFotoKtp,
                                           AVLoadingIndicatorView progressGambarKtp,
                                           TextView tanggalLahir,
                                           Spinner statusPerkawinan,
                                           EditText provinsi,
                                           EditText kabupaten,
                                           EditText kecamatan,
                                           EditText kelurahan,
                                           FragmentManager fragmentManager) {
        this.calendarLahir = Calendar.getInstance();
        this.provinceEditText = provinsi;
        this.cityEditText = kabupaten;
        this.kecamatanEditText = kecamatan;
        this.kelurahanEditText = kelurahan;
        this.labelFotoKtp = labelFotoKtp;
        this.tanggalLahir = tanggalLahir;
        this.circleProgressBarKtpImage = progressGambarKtp;
        this.fragmentManagerBankAccount = fragmentManager;
        this.ktpImage = (AppCompatImageView) fotoKtp;
        this.statusPerkawinanChooser = statusPerkawinan;
        this.fragmentManagerTambahNasabah = fragmentManager;
    }

    @Override
    public void showProvinceDialog() {
        baseTransactionViewModel.handleGetProvince(ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS);
    }

    @Override
    public void showCityDialog() {
        if (StringUtils.isNotBlank(idProvinsi))
            baseTransactionViewModel.getKota(idProvinsi, ConfigurationUtils.TAG_GET_KOTA_SUCCESS);
    }

    @Override
    public void showKecamatanDialog() {
        if (StringUtils.isNotBlank(idKota))
            baseTransactionViewModel.getKecamatan(idKota, ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS);
    }

    @Override
    public void showKelurahanDialog() {
        if (StringUtils.isNotBlank(idKecamatan))
            baseTransactionViewModel.getKelurahan(idKecamatan, ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS);
    }

    protected void getStatusPernikahan() {
        baseTransactionViewModel.getStatusPernikahan();
    }

    public void tambahNasabah() {
        baseTransactionViewModel.tambahNasabah();
    }

    protected void loadStatusPernikahan(Response response) {
        setDataMap((Map<String, String>) response.result);
        statusPernikahanList = new ArrayList<>(getDataMap().values());
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                statusPerkawinan = MapHelper.getKeyFromValue(getDataMap(), statusPernikahanList.get(i)).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        attachSpinner(statusPerkawinanChooser, statusPernikahanList, listener);
    }

    public void createNasabah(String noHp, String ibuKandung, String jenisKelamin, String namaNasabah,
                              String noIdentitas, String jalan,
                              String tempatLahir) {
        setResponseNasabah(
                new ResponseNasabah(
                        noHp, ibuKandung, jenisKelamin, namaNasabah,
                        noIdentitas, calendarLahir.getTimeInMillis(), statusPerkawinan, jalan,
                        new Domain(idKelurahan, kelurahanEditText.getText().toString()),
                        new Domain(idKecamatan, kecamatanEditText.getText().toString()),
                        new Domain(idKota, cityEditText.getText().toString()),
                        new Domain(idProvinsi, provinceEditText.getText().toString()),
                        tempatLahir, urlKtp
                )
        );
    }

    @Override
    public void printStruk(AppCompatImageView strukImageView) {

        //	byte[] buffer = PrinterCommand.POS_Set_PrtInit();
        Bitmap mBitmap = ((BitmapDrawable) strukImageView.getDrawable())
                .getBitmap();
        int nMode = 0;
        int nPaperWidth = 384;
        if (mBitmap != null) {
            /**
             * Parameters:
             * mBitmap  要打印的图片
             * nWidth   打印宽度（58和80）
             * nMode    打印模式
             * Returns: byte[]
             */
            byte[] data = PrintPicture.POS_PrintBMP(mBitmap, nPaperWidth, nMode);
            //	SendDataByte(buffer);
            SendDataByte(Command.ESC_Init);
            SendDataByte(Command.LF);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());
        }
    }

    /*
     *SendDataByte
	 */
    private void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        mService.write(data);
    }

    protected void startBluetoothService() {
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (mService == null)
                mService = new BluetoothService(this, mHandler);

            Intent serverIntent = new Intent(BaseTransactionActivity.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
        }
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            showSnackBar(BaseTransactionActivity.this, getString(R.string.device_is_connected));
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            System.out.println("aa");
                            break;
                        case BluetoothService.STATE_LISTEN:
                            System.out.println("aa");
                        case BluetoothService.STATE_NONE:
                            System.out.println("aa");
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    System.out.println("aa");
                    break;
                case MESSAGE_READ:
                    System.out.println("aa");
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    System.out.println("aa");
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    showSnackBar(BaseTransactionActivity.this, "Connected to " + mConnectedDeviceName);
                    break;
                case MESSAGE_TOAST:
                    System.out.println("aa");
                    break;
                case MESSAGE_CONNECTION_LOST:    //蓝牙已断开连接
                    System.out.println("aa");
                    showSnackBar(BaseTransactionActivity.this, "Device connection was lost");
                    break;
                case MESSAGE_UNABLE_CONNECT:     //无法连接设备
                    System.out.println("aa");
                    showSnackBar(BaseTransactionActivity.this, "Unable to connect device");
                    break;
            }
        }
    };

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    @Override
    public void shareFile() {
        baseTransactionViewModel.downloadFile(getDomain().getUrlPdf(), true);
    }

    @Override
    public void initComponentCetakBuktiTransaksi(AppCompatImageView buktiTransaksi) {
        this.buktiTransaksi = buktiTransaksi;
    }

    protected void loadBuktiTransaksi() {
        GlideApp.with(this)
                .load(getDomain().getUrl())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(buktiTransaksi);
    }

    @Override
    public void showDialogPin() {
        dialogPin = DialogPin.newInstance();
        dialogPin.setPositiveListener(view -> {
            validasiPin(dialogPin.getPinTransaksi());
            dialogPin.dismissDialog();
        });
        dialogPin.show(getSupportFragmentManager(), "");
    }

    private void validasiPin(String pin) {
        baseTransactionViewModel.validasiPin(pin);
    }

    public RequestSubmitGadai getRequestSubmitGadai() {
        return requestSubmitGadai;
    }

    public void setRequestSubmitGadai(RequestSubmitGadai requestSubmitGadai) {
        this.requestSubmitGadai = requestSubmitGadai;
    }

    public String getUrlKtp() {
        return urlKtp;
    }
}