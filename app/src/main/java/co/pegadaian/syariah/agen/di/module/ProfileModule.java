package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.profile.domain.interactor.AgentUseCase;
import co.pegadaian.syariah.agen.profile.domain.model.AgentRepository;
import co.pegadaian.syariah.agen.profile.viewmodel.factory.ProfileViewModelFactory;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public class ProfileModule {

    @Provides
    AgentRepository provideBerandaRepository(PreferenceManager preferenceManager) {
        return new AgentRepository(preferenceManager,
                RestApiAdapter.getRestApi(),
                RestApiAdapter.getLogoutRestApi());
    }

    @Provides
    ProfileViewModelFactory provideBerandaViewModelFactory(AgentUseCase useCase,
                                                           SchedulersFacade schedulersFacade,
                                                           @Named("JsonParser") JsonParser jsonParser,
                                                           @Named("RxBus") RxBus rxBus,
                                                           PreferenceManager preferenceManager,
                                                           RealmConfigurator realmConfigurator) {
        return new ProfileViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, useCase, realmConfigurator);
    }
}