package co.pegadaian.syariah.agen.object.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by T460s on 3/25/2018.
 */

@Parcel(Parcel.Serialization.BEAN)
public class DetailHistoryPembayaran implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pembayaranId")
    @Expose
    private String pembayaranId;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("nomor_akad")
    @Expose
    private Integer nomorAkad;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("hari_tariff")
    @Expose
    private Integer hariTariff;
    @SerializedName("marhub_bih")
    @Expose
    private Integer marhubBih;
    @SerializedName("cicilan")
    @Expose
    private Integer cicilan;
    @SerializedName("munah")
    @Expose
    private Integer munah;
    @SerializedName("munah_asuransi")
    @Expose
    private Integer munahAsuransi;
    @SerializedName("biaya_administrasi")
    @Expose
    private Integer biayaAdministrasi;
    @SerializedName("total")
    @Expose
    private Integer total;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPembayaranId() {
        return pembayaranId;
    }

    public void setPembayaranId(String pembayaranId) {
        this.pembayaranId = pembayaranId;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public Integer getNomorAkad() {
        return nomorAkad;
    }

    public void setNomorAkad(Integer nomorAkad) {
        this.nomorAkad = nomorAkad;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public Integer getHariTariff() {
        return hariTariff;
    }

    public void setHariTariff(Integer hariTariff) {
        this.hariTariff = hariTariff;
    }

    public Integer getMarhubBih() {
        return marhubBih;
    }

    public void setMarhubBih(Integer marhubBih) {
        this.marhubBih = marhubBih;
    }

    public Integer getCicilan() {
        return cicilan;
    }

    public void setCicilan(Integer cicilan) {
        this.cicilan = cicilan;
    }

    public Integer getMunah() {
        return munah;
    }

    public void setMunah(Integer munah) {
        this.munah = munah;
    }

    public Integer getMunahAsuransi() {
        return munahAsuransi;
    }

    public void setMunahAsuransi(Integer munahAsuransi) {
        this.munahAsuransi = munahAsuransi;
    }

    public Integer getBiayaAdministrasi() {
        return biayaAdministrasi;
    }

    public void setBiayaAdministrasi(Integer biayaAdministrasi) {
        this.biayaAdministrasi = biayaAdministrasi;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}