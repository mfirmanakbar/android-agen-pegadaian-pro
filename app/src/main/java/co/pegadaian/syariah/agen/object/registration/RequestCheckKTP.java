package co.pegadaian.syariah.agen.object.registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/10/2018.
 */

public class RequestCheckKTP {
    @SerializedName("noIdentitas")
    @Expose
    private String noIdentitas;

    public RequestCheckKTP(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }
}
