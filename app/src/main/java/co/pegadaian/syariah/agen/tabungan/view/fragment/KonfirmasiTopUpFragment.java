package co.pegadaian.syariah.agen.tabungan.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.TopUpTabunganEmasActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class KonfirmasiTopUpFragment extends BaseFragment {

    @BindView(R.id.data_nasabah)
    RecyclerView dataNasabah;
    @BindView(R.id.data_transaksi)
    RecyclerView dataTransaksi;
    @BindView(R.id.saldo_awal)
    CustomFontLatoBoldTextView saldoAwal;
    @BindView(R.id.saldo_awal_emas)
    CustomFontLatoBoldTextView saldoAwalEmas;
    @BindView(R.id.label_saldo_awal)
    CustomFontLatoRegularTextView labelSaldoAwal;
    @BindView(R.id.label_saldo_awal_emas)
    CustomFontLatoRegularTextView labelSaldoAwalEmas;
    private Unbinder unbinder;
    private static TopUpTabunganEmasActivity topUpTabunganEmasActivity;

    public static KonfirmasiTopUpFragment newInstance(TopUpTabunganEmasActivity activity) {
        KonfirmasiTopUpFragment simulasiTopUpFragment = new KonfirmasiTopUpFragment();
        topUpTabunganEmasActivity = activity;
        return simulasiTopUpFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_konfirmasi_transaksi, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        topUpTabunganEmasActivity.initComponentKonfirmasi(
                dataNasabah, dataTransaksi, saldoAwal,
                saldoAwalEmas, labelSaldoAwal, labelSaldoAwalEmas);

    }

    @Override
    public void onValidationSucceeded() {

    }

    @OnClick(R.id.button_lanjutkan_pendaftaran)
    public void onViewClicked() {
        topUpTabunganEmasActivity.initDialogPin();
    }
}