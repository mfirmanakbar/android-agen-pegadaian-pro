package co.pegadaian.syariah.agen.object.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by Dell on 3/17/2018.
 */

public class MasterSingleResponse {

    @SerializedName("taksiran")
    @Expose
    private BigDecimal taksiran;

    public BigDecimal getTaksiran() {
        return taksiran;
    }

    public void setTaksiran(BigDecimal taksiran) {
        this.taksiran = taksiran;
    }
}
