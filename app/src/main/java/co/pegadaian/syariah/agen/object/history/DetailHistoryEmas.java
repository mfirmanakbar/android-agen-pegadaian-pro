package co.pegadaian.syariah.agen.object.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by T460s on 3/22/2018.
 */

@Parcel(Parcel.Serialization.BEAN)
public class DetailHistoryEmas implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("history-tabungan-emaId")
    @Expose
    private String historyTabunganEmaId;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("nominal")
    @Expose
    private Integer nominal;
    @SerializedName("biaya_titip")
    @Expose
    private Integer biayaTitip;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("biaya_administrasi")
    @Expose
    private Integer biayaAdministrasi;
    @SerializedName("biaya_transaksi")
    @Expose
    private Integer biayaTransaksi;
    @SerializedName("saldo_rp")
    @Expose
    private Integer saldoRp;
    @SerializedName("saldo_gr")
    @Expose
    private Integer saldoGr;
    @SerializedName("nomor_rekening_emas")
    @Expose
    private Integer nomorRekeningEmas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHistoryTabunganEmaId() {
        return historyTabunganEmaId;
    }

    public void setHistoryTabunganEmaId(String historyTabunganEmaId) {
        this.historyTabunganEmaId = historyTabunganEmaId;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Integer getBiayaTitip() {
        return biayaTitip;
    }

    public void setBiayaTitip(Integer biayaTitip) {
        this.biayaTitip = biayaTitip;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public Integer getBiayaAdministrasi() {
        return biayaAdministrasi;
    }

    public void setBiayaAdministrasi(Integer biayaAdministrasi) {
        this.biayaAdministrasi = biayaAdministrasi;
    }

    public Integer getBiayaTransaksi() {
        return biayaTransaksi;
    }

    public void setBiayaTransaksi(Integer biayaTransaksi) {
        this.biayaTransaksi = biayaTransaksi;
    }

    public Integer getSaldoRp() {
        return saldoRp;
    }

    public void setSaldoRp(Integer saldoRp) {
        this.saldoRp = saldoRp;
    }

    public Integer getSaldoGr() {
        return saldoGr;
    }

    public void setSaldoGr(Integer saldoGr) {
        this.saldoGr = saldoGr;
    }

    public Integer getNomorRekeningEmas() {
        return nomorRekeningEmas;
    }

    public void setNomorRekeningEmas(Integer nomorRekeningEmas) {
        this.nomorRekeningEmas = nomorRekeningEmas;
    }

}
