package co.pegadaian.syariah.agen.history.domain.model;

import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class HistoryRepository extends BaseRepository {

    private final RestApi restApi;

    public HistoryRepository(PreferenceManager preferenceManager,
                             RestApi restApi) {
        super(preferenceManager);
        this.restApi = restApi;
    }

    public Observable<Object> getLastHistoryGadai() {
        return restApi.getLastHistoryGadai();
    }

    public Observable<Object> getLastHistoryEmas() {
        return restApi.getLastHistoryEmas();
    }

    public Observable<Object> getLastHistoryPemasaran() {
        return restApi.getLastHistoryPemasaran();
    }

    public Observable<Object> getLastHistorypembayaran() {
        return restApi.getLastHistorypembayaran();
    }


}