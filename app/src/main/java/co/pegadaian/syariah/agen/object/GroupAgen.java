package co.pegadaian.syariah.agen.object;

/**
 * Created by ArLoJi on 05/03/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupAgen {

    @Expose
    private String id;
    @Expose
    private Integer version;
    @Expose
    private String name;
    @Expose
    private String accessLevel;
    @SerializedName("privilegeList")
    @Expose
    private List<PrivilegeNow> privilegeList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public List<PrivilegeNow> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<PrivilegeNow> privilegeList) {
        this.privilegeList = privilegeList;
    }
}