package co.pegadaian.syariah.agen.realm;

import android.content.Context;

import co.pegadaian.syariah.agen.module.AgentModule;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Dell on 8/1/2017.
 */

public class RealmConfigurator {

    private RealmConfiguration realmConfiguration;
    private Realm realm;

    public RealmConfigurator(Context context) {
        Realm.init(context);
    }

    public RealmConfigurator() {
        configRealm();
    }

    private void configRealm() {
        realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(0)
                .modules(new AgentModule())
                .name("AgentDb.realm")
                .build();
        realm = Realm.getInstance(realmConfiguration);
    }

    public RealmConfiguration getRealmConfiguration() {
        return realmConfiguration;
    }

    public Realm getRealm() {
        return realm;
    }
}