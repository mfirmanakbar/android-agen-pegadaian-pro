package co.pegadaian.syariah.agen.sample.samplemap.domain.model;

import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/4/2018.
 */

public class SampleMapRepository extends BaseRepository {

    public SampleMapRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }
}
