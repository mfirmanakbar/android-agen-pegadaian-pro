package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiRemoteUseCase;
import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiUseCase;
import co.pegadaian.syariah.agen.aktivasi.domain.model.AktivasiRemoteRepository;
import co.pegadaian.syariah.agen.aktivasi.domain.model.AktivasiRepository;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.factory.AktivasiViewModelFactory;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public class AktivasiModule {

    @Provides
    AktivasiRepository provideAktivasiRepository(PreferenceManager preferenceManager) {
        return new AktivasiRepository(preferenceManager);
    }

    @Provides
    AktivasiRemoteRepository provideAktivasiRemoteRepository() {
        return new AktivasiRemoteRepository(RestApiAdapter.getRestApi());
    }

    @Provides
    AktivasiViewModelFactory provideAktivasiViewModelFactory(AktivasiUseCase aktivasiUseCase,
                                                             AktivasiRemoteUseCase aktivasiRemoteUseCase,
                                                             SchedulersFacade schedulersFacade,
                                                             @Named("JsonParser") JsonParser jsonParser,
                                                             @Named("RxBus") RxBus rxBus,
                                                             RealmConfigurator realmConfigurator,
                                                             PreferenceManager preferenceManager) {
        return new AktivasiViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, aktivasiUseCase, aktivasiRemoteUseCase, realmConfigurator);
    }
}
