package co.pegadaian.syariah.agen.transaction.domain.interactors;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.base.usecase.BaseTransactionUseCase;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiLogamMulia;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiNonPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.transaction.domain.model.TransactionRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/16/2018.
 */

public class TransactionUseCase extends BaseTransactionUseCase {

    @Inject
    public TransactionUseCase(TransactionRepository repository,
                              Compressor compressor) {
        super(repository, compressor);
    }

    public List<String> getKarat() {
        List<String> kadarEmas = new ArrayList<>();
        for (int i = 6; i <= 24; i++) {
            kadarEmas.add(String.valueOf(i));
        }
        return kadarEmas;
    }

    public int getProgress(int page, int totalPage) {
        return (100 / totalPage - 1) * (page + 1);
    }

    public Observable<Object> getJenisPerhiasan() {
        return ((TransactionRepository) getRepository()).getJenisPerhiasan();
    }

    public Observable<Object> submitBukaTabunganToBE(SubmitBE submitBE) {
        return ((TransactionRepository) getRepository()).submitBukaTabungan(submitBE);
    }

    public int getKarat(List<String> spinner, String key) {
        int index = 0;
        for (int i = 0; i < spinner.size(); i++) {
            if (key.equals(spinner.get(i))) {
                index = i;
            }
        }
        return index;
    }

    public BigDecimal getTotalTaksiran(List<ItemGadaiAdapter> itemGadaiAdapters) {
        BigDecimal taksir = BigDecimal.valueOf(0);
        for (ItemGadaiAdapter adapter : itemGadaiAdapters) {
            taksir = taksir.add(adapter.getItemGadai().getTaksiranRoundingUp());
        }
        return taksir;
    }

    public Observable<Object> taksir(ItemGadai itemGadai, String type) {
        if (ConfigurationUtils.TAG_KANTONG_PERHIASAN_PLG.equals(type)) {
            return ((TransactionRepository) getRepository()).taksirPerhiasan(itemGadai);
        } else if (ConfigurationUtils.TAG_KANTONG_LOGAM_MULIA.equals(type)) {
            return ((TransactionRepository) getRepository()).taksirLogamMulia(itemGadai);
        } else {
            return null;
        }
    }

    public Observable<Object> simulasiPerhiasan(RequestSimulasiPerhiasan requestSimulasiPerhiasan) {
        return ((TransactionRepository) getRepository()).simulasiPerhiasan(requestSimulasiPerhiasan);
    }

    private List<ItemGadai> convertFromAdapter(List<ItemGadaiAdapter> itemGadaiAdapters,
                                               String type) {
        List<ItemGadai> itemGadaiList = new ArrayList<>();
        for (ItemGadaiAdapter itemGadaiAdapter : itemGadaiAdapters) {
            ItemGadai itemGadai = itemGadaiAdapter.getItemGadai();
            itemGadai.setType(type);
            itemGadaiList.add(itemGadai);
        }
        return itemGadaiList;
    }

    public RequestSimulasiPerhiasan getRequestSimulasiPerhiasan(List<ItemGadaiAdapter> itemGadaiAdapters,
                                                                int tenor,
                                                                String type) {
        return new RequestSimulasiPerhiasan(convertFromAdapter(itemGadaiAdapters, type), tenor);
    }

    public RequestSimulasiLogamMulia getRequestSimulasiLogamMulia(List<ItemGadaiAdapter> itemGadaiAdapters,
                                                                  int tenor,
                                                                  String type) {
        return new RequestSimulasiLogamMulia(convertFromAdapter(itemGadaiAdapters, type), tenor);
    }

    public Observable<Object> getKepingLogam() {
        return ((TransactionRepository) getRepository()).getKepingLogam();
    }

    public Observable<Object> simulasiLogamMulia(RequestSimulasiLogamMulia requestSimulasiLogamMulia) {
        return ((TransactionRepository) getRepository()).simulasiLogamMulia(requestSimulasiLogamMulia);
    }
    public Observable<Object> getJenisKendaraan(int tipeKendaraan) {
        if(tipeKendaraan == 0) {
            return ((TransactionRepository) getRepository()).getJenisKendaraanMotor();
        } else {
            return ((TransactionRepository) getRepository()).getJenisKendaraanMobil();
        }
    }
    public Observable<Object> geBidangUsaha() {
        return ((TransactionRepository) getRepository()).getBidangUsaha();
    }

    public Observable<Object> getJenisTempatUsaha() {
        return ((TransactionRepository) getRepository()).getJenisTempatUsaha();
    }
    public Observable<Object> getStatusTempatUsaha() {
        return ((TransactionRepository) getRepository()).getStatusTempatUsaha();
    }

    public Observable<Object> simulasiKendaraan(RequestSimulasiNonPerhiasan requestSimulasiNonPerhiasan) {
        return ((TransactionRepository) getRepository()).simulasiKendaraan(requestSimulasiNonPerhiasan);
    }

    public Observable<Object> getKendaraan(String type) {
        if (ConfigurationUtils.TAG_MOBIL.equals(type)) {
            return ((TransactionRepository) getRepository()).getMobil();
        } else if (ConfigurationUtils.TAG_MOTOR.equals(type)) {
            return ((TransactionRepository) getRepository()).getMotor();
        } else {
            return null;
        }
    }

    public List<String> getListOfYears() {
        List<String> years = new ArrayList<>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1990; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        return years;
    }
}