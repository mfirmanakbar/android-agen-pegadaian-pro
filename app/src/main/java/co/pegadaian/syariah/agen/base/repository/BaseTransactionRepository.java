package co.pegadaian.syariah.agen.base.repository;

import java.math.BigDecimal;

import co.pegadaian.syariah.agen.object.SingleRequest;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSendOtpOpenGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Dell on 3/18/2018.
 */

public class BaseTransactionRepository extends BaseAgentRepository {

    public BaseTransactionRepository(PreferenceManager preferenceManager,
                                     RestApi restApi,
                                     LogoutRestApi logoutRestApi) {
        super(preferenceManager, restApi, logoutRestApi);
    }

    public Observable<Object> getNasabahByCif(String cif) {
        return getRestApi().getNasabahByCif(getToken(), cif);
    }

    public Observable<Object> getNasabahByKtp(String ktp) {
        return getRestApi().getNasabahByKtp(getToken(), ktp);
    }

    public Observable<Object> mulaiSimulasi(BigDecimal nominalAwal) {
        return getRestApi().mulaiSimulasi(getToken(), new SingleRequest(nominalAwal));
    }

    public Observable<Object> validasiSaldo(BigDecimal nominalAwal) {
        return getRestApi().validasiSaldo(getToken(), new SingleRequest(nominalAwal));
    }

    public Observable<Object> konfirmasiTabungan(BigDecimal nominalTabungan) {
        return getRestApi().konfirmasiPembukaanTabunganEmas(getToken(), new SingleRequest(nominalTabungan));
    }

    public Observable<Object> getListOfBank() {
        return getRestApi().getListOfBank();
    }

    public Observable<Object> checkBank(RequestCheckBank requestCheckBank) {
        return getRestApi().checkBank(getToken(), requestCheckBank);
    }

    public Observable<Object> sendOtpOpenGadai(RequestSendOtpOpenGadai requestSendOtpOpenGadai) {
        return getRestApi().sendOtpOpenGadai(getToken(), requestSendOtpOpenGadai);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return getRestApi().verifyOtp(requestVerifyOtp);
    }

    public void resetOtpCountDown() {
        getPreferenceManager().setCurrentOtpCountdown(ConfigurationUtils.TAG_OTP_COUNTDOWN);
    }

    public Long getCurrentOtpCountDown() {
        return getPreferenceManager().getCurrentOtpCountdown();
    }

    public Observable<Object> submitGadaiPerhiasan(RequestSubmitGadai requestSubmitGadai) {
        requestSubmitGadai.setKodeCabang(getAgentOutletId());
        return getRestApi().submitGadaiPerhiasan(getToken(), requestSubmitGadai);
    }

    public Observable<Object> submitGadaiLogamMulia(RequestSubmitGadai requestSubmitGadai) {
        requestSubmitGadai.setKodeCabang(getAgentOutletId());
        return getRestApi().submitGadaiLogamMulia(getToken(), requestSubmitGadai);
    }

    public Observable<Object> uploadImageRegistration(MultipartBody.Part image) {
        return getRestApi().uploadImage(image);
    }

    public Observable<Object> getStatusPernikahan() {
        return getRestApi().getStatusPernikahan();
    }

    public Observable<Object> validasiPin(String pin) {
        return getRestApi().validasiPin(getToken(), new SingleRequest(pin));
    }

    public Observable<Response<ResponseBody>> download(String url) {
        return getRestApi().downloadFileByUrl(url);
    }
}
