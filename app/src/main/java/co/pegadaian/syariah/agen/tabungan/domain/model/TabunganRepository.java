package co.pegadaian.syariah.agen.tabungan.domain.model;

import java.math.BigDecimal;

import co.pegadaian.syariah.agen.base.repository.BaseTransactionRepository;
import co.pegadaian.syariah.agen.object.SingleRequest;
import co.pegadaian.syariah.agen.object.tabungan.RequestTopUp;
import co.pegadaian.syariah.agen.object.tabungan.SubmitBeTopUp;
import co.pegadaian.syariah.agen.object.transaction.tabungan.DataPembayaran;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class TabunganRepository extends BaseTransactionRepository {

    private RestApi restApi;

    public TabunganRepository(PreferenceManager preferenceManager,
                              RestApi restApi, LogoutRestApi logoutRestApi) {
        super(preferenceManager, restApi, logoutRestApi);
        this.restApi = restApi;
    }

    public Observable<Object> mulaiSimulasi(BigDecimal nominalAwal){
        return restApi.mulaiSimulasi(getToken(),new SingleRequest(nominalAwal));
    }

    public Observable<Object> validasiSaldo(BigDecimal nominalAwal){
        return restApi.validasiSaldo(getToken(),new SingleRequest(nominalAwal));
    }

    public Observable<Object> konfirmasiTabungan(BigDecimal nominalTabungan){
        return restApi.konfirmasiPembukaanTabunganEmas(getToken(),new SingleRequest(nominalTabungan));
    }

    public Observable<Object> getNasabahByCif(String cif){
        return restApi.getNasabahByCif(getToken(), cif);
    }

    public Observable<Object> getNasabahByKtp(String ktp){
        return restApi.getNasabahByKtp(getToken(), ktp);
    }

    public Observable<Object> validasiPin(String pin) {
        return restApi.validasiPin(getToken(), new SingleRequest(pin));
    }

    public Observable<Object> sendSimulasiTopUp(String rekening, String nominal) {
        return restApi.sendValidasiTopUp(getToken(),new RequestTopUp(rekening,nominal));
    }

    public Observable<Object> submitTopUp(String idAgen, SubmitBeTopUp submitBeTopUp) {
        return restApi.submitTopUp(idAgen, getToken(), submitBeTopUp);
    }

    public Observable<Object> mulaiPembayaran(DataPembayaran dataPembayaran) {
        return restApi.mulaiPembayaran(getToken(), dataPembayaran);
    }

    public Observable<Object> submitKonfirmasiPembayaran(DataPembayaran dataPembayaran) {
        return restApi.submitKonfirmasiPembayaran(getToken(), dataPembayaran);
    }
}
