package co.pegadaian.syariah.agen.notification.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.notification.domain.interactor.NotificationUseCase;
import co.pegadaian.syariah.agen.notification.viewmodel.NotificationViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class NotificationViewModelFactory extends BaseViewModelFactory {

    private final NotificationUseCase notificationUseCase;

    public NotificationViewModelFactory(SchedulersFacade schedulersFacade,
                                        JsonParser jsonParser,
                                        RxBus rxBus,
                                        PreferenceManager preferenceManager,
                                        NotificationUseCase notificationUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.notificationUseCase = notificationUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NotificationViewModel.class)) {
            return (T) new NotificationViewModel(getSchedulersFacade(),
                    getJsonParser(),
                    getRxBus(),
                    getPreferenceManager(),
                    notificationUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
