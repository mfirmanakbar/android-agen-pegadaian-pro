package co.pegadaian.syariah.agen.history.adapter;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.pegadaian.syariah.agen.R;

/**
 * Created by T460s on 3/17/2018.
 */

public class DataAdapterEmas extends RecyclerView.Adapter<DataAdapterEmas.ViewHolder> {
    private List<String> countries;

    public DataAdapterEmas(List<String> countries) {
        this.countries = countries;
    }

    @Override
    public DataAdapterEmas.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_transaksi_emas, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapterEmas.ViewHolder viewHolder, int i) {
        Log.d("nama emas", countries.get(i));
        viewHolder.tv_country.setText(countries.get(i));
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_country;
        public ViewHolder(View view) {
            super(view);

            tv_country = (TextView)view.findViewById(R.id.tv_nama);
        }
    }

}
