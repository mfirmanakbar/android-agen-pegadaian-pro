package co.pegadaian.syariah.agen.history.view;

/**
 * Created by T460s on 3/20/2018.
 */

public interface HistoryListener {
    void loadHistoryGadai();
    void loadHistoryEmas();
    void loadHistoryPembayaran();
    void loadHistoryPemasaran();
}
