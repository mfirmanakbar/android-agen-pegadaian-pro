package co.pegadaian.syariah.agen.object.registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

import co.pegadaian.syariah.agen.object.master.Domain;

/**
 * Created by Dell on 3/10/2018.
 */

@Parcel(Parcel.Serialization.BEAN)
public class ResultCheckKTP implements Serializable {

    @SerializedName("cif")
    @Expose
    private String cif;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("kode_cabang")
    @Expose
    private Domain kodeCabang;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("tgl_lahir")
    @Expose
    private Long tglLahir;
    @SerializedName("tgl_kyc")
    @Expose
    private Long tglKyc;
    @SerializedName("status_kyc")
    @Expose
    private String statusKyc;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("kewarganegaraan")
    @Expose
    private String kewarganegaraan;
    @SerializedName("tipe_identitas")
    @Expose
    private String tipeIdentitas;
    @SerializedName("jalan")
    @Expose
    private String jalan;
    @SerializedName("kelurahan")
    @Expose
    private Domain kelurahan;
    @SerializedName("kecamatan")
    @Expose
    private Domain kecamatan;
    @SerializedName("city")
    @Expose
    private Domain city;
    @SerializedName("province")
    @Expose
    private Domain province;

    public ResultCheckKTP() {
    }

    public ResultCheckKTP(String cif,
                          String noHp,
                          String ibuKandung,
                          String jenisKelamin,
                          Domain kodeCabang,
                          String namaNasabah,
                          String noIdentitas,
                          String telp,
                          long tglLahir,
                          long tglKyc,
                          String statusKyc,
                          String statusKawin,
                          String kewarganegaraan,
                          String tipeIdentitas,
                          String jalan,
                          Domain kelurahan,
                          Domain kecamatan,
                          Domain city,
                          Domain province) {
        this.cif = cif;
        this.noHp = noHp;
        this.ibuKandung = ibuKandung;
        this.jenisKelamin = jenisKelamin;
        this.kodeCabang = kodeCabang;
        this.namaNasabah = namaNasabah;
        this.noIdentitas = noIdentitas;
        this.telp = telp;
        this.tglLahir = tglLahir;
        this.tglKyc = tglKyc;
        this.statusKyc = statusKyc;
        this.statusKawin = statusKawin;
        this.kewarganegaraan = kewarganegaraan;
        this.tipeIdentitas = tipeIdentitas;
        this.jalan = jalan;
        this.kelurahan = kelurahan;
        this.kecamatan = kecamatan;
        this.city = city;
        this.province = province;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public Domain getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(Domain kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public Long getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Long tglLahir) {
        this.tglLahir = tglLahir;
    }

    public Long getTglKyc() {
        return tglKyc;
    }

    public void setTglKyc(Long tglKyc) {
        this.tglKyc = tglKyc;
    }

    public String getStatusKyc() {
        return statusKyc;
    }

    public void setStatusKyc(String statusKyc) {
        this.statusKyc = statusKyc;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }

    public String getTipeIdentitas() {
        return tipeIdentitas;
    }

    public void setTipeIdentitas(String tipeIdentitas) {
        this.tipeIdentitas = tipeIdentitas;
    }

    public String getJalan() {
        return jalan;
    }

    public void setJalan(String jalan) {
        this.jalan = jalan;
    }

    public Domain getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(Domain kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Domain getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Domain kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Domain getCity() {
        return city;
    }

    public void setCity(Domain city) {
        this.city = city;
    }

    public Domain getProvince() {
        return province;
    }

    public void setProvince(Domain province) {
        this.province = province;
    }
}
