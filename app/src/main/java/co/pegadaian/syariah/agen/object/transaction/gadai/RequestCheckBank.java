package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/19/2018.
 */

public class RequestCheckBank {

    @SerializedName("kode_bank")
    @Expose
    private String kodeBank;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("no_rek")
    @Expose
    private String noRek;

    public RequestCheckBank(String kodeBank, String namaNasabah, String noRek) {
        this.kodeBank = kodeBank;
        this.namaNasabah = namaNasabah;
        this.noRek = noRek;
    }

    public String getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(String kodeBank) {
        this.kodeBank = kodeBank;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNoRek() {
        return noRek;
    }

    public void setNoRek(String noRek) {
        this.noRek = noRek;
    }
}
