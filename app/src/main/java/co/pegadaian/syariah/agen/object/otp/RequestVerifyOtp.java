package co.pegadaian.syariah.agen.object.otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/9/2018.
 */

public class RequestVerifyOtp {

    @SerializedName("kode_otp")
    @Expose
    private String kodeOtp;
    @SerializedName("no_hp")
    @Expose
    private String noHp;

    public RequestVerifyOtp(String kodeOtp, String noHp) {
        this.kodeOtp = kodeOtp;
        if (noHp.startsWith("0")) {
            this.noHp = noHp;
        } else {
            this.noHp = StringHelper.getStringBuilderToString("0", noHp);
        }
    }

    public String getKodeOtp() {
        return kodeOtp;
    }

    public void setKodeOtp(String kodeOtp) {
        this.kodeOtp = kodeOtp;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
