package co.pegadaian.syariah.agen.transaction.view.listener;

import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by Dell on 3/22/2018.
 */

public interface GadaiKendaraanListener extends BaseGadaiListener {
    void initGadaiKendaraanComponent(AppCompatSpinner merekKendaraan,
                                     ImageView gambarDepanKendaraan,
                                     ProgressBar progressDepan,
                                     ImageView gambarSampingKendaraan,
                                     ProgressBar progressSamping,
                                     ImageView gambarBelakangKendaraan,
                                     ProgressBar progressBelakang,
                                     ImageView gambarBpkbStnk,
                                     ProgressBar progressBpkbStnk,
                                     AppCompatEditText mobilEditText,
                                     AppCompatEditText silinderEditText,
                                     AppCompatSpinner year,
                                     AppCompatEditText policeNumberEditText,
                                     AppCompatEditText bpkbEditText,
                                     AppCompatEditText bpkbOwnerEditText,
                                     AppCompatEditText stnkNumberEditText,
                                     AppCompatEditText nomorRangkaEditText,
                                     AppCompatEditText nomorMesinEditText,
                                     AppCompatEditText warnaKendaraanEditText,
                                     AppCompatEditText tipeMobilEditText);
    void setVehicleType(String type);
    void loadKendaraan();
}
