package co.pegadaian.syariah.agen.object.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import co.pegadaian.syariah.agen.realm.RealmListParcelConverter;
import io.realm.GroupRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by Dell on 3/11/2018.
 */

@RealmClass
@Parcel(implementations = {GroupRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {Group.class})
public class Group extends RealmObject {

    @SerializedName("privilege_list")
    @Expose
    private RealmList<PrivilegeListSignIn> privilegeListSignIn = new RealmList<>();

    public RealmList<PrivilegeListSignIn> getPrivilegeListSignIn() {
        return privilegeListSignIn;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public void setPrivilegeListSignIn(RealmList<PrivilegeListSignIn> privilegeListSignIn) {
        this.privilegeListSignIn = privilegeListSignIn;
    }
}