package co.pegadaian.syariah.agen.object.otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/9/2018.
 */

public class RequestSendSms {

    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("tipe_otp")
    @Expose
    private String tipeOtp;

    public RequestSendSms(String noHp, String tipeOtp) {
        this.noHp = StringHelper.getStringBuilderToString("0", noHp);
        this.tipeOtp = tipeOtp;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getTipeOtp() {
        return tipeOtp;
    }

    public void setTipeOtp(String tipeOtp) {
        this.tipeOtp = tipeOtp;
    }
}
