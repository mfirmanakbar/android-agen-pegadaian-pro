package co.pegadaian.syariah.agen.object.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 19/03/2018.
 */

public class KonfirmasiPembayaran {
    @SerializedName("norek")
    @Expose
    private String norek;
    @SerializedName("namaNasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("jumlahHariReal")
    @Expose
    private String jumlahHariTarif;
    @SerializedName("nilaiTransaksi")
    @Expose
    private String nilaiTransaksi;
    @SerializedName("sewaModal")
    @Expose
    private String sewaModal;
    @SerializedName("administrasi")
    @Expose
    private String administrasi;
    @SerializedName("surcharge")
    @Expose
    private String surcharge;
    @SerializedName("totalKewajiban")
    @Expose
    private String totalKewajiban;
    @SerializedName("up")
    @Expose
    private String up;
    @SerializedName("minimalUpCicil")
    @Expose
    private String minimalUpCicil;
    @SerializedName("reffSwitching")
    @Expose
    private String reffSwitching;

    public KonfirmasiPembayaran(String norek, String namaNasabah, String jumlahHariTarif,
                                String nilaiTransaksi, String sewaModal, String administrasi,
                                String surcharge, String totalKewajiban, String up, String minimalUpCicil,
                                String reffSwitching) {
        this.norek = norek;
        this.namaNasabah = namaNasabah;
        this.jumlahHariTarif = jumlahHariTarif;
        this.nilaiTransaksi = nilaiTransaksi;
        this.sewaModal = sewaModal;
        this.administrasi = administrasi;
        this.surcharge = surcharge;
        this.totalKewajiban = totalKewajiban;
        this.up = up;
        this.minimalUpCicil = minimalUpCicil;
        this.reffSwitching = reffSwitching;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getJumlahHariTarif() {
        return jumlahHariTarif;
    }

    public void setJumlahHariTarif(String jumlahHariTarif) {
        this.jumlahHariTarif = jumlahHariTarif;
    }

    public String getNilaiTransaksi() {
        return nilaiTransaksi;
    }

    public void setNilaiTransaksi(String nilaiTransaksi) {
        this.nilaiTransaksi = nilaiTransaksi;
    }

    public String getSewaModal() {
        return sewaModal;
    }

    public void setSewaModal(String sewaModal) {
        this.sewaModal = sewaModal;
    }

    public String getAdministrasi() {
        return administrasi;
    }

    public void setAdministrasi(String administrasi) {
        this.administrasi = administrasi;
    }

    public String getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String surcharge) {
        this.surcharge = surcharge;
    }

    public String getTotalKewajiban() {
        return totalKewajiban;
    }

    public void setTotalKewajiban(String totalKewajiban) {
        this.totalKewajiban = totalKewajiban;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public String getMinimalUpCicil() {
        return minimalUpCicil;
    }

    public void setMinimalUpCicil(String minimalUpCicil) {
        this.minimalUpCicil = minimalUpCicil;
    }

    public String getReffSwitching() {
        return reffSwitching;
    }

    public void setReffSwitching(String reffSwitching) {
        this.reffSwitching = reffSwitching;
    }
}
