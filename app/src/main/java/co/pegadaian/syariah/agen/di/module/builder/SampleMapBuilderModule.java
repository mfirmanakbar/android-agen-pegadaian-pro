package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.SampleMapModule;
import co.pegadaian.syariah.agen.sample.samplemap.view.SampleMapsActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/4/2018.
 */

@Module
public abstract class SampleMapBuilderModule {

    @ContributesAndroidInjector(modules = {SampleMapModule.class})
    abstract SampleMapsActivity bindActivity();
}
