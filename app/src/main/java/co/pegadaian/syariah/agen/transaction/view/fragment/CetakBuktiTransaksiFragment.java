package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/21/2018.
 */

public class CetakBuktiTransaksiFragment extends BaseFragment {

    @BindView(R.id.bukti_traksaksi)
    AppCompatImageView buktiTraksaksi;
    private Unbinder unbinder;
    private static BaseTransactionActivity baseTransactionActivity;

    public static CetakBuktiTransaksiFragment newInstance(BaseTransactionActivity activity) {
        CetakBuktiTransaksiFragment cetakBuktiTransaksiFragment = new CetakBuktiTransaksiFragment();
        baseTransactionActivity = activity;
        return cetakBuktiTransaksiFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cetak_bukti_transaksi, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        baseTransactionActivity.initComponentCetakBuktiTransaksi(buktiTraksaksi);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {
    }

    @OnClick({R.id.print_bukti_transaksi, R.id.send_bukti_transaksi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.print_bukti_transaksi:
                baseTransactionActivity.printStruk(buktiTraksaksi);
                break;
            case R.id.send_bukti_transaksi:
                baseTransactionActivity.shareFile();
                break;
        }
    }
}
