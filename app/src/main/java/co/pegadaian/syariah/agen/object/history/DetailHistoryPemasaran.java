package co.pegadaian.syariah.agen.object.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by T460s on 3/25/2018.
 */

@Parcel(Parcel.Serialization.BEAN)
public class DetailHistoryPemasaran implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("pemasaranId")
    @Expose
    private String pemasaranId;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("bank_mitra")
    @Expose
    private String bankMitra;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("tenor")
    @Expose
    private Integer tenor;
    @SerializedName("total_transaksi")
    @Expose
    private Integer totalTransaksi;
    @SerializedName("uang_pinjaman")
    @Expose
    private Integer uangPinjaman;
    @SerializedName("munah_awal")
    @Expose
    private Integer munahAwal;
    @SerializedName("diskon_munah")
    @Expose
    private Integer diskonMunah;
    @SerializedName("munah_nett")
    @Expose
    private Integer munahNett;
    @SerializedName("angsuran")
    @Expose
    private Integer angsuran;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPemasaranId() {
        return pemasaranId;
    }

    public void setPemasaranId(String pemasaranId) {
        this.pemasaranId = pemasaranId;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getBankMitra() {
        return bankMitra;
    }

    public void setBankMitra(String bankMitra) {
        this.bankMitra = bankMitra;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public Integer getTotalTransaksi() {
        return totalTransaksi;
    }

    public void setTotalTransaksi(Integer totalTransaksi) {
        this.totalTransaksi = totalTransaksi;
    }

    public Integer getUangPinjaman() {
        return uangPinjaman;
    }

    public void setUangPinjaman(Integer uangPinjaman) {
        this.uangPinjaman = uangPinjaman;
    }

    public Integer getMunahAwal() {
        return munahAwal;
    }

    public void setMunahAwal(Integer munahAwal) {
        this.munahAwal = munahAwal;
    }

    public Integer getDiskonMunah() {
        return diskonMunah;
    }

    public void setDiskonMunah(Integer diskonMunah) {
        this.diskonMunah = diskonMunah;
    }

    public Integer getMunahNett() {
        return munahNett;
    }

    public void setMunahNett(Integer munahNett) {
        this.munahNett = munahNett;
    }

    public Integer getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(Integer angsuran) {
        this.angsuran = angsuran;
    }

}