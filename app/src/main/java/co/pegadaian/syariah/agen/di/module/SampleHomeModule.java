package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.sample.samplehome.domain.interactors.HomeLocalUseCase;
import co.pegadaian.syariah.agen.sample.samplehome.domain.model.HomeLocalRepository;
import co.pegadaian.syariah.agen.sample.samplehome.viewmodel.factory.HomeViewModelFactory;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 2/28/2018.
 */

@Module
public class SampleHomeModule {

    @Provides
    HomeLocalRepository provideHomeLocalRepository(PreferenceManager preferenceManager) {
        return new HomeLocalRepository(preferenceManager);
    }

    @Provides
    HomeViewModelFactory provideHomeViewModelFactory(SchedulersFacade schedulersFacade,
                                                     @Named("JsonParser") JsonParser jsonParser,
                                                     @Named("RxBus") RxBus rxBus,
                                                     PreferenceManager preferenceManager,
                                                     HomeLocalUseCase homeLocalUseCase) {
        return new HomeViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, homeLocalUseCase);
    }
}
