package co.pegadaian.syariah.agen.login.domain.model;


import co.pegadaian.syariah.agen.object.authentication.Authentication;
import io.reactivex.Observable;

/**
 * Created by Dell on 2/27/2018.
 */

public interface LoginRemoteModel {
    Observable<Object> login(Authentication authentication);
}
