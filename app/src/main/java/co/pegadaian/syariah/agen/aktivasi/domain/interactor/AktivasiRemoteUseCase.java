package co.pegadaian.syariah.agen.aktivasi.domain.interactor;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.aktivasi.domain.model.AktivasiRemoteRepository;
import co.pegadaian.syariah.agen.object.authentication.RequestChangePIN;
import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 08/03/2018.
 */

public class AktivasiRemoteUseCase {

    private AktivasiRemoteRepository remoteRepository;

    @Inject
    public AktivasiRemoteUseCase(AktivasiRemoteRepository repository) {
        this.remoteRepository = repository;
    }

    public Observable<Object> sendOtp(RequestSendSms requestSendSms){
        return remoteRepository.sendOtp(requestSendSms);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return remoteRepository.verifyOtp(requestVerifyOtp);
    }

    public Observable<Object> changePIN(String accessToken, RequestChangePIN requestChangePIN) {
        return remoteRepository.changePIN(accessToken, requestChangePIN);
    }

    public Observable<Object> getUserProfile(String accessToken) {
        return remoteRepository.getUserProfile(accessToken);
    }
}
