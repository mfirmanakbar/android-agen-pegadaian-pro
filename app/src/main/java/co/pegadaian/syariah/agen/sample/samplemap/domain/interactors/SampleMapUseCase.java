package co.pegadaian.syariah.agen.sample.samplemap.domain.interactors;

import android.location.Address;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.sample.samplemap.domain.model.SampleMapRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

/**
 * Created by Dell on 3/4/2018.
 */

public class SampleMapUseCase extends BaseUseCase {

    @Inject
    public SampleMapUseCase(SampleMapRepository repository,
                            Compressor compressor) {
        super(repository, compressor);
    }

    public Observable<String> getAddressObservable(ReactiveLocationProvider provider,
                                                   double latitude,
                                                   double longitude) {
        return getReverseGeocodeObservable(provider, latitude, longitude)
                .map(getListAddress())
                .map(getAddress());
    }

    public Observable<List<Address>> getReverseGeocodeObservable(ReactiveLocationProvider provider,
                                                                 double latitude,
                                                                 double longitude) {
        return provider.getReverseGeocodeObservable(latitude, longitude, 1);
    }

    @NonNull
    public Function<List<Address>, Address> getListAddress() {
        return addresses -> addresses != null && !addresses.isEmpty() ? addresses.get(0) : null;
    }

    @NonNull
    public Function<Address, String> getAddress() {
        return address -> {
            if (address == null) return "";

            String addressLines = "";
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressLines += address.getAddressLine(i) + '\n';
            }
            return addressLines;
        };
    }
}