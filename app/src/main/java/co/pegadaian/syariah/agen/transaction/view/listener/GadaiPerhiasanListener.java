package co.pegadaian.syariah.agen.transaction.view.listener;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by Dell on 3/16/2018.
 */

public interface GadaiPerhiasanListener extends BaseGadaiListener {
    void initFormGadaiPerhiasanTaksir(RecyclerView listPerhiasan, AppCompatSpinner jenisPerhiasan, EditText jumlahPerhiasan,
                                      AppCompatSpinner kadarEmas, EditText beratKotorPerhiasan, EditText beratBersihPerhiasan,
                                      EditText keteranganBarang, ImageView gambarPerhiasan, ProgressBar progressPicture,
                                      AppCompatButton buttonTambahPerhiasan, AppCompatButton buttonSelanjutnya, View fakeLine);
}