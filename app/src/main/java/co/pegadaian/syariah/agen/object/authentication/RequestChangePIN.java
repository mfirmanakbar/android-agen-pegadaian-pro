package co.pegadaian.syariah.agen.object.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/11/2018.
 */

public class RequestChangePIN {
    @SerializedName("confirm_pin")
    @Expose
    private String confirmPin;
    @SerializedName("new_pin")
    @Expose
    private String newPin;

    public RequestChangePIN(String confirmPin, String newPin) {
        this.newPin = newPin;
        this.confirmPin = confirmPin;
    }

    public String getConfirmPin() {
        return confirmPin;
    }

    public void setConfirmPin(String confirmPin) {
        this.confirmPin = confirmPin;
    }

    public String getNewPin() {
        return newPin;
    }

    public void setNewPin(String newPin) {
        this.newPin = newPin;
    }
}
