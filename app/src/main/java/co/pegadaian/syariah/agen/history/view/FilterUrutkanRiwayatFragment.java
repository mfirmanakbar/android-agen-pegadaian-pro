package co.pegadaian.syariah.agen.history.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import javax.annotation.Nullable;

import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by T460s on 3/18/2018.
 */

public class FilterUrutkanRiwayatFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    public static FilterUrutkanRiwayatFragment newInstance() {
        return new FilterUrutkanRiwayatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_filter_urutkan_riwayat, container,
                false);

        // get the views and attach the listener
        Button b = (Button) view.findViewById(R.id.btnSortTerlamaTerbaru);
        b.setOnClickListener(this);
        return view;

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSortTerlamaTerbaru:
//                HistoryDetailActivity historyDetailActivity = (HistoryDetailActivity) getActivity();
//                if(historyDetailActivity!=null){
////                    historyDetailActivity.renderList(2);
//                    this.dismiss();
//                }
                this.dismiss();
                break;
        }
    }

}
