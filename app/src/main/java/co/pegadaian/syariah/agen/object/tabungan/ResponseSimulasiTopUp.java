package co.pegadaian.syariah.agen.object.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by ArLoJi on 18/03/2018.
 */

public class ResponseSimulasiTopUp {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("version")
    @Expose
    private Integer version;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("surchange")
    @Expose
    private BigDecimal surchange;
    @SerializedName("gram")
    @Expose
    private BigDecimal gram;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("total_kewajiban")
    @Expose
    private BigDecimal totalKewajiban;
    @SerializedName("status_of_transaction")
    @Expose
    private Boolean statusOfTransaction;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getSurchange() {
        return surchange;
    }

    public void setSurchange(BigDecimal surchange) {
        this.surchange = surchange;
    }

    public BigDecimal getGram() {
        return gram;
    }

    public void setGram(BigDecimal gram) {
        this.gram = gram;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getTotalKewajiban() {
        return totalKewajiban;
    }

    public void setTotalKewajiban(BigDecimal totalKewajiban) {
        this.totalKewajiban = totalKewajiban;
    }

    public Boolean getStatusOfTransaction() {
        return statusOfTransaction;
    }

    public void setStatusOfTransaction(Boolean statusOfTransaction) {
        this.statusOfTransaction = statusOfTransaction;
    }
}
