package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class FormAmanahNasabah01Fragment extends BaseFragment {
    public static final int TIPE_NASABAH_KARYAWAN = 1;
    public static final int TIPE_NASABAH_PENGUSAHA_MIKRO = 2;
    public static final int TIPE_KENDARAAN_MOTOR = 1;
    public static final int TIPE_KENDARAAN_MOBIL = 2;
    Unbinder unbinder;
    private static AmanahActivity amanahActivity;

    @BindView(R.id.btn_nasabah_karyawan)
    AppCompatButton btnNasabahKaryawan;
    @BindView(R.id.btn_nasabah_pengusaha)
    AppCompatButton btnNasabahPengusaha;
    @BindView(R.id.btn_kendaraan_motor)
    AppCompatButton btnKendaraanMotor;
    @BindView(R.id.btn_kendaraan_mobil)
    AppCompatButton btnKendaraanMobil;

    @NotEmpty
    @BindView(R.id.tv_maksimal_plafon)
    CustomFontLatoRegulerEditText tvAmanahMaksimalPlafon;
    @NotEmpty
    @BindView(R.id.tv_amanah_uang_muka)
    CustomFontLatoRegulerEditText tvAmanahUangMuka;
    @NotEmpty
    @BindView(R.id.tv_amanah_harga_pasar)
    CustomFontLatoRegulerEditText tvAmanahHargaPasar;

    @BindColor(R.color.transparent)
    int bgColorTransparent;
    @BindColor(R.color.grey)
    int colorGrey;
    @BindColor(R.color.white)
    int colorWhite;
    @BindDrawable(R.drawable.button_green_selector)
    Drawable bgGreenSelector;

    private int tipeNasabah = TIPE_NASABAH_KARYAWAN;
    private int tipeKendaraan = TIPE_KENDARAAN_MOTOR;

    public static FormAmanahNasabah01Fragment newInstance(AmanahActivity activity) {
        FormAmanahNasabah01Fragment formAmanahNasabahFragment = new FormAmanahNasabah01Fragment();
        amanahActivity = activity;
        return formAmanahNasabahFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_amanah_nasabah, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        amanahActivity.initFormAmanahNasabah01(tvAmanahMaksimalPlafon, tvAmanahUangMuka, tvAmanahHargaPasar, tipeKendaraan, tipeNasabah);
    }

    @Override
    public void onValidationSucceeded() {
        amanahActivity.nextAmanahSimulasi();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_nasabah_karyawan,
            R.id.btn_nasabah_pengusaha,
            R.id.btn_kendaraan_motor,
            R.id.btn_kendaraan_mobil,
            R.id.button_next})
    public void onButtonNasabahToggleClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_nasabah_pengusaha:
                btnNasabahKaryawan.setBackgroundColor(bgColorTransparent);
                btnNasabahKaryawan.setTextColor(colorGrey);
                btnNasabahPengusaha.setBackground(bgGreenSelector);
                btnNasabahPengusaha.setTextColor(colorWhite);
                tipeNasabah = TIPE_NASABAH_KARYAWAN;
                break;
            case R.id.btn_nasabah_karyawan:
                btnNasabahKaryawan.setBackground(bgGreenSelector);
                btnNasabahKaryawan.setTextColor(colorWhite);
                btnNasabahPengusaha.setBackgroundColor(bgColorTransparent);
                btnNasabahPengusaha.setTextColor(colorGrey);
                tipeNasabah = TIPE_NASABAH_PENGUSAHA_MIKRO;
                break;
            case R.id.btn_kendaraan_mobil:
                btnKendaraanMotor.setBackgroundColor(bgColorTransparent);
                btnKendaraanMotor.setTextColor(colorGrey);
                btnKendaraanMobil.setBackground(bgGreenSelector);
                btnKendaraanMobil.setTextColor(colorWhite);
                tipeKendaraan = TIPE_KENDARAAN_MOTOR;
                break;
            case R.id.btn_kendaraan_motor:
                btnKendaraanMotor.setBackground(bgGreenSelector);
                btnKendaraanMotor.setTextColor(colorWhite);
                btnKendaraanMobil.setBackgroundColor(bgColorTransparent);
                btnKendaraanMobil.setTextColor(colorGrey);
                tipeKendaraan = TIPE_KENDARAAN_MOBIL;
                break;
            default:
                getValidator().validate();
                break;
        }
    }

}
