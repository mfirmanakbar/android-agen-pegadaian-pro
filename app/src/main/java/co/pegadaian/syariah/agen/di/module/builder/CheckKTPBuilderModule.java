package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.RegistrationModule;
import co.pegadaian.syariah.agen.registration.view.CheckUserKTPActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public abstract class CheckKTPBuilderModule {

    @ContributesAndroidInjector(modules = {RegistrationModule.class})
    abstract CheckUserKTPActivity bindActivity();
}
