package co.pegadaian.syariah.agen.login.domain.interactors;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.login.domain.model.LoginLocalRepository;
import co.pegadaian.syariah.agen.object.authentication.Authentication;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by ArLoJi on 06/03/2018.
 */

public class LoginLocalUseCase extends BaseAgentUseCase {

    private final LoginLocalRepository localRepository;

    @Inject
    public LoginLocalUseCase(LoginLocalRepository repository,
                             Compressor compressor) {
        super(repository, compressor);
        this.localRepository = repository;
    }

    public void insertToken(String accessToken, String id, String statusRegistrasi) {
        if (ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED.equals(statusRegistrasi)) {
            localRepository.setUID(id);
            localRepository.setToken(accessToken);
        }
    }

    public String getToken() {
        return localRepository.getToken();
    }

    public Authentication checkEmailPassword(String email, String password) {
        if (StringUtils.isNotBlank(email) && StringUtils.isNotBlank(password) && StringHelper.isEmail(email)) {
            return new Authentication(email, password);
        }
        return null;
    }
}