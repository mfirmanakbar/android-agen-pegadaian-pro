package co.pegadaian.syariah.agen.object.transaction.gadai;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;

/**
 * Created by Dell on 3/20/2018.
 */

public class ListGadai extends AbstractItem<ListGadai, ListGadai.ViewHolder> {

    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("tipe_jaminan")
    @Expose
    private String tipeJaminan;
    @SerializedName("berat")
    @Expose
    private Double berat;
    @SerializedName("berat_kotor")
    @Expose
    private Double beratKotor;
    @SerializedName("jenis_perhiasan")
    @Expose
    private String jenisPerhiasan;
    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("karat")
    @Expose
    private Integer karat;
    @SerializedName("foto_barang")
    @Expose
    private String fotoBarang;

    private String submitStatus;
    private File imageFile;
    private int index;

    //gadai kendaraan
    @SerializedName("harga_pasar")
    @Expose
    private BigDecimal hargaPasar;
    @SerializedName("isi_silinder")
    @Expose
    private String isiSilinder;
    @SerializedName("kendaraan_baru_bekas")
    @Expose
    private String kendaraanBaruBekas;
    @SerializedName("merek")
    @Expose
    private String merek;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("nama_bpkb")
    @Expose
    private String namaBpkb;
    @SerializedName("no_bpkb")
    @Expose
    private String noBpkb;
    @SerializedName("no_mesin")
    @Expose
    private String noMesin;
    @SerializedName("no_polisi")
    @Expose
    private String noPolisi;
    @SerializedName("no_rangka")
    @Expose
    private String noRangka;
    @SerializedName("no_stnk")
    @Expose
    private String noStnk;
    @SerializedName("tahun")
    @Expose
    private String tahun;
    @SerializedName("tahun_rakit")
    @Expose
    private String tahunRakit;
    @SerializedName("warna")
    @Expose
    private String warna;
    @SerializedName("taksiran")
    @Expose
    private Integer taksiran;
    @SerializedName("foto_barang_depan")
    @Expose
    private String fotoBarangDepan;
    @SerializedName("foto_barang_samping")
    @Expose
    private String fotoBarangSamping;
    @SerializedName("foto_barang_belakang")
    @Expose
    private String fotoBarangBelakang;
    @SerializedName("foto_barang_bpkb_stnk")
    @Expose
    private String fotoBarangBpkbStnk;

    public ListGadai() {
    }

    public ListGadai(String keterangan,
                     String tipeJaminan,
                     Double berat,
                     Double beratKotor,
                     String jenisPerhiasan,
                     Integer jumlah,
                     Integer karat,
                     File imageFile,
                     int index) {
        this.keterangan = keterangan;
        this.tipeJaminan = tipeJaminan;
        this.berat = berat;
        this.beratKotor = beratKotor;
        this.jenisPerhiasan = jenisPerhiasan;
        this.jumlah = jumlah;
        this.karat = karat;
        this.imageFile = imageFile;
        this.index = index;
    }

    public ListGadai(BigDecimal hargaPasar,
                     String isiSilinder,
                     String keterangan,
                     String kendaraanBaruBekas,
                     String merek,
                     String model,
                     String namaBpkb,
                     String noBpkb,
                     String noMesin,
                     String noPolisi,
                     String noRangka,
                     String noStnk,
                     String tahun,
                     String tahunRakit,
                     String tipeJaminan,
                     String warna,
                     Integer taksiran,
                     String fotoBarangDepan,
                     String fotoBarangSamping,
                     String fotoBarangBelakang,
                     String fotoBarangBpkbStnk) {
        this.hargaPasar = hargaPasar;
        this.isiSilinder = isiSilinder;
        this.keterangan = keterangan;
        this.kendaraanBaruBekas = kendaraanBaruBekas;
        this.merek = merek;
        this.model = model;
        this.namaBpkb = namaBpkb;
        this.noBpkb = noBpkb;
        this.noMesin = noMesin;
        this.noPolisi = noPolisi;
        this.noRangka = noRangka;
        this.noStnk = noStnk;
        this.tahun = tahun;
        this.tahunRakit = tahunRakit;
        this.tipeJaminan = tipeJaminan;
        this.warna = warna;
        this.taksiran = taksiran;
        this.fotoBarangDepan = fotoBarangDepan;
        this.fotoBarangSamping = fotoBarangSamping;
        this.fotoBarangBelakang = fotoBarangBelakang;
        this.fotoBarangBpkbStnk = fotoBarangBpkbStnk;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTipeJaminan() {
        return tipeJaminan;
    }

    public void setTipeJaminan(String tipeJaminan) {
        this.tipeJaminan = tipeJaminan;
    }

    public Double getBerat() {
        return berat;
    }

    public void setBerat(Double berat) {
        this.berat = berat;
    }

    public Double getBeratKotor() {
        return beratKotor;
    }

    public void setBeratKotor(Double beratKotor) {
        this.beratKotor = beratKotor;
    }

    public String getJenisPerhiasan() {
        return jenisPerhiasan;
    }

    public void setJenisPerhiasan(String jenisPerhiasan) {
        this.jenisPerhiasan = jenisPerhiasan;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Integer getKarat() {
        return karat;
    }

    public void setKarat(Integer karat) {
        this.karat = karat;
    }

    public String getFotoBarang() {
        return fotoBarang;
    }

    public void setFotoBarang(String fotoBarang) {
        this.fotoBarang = fotoBarang;
    }

    public String getSubmitStatus() {
        return submitStatus;
    }

    public void setSubmitStatus(String submitStatus) {
        this.submitStatus = submitStatus;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSubmitStatus(Context context) {
        return (StringUtils.isNotBlank(getSubmitStatus()) && ConfigurationUtils.TAG_UPLOADED.equals(getSubmitStatus())) ?
                context.getResources().getString(R.string.upload_done) :
                context.getResources().getString(R.string.upload_processing);
    }

    public int getIconStatus(Context context) {
        return (StringUtils.isNotBlank(getSubmitStatus(context)) && ConfigurationUtils.TAG_UPLOADED.equals(getSubmitStatus())) ?
                R.drawable.ic_check_circle_green_24dp :
                R.drawable.ic_radio_button_checked_green_24dp;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.submit_gadai_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_submit_gadai;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.gadai_item_name)
        CustomFontLatoBoldTextView gadaiItemName;
        @BindView(R.id.gadai_item_status)
        CustomFontLatoRegularTextView gadaiItemStatus;
        @BindView(R.id.gadai_item_status_icon)
        AppCompatImageView gadaiItemStatusIcon;

        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.gadaiItemName.setText(getKeterangan());
        holder.gadaiItemStatus.setText(getSubmitStatus(holder.context));
        holder.gadaiItemStatusIcon.setImageResource(getIconStatus(holder.context));
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
    }
}
