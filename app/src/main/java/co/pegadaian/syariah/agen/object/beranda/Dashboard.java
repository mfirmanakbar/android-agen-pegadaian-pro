package co.pegadaian.syariah.agen.object.beranda;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 3/15/2018.
 */

public class Dashboard {

    private List<Menu> rahn = new ArrayList<>();
    private List<Menu> emas = new ArrayList<>();
    private List<Menu> pembayaran = new ArrayList<>();
    private List<Menu> pemasaran = new ArrayList<>();

    public Dashboard() {
        this.rahn = new ArrayList<>();
        this.emas = new ArrayList<>();
        this.pembayaran = new ArrayList<>();
        this.pemasaran = new ArrayList<>();
    }

    public List<Menu> getRahn() {
        return rahn;
    }

    public List<Menu> getEmas() {
        return emas;
    }

    public List<Menu> getPembayaran() {
        return pembayaran;
    }

    public List<Menu> getPemasaran() {
        return pemasaran;
    }
}
