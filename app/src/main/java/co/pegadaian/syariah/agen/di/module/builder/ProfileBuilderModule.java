package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.ProfileModule;
import co.pegadaian.syariah.agen.profile.view.ChangeProfileActivity;
import co.pegadaian.syariah.agen.profile.view.ProfileActivity;
import co.pegadaian.syariah.agen.profile.view.UbahPasswordPinActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public abstract class ProfileBuilderModule {
    @ContributesAndroidInjector(modules = {ProfileModule.class})
    abstract ProfileActivity bindActivity();
    @ContributesAndroidInjector(modules = {ProfileModule.class})
    abstract UbahPasswordPinActivity changeActivity();
    @ContributesAndroidInjector(modules = {ProfileModule.class})
    abstract ChangeProfileActivity changeProfileActivity();
}
