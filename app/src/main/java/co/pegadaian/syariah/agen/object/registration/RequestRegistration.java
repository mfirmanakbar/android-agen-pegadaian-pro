package co.pegadaian.syariah.agen.object.registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.app.pegadaian.library.utils.ConfigurationUtils;

/**
 * Created by Dell on 3/10/2018.
 */

public class RequestRegistration {

    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("alamat_usaha")
    @Expose
    private String alamatUsaha;
    @SerializedName("cif")
    @Expose
    private String cif;
    @SerializedName("confirm_pass")
    @Expose
    private String confirmPass;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("expired_date")
    @Expose
    private Long expiredDate;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_kecamatan")
    @Expose
    private String idKecamatan;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;
    @SerializedName("id_kota")
    @Expose
    private String idKota;
    @SerializedName("id_provinsi")
    @Expose
    private String idProvinsi;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("jenis_usaha")
    @Expose
    private String jenisUsaha;
    @SerializedName("kecamatan_usaha")
    @Expose
    private String kecamatanUsaha;
    @SerializedName("kelurahan_usaha")
    @Expose
    private String kelurahanUsaha;
    @SerializedName("kode_bank")
    @Expose
    private String kodeBank;
    @SerializedName("kode_cabang")
    @Expose
    private String kodeCabang;
    @SerializedName("kota_usaha")
    @Expose
    private String kotaUsaha;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("nama_bank")
    @Expose
    private String namaBank;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("nama_pemilik")
    @Expose
    private String namaPemilik;
    @SerializedName("nama_usaha")
    @Expose
    private String namaUsaha;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("nomor_rekening")
    @Expose
    private String norekPemilik;
    @SerializedName("password")
    @Expose
    private String passsword;
    @SerializedName("photo_belakang")
    @Expose
    private String photoBelakang;
    @SerializedName("photo_depan")
    @Expose
    private String photoDepan;
    @SerializedName("photo_ktp")
    @Expose
    private String photoKtp;
    @SerializedName("photo_selfie")
    @Expose
    private String photoSelfie;
    @SerializedName("provinsi_usaha")
    @Expose
    private String provinsiUsaha;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("tanggal_lahir")
    @Expose
    private Long tanggalLahir;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("version")
    @Expose
    private Integer version;
    @SerializedName("origin")
    @Expose
    private String origin;

    public RequestRegistration(String alamat,
                               String alamatUsaha,
                               String cif,
                               String confirmPass,
                               String email,
                               Long expiredDate,
                               String ibuKandung,
                               String id,
                               String idKecamatan,
                               String idKelurahan,
                               String idKota,
                               String idProvinsi,
                               String jenisKelamin,
                               String jenisUsaha,
                               String kecamatanUsaha,
                               String kelurahanUsaha,
                               String kodeBank,
                               String kodeCabang,
                               String kotaUsaha,
                               String latitude,
                               String longitude,
                               String namaBank,
                               String namaNasabah,
                               String namaPemilik,
                               String namaUsaha,
                               String noHp,
                               String noIdentitas,
                               String norekPemilik,
                               String passsword,
                               String photoBelakang,
                               String photoDepan,
                               String photoKtp,
                               String photoSelfie,
                               String provinsiUsaha,
                               String statusKawin,
                               long tanggalLahir,
                               String tempatLahir) {
        this.alamat = alamat;
        this.alamatUsaha = alamatUsaha;
        this.cif = cif;
        this.confirmPass = confirmPass;
        this.email = email;
        this.expiredDate = expiredDate;
        this.ibuKandung = ibuKandung;
        this.id = id;
        this.idKecamatan = idKecamatan;
        this.idKelurahan = idKelurahan;
        this.idKota = idKota;
        this.idProvinsi = idProvinsi;
        this.jenisKelamin = jenisKelamin;
        this.jenisUsaha = jenisUsaha;
        this.kecamatanUsaha = kecamatanUsaha;
        this.kelurahanUsaha = kelurahanUsaha;
        this.kodeBank = kodeBank;
        this.kodeCabang = kodeCabang;
        this.kotaUsaha = kotaUsaha;
        this.latitude = latitude;
        this.longitude = longitude;
        this.namaBank = namaBank;
        this.namaNasabah = namaNasabah;
        this.namaPemilik = namaPemilik;
        this.namaUsaha = namaUsaha;
        this.noHp = noHp;
        this.noIdentitas = noIdentitas;
        this.norekPemilik = norekPemilik;
        this.passsword = passsword;
        this.photoBelakang = photoBelakang;
        this.photoDepan = photoDepan;
        this.photoKtp = photoKtp;
        this.photoSelfie = photoSelfie;
        this.provinsiUsaha = provinsiUsaha;
        this.statusKawin = statusKawin;
        this.tanggalLahir = tanggalLahir;
        this.tempatLahir = tempatLahir;
        setVersion(0);
        setOrigin(ConfigurationUtils.TAG_ANDROID);
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getAlamatUsaha() {
        return alamatUsaha;
    }

    public void setAlamatUsaha(String alamatUsaha) {
        this.alamatUsaha = alamatUsaha;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Long expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(String idProvinsi) {
        this.idProvinsi = idProvinsi;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getJenisUsaha() {
        return jenisUsaha;
    }

    public void setJenisUsaha(String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }

    public String getKecamatanUsaha() {
        return kecamatanUsaha;
    }

    public void setKecamatanUsaha(String kecamatanUsaha) {
        this.kecamatanUsaha = kecamatanUsaha;
    }

    public String getKelurahanUsaha() {
        return kelurahanUsaha;
    }

    public void setKelurahanUsaha(String kelurahanUsaha) {
        this.kelurahanUsaha = kelurahanUsaha;
    }

    public String getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(String kodeBank) {
        this.kodeBank = kodeBank;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getKotaUsaha() {
        return kotaUsaha;
    }

    public void setKotaUsaha(String kotaUsaha) {
        this.kotaUsaha = kotaUsaha;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public void setNamaBank(String namaBank) {
        this.namaBank = namaBank;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNamaUsaha() {
        return namaUsaha;
    }

    public void setNamaUsaha(String namaUsaha) {
        this.namaUsaha = namaUsaha;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getNorekPemilik() {
        return norekPemilik;
    }

    public void setNorekPemilik(String norekPemilik) {
        this.norekPemilik = norekPemilik;
    }

    public String getPasssword() {
        return passsword;
    }

    public void setPasssword(String passsword) {
        this.passsword = passsword;
    }

    public String getPhotoBelakang() {
        return photoBelakang;
    }

    public void setPhotoBelakang(String photoBelakang) {
        this.photoBelakang = photoBelakang;
    }

    public String getPhotoDepan() {
        return photoDepan;
    }

    public void setPhotoDepan(String photoDepan) {
        this.photoDepan = photoDepan;
    }

    public String getPhotoKtp() {
        return photoKtp;
    }

    public void setPhotoKtp(String photoKtp) {
        this.photoKtp = photoKtp;
    }

    public String getPhotoSelfie() {
        return photoSelfie;
    }

    public void setPhotoSelfie(String photoSelfie) {
        this.photoSelfie = photoSelfie;
    }

    public String getProvinsiUsaha() {
        return provinsiUsaha;
    }

    public void setProvinsiUsaha(String provinsiUsaha) {
        this.provinsiUsaha = provinsiUsaha;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public Long getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Long tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}