package co.pegadaian.syariah.agen.profile.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.MainActivity;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.beranda.Dashboard;
import co.pegadaian.syariah.agen.object.imageslider.DataDashboard;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PembukaanTabunganEmasActivity;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.object.userprofile.PrivilegeList;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.profile.view.adapter.ListAgenAdapter;
import co.pegadaian.syariah.agen.profile.viewmodel.AgentViewModel;
import co.pegadaian.syariah.agen.profile.viewmodel.factory.ProfileViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class ProfileActivity extends MainActivity {

    @Inject
    ProfileViewModelFactory viewModelFactory;
    AgentViewModel viewModel;
    @BindView(R.id.handler_setting)
    View handlerSetting;
    @BindView(R.id.nama_agen)
    TextView namaAgen;
    @BindView(R.id.id_agen)
    TextView idAgen;
    @BindView(R.id.email_agen)
    TextView emailAgen;
    @BindView(R.id.phone_agen)
    TextView phoneAgen;
    @BindView(R.id.button_ubah_foto_profil)
    TextView buttonUbahFotoProfil;
    @BindView(R.id.button_ubah_password)
    TextView buttonUbahPassword;
    @BindView(R.id.button_ubah_pin)
    TextView buttonUbahPin;
    @BindView(R.id.list_tipe_agen)
    RecyclerView listTipeAgen;
    @BindView(R.id.label_saldo)
    TextView labelSaldo;
    @BindView(R.id.button_faq)
    Button buttonFaq;
    @BindView(R.id.button_syarat_ketentuan)
    Button buttonSyaratKetentuan;
    @BindView(R.id.button_ketentuan_privasi)
    Button buttonKetentuanPrivasi;
    @BindView(R.id.button_kontak_pegadaian)
    Button buttonKontakPegadaian;
    @BindView(R.id.button_logout)
    Button buttonLogout;
    @BindView(R.id.layout_setting)
    LinearLayout layoutSetting;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;
    @BindView(R.id.saldo_text_view)
    TextView saldoTextView;

    private List<String> dataList = new ArrayList<>();
    private ListAgenAdapter agenTypeAdapter;
    private UserProfile userProfile;
    private DataDashboard dataDashboard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AgentViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configureFont();
        configureComponent();
        configureRecyclerAgenType();
    }

    private void configureRecyclerAgenType() {
        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);
        listTipeAgen.setLayoutManager(horizontalLayoutManager);
        agenTypeAdapter = new ListAgenAdapter();
        listTipeAgen.setAdapter(agenTypeAdapter);
    }

    private void configureComponent() {
    }

    private void configureFont() {
        namaAgen.setTypeface(getLatoBold());
        idAgen.setTypeface(getLatoRegular());
        emailAgen.setTypeface(getLatoRegular());
        phoneAgen.setTypeface(getLatoRegular());
        labelSaldo.setTypeface(getLatoBold());
        buttonFaq.setTypeface(getLatoRegular());
        buttonKetentuanPrivasi.setTypeface(getLatoRegular());
        buttonKontakPegadaian.setTypeface(getLatoRegular());
        buttonLogout.setTypeface(getLatoRegular());
        buttonSyaratKetentuan.setTypeface(getLatoRegular());
        buttonUbahFotoProfil.setTypeface(getLatoRegular());
        buttonUbahPassword.setTypeface(getLatoRegular());
        buttonUbahPin.setTypeface(getLatoRegular());
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.handleGetUserProfile();
        viewModel.getSaldoProfile();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_profile;
    }

    @Override
    protected int getItemSelected() {
        return R.id.bottom_profile;
    }

    @Override
    protected String getTitleToolbarMain() {
        return "Profile";
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE.equals(response.type)) {
                    loadUserProfile(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)) {
                    viewModel.clearData();
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCES_DATA_SALDO.equals(response.type)) {
                    loadSaldoProfile(response);
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.failed_to_load_profile));
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    private void loadUserProfile(Response response) {
        userProfile = (UserProfile) response.result;
        namaAgen.setText(userProfile.getName());
        idAgen.setText(userProfile.getId());
        emailAgen.setText(userProfile.getEmail());
        phoneAgen.setText(userProfile.getNoHp());
//        saldoTextView.setText(userProfile.getSaldoFormatted());
        for (PrivilegeList privilegeList : userProfile.getPrivilegeList())
            dataList.add(privilegeList.getDeskripsi());
        agenTypeAdapter.setDataList(dataList);
    }

    private void loadSaldoProfile(Response response) {
        dataDashboard = (DataDashboard) response.result;
        saldoTextView.setText(dataDashboard.getSaldoFormatted());
    }

    @OnClick({R.id.handler_setting, R.id.button_setting, R.id.button_ubah_foto_profil,
            R.id.button_ubah_password, R.id.button_ubah_pin, R.id.button_faq,
            R.id.button_syarat_ketentuan, R.id.button_ketentuan_privasi, R.id.button_kontak_pegadaian,
            R.id.button_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.handler_setting:
                layoutSetting.setVisibility(View.GONE);
                handlerSetting.setVisibility(View.GONE);
                break;
            case R.id.button_setting:
                showPopupMenu(view);
//                if (layoutSetting.isShown()) {
//                    layoutSetting.setVisibility(View.GONE);
//                    handlerSetting.setVisibility(View.GONE);
//                } else {
//                    layoutSetting.setVisibility(View.VISIBLE);
//                    handlerSetting.setVisibility(View.VISIBLE);
//                }
                break;
            case R.id.button_ubah_foto_profil:
                showChangeProfileActivity();
                break;
            case R.id.button_ubah_password:
                showChangePasswordOrPinActivity(ConfigurationUtils.TAG_IS_PASSWORD, Parcels.wrap(userProfile));
                break;
            case R.id.button_ubah_pin:
                showChangePasswordOrPinActivity(ConfigurationUtils.TAG_IS_PIN, Parcels.wrap(userProfile));
                break;
            case R.id.button_faq:
                showActivity(this, PembukaanTabunganEmasActivity.class);
                break;
            case R.id.button_syarat_ketentuan:
                break;
            case R.id.button_ketentuan_privasi:
                break;
            case R.id.button_kontak_pegadaian:
                break;
            case R.id.button_logout:
                viewModel.doLogoutAgent();
                break;
        }
    }

    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.pop_up_user_profile_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.change_profile:
                    showChangeProfileActivity();
                    return true;
                case R.id.change_password:
                    showChangePasswordOrPinActivity(ConfigurationUtils.TAG_IS_PASSWORD, Parcels.wrap(userProfile));
                    return true;
                case R.id.change_pin:
                    showChangePasswordOrPinActivity(ConfigurationUtils.TAG_IS_PIN, Parcels.wrap(userProfile));
                    return true;
                default:
            }
            return false;
        });
        popup.show();
    }

    private void showChangeProfileActivity() {
        layoutSetting.setVisibility(View.GONE);
        handlerSetting.setVisibility(View.GONE);
        Intent intentChangeProfile = getIntent(this, ChangeProfileActivity.class);
        intentChangeProfile.putExtra("userProfile", Parcels.wrap(userProfile));
        showActivity(intentChangeProfile);
    }

    private void showChangePasswordOrPinActivity(String tagIsPassword, Parcelable wrap) {
        layoutSetting.setVisibility(View.GONE);
        handlerSetting.setVisibility(View.GONE);
        viewModel.setPinOrPassword(tagIsPassword);
        Intent intentChangePassword = getIntent(this, UbahPasswordPinActivity.class);
        intentChangePassword.putExtra("userProfile", wrap);
        showActivity(intentChangePassword);
    }
}