package co.pegadaian.syariah.agen.login.domain.model;

import co.pegadaian.syariah.agen.restapi.RestApi;
import co.pegadaian.syariah.agen.object.authentication.Authentication;
import io.reactivex.Observable;

/**
 * Created by Dell on 2/27/2018.
 */

public class LoginRemoteRepository implements LoginRemoteModel {

    private RestApi restApi;

    public LoginRemoteRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<Object> login(Authentication authentication) {
        return restApi.login(authentication);
    }

    public Observable<Object> getUserProfile(String accessToken) {
        return restApi.getUserProfile(accessToken);
    }
}