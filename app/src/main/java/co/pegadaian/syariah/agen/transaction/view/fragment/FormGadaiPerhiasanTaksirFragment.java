package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PerhiasanActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/16/2018.
 */

public class FormGadaiPerhiasanTaksirFragment extends BaseFragment {

    @BindView(R.id.list_perhiasan)
    RecyclerView listPerhiasan;
    @BindView(R.id.jenis_perhiasan)
    AppCompatSpinner jenisPerhiasan;
    @NotEmpty
    @BindView(R.id.jumlah_perhiasan)
    EditText jumlahPerhiasan;
    @BindView(R.id.kadar_emas)
    AppCompatSpinner kadarEmas;
    @NotEmpty
    @Length(max = 4)
    @BindView(R.id.berat_kotor_perhiasan)
    EditText beratKotorPerhiasan;
    @NotEmpty
    @Length(max = 4)
    @BindView(R.id.berat_bersih_perhiasan)
    EditText beratBersihPerhiasan;
    @NotEmpty
    @BindView(R.id.keterangan_barang)
    EditText keteranganBarang;
    @BindView(R.id.gambar_perhiasan)
    ImageView gambarPerhiasan;
    @BindView(R.id.label_gambar_perhiasan)
    TextView labelGambarPerhiasan;
    @BindView(R.id.progressPicture)
    ProgressBar progressPicture;
    @BindView(R.id.button_tambah_perhiasan)
    AppCompatButton buttonTambahPerhiasan;
    @BindView(R.id.button_selanjutnya)
    AppCompatButton buttonSelanjutnya;
    @BindView(R.id.fake_line)
    View fakeLine;
    private Unbinder unbinder;
    private static PerhiasanActivity perhiasanActivity;
    private String urlImage;

    public static FormGadaiPerhiasanTaksirFragment newInstance(PerhiasanActivity activity) {
        FormGadaiPerhiasanTaksirFragment formGadaiPerhiasanTaksirFragment = new FormGadaiPerhiasanTaksirFragment();
        perhiasanActivity = activity;
        return formGadaiPerhiasanTaksirFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_data_perhiasan, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        perhiasanActivity.initFormGadaiPerhiasanTaksir(listPerhiasan,
                jenisPerhiasan,
                jumlahPerhiasan,
                kadarEmas,
                beratKotorPerhiasan,
                beratBersihPerhiasan,
                keteranganBarang,
                gambarPerhiasan,
                progressPicture,
                buttonTambahPerhiasan,
                buttonSelanjutnya,
                fakeLine);
    }

    @Override
    public void onValidationSucceeded() {
        perhiasanActivity.addItemGadai();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_tambah_perhiasan, R.id.button_selanjutnya})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_tambah_perhiasan:
                getValidator().validate();
                break;
            case R.id.button_selanjutnya:
                perhiasanActivity.nextToGadaiSimulasi();
                break;
        }
    }
}
