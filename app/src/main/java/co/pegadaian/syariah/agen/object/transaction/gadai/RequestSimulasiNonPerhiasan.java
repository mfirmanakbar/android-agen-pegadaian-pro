package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by Dell on 3/23/2018.
 */

public class RequestSimulasiNonPerhiasan {

    @SerializedName("tenor")
    @Expose
    private Integer tenor;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("taksiran")
    @Expose
    private BigDecimal taksiran;

    public RequestSimulasiNonPerhiasan(Integer tenor, String type, BigDecimal taksiran) {
        this.tenor = tenor;
        this.type = type;
        this.taksiran = taksiran;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getTaksiran() {
        return taksiran;
    }

    public void setTaksiran(BigDecimal taksiran) {
        this.taksiran = taksiran;
    }
}
