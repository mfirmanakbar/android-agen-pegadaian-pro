package co.pegadaian.syariah.agen.di.component;

import javax.inject.Singleton;

import co.pegadaian.syariah.agen.di.module.builder.AktivasiBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.BerandaBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.TransactionBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.HistoryBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.AktivasiOtpBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.CheckKTPBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.LoginBuilderModule;
import co.pegadaian.syariah.agen.di.module.CompressorModule;
import co.pegadaian.syariah.agen.di.module.builder.NotificationBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.ProfileBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.RegistrationBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.SampleMapBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.ScanQrCodeBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.SetupPinBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.TabunganBuilderModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import co.pegadaian.syariah.agen.AgentApp;
import co.pegadaian.syariah.agen.di.module.RealmModule;
import co.pegadaian.syariah.agen.di.module.builder.SampleHomeBuilderModule;
import co.pegadaian.syariah.agen.di.module.builder.SampleDetailBuilderModule;
import co.pegadaian.syariah.agen.di.module.AppModule;
import id.app.pegadaian.library.di.module.GsonModule;
import id.app.pegadaian.library.di.module.JsonParserModule;
import id.app.pegadaian.library.di.module.PreferenceModule;
import id.app.pegadaian.library.di.module.RxBusModule;

/**
 * Created by Dell on 2/27/2018.
 */

@Singleton
@Component(modules = {
        /* Use AndroidInjectionModule.class if you're not using support library */
        AndroidSupportInjectionModule.class,
        AppModule.class,
        GsonModule.class,
        SampleHomeBuilderModule.class,
        SampleDetailBuilderModule.class,
        JsonParserModule.class,
        RxBusModule.class,
        RealmModule.class,
        PreferenceModule.class,
        SampleMapBuilderModule.class,
        RegistrationBuilderModule.class,
        LoginBuilderModule.class,
        CompressorModule.class,
        AktivasiBuilderModule.class,
        BerandaBuilderModule.class,
        HistoryBuilderModule.class,
        NotificationBuilderModule.class,
        ProfileBuilderModule.class,
        AktivasiBuilderModule.class,
        AktivasiOtpBuilderModule.class,
        SetupPinBuilderModule.class,
        CheckKTPBuilderModule.class,
        TransactionBuilderModule.class,
        TabunganBuilderModule.class,
        ScanQrCodeBuilderModule.class,
})

public interface AgentComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(AgentApp application);

        AgentComponent build();
    }

    void inject(AgentApp app);
}