package co.pegadaian.syariah.agen.beranda.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.beranda.domain.interactor.BerandaUseCase;
import co.pegadaian.syariah.agen.beranda.viewmodel.BerandaViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class BerandaViewModelFactory extends BaseViewModelFactory {

    private final BerandaUseCase berandaUseCase;
    private final RealmConfigurator realmConfigurator;

    public BerandaViewModelFactory(SchedulersFacade schedulersFacade,
                                   JsonParser jsonParser,
                                   RxBus rxBus,
                                   PreferenceManager preferenceManager,
                                   BerandaUseCase berandaUseCase,
                                   RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.berandaUseCase = berandaUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(BerandaViewModel.class)) {
            return (T) new BerandaViewModel(getSchedulersFacade(),
                    getJsonParser(),
                    getRxBus(),
                    getPreferenceManager(),
                    berandaUseCase,
                    realmConfigurator);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
