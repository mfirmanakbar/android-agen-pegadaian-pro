package co.pegadaian.syariah.agen.registration.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.activity.BaseActivity;

public class RegistrationChooserActivity extends BaseActivity {

    @BindView(R.id.registration_chooser_text)
    AppCompatTextView registrationChooserText;
    @BindView(R.id.not_yet_button)
    AppCompatButton notYetButton;
    @BindView(R.id.done_button)
    AppCompatButton doneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        configureFont();
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_registration_chooser;
    }

    private void configureFont() {
        registrationChooserText.setTypeface(getLatoRegular());
        notYetButton.setTypeface(getRonniaBold());
        doneButton.setTypeface(getRonniaBold());
    }

    @OnClick({R.id.not_yet_button, R.id.done_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.not_yet_button:
                showActivity(new Intent(this, RegistrationActivity.class));
                break;
            case R.id.done_button:
                showActivity(new Intent(this, CheckUserKTPActivity.class));
                break;
        }
    }
}