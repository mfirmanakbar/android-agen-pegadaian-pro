package co.pegadaian.syariah.agen.beranda.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.SlidingSplashView;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.MainActivity;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.beranda.view.adapter.ImageSliderAdapter;
import co.pegadaian.syariah.agen.beranda.viewmodel.BerandaViewModel;
import co.pegadaian.syariah.agen.beranda.viewmodel.factory.BerandaViewModelFactory;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.object.beranda.Dashboard;
import co.pegadaian.syariah.agen.object.beranda.Menu;
import co.pegadaian.syariah.agen.object.imageslider.DataDashboard;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.Utils;
import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class BerandaActivity extends MainActivity {

    @BindView(R.id.pager_promo)
    SlidingSplashView pagerPromo;

    @Inject
    BerandaViewModelFactory viewModelFactory;
    BerandaViewModel viewModel;
    @BindView(R.id.label_saldo)
    TextView labelSaldo;
    @BindView(R.id.saldo)
    TextView saldo;
    @BindView(R.id.harga_jual_emas)
    EditText hargaJualEmas;
    @BindView(R.id.harga_beli_emas)
    EditText hargaBeliEmas;
    @BindView(R.id.list_gadai)
    RecyclerView listGadai;
    @BindView(R.id.list_emas)
    RecyclerView listEmas;
    @BindView(R.id.list_pembayaran)
    RecyclerView listPembayaran;
    @BindView(R.id.list_pemasaran)
    RecyclerView listPemasaran;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private FastItemAdapter<Menu> menuGadaiAdapter = new FastItemAdapter<>();
    private FastItemAdapter<Menu> menuEmasAdapter = new FastItemAdapter<>();
    private FastItemAdapter<Menu> menuPembayaranAdapter = new FastItemAdapter<>();
    private FastItemAdapter<Menu> menuPemasaranAdapter = new FastItemAdapter<>();
    private ViewPager.OnAdapterChangeListener pageChangeListener;
    private ImageSliderAdapter imageSliderAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BerandaViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        ButterKnife.bind(this);
        configureFont();
        configureComponent();
        configureMenuAdapter();

    }

    private void configureMenuAdapter() {
        configureMenuAdapter(new GridLayoutManager(this, 4), menuGadaiAdapter, listGadai);
        configureMenuAdapter(new GridLayoutManager(this, 4), menuEmasAdapter, listEmas);
        configureMenuAdapter(new GridLayoutManager(this, 4), menuPembayaranAdapter, listPembayaran);
        configureMenuAdapter(new GridLayoutManager(this, 4), menuPemasaranAdapter, listPemasaran);
    }

    private void configureMenuAdapter(GridLayoutManager gridLayoutManager,
                                      FastItemAdapter<Menu> menuAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(menuAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        menuAdapter.withSelectable(true);
        menuAdapter.withOnClickListener((v, adapter, item, position) -> {
            try {
                Class activityClass = Utils.getActivity("co.pegadaian.syariah.agen.transaction.view.activity.dashboard.", item.getTitle());
                showActivity(new Intent(BerandaActivity.this, activityClass));
            } catch (ClassNotFoundException e) {
                showDialogError(getString(R.string.menu_not_avalable));
                e.printStackTrace();
            }
            return true;
        });
    }

    private void configureComponent() {
        labelSaldo.setTypeface(getLatoRegular());
        saldo.setTypeface(getLatoRegular());
        hargaBeliEmas.setTypeface(getLatoRegular());
        hargaJualEmas.setTypeface(getLatoRegular());
    }

    private void configureFont() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.routingFlowAuthentication();
        viewModel.handleGetUserProfile();
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_beranda;
    }

    @Override
    protected int getItemSelected() {
        return R.id.bottom_beranda;
    }

    @Override
    protected String getTitleToolbarMain() {
        return "Beranda";
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_NOT_LOGIN.equals(response.type)) {
                    showActivityAndFinishCurrentActivity(new Intent(BerandaActivity.this,
                            LandingActivity.class));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE.equals(response.type)) {
                    loadUserProfile(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_DASHBOARD.equals(response.type)) {
                    loadDashBoard(response);
                    viewModel.handleDataDashboard();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCES_DATA_DASHBOARD.equals(response.type)) {
                    //TODO load imagelist ke adapter
                    loadDataDashboard(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)) {
                    viewModel.clearData();
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.failed_to_load_profile));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_FAILED_LOAD_DASHBOARD.equals(response.type)) {
                    showDialogError(response.type.toString());
                }
                break;
        }
    }

    private void loadDashBoard(Response response) {
        Dashboard dashboard = (Dashboard) response.result;
        menuGadaiAdapter.clear();
        menuEmasAdapter.clear();
        menuPembayaranAdapter.clear();
        menuPemasaranAdapter.clear();
        menuGadaiAdapter.add(dashboard.getRahn());
        menuEmasAdapter.add(dashboard.getEmas());
        menuPembayaranAdapter.add(dashboard.getPembayaran());
        menuPemasaranAdapter.add(dashboard.getPemasaran());
    }

    private void loadUserProfile(Response response) {
        UserProfile userProfile = (UserProfile) response.result;
        viewModel.getMenuList(userProfile);
    }

    private void loadDataDashboard(Response response) {
        DataDashboard dataDashboard = (DataDashboard) response.result;
        saldo.setText(dataDashboard.getSaldoFormatted());
        hargaJualEmas.setText(String.valueOf(dataDashboard.getHargaEmasJualFormatted()));
        hargaBeliEmas.setText(String.valueOf(dataDashboard.getHargaEmasStlFormatted()));
    }

}
