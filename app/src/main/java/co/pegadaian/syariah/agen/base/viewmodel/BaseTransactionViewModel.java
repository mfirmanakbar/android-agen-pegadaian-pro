package co.pegadaian.syariah.agen.base.viewmodel;

import android.support.v7.widget.AppCompatEditText;

import com.google.gson.reflect.TypeToken;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.usecase.BaseTransactionUseCase;
import co.pegadaian.syariah.agen.object.master.Domain;
import co.pegadaian.syariah.agen.object.master.MasterData;
import co.pegadaian.syariah.agen.object.master.Region;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSendOtpOpenGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.object.transaction.nasabah.Cif;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseTabungan;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.registration.domain.interactors.RegistrationUseCase;
import co.pegadaian.syariah.agen.transaction.domain.interactors.TransactionUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.file.DownloadHelper;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.utils.time.TimeUtils;
import io.reactivex.disposables.Disposable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Dell on 3/18/2018.
 */

public class BaseTransactionViewModel extends BaseAgentViewModel {

    private Disposable timerDisposable;
    private Subject<Integer> counterUploadImageObservable = PublishSubject.create();
    private RequestSubmitGadai requestSubmitGadai;
    private int counterUploadImage = 0;

    public BaseTransactionViewModel(SchedulersFacade schedulersFacade,
                                    JsonParser jsonParser,
                                    RxBus rxBus,
                                    PreferenceManager preferenceManager,
                                    BaseTransactionUseCase baseUseCase,
                                    RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, baseUseCase, realmConfigurator);
    }

    public void checkKtpOrCif(String ktpOrCif, String type) {
        if (((BaseTransactionUseCase) getBaseUseCase()).checkKtpOrCifLengt(ktpOrCif)) {
            getDisposables().add(checkKtpOrCifRemote(ktpOrCif, type));
        } else {
            response().postValue(Response.error("Minimum input 10 digit",
                    ConfigurationUtils.TAG_INVALID_LENGTH));
        }
    }

    private Disposable checkKtpOrCifRemote(String ktpOrCif, String type) {
        return ((BaseTransactionUseCase) getBaseUseCase()).checkKtpOrCif(ktpOrCif, type)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (response().hasActiveObservers()) {
                            if (ConfigurationUtils.TAG_OK.equals(respon.getMessage())) {
                                ResponseNasabah responseNasabah = getJsonParser().getObject(
                                        respon.getResult(), ResponseNasabah.class);
                                response().postValue(Response.success(responseNasabah,
                                        ConfigurationUtils.TAG_NASABAH_FOUND));
                            } else {
                                if (ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)) {
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                } else if (ConfigurationUtils.TAG_CHECK_CIF.equals(type)
                                        || ConfigurationUtils.TAG_CHECK_KTP.equals(type)) {
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_NASABAH_NOT_FOUND));
                                }
                            }
                        }
                    }
                }, handleError());
    }

    public void checkAmount(String nominalAwal, String type) {
        if (((BaseTransactionUseCase) getBaseUseCase()).minimumAmount(nominalAwal)) {
            getDisposables().add(mulaiSimulasi(nominalAwal, type));
        } else {
            response().postValue(Response.error("", ConfigurationUtils.TAG_NOMINAL_SALAH));
        }
    }

    private Disposable mulaiSimulasi(String nominalAwal, String type) {
        return ((BaseTransactionUseCase) getBaseUseCase()).mulaiSimulasi(nominalAwal, type)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (response().hasActiveObservers()) {
                            if (ConfigurationUtils.TAG_OK.equals(respon.getMessage())) {
                                if (ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)) {
                                    ResponseTabungan responseTabungan = getJsonParser()
                                            .getObject(respon.getResult(), ResponseTabungan.class);
                                    response().postValue(Response.success(responseTabungan,
                                            ConfigurationUtils.TAG_SIMULASI_BERHASIL));
                                } else if (ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)) {
                                    response().postValue(Response.success("",
                                            ConfigurationUtils.TAG_SALDO_SESUAI));
                                } else if (ConfigurationUtils.TAG_KONFIRMASI_PEMBUKAAN_TABUNGAN.equals(type)) {
                                    ResponseTabungan responseTabungan = getJsonParser()
                                            .getObject(respon.getResult(), ResponseTabungan.class);
                                    response().postValue(Response.success(responseTabungan,
                                            ConfigurationUtils.TAG_KONFIRMASI_BERHASIL));
                                }
                            } else {
                                if (ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)) {
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                } else if (ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)) {
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                }
                            }
                        }
                    }
                }, handleError());
    }

    public void validasiPin(String pin) {
        if (((BaseTransactionUseCase) getBaseUseCase()).checkPin(pin)) {
            getDisposables().add(validasiRemotePin(pin));
        } else {
            response().postValue(Response.error("Minimal PIN 6 Digit", ConfigurationUtils.TAG_MINIMAL_PIN_INVALID));
        }
    }

    private Disposable validasiRemotePin(String pin) {
        return ((BaseTransactionUseCase) getBaseUseCase()).validasiPin(pin)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (response().hasActiveObservers()) {
                            if (ConfigurationUtils.TAG_OK.equals(respon.getMessage())) {
                                response().postValue(Response.success("", ConfigurationUtils.TAG_PIN_VALID));
                            } else {
                                response().postValue(Response.error(respon.getResult(), ConfigurationUtils.TAG_PIN_INVALID));
                            }
                        }
                    }
                }, handleError());
    }

    public void routeToNextPage(String activity, Object value) {
        if (response().hasActiveObservers())
            response().postValue(Response.success(value, activity));
    }

    public void transactionBinding() {
        ConnectableFlowable<Object> tapEventEmitter = getRxBus().asFlowable().publish();
        getDisposables().add(tapEventEmitter
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object event) throws Exception {
                                   if (event instanceof Cif.CifEvent) {
                                       Object result = ((Cif.CifEvent) event).getResult();
                                       response().postValue(Response.success(result.toString(), ConfigurationUtils.TAG_SCAN_CIF));
                                   }
                               }
                           },
                        new ConsumerThrowableAdapter() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response().setValue(Response.error(getActivity().getResources().getString(R.string.error_scan_cif)));
                            }
                        })
        );
        getDisposables().add(tapEventEmitter.connect());
    }

    public void handleScanCif(String cif) {
        if (getRxBus().hasObservers())
            getRxBus().send(new Cif.CifEvent(cif));
    }

    public void getListOfBank() {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .getListOfBank()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<MasterData>>() {
                            }.getType();
                            List<MasterData> masterDataList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (MasterData masterData : masterDataList) {
                                regionsMap.put(masterData.getSortIdBank(), masterData.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap, ConfigurationUtils.TAG_GET_LIST_OF_BANK));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void checkBank(RequestCheckBank requestCheckBank) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .checkBank(requestCheckBank)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success("",
                                        ConfigurationUtils.TAG_SUCCESS_CHECK_BANK));
                            }
                        } else {
                            response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                    ConfigurationUtils.TAG_FAILED_CHECK_BANK));
                        }
                    }
                }, handleError()));
    }

    public void getStatusPernikahan() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getStatusPernikahan()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<MasterData>>() {
                            }.getType();
                            List<MasterData> statusNikahList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (MasterData statusNikah : statusNikahList) {
                                map.put(statusNikah.getValue(), statusNikah.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_STATUS_NIKAH));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void bindingSearchEditText(AppCompatEditText dialogSearchEditText) {
        getDisposables().add(RxTextView.textChanges(dialogSearchEditText)
                .debounce(ConfigurationUtils.SEARCH_DELAY, TimeUnit.SECONDS)
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {
                        super.accept(charSequence);
                        if (response().hasActiveObservers() && StringUtils.isNotBlank(charSequence))
                            response().postValue(Response.success(charSequence, ConfigurationUtils.TAG_SEARCH_DIALOG));
                    }
                }));
    }

    public void timer() {
        timerDisposable = ((TransactionUseCase) getBaseUseCase())
                .timer()
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        String formattedTimer;
                        if (aLong == 0) {
                            ((TransactionUseCase) getBaseUseCase()).resetOtpCountDown();
                            if (response().hasActiveObservers()) {
                                Long currentOtpCountDown = ((TransactionUseCase) getBaseUseCase())
                                        .getCurrentOtpCountDown();
                                formattedTimer = TimeUtils.getFormattedTime(currentOtpCountDown / 1000);
                                if (response().hasActiveObservers())
                                    response().postValue(Response.success(formattedTimer,
                                            ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER));
                            }
                        } else {
                            long parkingTime = (aLong * 1000);
                            formattedTimer = TimeUtils.getFormattedTime(parkingTime / 1000);
                            if (response().hasActiveObservers())
                                response().postValue(Response.success(formattedTimer,
                                        ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER));
                        }

                    }
                });
        getDisposables().add(timerDisposable);
    }

    public void sendOtp(RequestSendOtpOpenGadai requestSendOtpOpenGadai, String type) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .sendOtpOpenGadai(requestSendOtpOpenGadai)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   if (response().hasActiveObservers()) {
                                       ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                                       if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                               && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                           if (ConfigurationUtils.TAG_SEND_OTP.equals(type)) {
                                               response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_OTP_SUCCESS));
                                           } else if (ConfigurationUtils.TAG_SEND_RETRY_OTP.equals(type)) {
                                               response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS));
                                           }
                                       } else {
                                           response().postValue(Response.error(getActivity()
                                                           .getResources().getString(R.string.failed_to_send_otp),
                                                   ConfigurationUtils.TAG_SEND_OTP_FAILED));
                                       }
                                   }
                               }
                           },
                        handleError()));
    }

    public void verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .verifyOtp(requestVerifyOtp)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                response().postValue(Response.success(null, ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                        ConfigurationUtils.TAG_VERIFY_OTP_FAILED));
                            }
                        }
                    }
                }, handleError()));
    }

    public String getRessetedTimer() {
        return TimeUtils.getFormattedTime(ConfigurationUtils.TAG_OTP_COUNTDOWN / 1000);
    }

    public void resetOtpCountDown() {
        if (timerDisposable != null)
            timerDisposable.dispose();

        ((TransactionUseCase) getBaseUseCase()).resetOtpCountDown();
    }

    public RequestSubmitGadai getListItemGadai() {
        return requestSubmitGadai;
    }

    public void setRequestSubmitGadai(RequestSubmitGadai requestSubmitGadai) {
        this.requestSubmitGadai = requestSubmitGadai;
    }

    public void submitGadai() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .submitGadai(requestSubmitGadai)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   if (response().hasActiveObservers()) {
                                       ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                                       if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                               && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                           Domain domain = getJsonParser().getObject(responseNostraAPI.getResult(), Domain.class);
                                           response().postValue(Response.success(domain, ConfigurationUtils.TAG_SUCCESS_SUBMIT_GADAI));
                                       } else {
                                           response().postValue(Response.error(responseNostraAPI.getResult().toString(),
                                                   ConfigurationUtils.TAG_FAILED_SUBMIT_GADAI));
                                       }
                                   }
                               }
                           },
                        handleError()));
    }

    public void handleSubmitGadai() {
        counterUploadImage = 0;
        for (ListGadai listGadai : requestSubmitGadai.getListGadai()) {
            uploadImage(listGadai);
        }
    }

    public void uploadImage(ListGadai listGadai) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .uploadImage(listGadai.getImageFile())
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            counterUploadImage++;
                            counterUploadImageObservable.onNext(counterUploadImage);
                            listGadai.setSubmitStatus(ConfigurationUtils.TAG_UPLOADED);
                            listGadai.setFotoBarang(responseNostraAPI.getResult().toString());
                            response().setValue(Response.success(listGadai, ConfigurationUtils.TAG_SUCCESS_UPLOAD_IMAGE));
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image)));
                        }
                    }
                }));
    }

    public void handleCounterUploadImageObservable() {
        Disposable subscribe = counterUploadImageObservable
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Integer>() {
                    @Override
                    public void accept(Integer value) throws Exception {
                        super.accept(value);
                        if ((requestSubmitGadai.getListGadai().size() != 0 &&
                                value == requestSubmitGadai.getListGadai().size()) ||
                                requestSubmitGadai.getListGadai().size() == 0) {
                            submitGadai();
                        }
                    }
                });
        getDisposables().add(subscribe);
    }

    public void uploadImage(File file) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .uploadImage(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            response().setValue(Response.success(responseNostraAPI.getResult(),
                                    ConfigurationUtils.TAG_SUCCESS_UPLOAD_IMAGE));
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image)));
                        }
                    }
                }));
    }

    public void uploadImage(File file, String type) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .uploadImage(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            response().setValue(Response.success(responseNostraAPI.getResult().toString(), type));
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image)));
                        }
                    }
                }));
    }

    public void handleGetProvince(String type) {
        getDisposables().add(getProvince(type));
    }

    private Disposable getProvince(String provinceType) {
        return ((BaseTransactionUseCase) getBaseUseCase())
                .getRegion()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdProvinsi(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, provinceType));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }

                    }
                }, handleError());
    }

    public void getKota(String idProvinsi, String cityType) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .getKota(idProvinsi)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKota(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, cityType));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getKecamatan(String idKota, String tipeKecamatan) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .getKecamatan(idKota)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKecamatan(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, tipeKecamatan));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }

                    }
                }, handleError()));
    }

    public void getKelurahan(String idKecamatan, String tipeKelurahan) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .getKelurahan(idKecamatan)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKelurahan(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, tipeKelurahan));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void tambahNasabah() {
        response().postValue(Response.success("", ConfigurationUtils.TAG_TAMBAH_NASABAH));
    }

    public void downloadFile(String url, boolean isShareFile) {
        getDisposables().add(((BaseTransactionUseCase) getBaseUseCase())
                .download(url)
                .flatMap(DownloadHelper.processResponse(FilenameUtils.getName(url)))
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<File>() {
                    @Override
                    public void accept(File file) throws Exception {
                        super.accept(file);
                        if (response().hasActiveObservers()) {
                            if (isShareFile) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SHARE_DOWNLOAD_FILE));
                            } else {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_DOWNLOAD_FILE));
                            }
                        }
                    }
                }, handleError()));
    }


    public List<ListGadai> getListItemGadai(FastItemAdapter<ItemGadaiAdapter> itemGadaiAdapter) {
        return ((BaseTransactionUseCase) getBaseUseCase()).getListItemGadai(itemGadaiAdapter);
    }
}