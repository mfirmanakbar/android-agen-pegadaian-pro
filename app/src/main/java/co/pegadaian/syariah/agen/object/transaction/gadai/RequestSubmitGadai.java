package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import co.pegadaian.syariah.agen.object.master.Domain;

/**
 * Created by Dell on 3/20/2018.
 */

public class RequestSubmitGadai {

    @SerializedName("cif")
    @Expose
    private String cif;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;
    @SerializedName("jalan")
    @Expose
    private String jalan;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("kewarganegaraan")
    @Expose
    private String kewarganegaraan;
    @SerializedName("no_rek_bank")
    @Expose
    private String noRekBank;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("kode_bank")
    @Expose
    private String kodeBank;
    @SerializedName("kode_cabang")
    @Expose
    private String kodeCabang;
    @SerializedName("kode_produk")
    @Expose
    private String kodeProduk;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("nama_nasabah_bank")
    @Expose
    private String namaNasabahBank;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("reff_client")
    @Expose
    private String reffClient;
    @SerializedName("reff_switching")
    @Expose
    private String reffSwitching;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("tanggal_expired_id")
    @Expose
    private Long tanggalExpiredId;
    @SerializedName("tanggal_lahir")
    @Expose
    private Long tanggalLahir;
    @SerializedName("tanggal_pengajuan")
    @Expose
    private Long tanggalPengajuan;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("tipe_identitas")
    @Expose
    private String tipeIdentitas;
    @SerializedName("list_perhiasan")
    @Expose
    private List<ListGadai> listGadai = new ArrayList<>();
    @SerializedName("tenor")
    @Expose
    private Integer tenor;
    @SerializedName("up")
    @Expose
    private BigDecimal up;
    @SerializedName("foto_ktp")
    @Expose
    private String fotoKtp;
    @SerializedName("jaminan_kendaraan")
    @Expose
    private ListGadai gadai = new ListGadai();

    public RequestSubmitGadai(String cif,
                              String ibuKandung,
                              String idKelurahan,
                              String jalan,
                              String jenisKelamin,
                              String kewarganegaraan,
                              String noRekBank,
                              String paymentMethod,
                              String kodeBank,
                              String kodeCabang,
                              String kodeProduk,
                              String noIdentitas,
                              String namaNasabah,
                              String namaNasabahBank,
                              String noHp,
                              String reffClient,
                              String reffSwitching,
                              String statusKawin,
                              Long tanggalExpiredId,
                              Long tanggalLahir,
                              Long tanggalPengajuan,
                              String tempatLahir,
                              String tipeIdentitas,
                              List<ListGadai> listGadai,
                              Integer tenor,
                              BigDecimal up) {
        this.cif = cif;
        this.ibuKandung = ibuKandung;
        this.idKelurahan = idKelurahan;
        this.jalan = jalan;
        this.jenisKelamin = jenisKelamin;
        this.kewarganegaraan = kewarganegaraan;
        this.noRekBank = noRekBank;
        this.paymentMethod = paymentMethod;
        this.kodeBank = kodeBank;
        this.kodeCabang = kodeCabang;
        this.kodeProduk = kodeProduk;
        this.noIdentitas = noIdentitas;
        this.namaNasabah = namaNasabah;
        this.namaNasabahBank = namaNasabahBank;
        this.noHp = noHp;
        this.reffClient = reffClient;
        this.reffSwitching = reffSwitching;
        this.statusKawin = statusKawin;
        this.tanggalExpiredId = tanggalExpiredId;
        this.tanggalLahir = tanggalLahir;
        this.tanggalPengajuan = tanggalPengajuan;
        this.tempatLahir = tempatLahir;
        this.tipeIdentitas = tipeIdentitas;
        this.listGadai = listGadai;
        this.tenor = tenor;
        this.up = up;
    }

    public RequestSubmitGadai(String cif,
                              String ibuKandung,
                              String idKelurahan,
                              String jalan,
                              String jenisKelamin,
                              String kewarganegaraan,
                              String noRekBank,
                              String paymentMethod,
                              String kodeBank,
                              String kodeProduk,
                              String noIdentitas,
                              String namaNasabah,
                              String namaNasabahBank,
                              String noHp,
                              String reffClient,
                              String reffSwitching,
                              String statusKawin,
                              Long tanggalExpiredId,
                              Long tanggalLahir,
                              Long tanggalPengajuan,
                              String tempatLahir,
                              String tipeIdentitas,
                              List<ListGadai> listGadai,
                              Integer tenor,
                              BigDecimal up,
                              String fotoKtp) {
        this.cif = cif;
        this.ibuKandung = ibuKandung;
        this.idKelurahan = idKelurahan;
        this.jalan = jalan;
        this.jenisKelamin = jenisKelamin;
        this.kewarganegaraan = kewarganegaraan;
        this.noRekBank = noRekBank;
        this.paymentMethod = paymentMethod;
        this.kodeBank = kodeBank;
        this.kodeProduk = kodeProduk;
        this.noIdentitas = noIdentitas;
        this.namaNasabah = namaNasabah;
        this.namaNasabahBank = namaNasabahBank;
        this.noHp = noHp;
        this.reffClient = reffClient;
        this.reffSwitching = reffSwitching;
        this.statusKawin = statusKawin;
        this.tanggalExpiredId = tanggalExpiredId;
        this.tanggalLahir = tanggalLahir;
        this.tanggalPengajuan = tanggalPengajuan;
        this.tempatLahir = tempatLahir;
        this.tipeIdentitas = tipeIdentitas;
        this.listGadai = listGadai;
        this.tenor = tenor;
        this.up = up;
        this.fotoKtp = fotoKtp;
    }

    public RequestSubmitGadai(String cif,
                              String ibuKandung,
                              String idKelurahan,
                              String jalan,
                              String jenisKelamin,
                              String kewarganegaraan,
                              String noRekBank,
                              String paymentMethod,
                              String kodeBank,
                              String kodeProduk,
                              String noIdentitas,
                              String namaNasabah,
                              String namaNasabahBank,
                              String noHp,
                              String reffClient,
                              String reffSwitching,
                              String statusKawin,
                              Long tanggalExpiredId,
                              Long tanggalLahir,
                              Long tanggalPengajuan,
                              String tempatLahir,
                              String tipeIdentitas,
                              ListGadai gadai,
                              Integer tenor,
                              BigDecimal up,
                              String fotoKtp) {
        this.cif = cif;
        this.ibuKandung = ibuKandung;
        this.idKelurahan = idKelurahan;
        this.jalan = jalan;
        this.jenisKelamin = jenisKelamin;
        this.kewarganegaraan = kewarganegaraan;
        this.noRekBank = noRekBank;
        this.paymentMethod = paymentMethod;
        this.kodeBank = kodeBank;
        this.kodeProduk = kodeProduk;
        this.noIdentitas = noIdentitas;
        this.namaNasabah = namaNasabah;
        this.namaNasabahBank = namaNasabahBank;
        this.noHp = noHp;
        this.reffClient = reffClient;
        this.reffSwitching = reffSwitching;
        this.statusKawin = statusKawin;
        this.tanggalExpiredId = tanggalExpiredId;
        this.tanggalLahir = tanggalLahir;
        this.tanggalPengajuan = tanggalPengajuan;
        this.tempatLahir = tempatLahir;
        this.tipeIdentitas = tipeIdentitas;
        this.gadai = gadai;
        this.tenor = tenor;
        this.up = up;
        this.fotoKtp = fotoKtp;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getJalan() {
        return jalan;
    }

    public void setJalan(String jalan) {
        this.jalan = jalan;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }

    public String getNoRekBank() {
        return noRekBank;
    }

    public void setNoRekBank(String noRekBank) {
        this.noRekBank = noRekBank;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(String kodeBank) {
        this.kodeBank = kodeBank;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getKodeProduk() {
        return kodeProduk;
    }

    public void setKodeProduk(String kodeProduk) {
        this.kodeProduk = kodeProduk;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNamaNasabahBank() {
        return namaNasabahBank;
    }

    public void setNamaNasabahBank(String namaNasabahBank) {
        this.namaNasabahBank = namaNasabahBank;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getReffClient() {
        return reffClient;
    }

    public void setReffClient(String reffClient) {
        this.reffClient = reffClient;
    }

    public String getReffSwitching() {
        return reffSwitching;
    }

    public void setReffSwitching(String reffSwitching) {
        this.reffSwitching = reffSwitching;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public Long getTanggalExpiredId() {
        return tanggalExpiredId;
    }

    public void setTanggalExpiredId(Long tanggalExpiredId) {
        this.tanggalExpiredId = tanggalExpiredId;
    }

    public Long getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Long tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public Long getTanggalPengajuan() {
        return tanggalPengajuan;
    }

    public void setTanggalPengajuan(Long tanggalPengajuan) {
        this.tanggalPengajuan = tanggalPengajuan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTipeIdentitas() {
        return tipeIdentitas;
    }

    public void setTipeIdentitas(String tipeIdentitas) {
        this.tipeIdentitas = tipeIdentitas;
    }

    public List<ListGadai> getListGadai() {
        return listGadai;
    }

    public void setListGadai(List<ListGadai> listGadai) {
        this.listGadai = listGadai;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public BigDecimal getUp() {
        return up;
    }

    public void setUp(BigDecimal up) {
        this.up = up;
    }

    public String getFotoKtp() {
        return fotoKtp;
    }

    public void setFotoKtp(String fotoKtp) {
        this.fotoKtp = fotoKtp;
    }
}
