package co.pegadaian.syariah.agen.tabungan.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.TopUpTabunganEmasActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class SimulasiTopUpFragment extends BaseFragment {

    @NotEmpty
    @BindView(R.id.nomor_rekening)
    EditText nomorRekening;
    @NotEmpty
    @Min(50000)
    @Max(10000000)
    @BindView(R.id.nominal_top_up)
    EditText nominalTopUp;
    @BindView(R.id.button_selanjutnya)
    AppCompatButton buttonSelanjutnya;
    private Unbinder unbinder;
    private static TopUpTabunganEmasActivity topUpTabunganEmasActivity;

    public static SimulasiTopUpFragment newInstance(TopUpTabunganEmasActivity activity) {
        SimulasiTopUpFragment simulasiTopUpFragment = new SimulasiTopUpFragment();
        topUpTabunganEmasActivity = activity;
        return simulasiTopUpFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_emas_simulasi_top_up, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        return view;
    }

    private void configureFont() {
        buttonSelanjutnya.setTypeface(getRonniaRegular());
        nominalTopUp.setTypeface(getLatoRegular());
        nomorRekening.setTypeface(getLatoRegular());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        topUpTabunganEmasActivity.initComponentSimulasi(nomorRekening,nominalTopUp);

    }

    @Override
    public void onValidationSucceeded() {
        topUpTabunganEmasActivity.sendSimulasiTopUp(nomorRekening.getText().toString(),
                nominalTopUp.getText().toString());
    }

    @OnClick(R.id.button_selanjutnya)
    public void onViewClicked() {
        getValidator().validate();
    }

}