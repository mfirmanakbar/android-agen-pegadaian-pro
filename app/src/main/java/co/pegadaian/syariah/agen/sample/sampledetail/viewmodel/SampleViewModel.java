package co.pegadaian.syariah.agen.sample.sampledetail.viewmodel;

import android.support.annotation.NonNull;

import org.apache.commons.lang3.StringUtils;

import id.app.pegadaian.library.common.ErrorContent;
import co.pegadaian.syariah.agen.object.event.ThrowableEvent;
import co.pegadaian.syariah.agen.object.sample.Request;
import co.pegadaian.syariah.agen.object.sample.Token;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleLocalUseCase;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleRemoteUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import retrofit2.HttpException;

/**
 * Created by Dell on 2/27/2018.
 */

public class SampleViewModel extends BaseViewModel {

    private final SampleRemoteUseCase sampleRemoteUseCase;
    private final RealmConfigurator realmConfigurator;
    private final SampleLocalUseCase sampleLocalUseCase;

    public SampleViewModel(SampleRemoteUseCase sampleRemoteUseCase,
                           SampleLocalUseCase sampleLocalUseCase,
                           RealmConfigurator realmConfigurator,
                           SchedulersFacade schedulersFacade,
                           JsonParser jsonParser,
                           RxBus rxBus,
                           PreferenceManager preferenceManager) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, sampleLocalUseCase);
        this.sampleRemoteUseCase = sampleRemoteUseCase;
        this.realmConfigurator = realmConfigurator;
        this.sampleLocalUseCase = ((SampleLocalUseCase) getBaseUseCase());
    }

    @NonNull
    private Disposable getRemoteToken(Request request) {
        return sampleRemoteUseCase.getToken(request)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (SampleViewModel.this.response().hasActiveObservers()) {
                            Token token = SampleViewModel.this.getJsonParser().getObject(object, Token.class);
                            handleSetToken(token);
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        String errorMessage;
                        if (StringUtils.isNotBlank(throwable.getMessage())) {
                            ErrorContent errorContent = getJsonParser().getObject(((HttpException) throwable).response().errorBody().string(),
                                    ErrorContent.class);
                            errorMessage = errorContent.getErrorDescription();
                            if (SampleViewModel.this.getRxBus().hasObservers())
                                SampleViewModel.this.getRxBus().send(new ErrorContent.ErrorContentEvent(errorContent));
                        } else {
                            errorMessage = throwable.fillInStackTrace().toString();
                            if (SampleViewModel.this.getRxBus().hasObservers())
                                SampleViewModel.this.getRxBus().send(new ThrowableEvent(throwable));
                        }
                        SampleViewModel.this.response().setValue(Response.error(errorMessage));
                    }
                });
    }

    public void handleGetToken(Request request) {
        Disposable localToken = getLocalToken();
        getDisposables().add((localToken == null) ? getRemoteToken(request) : localToken);
    }

    private Disposable getLocalToken() {
        return (sampleLocalUseCase.getToken(realmConfigurator.getRealm()) == null) ?
                null : (sampleLocalUseCase.getToken(realmConfigurator.getRealm())
                .observeOn(getSchedulersFacade().ui())
                .doOnSubscribe(subscription -> response().postValue(Response.loading()))
                .filter(token -> token.isValid() && token.isLoaded())
                .subscribe(new ConsumerAdapter<Token>() {
                    @Override
                    public void accept(Token token) throws Exception {
                        super.accept(token);
                        handleOnSuccessToken(token);
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        handleOnErrorToken(throwable);
                    }
                }));
    }

    private void handleSetToken(Token token) {
        realmConfigurator.getRealm().executeTransaction(realm -> setToken(realm, token));
    }

    private Disposable setToken(Realm realm, Token token) {
        return sampleLocalUseCase.insertToken(realm, token)
                .observeOn(getSchedulersFacade().ui())
                .doOnSubscribe(subscription -> response().postValue(Response.loading()))
                .filter(token1 -> token1.isValid() && token1.isLoaded())
                .subscribe(new ConsumerAdapter<Token>() {
                    @Override
                    public void accept(Token token) throws Exception {
                        super.accept(token);
                        handleOnSuccessToken(token);
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        handleOnErrorToken(throwable);
                    }
                });
    }

    private void handleOnSuccessToken(Token token) {
        if (token != null && StringUtils.isNotBlank(token.getAccessToken())) {
            if (response().hasActiveObservers()) {
                response().postValue(Response.success(token.getAccessToken()));
                if (getRxBus().hasObservers())
                    getRxBus().send(new Token.TokenEventLocal(token));
            }
        }
    }

    private void handleOnErrorToken(Throwable throwable) {
        SampleViewModel.this.response().setValue(Response.error(throwable.getMessage()));
        if (SampleViewModel.this.getRxBus().hasObservers())
            SampleViewModel.this.getRxBus().send(new ThrowableEvent(throwable));
    }
}