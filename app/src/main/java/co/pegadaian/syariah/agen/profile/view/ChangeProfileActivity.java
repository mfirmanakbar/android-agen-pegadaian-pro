package co.pegadaian.syariah.agen.profile.view;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.custom.GlideRequestListener;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.profile.viewmodel.AgentViewModel;
import co.pegadaian.syariah.agen.profile.viewmodel.factory.ProfileViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.easyimage.DefaultCallback;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;

/**
 * Created by ArLoJi on 11/03/2018.
 */

public class ChangeProfileActivity extends BaseToolbarActivity {

    @Inject
    ProfileViewModelFactory viewModelFactory;
    AgentViewModel viewModel;

    @BindView(R.id.image_profil)
    ImageView imageProfil;
    @BindView(R.id.button_ubah_foto)
    Button buttonUbahFoto;
    @BindView(R.id.edit_nama)
    EditText editNama;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_nomor_hp)
    EditText editNomorHp;
    @BindView(R.id.button_simpan_perubahan)
    Button buttonSimpanPerubahan;
    @BindView(R.id.selfie_progress_bar)
    ProgressBar selfieProgressBar;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private File urlImage;
    private UserProfile userProfile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AgentViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        userProfile = Parcels.unwrap(getIntent().getParcelableExtra("userProfile"));
        configureToolbar("Ubah Profil");
        configureComponent(userProfile);
        configureFont();
    }

    private void configureComponent(UserProfile userProfile) {
        editNama.setText(userProfile.getName());
        editEmail.setText(userProfile.getEmail());
        editNomorHp.setText(userProfile.getNoHp());
        GlideApp.with(this)
                .load(userProfile.getUrlSelfie())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new GlideRequestListener(selfieProgressBar))
                .placeholder(R.drawable.ic_placeholder)
                .transform(new CircleTransform(this))
                .into(imageProfil);
    }

    private void configureFont() {
        buttonUbahFoto.setTypeface(getLatoRegular());
        editNama.setTypeface(getLatoRegular());
        editEmail.setTypeface(getLatoRegular());
        editNomorHp.setTypeface(getLatoRegular());
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart(Manifest.permission.CAMERA);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @OnClick({R.id.button_ubah_foto, R.id.button_simpan_perubahan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_ubah_foto:
                EasyImage.openCamera(ChangeProfileActivity.this, 0);
                break;
            case R.id.button_simpan_perubahan:
                viewModel.uploadImageChangeProfile(urlImage);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                viewModel.compressFile(imageFiles.get(0), type);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ChangeProfileActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_change_profile;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_COMPRESS_FILE_USER_PROFILE.equals(response.type)) {
                    urlImage = response.result != null ? ((File) response.result) : null;
                    GlideApp.with(this)
                            .load(urlImage != null ? urlImage.getAbsolutePath() : "")
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(selfieProgressBar))
                            .transform(new CircleTransform(this))
                            .into(imageProfil);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_ALL_IMAGE.equals(response.type)) {
                    userProfile.setName(editNama.getText().toString());
                    userProfile.setUrlSelfie(response.result.toString());
                    viewModel.changeProfile(userProfile);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED.equals(response.type)) {
                    showDialogSuccess(getResources().getString(R.string.success_to_change_user_profile),
                            dialog -> finishActivity());
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PERMISSION_DENIED.equals(response.type)) {
                    finishActivity();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_FAILED_CHANGE_PROFILE.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.failed_to_change_profile));
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }
}
