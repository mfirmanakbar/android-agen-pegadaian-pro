package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dell on 3/17/2018.
 */

public class RequestSimulasiPerhiasan {

    @SerializedName("list_perhiasan")
    @Expose
    private List<ItemGadai> listPerhiasan = null;
    @SerializedName("tenor")
    @Expose
    private Integer tenor;

    public RequestSimulasiPerhiasan(List<ItemGadai> listPerhiasan, Integer tenor) {
        this.listPerhiasan = listPerhiasan;
        this.tenor = tenor;
    }

    public List<ItemGadai> getListPerhiasan() {
        return listPerhiasan;
    }

    public void setListPerhiasan(List<ItemGadai> listPerhiasan) {
        this.listPerhiasan = listPerhiasan;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

}
