package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PerhiasanActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public class CariPenggunaFragment extends BaseFragment {

    @BindView(R.id.choose_ktp)
    CustomFontLatoBoldTextView chooseKtp;
    @BindView(R.id.choose_cif)
    CustomFontLatoBoldTextView chooseCif;
    @BindView(R.id.choose_qr_code)
    CustomFontLatoBoldTextView chooseQrCode;
    @BindView(R.id.image_search)
    ImageView imageSearch;
    @BindView(R.id.button_daftarkan)
    AppCompatButton buttonDaftarkan;
    @BindView(R.id.area_register_nasabah)
    RelativeLayout areaRegisterNasabah;
    @BindView(R.id.nomor_cif_nasabah)
    CustomFontLatoRegularTextView nomorCifNasabah;
    @BindView(R.id.nomor_ktp_nasabah)
    CustomFontLatoRegularTextView nomorKtpNasabah;
    @BindView(R.id.button_selanjutnya)
    AppCompatButton buttonSelanjutnya;
    @BindView(R.id.area_nasabah_ditemukan)
    RelativeLayout areaNasabahDitemukan;
    @BindView(R.id.nomor_hp)
    AppCompatEditText nomorHp;
    @BindView(R.id.search_nasabah)
    EditText searchNasabah;
    @BindView(R.id.body_nama_nasabah)
    CustomFontLatoBoldTextView bodyNamaNasabah;
    @BindView(R.id.card_info_nasabah)
    CardView cardInfoNasabah;
    @BindView(R.id.ktp_image)
    AppCompatImageView ktpImage;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBarKtpImage;
    @BindView(R.id.area_ktp_image)
    RelativeLayout areaKtpImage;
    private Unbinder unbinder;
    private static BaseTransactionActivity baseTransactionActivity;
    private String type = ConfigurationUtils.TAG_CHECK_KTP;
    private String activity;

    public static CariPenggunaFragment newInstance(BaseTransactionActivity baseTransactionActivity,
                                                   String activity) {
        CariPenggunaFragment cariPenggunaFragment = new CariPenggunaFragment();
        CariPenggunaFragment.baseTransactionActivity = baseTransactionActivity;
        Bundle args = new Bundle();
        args.putString("activity", activity);
        cariPenggunaFragment.setArguments(args);
        return cariPenggunaFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getArguments().getString("activity");
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_data_nasabah, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        baseTransactionActivity.initComponentCariPengguna(bodyNamaNasabah, imageSearch, buttonDaftarkan, areaRegisterNasabah,
                nomorCifNasabah, nomorKtpNasabah, buttonSelanjutnya, areaNasabahDitemukan,
                nomorHp, searchNasabah, ktpImage, circleProgressBarKtpImage, areaKtpImage);
    }

    @Override
    public void onValidationSucceeded() {

    }

    private void changeKtpView() {
        type = ConfigurationUtils.TAG_CHECK_KTP;
        searchNasabah.setHint(R.string.masukkan_nomor_ktp_nasabah);
        chooseCif.setTextColor(getResources().getColor(R.color.grey));
        chooseQrCode.setTextColor(getResources().getColor(R.color.grey));
        chooseKtp.setTextColor(getResources().getColor(R.color.white));
        chooseCif.setBackgroundColor(getResources().getColor(R.color.white));
        chooseQrCode.setBackgroundColor(getResources().getColor(R.color.white));
        chooseKtp.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }

    private void changeCifView() {
        type = ConfigurationUtils.TAG_CHECK_CIF;
        searchNasabah.setHint(R.string.masukkan_nomor_cif_nasabah);
        chooseKtp.setTextColor(getResources().getColor(R.color.grey));
        chooseQrCode.setTextColor(getResources().getColor(R.color.grey));
        chooseCif.setTextColor(getResources().getColor(R.color.white));
        chooseKtp.setBackgroundColor(getResources().getColor(R.color.white));
        chooseQrCode.setBackgroundColor(getResources().getColor(R.color.white));
        chooseCif.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }

    private void changeScanCifView() {
        type = ConfigurationUtils.TAG_CHECK_CIF;
        searchNasabah.setHint(R.string.masukkan_nomor_cif_nasabah);
        chooseKtp.setTextColor(getResources().getColor(R.color.grey));
        chooseQrCode.setTextColor(getResources().getColor(R.color.white));
        chooseCif.setTextColor(getResources().getColor(R.color.grey));
        chooseKtp.setBackgroundColor(getResources().getColor(R.color.white));
        chooseCif.setBackgroundColor(getResources().getColor(R.color.white));
        chooseQrCode.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }

    @OnClick({R.id.choose_ktp, R.id.choose_cif, R.id.choose_qr_code, R.id.image_search,
            R.id.button_daftarkan, R.id.button_selanjutnya, R.id.ktp_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.choose_ktp:
                dismissKeyboard();
                changeKtpView();
                break;
            case R.id.choose_cif:
                dismissKeyboard();
                changeCifView();
                break;
            case R.id.choose_qr_code:
                dismissKeyboard();
                changeScanCifView();
                baseTransactionActivity.showScanQrCode();
                break;
            case R.id.image_search:
                dismissKeyboard();
                baseTransactionActivity.checkKtpOrCif(searchNasabah.getText().toString(), type);
                break;
            case R.id.button_daftarkan:
                baseTransactionActivity.tambahNasabah();
                break;
            case R.id.button_selanjutnya:
                dismissKeyboard();
                baseTransactionActivity.goToNextPage(activity, "");
                break;
            case R.id.ktp_image:
                dismissKeyboard();
                baseTransactionActivity.addKtpImage();
                break;
        }
    }
}