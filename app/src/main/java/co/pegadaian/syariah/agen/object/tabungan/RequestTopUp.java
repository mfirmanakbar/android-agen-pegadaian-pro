package co.pegadaian.syariah.agen.object.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 18/03/2018.
 */

public class RequestTopUp {

    @SerializedName("norek")
    @Expose
    private String nomorRekening;
    @SerializedName("amount")
    @Expose
    private String nominal;

    public RequestTopUp(String nomorRekening, String nominal) {
        this.nomorRekening = nomorRekening;
        this.nominal = nominal;
    }

    public String getNomorRekening() {
        return nomorRekening;
    }

    public void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }
}
