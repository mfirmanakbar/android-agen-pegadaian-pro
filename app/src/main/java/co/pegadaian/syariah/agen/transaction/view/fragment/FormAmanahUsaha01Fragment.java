package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class FormAmanahUsaha01Fragment extends BaseFragment {

    private  static AmanahActivity amanahActivity;
    Unbinder unbinder;

    @BindView(R.id.et_nama_usaha)
    CustomFontLatoRegulerEditText etNamaUsaha;
    @BindView(R.id.spinner_bidang_usaha)
    AppCompatSpinner spinnerBidangUsaha;
    @BindView(R.id.et_lama_usaha)
    CustomFontLatoRegulerEditText etLamaUsaha;
    @BindView(R.id.spinner_jenis_tempat_usaha)
    AppCompatSpinner spinnerJenisTempatUsaha;
    @BindView(R.id.spinner_status_tempat_usaha)
    AppCompatSpinner spinnerStatusTempatUsaha;


    public static FormAmanahUsaha01Fragment newInstance(AmanahActivity activity) {
        FormAmanahUsaha01Fragment formAmanahNasabah01Fragment = new FormAmanahUsaha01Fragment();
        amanahActivity = activity;
        return formAmanahNasabah01Fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_amanah_usaha, container, false);
        unbinder = ButterKnife.bind(this, view);
        amanahActivity.initFormAmanahUsaha01(
                etNamaUsaha,
                spinnerBidangUsaha,
                etLamaUsaha,
                spinnerJenisTempatUsaha,
                spinnerStatusTempatUsaha);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        amanahActivity.initFormAmanahUsaha01(
                etNamaUsaha,
                spinnerBidangUsaha,
                etLamaUsaha,
                spinnerJenisTempatUsaha,
                spinnerStatusTempatUsaha);
    }

    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onValidationSucceeded() {
        
    }

    @OnClick(R.id.button_next)
    public void onClickButtonNext() {
        amanahActivity.nextAgunanForm();

    }
}
