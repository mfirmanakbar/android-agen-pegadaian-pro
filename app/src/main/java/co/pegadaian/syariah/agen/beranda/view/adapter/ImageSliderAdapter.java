package co.pegadaian.syariah.agen.beranda.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by prabowo on 3/19/18.
 */

public class ImageSliderAdapter extends PagerAdapter {

    Context context;

    ImageSliderAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return imageSlider.length;
    }

    private int[] imageSlider = new int[]{
    };

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView mImageView = new ImageView(context);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImageView.setImageResource(imageSlider[i]);
        ((ViewPager) container).addView(mImageView, 0);

        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }

}
