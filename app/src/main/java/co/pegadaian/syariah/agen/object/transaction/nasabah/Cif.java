package co.pegadaian.syariah.agen.object.transaction.nasabah;

/**
 * Created by Dell on 3/18/2018.
 */

public class Cif {

    private String cifCode;

    public Cif(String cifCode) {
        this.cifCode = cifCode;
    }

    public String getCifCode() {
        return cifCode;
    }

    public static class CifEvent {
        private String result;

        public CifEvent(String result) {
            this.result = result;
        }

        public String getResult() {
            return result;
        }
    }
}