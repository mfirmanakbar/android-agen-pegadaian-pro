package co.pegadaian.syariah.agen.aktivasi.view;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.AktivasiViewModel;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.factory.AktivasiViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.edittext.CustomPrefixPhoneNumberEditText;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseValidatorActivity;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class AktivasiActivity extends BaseValidatorActivity {

    @NotEmpty
    @BindView(R.id.edit_nomor_hp)
    CustomPrefixPhoneNumberEditText editNomorHp;
    @BindView(R.id.button_kirim_otp)
    Button buttonKirimOtp;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    @Inject
    AktivasiViewModelFactory viewModelFactory;
    AktivasiViewModel viewModel;
    private String accessToken_Id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        accessToken_Id = getIntent().getStringExtra("accessToken_Id");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AktivasiViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configureToolbar(getString(R.string.aktivasi_akun));
        initFont();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart(Manifest.permission.RECEIVE_SMS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_aktivasi_akun_1;
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    private void initFont() {
        editNomorHp.setTypeface(getLatoRegular());
        buttonKirimOtp.setTypeface(getLatoBold());
    }

    @OnClick(R.id.button_kirim_otp)
    public void onViewClicked() {
        getValidator().validate();
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                circleProgressBar.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_OTP_SUCCESS.equals(response.type)) {
                    Intent intent = getIntent(AktivasiActivity.this, AktivasiOtpActivity.class);
                    intent.putExtra("editNomorHp", editNomorHp.getText().toString());
                    intent.putExtra("accessToken_Id", accessToken_Id);
                    showActivityAndFinishCurrentActivity(intent);
                }
                break;

            case ERROR:
                circleProgressBar.setVisibility(View.GONE);
                showDialogError(response.result.toString());
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        viewModel.agenAktivasi(editNomorHp.getText().toString(), ConfigurationUtils.TAG_SEND_OTP);
    }
}
