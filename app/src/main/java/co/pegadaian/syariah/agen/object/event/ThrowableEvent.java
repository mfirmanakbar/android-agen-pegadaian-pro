package co.pegadaian.syariah.agen.object.event;

/**
 * Created by Dell on 3/1/2018.
 */

public class ThrowableEvent {

    private Throwable result;

    public ThrowableEvent(Throwable result) {
        this.result = result;
    }

    public Throwable getResult() {
        return result;
    }
}
