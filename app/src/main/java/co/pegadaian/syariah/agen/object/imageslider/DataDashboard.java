package co.pegadaian.syariah.agen.object.imageslider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by prabowo on 3/19/18.
 */

public class DataDashboard {

    @SerializedName("list_images")
    @Expose
    private List<String> listImages = null;
    @SerializedName("harga_emas_jual")
    @Expose
    private Integer hargaEmasJual;
    @SerializedName("harga_emas_stl")
    @Expose
    private Integer hargaEmasStl;
    @SerializedName("wallet")
    @Expose
    private Integer wallet;

    public List<String> getListImages() {
        return listImages;
    }

    public void setListImages(List<String> listImages) {
        this.listImages = listImages;
    }

    public Integer getHargaEmasJual() {
        return hargaEmasJual;
    }

    public void setHargaEmasJual(Integer hargaEmasJual) {
        this.hargaEmasJual = hargaEmasJual;
    }

    public Integer getHargaEmasStl() {
        return hargaEmasStl;
    }

    public void setHargaEmasStl(Integer hargaEmasStl) {
        this.hargaEmasStl = hargaEmasStl;
    }

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }

    public String getSaldoFormatted() {
        return StringHelper.getPriceInRp(Double.valueOf(getWallet()));
    }

    public String getHargaEmasJualFormatted() {
        return StringHelper.getPriceInRp(Double.valueOf(getHargaEmasJual()));
    }

    public String getHargaEmasStlFormatted() {
        return StringHelper.getPriceInRp(Double.valueOf(getHargaEmasStl()));
    }
}
