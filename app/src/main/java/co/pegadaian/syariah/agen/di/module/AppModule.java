package co.pegadaian.syariah.agen.di.module;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import co.pegadaian.syariah.agen.AgentApp;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Dell on 1/9/2018.
 */

@Module
public class AppModule {

    @Named("ApplicationContext")
    @Provides
    @Singleton
    Context provideContext(AgentApp application) {
        return application.getApplicationContext();
    }
}
