package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PembukaanTabunganEmasActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class SimulasiPembukaanTabunganFragment extends BaseFragment {


    @NotEmpty
    @BindView(R.id.nominal_awal_pembukaan)
    EditText nominalAwalPembukaan;
    @BindView(R.id.nominal_pembukaan)
    CustomFontLatoBoldTextView nominalPembukaan;
    @BindView(R.id.biaya_titip)
    CustomFontLatoBoldTextView biayaTitip;
    @BindView(R.id.biaya_administrasi)
    CustomFontLatoBoldTextView biayaAdministrasi;
    @BindView(R.id.biaya_transaksi)
    CustomFontLatoBoldTextView biayaTransaksi;
    @BindView(R.id.saldo_awal)
    CustomFontLatoBoldTextView saldoAwal;
    @BindView(R.id.saldo_awal_emas)
    CustomFontLatoBoldTextView saldoAwalEmas;
    @BindView(R.id.area_simulasi)
    LinearLayout areaSimulasi;
    private Unbinder unbinder;
    private static PembukaanTabunganEmasActivity pembukaanTabunganEmasActivity;

    public static SimulasiPembukaanTabunganFragment newInstance(PembukaanTabunganEmasActivity activity) {
        SimulasiPembukaanTabunganFragment registrationPersonalInfoFragment = new SimulasiPembukaanTabunganFragment();
        pembukaanTabunganEmasActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buka_tabungan_emas, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        return view;
    }

    private void configureFont() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        pembukaanTabunganEmasActivity.initComponentSimulasi(nominalPembukaan, biayaTitip, biayaAdministrasi,
                biayaTransaksi, saldoAwal, saldoAwalEmas, areaSimulasi);
    }

    @Override
    public void onValidationSucceeded() {
        pembukaanTabunganEmasActivity.initTabungan(nominalAwalPembukaan.getText().toString(),
                ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN);
    }

    @OnClick({R.id.button_simulasikan, R.id.button_lanjutkan_pendaftaran})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_simulasikan:
                getValidator().validate();
                break;
            case R.id.button_lanjutkan_pendaftaran:
                pembukaanTabunganEmasActivity.initTabungan(nominalAwalPembukaan.getText().toString(),
                        ConfigurationUtils.TAG_CEK_SALDO_AGEN);
                break;
        }
    }
}