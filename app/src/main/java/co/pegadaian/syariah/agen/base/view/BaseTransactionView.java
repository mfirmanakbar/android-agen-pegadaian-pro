package co.pegadaian.syariah.agen.base.view;

import android.app.FragmentManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import co.pegadaian.syariah.agen.base.viewmodel.BaseTransactionViewModel;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;

/**
 * Created by Dell on 3/18/2018.
 */

public interface BaseTransactionView {

    void configViewModel(BaseTransactionViewModel baseTransactionViewModel);
    void initComponentCariPengguna(CustomFontLatoBoldTextView bodyNamaNasabah,
                                   ImageView imageSearch,
                                   AppCompatButton buttonDaftarkan,
                                   RelativeLayout areaRegisterNasabah,
                                   CustomFontLatoRegularTextView nomorCifNasabah,
                                   CustomFontLatoRegularTextView nomorKtpNasabah,
                                   AppCompatButton buttonSelanjutnya,
                                   RelativeLayout areaNasabahDitemukan,
                                   AppCompatEditText nomorHp, EditText searchNasabah, AppCompatImageView ktpImage,
                                   AVLoadingIndicatorView circleProgressBarKtpImage, RelativeLayout areaKtpImage);
    void checkKtpOrCif(String ktpOrCif, String type);
    void sendKonfirmasiTabungan(String nominalPembukaan, String type);
    void addKtpImage();
    void goToNextPage(String activity, Object value);
    void showScanQrCode();
    void initComponentRekeningTujuan(AppCompatEditText namaBankEditText, AppCompatEditText namaPemilikEditText,
                                     AppCompatEditText nomorRekeningEditText, FragmentManager fragmentManager);
    void showListOfBankDialog();
    void initComponentKonfirmasi(RecyclerView dataNasabah,
                                 RecyclerView dataTransaksi, CustomFontLatoBoldTextView namaBank, CustomFontLatoBoldTextView nomorRekening, CustomFontLatoRegularTextView namaNasabah);
    void nextToOtp();
    void initOtpCountDownFragment(AppCompatTextView textView, LinearLayout retryArea, AppCompatButton retryButton, AppCompatEditText otpEditText);
    void nextToSubmitGadai();
    void initComponentSubmitGadai(FastItemAdapter<ListGadai> listPerhiasanFastItemAdapter);
    void showProvinceDialog();
    void showCityDialog();
    void showKecamatanDialog();
    void showKelurahanDialog();
    void printStruk(AppCompatImageView strukImageView);
    void shareFile();
    void initComponentCetakBuktiTransaksi(AppCompatImageView buktiTransaksi);
    void showDialogPin();
}
