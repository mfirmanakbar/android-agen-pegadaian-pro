package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;


public class FormAmanahDataNasabahFragment extends BaseFragment {

    private static AmanahActivity amanahActivity;

    public static FormAmanahAgunanKendaraanFragment newInstance(AmanahActivity activity) {
        FormAmanahAgunanKendaraanFragment formAmanahAgunanKendaraanFragment = new FormAmanahAgunanKendaraanFragment();
        amanahActivity = activity;
        return formAmanahAgunanKendaraanFragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_amanah_data_nasabah, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onValidationSucceeded() {

    }
}
