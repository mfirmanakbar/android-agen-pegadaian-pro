package co.pegadaian.syariah.agen.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import id.app.pegadaian.library.view.activity.BaseActivity;

/**
 * Created by prabowo on 3/22/18.
 */

public class SplashScreenActivity extends BaseActivity {

    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        postDelayed(() -> {
            showActivityAndFinishCurrentActivity(SplashScreenActivity.this, BerandaActivity.class);
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        },2000);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_splashscreen;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
