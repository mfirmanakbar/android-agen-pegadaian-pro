package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public class TambahNasabahFragment extends BaseFragment {

    @BindView(R.id.button_data_satu)
    AppCompatButton buttonDataSatu;
    @NotEmpty
    @BindView(R.id.nama_nasabah)
    EditText namaNasabah;
    @NotEmpty
    @BindView(R.id.nomor_hp)
    EditText nomorHp;
    @NotEmpty
    @BindView(R.id.nomor_ktp)
    EditText nomorKtp;
    @BindView(R.id.foto_ktp)
    ImageView fotoKtp;
    @BindView(R.id.label_foto_ktp)
    TextView labelFotoKtp;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView progressGambarKtp;
    @BindView(R.id.text_pria)
    TextView textPria;
    @BindView(R.id.text_wanita)
    TextView textWanita;
    @NotEmpty
    @BindView(R.id.tempat_lahir)
    EditText tempatLahir;
    @BindView(R.id.status_perkawinan)
    Spinner statusPerkawinan;
    @NotEmpty
    @BindView(R.id.nama_ibu_kandung)
    EditText namaIbuKandung;
    @BindView(R.id.form_tambah_nasabah_page_pertama)
    LinearLayout formTambahNasabahPagePertama;
    @BindView(R.id.button_data_dua)
    AppCompatButton buttonDataDua;
    @NotEmpty
    @BindView(R.id.alamat_nasabah)
    EditText alamatNasabah;
    @NotEmpty
    @BindView(R.id.provinsi)
    EditText provinsi;
    @NotEmpty
    @BindView(R.id.kabupaten)
    EditText kabupaten;
    @NotEmpty
    @BindView(R.id.kecamatan)
    EditText kecamatan;
    @NotEmpty
    @BindView(R.id.kelurahan)
    EditText kelurahan;
    @BindView(R.id.form_tambah_nasabah_page_kedua)
    LinearLayout formTambahNasabahPageKedua;
    @BindView(R.id.tanggal_lahir)
    TextView tanggalLahir;

    private String gender = ConfigurationUtils.TAG_LAKI_LAKI;
    private Unbinder unbinder;
    private static BaseTransactionActivity baseTransactionActivity;
    private String activity;
    private boolean isDoneFirst = false;
    private boolean isDoneSecond = false;

    public static TambahNasabahFragment newInstance(BaseTransactionActivity baseActivity,
                                                    String activity) {
        TambahNasabahFragment tambahNasabahFragment = new TambahNasabahFragment();
        baseTransactionActivity = baseActivity;
        Bundle args = new Bundle();
        args.putString("activity", activity);
        tambahNasabahFragment.setArguments(args);
        return tambahNasabahFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getArguments().getString("activity");
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tambah_nasabah, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        handleOnTouchListener();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        baseTransactionActivity.initComponentTambahNasabah(
                fotoKtp, labelFotoKtp, progressGambarKtp, tanggalLahir, statusPerkawinan, provinsi, kabupaten,
                kecamatan, kelurahan, getActivity().getFragmentManager());
    }

    private void handleOnTouchListener() {
        provinsi.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                baseTransactionActivity.showProvinceDialog();
                return true;
            }
            return false;
        });
        kabupaten.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                baseTransactionActivity.showCityDialog();
                return true;
            }
            return false;
        });
        kecamatan.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                baseTransactionActivity.showKecamatanDialog();
                return true;
            }
            return false;
        });
        kelurahan.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                baseTransactionActivity.showKelurahanDialog();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onValidationSucceeded() {
        if (isDoneSecond) {
            dismissKeyboard();
            baseTransactionActivity.createNasabah(nomorHp.getText().toString(),
                    namaIbuKandung.getText().toString(),
                    gender, namaNasabah.getText().toString(),
                    nomorKtp.getText().toString(), alamatNasabah.getText().toString(),
                    tempatLahir.getText().toString());
            baseTransactionActivity.goToNextPage(activity, "");
        } else if (isDoneFirst) {
            showHideForm(formTambahNasabahPageKedua, formTambahNasabahPagePertama);
        }
    }


    private void setSelectedGender(TextView gender, TextView genderOther, String labelGender) {
        gender.setBackground(getResources().getDrawable(R.drawable.button_green));
        gender.setTextColor(getResources().getColor(R.color.white));
        this.gender = labelGender;
        genderOther.setBackground(getResources().getDrawable(R.drawable.border_green));
        genderOther.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void showHideForm(LinearLayout open, LinearLayout close) {
        open.setVisibility(View.VISIBLE);
        close.setVisibility(View.GONE);
    }

    @OnClick({R.id.button_data_satu, R.id.foto_ktp, R.id.text_pria, R.id.text_wanita, R.id.tanggal_lahir,
            R.id.button_selanjutnya, R.id.button_tambahkan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_data_satu:
                isDoneFirst = false;
                showHideForm(formTambahNasabahPagePertama, formTambahNasabahPageKedua);
                break;
            case R.id.foto_ktp:
                dismissKeyboard();
                baseTransactionActivity.addKtpImage();
                break;
            case R.id.text_pria:
                setSelectedGender(textPria, textWanita, ConfigurationUtils.TAG_LAKI_LAKI);
                break;
            case R.id.text_wanita:
                setSelectedGender(textWanita, textPria, ConfigurationUtils.TAG_LAKI_LAKI);
                break;
            case R.id.tanggal_lahir:
                baseTransactionActivity.getDate();
                break;
            case R.id.button_selanjutnya:
                isDoneFirst = true;
                getValidator().validate();
                break;
            case R.id.button_tambahkan:
                isDoneSecond = true;
                getValidator().validate();
                break;
        }
    }
}
