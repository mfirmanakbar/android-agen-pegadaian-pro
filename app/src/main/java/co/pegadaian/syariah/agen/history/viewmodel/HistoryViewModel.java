package co.pegadaian.syariah.agen.history.viewmodel;

import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import co.pegadaian.syariah.agen.history.domain.interactor.HistoryUseCase;
import co.pegadaian.syariah.agen.object.history.HistoryEmas;
import co.pegadaian.syariah.agen.object.history.HistoryGadai;
import co.pegadaian.syariah.agen.object.history.HistoryPemasaran;
import co.pegadaian.syariah.agen.object.history.HistoryPembayaran;
import co.pegadaian.syariah.agen.object.transaction.gadai.JenisPerhiasan;
import co.pegadaian.syariah.agen.transaction.domain.interactors.TransactionUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class HistoryViewModel extends BaseViewModel {

    private final HistoryUseCase useCase;

    public HistoryViewModel(SchedulersFacade schedulersFacade,
                            JsonParser jsonParser,
                            RxBus rxBus,
                            PreferenceManager preferenceManager,
                            HistoryUseCase useCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, useCase);
        this.useCase = useCase;
    }

    public void getLastHistoryGadai() {
        getDisposables().add(((HistoryUseCase) getBaseUseCase())
                .getLastHistoryGadai()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<HistoryGadai>>() {
                            }.getType();
                            List<HistoryGadai> historyGadaiList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(historyGadaiList, ConfigurationUtils.TAG_LOAD_LIST_GADAI_TERAKHIR));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getLastHistoryEmas() {
        getDisposables().add(((HistoryUseCase) getBaseUseCase())
                .getLastHistoryEmas()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<HistoryEmas>>() {
                            }.getType();
                            List<HistoryEmas> historyEmasList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(historyEmasList, ConfigurationUtils.TAG_LOAD_LIST_EMAS_TERAKHIR));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getLastHistoryPembayaran() {
        getDisposables().add(((HistoryUseCase) getBaseUseCase())
                .getLastHistorypembayaran()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<HistoryPembayaran>>() {
                            }.getType();
                            List<HistoryPembayaran> historyPembayaranList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(historyPembayaranList, ConfigurationUtils.TAG_LOAD_LIST_PEMBAYARAN_TERAKHIR));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getLastHistoryPemasaran() {
        getDisposables().add(((HistoryUseCase) getBaseUseCase())
                .getLastHistoryPemasaran()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<HistoryPemasaran>>() {
                            }.getType();
                            List<HistoryPemasaran> historyPemasaranList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(historyPemasaranList, ConfigurationUtils.TAG_LOAD_LIST_PEMASARAN_TERAKHIR));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

}
