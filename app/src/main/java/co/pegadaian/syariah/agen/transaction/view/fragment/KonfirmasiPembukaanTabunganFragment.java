package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PembukaanTabunganEmasActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class KonfirmasiPembukaanTabunganFragment extends BaseFragment {

    @BindView(R.id.nama_nasabah)
    CustomFontLatoBoldTextView namaNasabah;
    @BindView(R.id.nomor_ktp)
    CustomFontLatoBoldTextView nomorKtp;
    @BindView(R.id.nominal_pembukaan)
    CustomFontLatoBoldTextView nominalPembukaan;
    @BindView(R.id.biaya_titip)
    CustomFontLatoBoldTextView biayaTitip;
    @BindView(R.id.biaya_administrasi)
    CustomFontLatoBoldTextView biayaAdministrasi;
    @BindView(R.id.biaya_transaksi)
    CustomFontLatoBoldTextView biayaTransaksi;
    @BindView(R.id.saldo_awal)
    CustomFontLatoBoldTextView saldoAwal;
    @BindView(R.id.saldo_awal_emas)
    CustomFontLatoBoldTextView saldoAwalEmas;
    private Unbinder unbinder;
    private static PembukaanTabunganEmasActivity pembukaanTabunganEmasActivity;

    public static KonfirmasiPembukaanTabunganFragment newInstance(PembukaanTabunganEmasActivity activity) {
        KonfirmasiPembukaanTabunganFragment registrationPersonalInfoFragment = new KonfirmasiPembukaanTabunganFragment();
        pembukaanTabunganEmasActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_konfirmasi_transaksi, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        pembukaanTabunganEmasActivity.initComponentKonfirmasiPembukaan(
                namaNasabah, nomorKtp, nominalPembukaan,
                biayaTitip, biayaAdministrasi, biayaTransaksi, saldoAwal, saldoAwalEmas
        );
    }

    @OnClick(R.id.button_lanjutkan_pendaftaran)
    public void onViewClicked() {
        pembukaanTabunganEmasActivity.initDialogPin();
    }

    @Override
    public void onValidationSucceeded() {

    }
}