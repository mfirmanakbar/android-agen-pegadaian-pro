package co.pegadaian.syariah.agen.profile.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.profile.viewmodel.AgentViewModel;
import co.pegadaian.syariah.agen.profile.viewmodel.factory.ProfileViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;

/**
 * Created by ArLoJi on 27/02/2018.
 */

public class UbahPasswordPinActivity extends BaseToolbarActivity {

    @Inject
    ProfileViewModelFactory viewModelFactory;
    AgentViewModel viewModel;

    @BindView(R.id.pengantar_form)
    TextView pengantarForm;
    @BindView(R.id.edit_pass_pin_lama)
    TextInputEditText editPassPinLama;
    @BindView(R.id.edit_pass_pin_baru)
    TextInputEditText editPassPinBaru;
    @BindView(R.id.button_simpan)
    Button buttonSimpan;
    @BindView(R.id.confirm_pass_pin_baru)
    TextInputEditText confirmPassPinBaru;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private String typeForm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AgentViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configureFont();
    }

    private void configureFont() {
        pengantarForm.setTypeface(getLatoRegular());
        editPassPinLama.setTypeface(getLatoRegular());
        editPassPinBaru.setTypeface(getLatoRegular());
        confirmPassPinBaru.setTypeface(getLatoRegular());
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getPinOrPassword();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_ubah_password_pin;
    }

    private void initPinComponent() {
        configureToolbar("Ubah PIN");
        pengantarForm.setText(R.string.masukkan_pin_lama_dan_pin_baru);
        editPassPinLama.setHint(R.string.pin_lama);
        editPassPinBaru.setHint(R.string.pin_baru);
        confirmPassPinBaru.setHint(R.string.konfirmasi_pin_ubah);
        editPassPinLama.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        editPassPinBaru.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        confirmPassPinBaru.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        editPassPinLama.setInputType(InputType.TYPE_CLASS_NUMBER);
        editPassPinBaru.setInputType(InputType.TYPE_CLASS_NUMBER);
        confirmPassPinBaru.setInputType(InputType.TYPE_CLASS_NUMBER);
        editPassPinLama.setTransformationMethod(PasswordTransformationMethod.getInstance());
        editPassPinBaru.setTransformationMethod(PasswordTransformationMethod.getInstance());
        confirmPassPinBaru.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    private void initPasswordComponent() {
        configureToolbar("Ubah Password");
        pengantarForm.setText(R.string.masukkan_password_lama_dan_password_baru_anda);
        editPassPinLama.setHint(R.string.password_lama);
        editPassPinBaru.setHint(R.string.password_baru);
        confirmPassPinBaru.setHint(R.string.konfirmasi_password_ubah);
    }

    @OnClick(R.id.button_simpan)
    public void onViewClicked() {
        viewModel.changePinOrPassword(editPassPinLama.getText().toString(),
                editPassPinBaru.getText().toString(),
                confirmPassPinBaru.getText().toString());
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_IS_PASSWORD.equals(response.type)) {
                    initPasswordComponent();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_IS_PIN.equals(response.type)) {
                    initPinComponent();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHANGE_PIN_PASSWORD_SUCCESS.equals(response.type)) {
                    showDialogSuccess(getResources().getString(R.string.success_to_change_pin_password),
                            dialog -> finishActivity());
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHANGE_PIN_PASSWORD_FAILED.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.failed_to_change_pin));
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }
}
