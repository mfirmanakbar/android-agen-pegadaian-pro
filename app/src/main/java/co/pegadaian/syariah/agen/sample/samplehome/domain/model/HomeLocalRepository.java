package co.pegadaian.syariah.agen.sample.samplehome.domain.model;

import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/3/2018.
 */

public class HomeLocalRepository extends BaseRepository implements HomeLocalModel {

    public HomeLocalRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }

    @Override
    public Integer getValue() {
        return getPreferenceManager().getSampleValue();
    }

    @Override
    public boolean setValue(Integer value) {
        getPreferenceManager().setSampleValue(value);
        return true;
    }
}