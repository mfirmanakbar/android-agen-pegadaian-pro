package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.BayarRahnActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PembukaanTabunganEmasActivity;
import co.pegadaian.syariah.agen.di.module.TabunganModule;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.TopUpTabunganEmasActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public abstract class TabunganBuilderModule {
    @ContributesAndroidInjector(modules = {TabunganModule.class})
    abstract TopUpTabunganEmasActivity topUpActivity();
    @ContributesAndroidInjector(modules = {TabunganModule.class})
    abstract BayarRahnActivity bayarRahnActivity();
}
