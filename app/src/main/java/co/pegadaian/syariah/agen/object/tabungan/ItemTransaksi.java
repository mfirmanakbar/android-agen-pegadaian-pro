package co.pegadaian.syariah.agen.object.tabungan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;

/**
 * Created by Dell on 3/13/2018.
 */

public class ItemTransaksi extends AbstractItem<ItemTransaksi, ItemTransaksi.ViewHolder> {

    private String labelItem;
    private String valueItem;

    public ItemTransaksi(String labelItem, String valueItem) {
        this.labelItem = labelItem;
        this.valueItem = valueItem;
    }

    private String getLabelItem() {
        return labelItem;
    }

    private String getValueItem() {
        return valueItem;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_item_transaksi;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.label_item_transaksi)
        CustomFontLatoRegularTextView labelItemTransaksi;
        @BindView(R.id.value_transaksi)
        CustomFontLatoBoldTextView valueTransaksi;

        Context context;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(@NonNull ViewHolder holder, @NonNull List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.labelItemTransaksi.setText(getLabelItem());
        holder.valueTransaksi.setText(getValueItem());
    }

    @Override
    public void unbindView(@NonNull ViewHolder holder) {
        super.unbindView(holder);
        holder.labelItemTransaksi.setText(null);
        holder.valueTransaksi.setText(null);
    }
}