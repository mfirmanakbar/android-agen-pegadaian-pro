package co.pegadaian.syariah.agen.registration.view;

import android.app.FragmentManager;
import android.location.Location;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by Dell on 3/6/2018.
 */

public interface RegistrationListener {
    void onNext();
    void onFrontImageClicked();
    void onBackImageClicked();
    void onKtpImageClicked();
    void onSelfieImageClicked();
    void initKtpImagePersonalInfo(AppCompatImageView ktpImage, ProgressBar progressBar);
    void initSelfieImagePersonalInfo(AppCompatImageView selfieImage, ProgressBar progressBar);
    void initFrontImageBadanUsaha(AppCompatImageView frontImage, ProgressBar progressBar);
    void initBackImageBadanUsaha(AppCompatImageView backImage, ProgressBar progressBar);
    void initOtpCountDownFragment(AppCompatTextView textView, LinearLayout retryArea, AppCompatButton retryButton, AppCompatEditText otpEditText);
    void initMapsFragment(AppCompatEditText outletSearchEditText);
    void initLocation(Location location);
    void initMap(GoogleMap map);
    void showProvinceDialog();
    void showCityDialog();
    void showKecamatanDialog();
    void showKelurahanDialog();
    void showProvinceDataUsahaDialog();
    void showCityDataUsahaDialog();
    void showKecamatanDataUsahaDialog();
    void showKelurahanDataUsahaDialog();
    void initInputanPersonalInfo(AppCompatEditText provinceEditText, AppCompatEditText cityEditText,
                                 AppCompatEditText kecamatanEditText, AppCompatEditText kelurahanEditText,
                                 AppCompatEditText hpEditText, AppCompatEditText addressEditText,
                                 AppCompatEditText emailEditText, AppCompatEditText nameEditText, AppCompatEditText ktpEditText, FragmentManager fragmentManager);
    void showBadanUsahaDialog();
    void initInputanDataUsaha(AppCompatEditText jenisUsahaEditText, AppCompatEditText provinceEditText,
                              AppCompatEditText cityEditText, AppCompatEditText kecamatanEditText,
                              AppCompatEditText kelurahanEditText, AppCompatEditText alamatUsahaEditText,
                              AppCompatEditText namaUsahaEditText, FragmentManager fragmentManager);
    void initInputanBankAccount(AppCompatEditText namaBankEditText, AppCompatEditText namaPemilikEditText, AppCompatEditText nomorRekeningEditText, FragmentManager fragmentManager);
    void showListOfBankDialog();
    void initInputanPassword(TextInputEditText confirmPasswordEditText, TextInputEditText passwordEditText, AppCompatButton finishButton);
}