package co.pegadaian.syariah.agen.object.userprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/14/2018.
 */

public class RequestChangeProfile {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("photo_selfie")
    @Expose
    private String photoSelfie;

    public RequestChangeProfile(String name, String photoSelfie) {
        this.name = name;
        this.photoSelfie = photoSelfie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoSelfie() {
        return photoSelfie;
    }

    public void setPhotoSelfie(String photoSelfie) {
        this.photoSelfie = photoSelfie;
    }
}
