package co.pegadaian.syariah.agen.history.domain.interactor;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.history.domain.model.HistoryRepository;
import co.pegadaian.syariah.agen.transaction.domain.model.TransactionRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.domain.model.BaseRepository;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class HistoryUseCase extends BaseUseCase {

    @Inject
    public HistoryUseCase(HistoryRepository repository,
                          Compressor compressor) {
        super(repository, compressor);
    }

    public Observable<Object> getLastHistoryGadai() {
        return ((HistoryRepository) getRepository()).getLastHistoryGadai();
    }


    public Observable<Object> getLastHistoryEmas() {
        return ((HistoryRepository) getRepository()).getLastHistoryEmas();
    }

    public Observable<Object> getLastHistoryPemasaran() {
        return ((HistoryRepository) getRepository()).getLastHistoryPemasaran();
    }

    public Observable<Object> getLastHistorypembayaran() {
        return ((HistoryRepository) getRepository()).getLastHistorypembayaran();
    }

}
