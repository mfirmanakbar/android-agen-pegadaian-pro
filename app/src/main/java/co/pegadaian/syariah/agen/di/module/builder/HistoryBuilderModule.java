package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.HistoryModule;
import co.pegadaian.syariah.agen.history.view.HistoryActivity;
import co.pegadaian.syariah.agen.history.view.HistoryDetailActivity;
import co.pegadaian.syariah.agen.history.view.HistoryDetailEmasActivity;
import co.pegadaian.syariah.agen.history.view.HistoryDetailPemasaranActivity;
import co.pegadaian.syariah.agen.history.view.HistoryDetailPembayaranActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public abstract class HistoryBuilderModule {
    @ContributesAndroidInjector(modules = {HistoryModule.class})
    abstract HistoryActivity bindHistoryActivity();

    @ContributesAndroidInjector(modules = {HistoryModule.class})
    abstract HistoryDetailActivity biHistoryDetailActivity();

    @ContributesAndroidInjector(modules = {HistoryModule.class})
    abstract HistoryDetailEmasActivity biHistoryDetailEmasActivity();

    @ContributesAndroidInjector(modules = {HistoryModule.class})
    abstract HistoryDetailPembayaranActivity biHistoryDetailPembayaranActivity();

    @ContributesAndroidInjector(modules = {HistoryModule.class})
    abstract HistoryDetailPemasaranActivity biHistoryDetailPemasaranActivity();
}
