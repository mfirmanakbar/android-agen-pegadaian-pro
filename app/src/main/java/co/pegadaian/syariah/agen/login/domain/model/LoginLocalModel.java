package co.pegadaian.syariah.agen.login.domain.model;

import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import io.realm.Realm;

/**
 * Created by Dell on 3/3/2018.
 */

public interface LoginLocalModel {
    String getToken();
    UserProfile setUserProfile(Realm realm,UserProfile agen);
}
