package co.pegadaian.syariah.agen.login.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.aktivasi.view.AktivasiActivity;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.login.viewmodel.LoginViewModel;
import co.pegadaian.syariah.agen.login.viewmodel.factory.LoginViewModelFactory;
import co.pegadaian.syariah.agen.registration.view.CheckUserKTPActivity;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;

/**
 * Created by Dell on 2/28/2018.
 */

public class LoginActivity extends BaseToolbarActivity {

    @Inject
    LoginViewModelFactory viewModelFactory;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.button_masuk)
    Button buttonMasuk;
    @BindView(R.id.button_daftar)
    TextView buttonDaftar;
    @BindView(R.id.lupa_password)
    TextView lupaPassword;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private LoginViewModel loginViewModel;
    //private Authentication authentication;
    private String email;
    private String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        configureToolbar(getResources().getString(R.string.masuk));
        initFont();
        loginViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel.class);
        loginViewModel.setActivity(this);
        loginViewModel.response().observe(this, this::processResponse);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_login;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;

            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED.equals(response.type)) {
                    showActivityAndFinishCurrentActivity(this, BerandaActivity.class);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_STATUS_AGEN_APPROVED.equals(response.type)) {
                    Intent intent = getIntent(LoginActivity.this, AktivasiActivity.class);
                    intent.putExtra("accessToken_Id", response.result.toString());
                    showActivityAndFinishCurrentActivity(intent);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_STATUS_AGEN_PENDING.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.error_activation_registration_info));
                } else if (StringUtils.isNotBlank(response.type)
                        && (ConfigurationUtils.TAG_STATUS_AGEN_VERIFIED.equals(response.type) ||
                        ConfigurationUtils.TAG_STATUS_AGEN_SURVEYED.equals(response.type))) {
                    showDialogError(getResources().getString(R.string.still_processing_registration_info));
                }
                break;

            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_NO_USERNAME_OR_PASSWORD.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.email_password_not_valid));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED.equals(response.type)) {
                    showDialogError(getResources().getString(R.string.failed_to_load_profile));
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    private void initFont() {
        toolbarTitle.setTypeface(getRonniaBold());
        lupaPassword.setTypeface(getLatoRegular());
        lupaPassword.setVisibility(View.GONE);
        buttonDaftar.setTypeface(getLatoBold());
        buttonMasuk.setTypeface(getLatoBold());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginViewModel.unBinding();

    }

    @OnClick({R.id.button_masuk, R.id.button_daftar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_masuk:
                dismissKeyboard();
                email = editEmail.getText().toString();
                password = editPassword.getText().toString();
                //authentication = new Authentication(email, password);
                loginViewModel.handleLoginAgent(email, password);
                break;
            case R.id.button_daftar:
                dismissKeyboard();
                showActivity(this, CheckUserKTPActivity.class);
                break;
        }
    }
}
