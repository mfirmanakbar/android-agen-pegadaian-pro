package co.pegadaian.syariah.agen.object.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by ArLoJi on 18/03/2018.
 */

public class SubmitBeTopUp {
    @SerializedName("norek")
    @Expose
    private String norek;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("status_of_transaction")
    @Expose
    private Boolean statusOfTransaction;
    @SerializedName("version")
    @Expose
    private Integer version;

    public SubmitBeTopUp(String norek, BigDecimal amount, Boolean statusOfTransaction, Integer version) {
        this.norek = norek;
        this.amount = amount;
        this.statusOfTransaction = statusOfTransaction;
        this.version = version;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getStatusOfTransaction() {
        return statusOfTransaction;
    }

    public void setStatusOfTransaction(Boolean statusOfTransaction) {
        this.statusOfTransaction = statusOfTransaction;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
