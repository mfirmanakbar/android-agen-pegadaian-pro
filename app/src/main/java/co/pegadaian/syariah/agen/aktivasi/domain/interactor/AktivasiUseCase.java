package co.pegadaian.syariah.agen.aktivasi.domain.interactor;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.aktivasi.domain.model.AktivasiRepository;
import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.object.aktivasi.Aktivasi;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 07/03/2018.
 */

public class AktivasiUseCase extends BaseAgentUseCase {

    @Inject
    public AktivasiUseCase(AktivasiRepository repository,
                           Compressor compressor) {
        super(repository, compressor);
    }

    public Observable<Long> timer() {
        long count = getRepository().getPreferenceManager().getCurrentOtpCountdown() + 1;
        return Observable.interval(1, TimeUnit.SECONDS)
                .take(count)
                .map(aLong -> {
                    long time = count - aLong;
                    getRepository().getPreferenceManager().setCurrentOtpCountdown(time);
                    return time - 1;
                });
    }

    public String checkPhone(String phone) {
        if (StringHelper.isPhone(phone)) {
            return phone;
        } else {
            return null;
        }
    }

    public Long getCurrentOtpCountDown() {
        return ((AktivasiRepository) getRepository()).getCurrentOtpCountDown();
    }

    public void resetOtpCountDown() {
        ((AktivasiRepository) getRepository()).resetOtpCountDown();
    }
}
