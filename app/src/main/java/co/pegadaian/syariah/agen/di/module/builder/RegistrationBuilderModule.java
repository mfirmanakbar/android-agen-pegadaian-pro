package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.RegistrationModule;
import co.pegadaian.syariah.agen.registration.view.RegistrationActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public abstract class RegistrationBuilderModule {

    @ContributesAndroidInjector(modules = {RegistrationModule.class})
    abstract RegistrationActivity bindActivity();
}
