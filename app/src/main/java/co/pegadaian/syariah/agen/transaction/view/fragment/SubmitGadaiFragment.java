package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/20/2018.
 */

public class SubmitGadaiFragment extends BaseFragment {

    @BindView(R.id.list_gadai)
    RecyclerView listGadai;
    private Unbinder unbinder;
    private static BaseTransactionActivity baseTransactionActivity;
    private FastItemAdapter<ListGadai> listItemAdapter = new FastItemAdapter<>();

    public static SubmitGadaiFragment newInstance(BaseTransactionActivity activity) {
        SubmitGadaiFragment submitGadaiFragment = new SubmitGadaiFragment();
        baseTransactionActivity = activity;
        return submitGadaiFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submit_gadai, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        configureAdapter(listItemAdapter, listGadai);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        baseTransactionActivity.initComponentSubmitGadai(listItemAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {
        
    }
}
