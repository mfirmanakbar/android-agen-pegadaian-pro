package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseGadaiActivity;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/16/2018.
 */

public class FormGadaiSimulasiFragment extends BaseFragment {

    @BindView(R.id.spinner_tennor)
    AppCompatSpinner spinnerTennor;
    @BindView(R.id.batas_mahrum_bih)
    CustomFontLatoBoldTextView batasMahrumBih;
    @BindView(R.id.taksiran_edit_text)
    CustomFontLatoRegulerEditText taksiranTextView;
    @NotEmpty
    @BindView(R.id.marhun_bih_edit_text)
    CustomFontLatoRegulerEditText marhunBihTextView;
    private Unbinder unbinder;
    private static BaseGadaiActivity baseTransactionActivity;

    public static FormGadaiSimulasiFragment newInstance(BaseGadaiActivity activity) {
        FormGadaiSimulasiFragment formGadaiSimulasiFragment = new FormGadaiSimulasiFragment();
        baseTransactionActivity = activity;
        return formGadaiSimulasiFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_simulasi, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        attachSpinner(baseTransactionActivity, spinnerTennor, Arrays.asList(ConfigurationUtils.TENOR), new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                baseTransactionActivity.initSpinnerTenorOnSelectedListener(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        baseTransactionActivity.initFormGadaiSimulasi(spinnerTennor, taksiranTextView, batasMahrumBih, marhunBihTextView);
    }

    @Override
    public void onValidationSucceeded() {
        baseTransactionActivity.nextToCariNasabah();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_selanjutnya)
    public void onViewClicked() {
        getValidator().validate();
    }
}
