package co.pegadaian.syariah.agen.sample.sampledetail.domain.model;


import co.pegadaian.syariah.agen.object.sample.Request;
import io.reactivex.Observable;

/**
 * Created by Dell on 2/27/2018.
 */

public interface SampleRemoteModel {
    Observable<Object> getToken(Request request);
}
