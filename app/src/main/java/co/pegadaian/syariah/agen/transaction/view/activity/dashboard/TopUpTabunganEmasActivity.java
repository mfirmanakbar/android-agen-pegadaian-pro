package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.support.v7.widget.RecyclerView.LayoutManager;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import co.pegadaian.syariah.agen.object.tabungan.ResponseSimulasiTopUp;
import co.pegadaian.syariah.agen.object.tabungan.SubmitBeTopUp;
import co.pegadaian.syariah.agen.tabungan.view.fragment.KonfirmasiTopUpFragment;
import co.pegadaian.syariah.agen.tabungan.view.fragment.SimulasiTopUpFragment;
import co.pegadaian.syariah.agen.tabungan.viewmodel.TabunganViewModel;
import co.pegadaian.syariah.agen.tabungan.viewmodel.factory.TabunganViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import id.app.pegadaian.library.widget.colordialog.PromptDialog;
import id.app.pegadaian.library.widget.dialog.DialogPin;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by ArLoJi on 18/03/2018.
 */

public class TopUpTabunganEmasActivity extends BaseToolbarActivity {
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    @Inject
    TabunganViewModelFactory viewModelFactory;
    TabunganViewModel viewModel;

    private ViewPager.OnPageChangeListener pageChangeListener;
    private ViewPagerAdapter adapter;
    private DialogPin dialogPin;
    private String titleTopUp;
    private String deskripsiSimulasi;
    private String deskripsiKonfirmasiTopUp;
    private int tempPage = 0;

    //init komponen simulasi
    EditText nomorRekening;
    EditText nominalTopUp;
    private ResponseSimulasiTopUp responseSimulasiTopUp;

    //init komponen konfirmasi
    RecyclerView dataNasabah;
    RecyclerView dataTransaksi;
    CustomFontLatoBoldTextView saldoAwal;
    CustomFontLatoBoldTextView saldoAwalEmas;
    CustomFontLatoRegularTextView labelSaldoAwal;
    CustomFontLatoRegularTextView labelSaldoAwalEmas;
    private FastItemAdapter<ItemTransaksi> listDataNasabah = new FastItemAdapter<>();
    private FastItemAdapter<ItemTransaksi> listDataTransaksi = new FastItemAdapter<>();

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_tabungan;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TabunganViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        ButterKnife.bind(this);
        configureTitle();
        configureToolbar(titleTopUp, deskripsiSimulasi);
        initListener();
        viewPagerConfigurator();
    }

    private void configureTitle(){
        titleTopUp = getString(R.string.emas);
        deskripsiSimulasi = getString(R.string.simulasi_top_ups);
        deskripsiKonfirmasiTopUp = getString(R.string.konfirmasi_top_ups);
    }

    private void configureItemAdapter(){
        configureMenuAdapter(new LinearLayoutManager(this), listDataNasabah, dataNasabah);
        configureMenuAdapter(new LinearLayoutManager(this), listDataTransaksi, dataTransaksi);
    }

    private void configureMenuAdapter(LayoutManager linearLayout,
                                      FastItemAdapter<ItemTransaksi> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void viewPagerConfigurator() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(SimulasiTopUpFragment.newInstance(this), "");
        adapter.addFragment(KonfirmasiTopUpFragment.newInstance(this), "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(pageChangeListener);
        progressBar.setProgress(viewModel.getProgress(0,adapter.getCount()));
    }

    private void initListener() {
        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //handle action@page
                if (position == ConfigurationUtils.TAG_PAGE_PERTAMA) {
                    configureToolbar(titleTopUp, deskripsiSimulasi);
                } else if (position == ConfigurationUtils.TAG_PAGE_KEDUA){
                    configureToolbar(titleTopUp,deskripsiKonfirmasiTopUp);
                    initHasilKonfirmasi();
                }

                //handle progressbar
                progressBar.setProgress(viewModel.getProgress(position,adapter.getCount()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        handleOnBack();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    public void handleOnNext(){
        if (tempPage < adapter.getCount()-1) {
            tempPage = tempPage + 1;
            pager.setCurrentItem(tempPage);
        }
    }

    public void handleOnBack(){
        if (tempPage > 0) {
            tempPage = tempPage - 1;
            pager.setCurrentItem(tempPage);
        } else {
            finishActivity();
        }
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SIMULASI_TOPUP_SUCCESS.equals(response.type)){
                    responseSimulasiTopUp = (ResponseSimulasiTopUp) response.result;
                    handleOnNext();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_VALID.equals(response.type)){
                    submitTopUp();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)){
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_TOP_UP_BERHASIL.equals(response.type)){
                    showDialogSuccessNoCancel("TOP UP Berhasil", dialog -> {
                        dialog.dismiss();
                        showActivityAndFinishCurrentActivity(this, BerandaActivity.class);
                    });
                }
                circleProgressBar.smoothToHide();
                break;
            case ERROR:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_INVALID.equals(response.type)){
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_MINIMAL_PIN_INVALID.equals(response.type)){
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else {
                    showDialogError(response.result.toString());
                }
                circleProgressBar.smoothToHide();
                break;
        }
    }

    private void submitTopUp() {
        circleProgressBar.smoothToShow();
        SubmitBeTopUp submitBeTopUp = new SubmitBeTopUp(
                responseSimulasiTopUp.getAccountNumber(),
                responseSimulasiTopUp.getAmount(),
                true,
                responseSimulasiTopUp.getVersion()
        );

        viewModel.submitTopUp(responseSimulasiTopUp.getId(),submitBeTopUp);
    }

    //Handler untuk fragment simulasi top up
    public void initComponentSimulasi(EditText nomorRekening,
                                      EditText nominalTopUp) {
        this.nomorRekening = nomorRekening;
        this.nominalTopUp = nominalTopUp;
    }

    public void sendSimulasiTopUp(String rekening, String nominal) {
        circleProgressBar.smoothToShow();
        viewModel.sendSimulasiTopUp(rekening,nominal);
    }

    //Handler untuk fragment konfirmasi top up
    public void initComponentKonfirmasi(RecyclerView dataNasabah,
                                        RecyclerView dataTransaksi,
                                        CustomFontLatoBoldTextView saldoAwal,
                                        CustomFontLatoBoldTextView saldoAwalEmas,
                                        CustomFontLatoRegularTextView labelSaldoAwal,
                                        CustomFontLatoRegularTextView labelSaldoAwalEmas) {
        this.dataNasabah = dataNasabah;
        this.dataTransaksi = dataTransaksi;
        this.saldoAwal = saldoAwal;
        this.saldoAwalEmas = saldoAwalEmas;
        this.labelSaldoAwal = labelSaldoAwal;
        this.labelSaldoAwalEmas = labelSaldoAwalEmas;
        configureItemAdapter();
    }

    private void initHasilKonfirmasi(){
        listDataNasabah.clear();
        listDataTransaksi.clear();
        listDataNasabah.add(new ItemTransaksi("Nomor Rekening",
                responseSimulasiTopUp.getAccountNumber()));
        listDataNasabah.add(new ItemTransaksi("Nama Nasabah",
                responseSimulasiTopUp.getCustomerName()));
        listDataTransaksi.add(new ItemTransaksi("Nilai Transaksi",
                StringHelper.getPriceInRp(responseSimulasiTopUp.getAmount())));
        listDataTransaksi.add(new ItemTransaksi("Total Bayar",
                StringHelper.getPriceInRp(responseSimulasiTopUp.getTotalKewajiban())));
        listDataTransaksi.add(new ItemTransaksi("Biaya Administrasi",
                StringHelper.getPriceInRp(responseSimulasiTopUp.getSurchange())));
        saldoAwal.setText(StringHelper.getPriceInRp(responseSimulasiTopUp.getAmount()));
        saldoAwalEmas.setText(StringHelper.getStringBuilderToString(responseSimulasiTopUp.getGram().toString()
                ," gr"));
    }

    public void initDialogPin(){
        dialogPin = DialogPin.newInstance();
        dialogPin.setPositiveListener(view -> {
            TopUpTabunganEmasActivity.this.validasiPin(dialogPin.getPinTransaksi());
            dialogPin.dismissDialog();
        });
        dialogPin.show(getSupportFragmentManager(), "");
    }

    private void validasiPin(String pin){
        circleProgressBar.smoothToShow();
        viewModel.validasiPin(pin);
    }
}
