package co.pegadaian.syariah.agen.aktivasi.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.AktivasiViewModel;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.factory.AktivasiViewModelFactory;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.object.authentication.RequestChangePIN;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseValidatorActivity;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class SetupPinActivity extends BaseValidatorActivity {

    @Inject
    AktivasiViewModelFactory viewModelFactory;
    AktivasiViewModel viewModel;

    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.NUMERIC)
    @BindView(R.id.pin)
    TextInputEditText pin;
    @NotEmpty
    @ConfirmPassword
    @BindView(R.id.pin_confirm)
    TextInputEditText pinConfirm;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;
    private String accessToken_Id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        configureToolbar(getResources().getString(R.string.setup_pin_transaksi));
        accessToken_Id = getIntent().getStringExtra("accessToken_Id");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AktivasiViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                circleProgressBar.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHANGE_PIN_SUCCESS.equals(response.type)) {
                    viewModel.getUserProfile(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED.equals(response.type)) {
                    showActivityAndFinishCurrentActivity(this, BerandaActivity.class);
                }
                break;

            case ERROR:
                circleProgressBar.setVisibility(View.GONE);
                showDialogError(response.result.toString());
                break;
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_setup_akun;
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @OnClick(R.id.button_simpan_selesai)
    public void onViewClicked() {
        getValidator().validate();
    }

    @Override
    public void onValidationSucceeded() {
        RequestChangePIN requestChangePIN = new RequestChangePIN(pin.getText().toString(),
                pinConfirm.getText().toString());
        viewModel.changePIN(requestChangePIN, accessToken_Id);
    }
}