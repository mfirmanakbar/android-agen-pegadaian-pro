package co.pegadaian.syariah.agen.base.viewmodel;

import java.io.File;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;

/**
 * Created by Dell on 3/15/2018.
 */

public class BaseAgentViewModel extends BaseViewModel {

    private final RealmConfigurator realmConfigurator;

    public BaseAgentViewModel(SchedulersFacade schedulersFacade,
                              JsonParser jsonParser,
                              RxBus rxBus,
                              PreferenceManager preferenceManager,
                              BaseAgentUseCase baseUseCase,
                              RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, baseUseCase);
        this.realmConfigurator = realmConfigurator;
    }

    public void handleGetUserProfile() {
        Disposable localUserProfile = getLocalUserProfile();
        if (localUserProfile != null)
            getDisposables().add(localUserProfile);
    }

    private Disposable getLocalUserProfile() {
        Flowable<UserProfile> userProfile = ((BaseAgentUseCase) getBaseUseCase()).getUserProfile(realmConfigurator.getRealm());
        return (userProfile == null) ?
                null : userProfile
                .observeOn(getSchedulersFacade().ui())
                .doOnSubscribe(subscription -> response().postValue(Response.loading()))
                .filter(token -> token.isValid() && token.isLoaded())
                .subscribe(new ConsumerAdapter<UserProfile>() {
                    @Override
                    public void accept(UserProfile userProfile1) throws Exception {
                        super.accept(userProfile1);
                        response().postValue(Response.success(userProfile1, ConfigurationUtils.TAG_LOAD_USER_PROFILE));
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        response().postValue(Response.error("", ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED));
                    }
                });
    }

    protected void handleSetAgen(UserProfile userProfile) {
        getBaseUseCase().insertAgentOutletid(userProfile.getKodeOutlet());
        realmConfigurator.getRealm().executeTransaction(realm -> setAgentToLocal(realm, userProfile));
    }

    private void setAgentToLocal(Realm realm, UserProfile userProfile) {
        Disposable disposable = ((BaseAgentUseCase) getBaseUseCase()).insertAgent(realm, userProfile).
                observeOn(getSchedulersFacade().ui())
                .doOnSubscribe(subscription -> response()
                        .postValue(Response.loading()))
                .filter(agen1 -> agen1.isValid() && agen1.isLoaded())
                .subscribe(new ConsumerAdapter<UserProfile>() {
                    @Override
                    public void accept(UserProfile agen1) throws Exception {
                        super.accept(agen1);
//                        LoginViewModel.this.response().postValue(Response.success("", userProfile.getSignInResponse()
//                                .getStatusRegistrasi()));
                        handleAfterAcceptSetAgentToLocal(agen1);
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        response().setValue(Response.error(throwable.getMessage()));
                    }
                });
        getDisposables().add(disposable);
    }

    protected void handleAfterAcceptSetAgentToLocal(UserProfile agen1) {
        response().postValue(Response.success(agen1.getSignInResponse().getAccessToken(),
                agen1.getSignInResponse().getStatusRegistrasi()));
    }

    public void clearData() {
        if (realmConfigurator.getRealm() != null)
            realmConfigurator.getRealm().executeTransaction(realm -> realm.delete(UserProfile.class));
    }

    public void compressFile(File file, int type) {
        getDisposables().add(getBaseUseCase()
                .compressFile(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<File>() {
                    @Override
                    public void accept(File file) throws Exception {
                        super.accept(file);
                        if (response().hasActiveObservers()) {
                            if (type == 0) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_IMAGE));
                            } else if (type == 1) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE));
                            } else if (type == 4) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE));
                            } else if (type == 5) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_SIDE_IMAGE));
                            } else if (type == 6) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE));
                            } else if (type == 7) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_BPKB_IMAGE));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                    ConfigurationUtils.TAG_FAILED_ADD_IMAGE));
                        }
                    }
                }));
    }
}