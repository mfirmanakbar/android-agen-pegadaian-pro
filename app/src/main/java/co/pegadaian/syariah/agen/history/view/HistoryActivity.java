package co.pegadaian.syariah.agen.history.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.pegadaian.syariah.agen.MainActivity;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.history.adapter.DataAdapter;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryEmas;
import co.pegadaian.syariah.agen.object.history.DetailHistoryGadai;
import co.pegadaian.syariah.agen.object.history.DetailHistoryPemasaran;
import co.pegadaian.syariah.agen.object.history.DetailHistoryPembayaran;
import co.pegadaian.syariah.agen.object.history.HistoryEmas;
import co.pegadaian.syariah.agen.object.history.HistoryGadai;
import co.pegadaian.syariah.agen.object.history.HistoryPemasaran;
import co.pegadaian.syariah.agen.object.history.HistoryPembayaran;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;


/**
 * Created by ArLoJi on 09/03/2018.
 */

public class HistoryActivity extends MainActivity implements HistoryListener {

    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    CustomViewPager viewpager;

    TextView textTransaksi;
    RecyclerView listTransaksiAkhir;
    RecyclerView listTransaksiAkhirEmas;
    RecyclerView listTransaksiAkhirPembayaran;
    RecyclerView listTransaksiAkhirPemasaran;
    List<HistoryGadai> historyGadaiList = new ArrayList<>();
    List<HistoryEmas> historyEmasList = new ArrayList<>();
    List<HistoryPembayaran> historyPembayaranList = new ArrayList<>();
    List<HistoryPemasaran> historyPemasaranList = new ArrayList<>();
    private int tempPage = 3;


    private FastItemAdapter<HistoryGadai> listDataHistoryGadai = new FastItemAdapter<>();
    private FastItemAdapter<HistoryEmas> listDataHistoryEmas = new FastItemAdapter<>();
    private FastItemAdapter<HistoryPembayaran> listDataHistoryPembayaran = new FastItemAdapter<>();
    private FastItemAdapter<HistoryPemasaran> listDataHistoryPemasaran = new FastItemAdapter<>();


    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private String tabsTitle[] = {"gadai","emas","pembayaran","pemasaran"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        initListener();
        configureViewPage();
        loadHistoryGadai();
    }

    private void configureViewPage() {

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(HistoryGadaiFragment.newInstance(this,"Tampilkan semua transaksi gadai"), "");
        adapter.addFragment(HistoryEmasFragment.newInstance(this, "Tampilkan semua transaksi emas"), "");
        adapter.addFragment(HistoryPembayaranFragment.newInstance(this, "Tampilkan semua transaksi pembayaran"), "");
        adapter.addFragment(HistoryPemasaranFragment.newInstance(this, "Tampilkan semua transaksi  pemasaran"), "");
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(pageChangeListener);
        tabLayout.setupWithViewPager(viewpager);
        for(int i=0; i<tabLayout.getTabCount();i++){
            tabLayout.getTabAt(i).setIcon(R.drawable.ic_placeholder);
            tabLayout.getTabAt(i).setText(tabsTitle[i]);
        }
    }

    public void setComponentFragment(TextView textTransaksi, RecyclerView listTransaksiAkhir){
        this.textTransaksi = textTransaksi;
        this.listTransaksiAkhir = listTransaksiAkhir;
        configureItemAdapter();
    }

    public void setComponentFragmentEmas(TextView textTransaksi, RecyclerView listTransaksiAkhirEmas){
        this.textTransaksi = textTransaksi;
        this.listTransaksiAkhirEmas = listTransaksiAkhirEmas;
        configureItemAdapterEmas();
    }

    public void setComponentFragmentPembayaran(TextView textTransaksi, RecyclerView listTransaksiAkhirPembayaran){
        this.textTransaksi = textTransaksi;
        this.listTransaksiAkhirPembayaran = listTransaksiAkhirPembayaran;
        configureItemAdapterPembayaran();
    }

    public void setComponentFragmentPemasaran(TextView textTransaksi, RecyclerView listTransaksiAkhirPemasaran){
        this.textTransaksi = textTransaksi;
        this.listTransaksiAkhirPemasaran = listTransaksiAkhirPemasaran;
        configureItemAdapterPemasaran();
    }

    private void configureItemAdapterEmas(){
        configureMenuAdapterEmas(new LinearLayoutManager(this), listDataHistoryEmas, listTransaksiAkhirEmas);
    }

    private void configureItemAdapterPembayaran(){
        configureMenuAdapterPembayaran(new LinearLayoutManager(this), listDataHistoryPembayaran, listTransaksiAkhirPembayaran);
    }

    private void configureItemAdapterPemasaran(){
        configureMenuAdapterPemasaran(new LinearLayoutManager(this), listDataHistoryPemasaran, listTransaksiAkhirPemasaran);
    }

    private void configureItemAdapter(){
        configureMenuAdapter(new LinearLayoutManager(this), listDataHistoryGadai, listTransaksiAkhir);
    }

    private void configureMenuAdapter(RecyclerView.LayoutManager linearLayout,
                                      FastItemAdapter<HistoryGadai> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
//        itemAdapter.withSelectable(true);
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            HistoryGadai historyGadai = historyGadaiList.get(position);
            List<DetailHistoryGadai> detailHistoryGadai = historyGadai.getDetailHistory();
            if(null != detailHistoryGadai && detailHistoryGadai.size() > 0){
                Intent intent = new Intent(HistoryActivity.this, HistoryDetailRiwayatActivity.class);
                intent.putExtra("detailHistoryGadai", Parcels.wrap(detailHistoryGadai.get(0)));
                showActivity(intent);
            } else {
                Toast.makeText(this, "Detail History tidak ditemukan", Toast.LENGTH_SHORT).show();
            }

            return true;
        });
    }

    private void configureMenuAdapterEmas(RecyclerView.LayoutManager linearLayout,
                                      FastItemAdapter<HistoryEmas> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
//        itemAdapter.withSelectable(true);
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            HistoryEmas historyEmas = historyEmasList.get(position);
            List<DetailHistoryEmas> detailHistoryEmas = historyEmas.getDetailEmas();
            if(null != detailHistoryEmas && detailHistoryEmas.size() > 0){
                Intent intent = new Intent(HistoryActivity.this, HistoryDetailRiwayatEmasActivity.class);
                intent.putExtra("detailHistoryEmas", Parcels.wrap(detailHistoryEmas.get(0)));
                showActivity(intent);
            } else {
                Toast.makeText(this, "Detail History tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
            return true;
        });
    }

    private void configureMenuAdapterPembayaran(RecyclerView.LayoutManager linearLayout,
                                          FastItemAdapter<HistoryPembayaran> itemAdapter,
                                          RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            HistoryPembayaran historyPembayaran = historyPembayaranList.get(position);
            List<DetailHistoryPembayaran> detailHistoryPembayaran = historyPembayaran.getDetailPembayaran();
            if(null != detailHistoryPembayaran && detailHistoryPembayaran.size() > 0){
                Intent intent = new Intent(HistoryActivity.this, HistoryDetailRiwayatPembayaranActivity.class);
                intent.putExtra("detailHistoryPembayaran", Parcels.wrap(detailHistoryPembayaran.get(0)));
                showActivity(intent);
            } else {
                Toast.makeText(this, "Detail History tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
            return true;
        });
    }

    private void configureMenuAdapterPemasaran(RecyclerView.LayoutManager linearLayout,
                                                FastItemAdapter<HistoryPemasaran> itemAdapter,
                                                RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            HistoryPemasaran historyPemasaran = historyPemasaranList.get(position);
            List<DetailHistoryPemasaran> detailHistoryPemasaran = historyPemasaran.getDetailPemasaran();
            if(null != detailHistoryPemasaran && detailHistoryPemasaran.size() > 0){
                Intent intent = new Intent(HistoryActivity.this, HistoryDetailRiwayatPemasaranActivity.class);
                intent.putExtra("detailHistoryPemasaran", Parcels.wrap(detailHistoryPemasaran.get(0)));
                showActivity(intent);
            } else {
                Toast.makeText(this, "Detail History tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
            return true;
        });
    }

    private void initListener() {
        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position == 1 && historyEmasList.size() == 0){
                    loadHistoryEmas();
                } else  if(position == 2 && historyPembayaranList.size() == 0){
                    loadHistoryPembayaran();
                } else  if(position == 3 && historyPemasaranList.size() == 0){
                    loadHistoryPemasaran();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_transaksi;
    }

    @Override
    protected int getItemSelected() {
        return R.id.bottom_riwayat;
    }

    @Override
    protected String getTitleToolbarMain() {
        return "Riwayat";
    }


    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_LIST_GADAI_TERAKHIR.equals(response.type)) {
                    historyGadaiList = (List<HistoryGadai>) response.result;
                    listDataHistoryGadai.clear();
                    listDataHistoryGadai.add(historyGadaiList);
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_LIST_EMAS_TERAKHIR.equals(response.type)){
                    historyEmasList = (List<HistoryEmas>) response.result;
                    listDataHistoryEmas.clear();
                    listDataHistoryEmas.add(historyEmasList);
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_LIST_PEMBAYARAN_TERAKHIR.equals(response.type)){
                    historyPembayaranList = (List<HistoryPembayaran>) response.result;
                    listDataHistoryPembayaran.clear();
                    listDataHistoryPembayaran.add(historyPembayaranList);
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_LIST_PEMASARAN_TERAKHIR.equals(response.type)){
                    historyPemasaranList = (List<HistoryPemasaran>) response.result;
                    listDataHistoryPemasaran.clear();
                    listDataHistoryPemasaran.add(historyPemasaranList);
                }
                break;
            case ERROR:
                showDialogError(response.result.toString());
                break;
        }
    }


    @Override
    public void loadHistoryGadai() {
        viewModel.getLastHistoryGadai();
    }

    @Override
    public void loadHistoryEmas(){
        viewModel.getLastHistoryEmas();
    }

    @Override
    public void loadHistoryPembayaran(){
        viewModel.getLastHistoryPembayaran();
    }

    @Override
    public void loadHistoryPemasaran(){
        viewModel.getLastHistoryPemasaran();
    }
}
