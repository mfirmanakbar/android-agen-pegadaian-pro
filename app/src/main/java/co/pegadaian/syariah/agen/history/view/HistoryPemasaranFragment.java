package co.pegadaian.syariah.agen.history.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryPemasaranFragment extends BaseFragment {

    @BindView(R.id.text_transaksi)
    TextView textTransaksi;
    @BindView(R.id.list_transaksi_akhir)
    RecyclerView listTransaksiAkhir;
    private Unbinder unbinder;
    private static HistoryActivity historyActivity;
    private static String titles;

    public static HistoryPemasaranFragment newInstance(HistoryActivity activity, String title) {
        HistoryPemasaranFragment registrationDataNasabahFragment = new HistoryPemasaranFragment();
        historyActivity = activity;
        titles = title;
        return registrationDataNasabahFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaksi_pemasaran, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        configureComponent();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void configureComponent() {
        textTransaksi.setText(titles);
    }

    private void configureFont() {

    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
        historyActivity.setComponentFragmentPemasaran(textTransaksi, listTransaksiAkhir);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {

    }

    @OnClick(R.id.button_tampil_transaksi)
    public void onViewClicked() {
        Intent intent = getIntent(getContext(), HistoryDetailPemasaranActivity.class);
        showActivity(intent);
    }

}
