package co.pegadaian.syariah.agen.registration.domain.interactors;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.object.master.Outlet;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.registration.RequestCheckKTP;
import co.pegadaian.syariah.agen.object.registration.RequestRegistration;
import co.pegadaian.syariah.agen.registration.domain.model.RegistrationRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Dell on 3/6/2018.
 */

public class RegistrationUseCase extends BaseUseCase {

    @Inject
    public RegistrationUseCase(RegistrationRepository repository,
                               Compressor compressor) {
        super(repository, compressor);
    }

    public int getProgress(int page, int totalPage) {
        return (100 / totalPage) * (page + 1);
    }

    public Observable<Long> timer() {
        long count = getRepository().getPreferenceManager().getCurrentOtpCountdown() + 1;
        return Observable.interval(1, TimeUnit.SECONDS)
                .take(count)
                .map(aLong -> {
                    long time = count - aLong;
                    getRepository().getPreferenceManager().setCurrentOtpCountdown(time);
                    return time - 1;
                });
    }

    public Long getCurrentOtpCountDown() {
        return ((RegistrationRepository) getRepository()).getCurrentOtpCountDown();
    }

    public void resetOtpCountDown() {
        ((RegistrationRepository) getRepository()).resetOtpCountDown();
    }

    public Observable<Object> getRegion() {
        return ((RegistrationRepository) getRepository()).getRegion();
    }

    public Observable<Object> getKota(String idProvinsi) {
        return ((RegistrationRepository) getRepository()).getKota(idProvinsi);
    }

    public Observable<Object> getKecamatan(String idKota) {
        return ((RegistrationRepository) getRepository()).getKecamatan(idKota);
    }

    public Observable<Object> getKelurahan(String idKecamatan) {
        return ((RegistrationRepository) getRepository()).getKelurahan(idKecamatan);
    }

    public Observable<Object> sendOtp(RequestSendSms requestSendSms) {
        return ((RegistrationRepository) getRepository()).sendOtp(requestSendSms);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return ((RegistrationRepository) getRepository()).verifyOtp(requestVerifyOtp);
    }

    public Observable<Object> getBidangUsaha() {
        return ((RegistrationRepository) getRepository()).getBidangUsaha();
    }

    public Observable<Object> getOutlet(LatLng latLang) {
        return ((RegistrationRepository) getRepository()).getOutlet(String.valueOf(latLang.latitude),
                String.valueOf(latLang.longitude));
    }

    public Observable<Outlet> getFilteredOutlets(CharSequence filter, List<Outlet> outlets) {
        return Observable
                .fromIterable(outlets)
                .filter(outlet -> outlet.getAddressOutlet().toLowerCase()
                        .contains(filter.toString().toLowerCase()) ||
                        outlet.getDistanceOutlet().toLowerCase()
                                .contains(filter.toString().toLowerCase()));
    }

    public Observable<Object> getListOfBank() {
        return ((RegistrationRepository) getRepository()).getListOfBank();
    }

    public Observable<Object> setAgent(RequestRegistration requestRegistration) {
        return ((RegistrationRepository) getRepository()).setAgent(requestRegistration);
    }

    public Observable<Object> uploadImageRegistration(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        return ((RegistrationRepository) getRepository()).uploadImageRegistration(body);
    }

    public Observable<Object> checkKTP(RequestCheckKTP requestCheckKTP) {
        return ((RegistrationRepository) getRepository()).checkKTP(requestCheckKTP);
    }
}