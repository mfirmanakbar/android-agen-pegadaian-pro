package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.GadaiModule;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.KendaraanActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.LogamMuliaActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PembukaanTabunganEmasActivity;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.PerhiasanActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/16/2018.
 */

@Module
public abstract class TransactionBuilderModule {

    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract PerhiasanActivity bindActivity();
    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract PembukaanTabunganEmasActivity pembukaanTabunganEmasActivity();
    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract AmanahActivity amanahActivity();
    abstract PembukaanTabunganEmasActivity bindPembukaanTabunganEmasActivity();
    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract LogamMuliaActivity bindLogamMuliaActivity();
    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract KendaraanActivity bindKendaraanActivity();
}