package co.pegadaian.syariah.agen.object.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.utils.time.TimeUtils;

/**
 * Created by T460s on 3/20/2018.
 */


public class HistoryGadai extends AbstractItem<ItemTransaksi, HistoryGadai.ViewHolder> {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("barang_jaminan")
    @Expose
    private String barangJaminan;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("no_rekening")
    @Expose
    private String noRekening;
    @SerializedName("tgl_transaksi")
    @Expose
    private Integer tglTransaksi;
    @SerializedName("taksir_agen")
    @Expose
    private Integer taksirAgen;
    @SerializedName("taksir_pegadaian")
    @Expose
    private Integer taksirPegadaian;
    @SerializedName("detail-history")
    @Expose
    private List<DetailHistoryGadai> detailHistory = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarangJaminan() {
        return barangJaminan;
    }

    public void setBarangJaminan(String barangJaminan) {
        this.barangJaminan = barangJaminan;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }

    public Integer getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Integer tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public Integer getTaksirAgen() {
        return taksirAgen;
    }

    public void setTaksirAgen(Integer taksirAgen) {
        this.taksirAgen = taksirAgen;
    }

    public Integer getTaksirPegadaian() {
        return taksirPegadaian;
    }

    public void setTaksirPegadaian(Integer taksirPegadaian) {
        this.taksirPegadaian = taksirPegadaian;
    }
    public List<DetailHistoryGadai> getDetailHistory() {
        return detailHistory;
    }

    public void setDetailHistory(List<DetailHistoryGadai> detailHistory) {
        this.detailHistory = detailHistory;
    }
    @NonNull
    @Override
    public HistoryGadai.ViewHolder getViewHolder(View v) {
        return new HistoryGadai.ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.card_transaksi;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_jenis)
        TextView jenis;

        @BindView(R.id.tv_nama)
        TextView nama;

        @BindView(R.id.tv_no)
        TextView noNasabah;

        @BindView(R.id.tv_status)
        TextView status;

        @BindView(R.id.tv_tanggal)
        TextView tanggal;

        @BindView(R.id.tv_tak_agen)
        TextView taksiranAgen;

        @BindView(R.id.tv_tak_pegadaian)
        TextView taksiranPegadaian;

        Context context;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(@NonNull HistoryGadai.ViewHolder holder, @NonNull List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.nama.setText(getNamaNasabah());
        holder.jenis.setText(getBarangJaminan());
        holder.noNasabah.setText(getNoRekening());
        holder.status.setText(getStatus());
        holder.tanggal.setText(TimeUtils.getDateFormated("dd/MM/yyyy", getTglTransaksi()));
        holder.taksiranAgen.setText(null != getTaksirAgen() ? StringHelper.getSimplePriceFormatter(getTaksirAgen().doubleValue()) : "0");
        holder.taksiranPegadaian.setText(StringHelper.getPriceInRp(getTaksirPegadaian().doubleValue()));
    }

    @Override
    public void unbindView(@NonNull HistoryGadai.ViewHolder holder) {
        super.unbindView(holder);
        holder.nama.setText(null);
    }

}
