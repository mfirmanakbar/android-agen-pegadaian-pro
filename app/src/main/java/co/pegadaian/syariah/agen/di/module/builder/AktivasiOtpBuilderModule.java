package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.aktivasi.view.AktivasiOtpActivity;
import co.pegadaian.syariah.agen.di.module.AktivasiModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public abstract class AktivasiOtpBuilderModule {

    @ContributesAndroidInjector(modules = {AktivasiModule.class})
    abstract AktivasiOtpActivity bindActivity();
}
