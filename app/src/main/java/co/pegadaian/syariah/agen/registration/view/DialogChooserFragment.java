package co.pegadaian.syariah.agen.registration.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseDialogChooserListener;

public class DialogChooserFragment extends DialogFragment {

    public static final String TAG_SHOW_LIST = "TAG_SHOW_LIST";
    @BindView(R.id.close_button)
    AppCompatImageView closeButton;
    @BindView(R.id.list_item)
    ListView listItem;
    @BindView(R.id.dialog_title)
    AppCompatTextView dialogTitle;
    @BindView(R.id.dialog_search_edit_text)
    AppCompatEditText dialogSearchEditText;

    private Unbinder unbinder;
    private static BaseDialogChooserListener listener;
    private static ArrayList<String> valueList = new ArrayList<>();
    private static String titleDialog;
    private static String searchHintDialog;
    private ArrayAdapter<String> adapter;
    private String type;
    private static Activity activity;

    public static DialogChooserFragment newInstance(BaseDialogChooserListener listener,
                                                    ArrayList<String> values,
                                                    String title,
                                                    String searchHint,
                                                    String type,
                                                    Activity activity) {
        DialogChooserFragment dialogChooserFragment = new DialogChooserFragment();
        DialogChooserFragment.listener = listener;
        DialogChooserFragment.activity = activity;
        Bundle args = new Bundle();
        args.putString("titleDialog", title);
        args.putString("searchHintDialog", searchHint);
        args.putStringArrayList("valueList", values);
        args.putString("type", type);
        dialogChooserFragment.setArguments(args);
        return dialogChooserFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initValue();
    }

    private void initValue() {
        titleDialog = getArguments().getString("titleDialog");
        searchHintDialog = getArguments().getString("searchHintDialog");
        valueList = getArguments().getStringArrayList("valueList");
        type = getArguments().getString("type");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View customView = inflater.inflate(R.layout.dialog_chooser_full_screen, null);
        unbinder = ButterKnife.bind(this, customView);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(customView);
        Dialog dialog = dialogBuilder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        adapter = new ArrayAdapter<>(activity,
                R.layout.simple_list_item, valueList);
        listItem.setAdapter(adapter);
        listItem.setOnItemClickListener((adapterView, view1, i, l)
                -> listener.handleOnItemSearchDialogClick(i, type));
        dialogTitle.setText(titleDialog);
        dialogTitle.setTypeface(getLatoBold());
        dialogSearchEditText.setHint(searchHintDialog);
        dialogSearchEditText.setTypeface(getLatoRegular());
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
        listener.initDialogChooserFragment(dialogSearchEditText, adapter);
    }

    @OnClick(R.id.close_button)
    public void onViewClicked() {
        dismiss();
    }

    public Typeface getLatoRegular() {
        return Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");
    }

    public Typeface getLatoBold() {
        return Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Bold.ttf");
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}