package co.pegadaian.syariah.agen.transaction.view.activity;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;

import com.google.android.gms.vision.barcode.Barcode;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.barcodereader.mobilevision.BarcodeGraphicTracker;
import id.app.pegadaian.library.barcodereader.view.QrCodePreview;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;

/**
 * Created by Dell on 3/18/2018.
 */

public class ScanQrCodeActivity extends BaseToolbarActivity implements BarcodeGraphicTracker.BarcodeUpdateListener {

    @BindView(R.id.scan_qr_code_view)
    QrCodePreview scanQrCodeView;

    @Inject
    TransactionViewModelFactory viewModelFactory;
    TransactionViewModel viewModel;

    private static boolean isScanQrProcessing = false;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configureToolbar("");
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED.equals(response.type)) {
                    restartScanProcessing();
                    scanQrCodeView.startCameraSource();
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart(Manifest.permission.CAMERA);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (scanQrCodeView != null) {
            scanQrCodeView.stop();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        restartScanProcessing();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (scanQrCodeView != null) {
            scanQrCodeView.release();
        }
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_scan_qr_code;
    }

    private void restartScanProcessing() {
        isScanQrProcessing = false;
    }

    @Override
    public void onBarcodeDetected(Barcode barcode) {
        if (barcode != null &&
                StringUtils.isNotBlank(barcode.displayValue) &&
                !isScanQrProcessing) {
            isScanQrProcessing = true;
            scanQrCodeView.handlerOnRetrieved();
            finishActivity();
            viewModel.handleScanCif(barcode.displayValue);
        }
    }
}