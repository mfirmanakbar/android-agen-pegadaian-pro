package co.pegadaian.syariah.agen.notification.viewmodel;

import co.pegadaian.syariah.agen.notification.domain.interactor.NotificationUseCase;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class NotificationViewModel extends BaseViewModel {

    private final NotificationUseCase notificationUseCase;

    public NotificationViewModel(SchedulersFacade schedulersFacade,
                                 JsonParser jsonParser,
                                 RxBus rxBus,
                                 PreferenceManager preferenceManager,
                                 NotificationUseCase notificationUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, notificationUseCase);
        this.notificationUseCase = notificationUseCase;
    }
}
