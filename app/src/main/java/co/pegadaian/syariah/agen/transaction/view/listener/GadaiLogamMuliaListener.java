package co.pegadaian.syariah.agen.transaction.view.listener;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by Dell on 3/22/2018.
 */

public interface GadaiLogamMuliaListener extends BaseGadaiListener {
    void initGadaiLogamMuliaComponent(RecyclerView listKeping, AppCompatSpinner kepingLogam, EditText jumlahLogam, EditText keteranganBarang, ImageView gambarLogam, ProgressBar progressPicture, AppCompatButton buttonTambahkanLogam, AppCompatButton buttonSelanjutnya);
}
