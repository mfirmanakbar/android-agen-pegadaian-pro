package co.pegadaian.syariah.agen.object.beranda;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import id.app.pegadaian.library.utils.Utils;

/**
 * Created by Dell on 3/13/2018.
 */

public class Menu extends AbstractItem<Menu, Menu.ViewHolder> {

    private String menuImage;
    private String title;

    public Menu(String title) {
        this.title = title;
    }

    public String getMenuImage() {
        return menuImage;
    }

    public String getTitle() {
        return title;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_menu;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.placeholder_menu_image)
        AppCompatImageView placeholderMenuImage;
        @BindView(R.id.menu_title_text)
        AppCompatTextView menuTitleText;

        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.menuTitleText.setText(getTitle());
        holder.placeholderMenuImage.setImageResource(Utils.getImageResource(getTitle(), holder.context));
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.menuTitleText.setText(null);
    }
}