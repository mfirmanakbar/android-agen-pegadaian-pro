package co.pegadaian.syariah.agen.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by ArLoJi on 16/03/2018.
 */

public class SingleRequest {

    @Expose
    private BigDecimal amount;

    @SerializedName("pin")
    @Expose
    private String pin;

    public SingleRequest(BigDecimal amount) {
        this.amount = amount;
    }

    public SingleRequest(String pin) {
        this.pin = pin;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
