package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class FormAmanahUsaha02Fragment extends BaseFragment {

    private  static AmanahActivity amanahActivity;
    Unbinder unbinder;

    @BindView(R.id.et_alamat_usaha)
    CustomFontLatoRegulerEditText etAlamatUsaha;
    @BindView(R.id.et_provinsi)
    CustomFontLatoRegulerEditText etProvinsi;
    @BindView(R.id.et_kabupaten)
    CustomFontLatoRegulerEditText etKabupaten;
    @BindView(R.id.et_kecamatan)
    CustomFontLatoRegulerEditText etKecamatan;
    @BindView(R.id.et_kelurahan)
    CustomFontLatoRegulerEditText etKelurahan;
    @BindView(R.id.et_jarak_usaha)
    CustomFontLatoRegulerEditText etJarakUsaha;
    @BindView(R.id.placeholder_front_image)
    AppCompatImageView imgPlacholderFrontImage;
    @BindView(R.id.front_image_text)
    CustomFontLatoRegulerEditText etImage;
    @BindView(R.id.front_image)
    AppCompatImageView imgFrontImage;
    @BindView(R.id.front_image_progress_bar)
    ProgressBar frontImageProgressBar;
    @BindView(R.id.placeholder_back_image)
    AppCompatImageView imgPlaceholderBackImage;
    @BindView(R.id.back_image_text)
    AppCompatTextView txtBackImage;
    @BindView(R.id.back_image)
    AppCompatImageView imgBack;
    @BindView(R.id.back_image_progress_bar)
    ProgressBar backImageProgressBar;



    public static FormAmanahUsaha01Fragment newInstance(AmanahActivity activity) {
        FormAmanahUsaha01Fragment formAmanahNasabah01Fragment = new FormAmanahUsaha01Fragment();
        amanahActivity = activity;
        return formAmanahNasabah01Fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_amanah_usaha02, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onValidationSucceeded() {

    }

    @OnClick(R.id.button_next)
    public void onClickButtonNext() {
        amanahActivity.nextAgunanForm02();

    }
}
