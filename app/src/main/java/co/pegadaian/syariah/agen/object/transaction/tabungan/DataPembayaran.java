package co.pegadaian.syariah.agen.object.transaction.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 19/03/2018.
 */

public class DataPembayaran {
    @SerializedName("agentId")
    @Expose
    private String agentId;
    @SerializedName("jenisTransaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("norek")
    @Expose
    private String norek;
    @SerializedName("noHpAgent")
    @Expose
    private String noHpAgent;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("amount")
    @Expose
    private String cicilan;
    @SerializedName("minimalUpCicil")
    @Expose
    private String minimumCicil;
    @SerializedName("reffSwitching")
    @Expose
    private String reffSwitching;

    public DataPembayaran(String agentId, String jenisTransaksi, String norek,
                          String noHpAgent, String paymentMethod, String cicilan) {
        this.agentId = agentId;
        this.jenisTransaksi = jenisTransaksi;
        this.norek = norek;
        this.noHpAgent = noHpAgent;
        this.paymentMethod = paymentMethod;
        this.cicilan = cicilan;
    }

    public DataPembayaran(String agentId, String jenisTransaksi, String norek, String noHpAgent,
                          String paymentMethod, String cicilan, String minimumCicil, String reffSwitching) {
        this(agentId,jenisTransaksi,norek,noHpAgent,paymentMethod,cicilan);
        this.minimumCicil = minimumCicil;
        this.reffSwitching = reffSwitching;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public String getNoHpAgent() {
        return noHpAgent;
    }

    public void setNoHpAgent(String noHpAgent) {
        this.noHpAgent = noHpAgent;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCicilan() {
        return cicilan;
    }

    public void setCicilan(String cicilan) {
        this.cicilan = cicilan;
    }

    public String getMinimumCicil() {
        return minimumCicil;
    }

    public void setMinimumCicil(String minimumCicil) {
        this.minimumCicil = minimumCicil;
    }

    public String getReffSwitching() {
        return reffSwitching;
    }

    public void setReffSwitching(String reffSwitching) {
        this.reffSwitching = reffSwitching;
    }
}
