package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.transaction.view.fragment.CariPenggunaFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahAgunanFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahAgunanKendaraanFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahNasabah01Fragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahNasabah02Fragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahUsaha01Fragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormAmanahUsaha02Fragment;
import co.pegadaian.syariah.agen.transaction.view.listener.GadaiPerhiasanListener;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.map.MapHelper;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;


public class AmanahActivity extends BaseTransactionActivity implements GadaiPerhiasanListener {

    private static final String TAG = AmanahActivity.class.getSimpleName();
    public static final int FORM_SIMULASI_AMANAH_02 = 1;
    public static final int FORM_AGUNAN_01 = 2;
    public static final int FORM_AGUNAN_02 = 3;
    public static final int FORM_DATA_NASABAH = 4;
    public static final int FORM_USAHA_01 = 5;
    public static final int FORM_USAHA_02 = 6;

    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    private ViewPagerAdapter adapter;

    @Inject
    TransactionViewModelFactory viewModelFactory;
    TransactionViewModel viewModel;

    // AMANA NASABAH 01 FORM
    CustomFontLatoRegulerEditText tvMaksimalPlafon;
    CustomFontLatoRegulerEditText tvAmanahUangMuka;
    CustomFontLatoRegulerEditText tvAmanahHargaPasar;
    int tipeKendaraan = FormAmanahNasabah01Fragment.TIPE_KENDARAAN_MOTOR;
    int tipeNasabah = FormAmanahNasabah01Fragment.TIPE_NASABAH_KARYAWAN;

    // FORM AGUNAN
    String agunanKondisiKendaraan = "";
    int kondisiKendaraan = FormAmanahAgunanFragment.TIPE_KENDARAAN_LAMA;
    int agunanTipeKendaraan = FormAmanahAgunanKendaraanFragment.TIPE_KENDARAAN_MOTOR;
    AppCompatSpinner agunanSpinnerMerekKendaraan;
    CustomFontLatoRegulerEditText agunanEtTipe;
    CustomFontLatoRegulerEditText agunanEtTipKendaraan;
    CustomFontLatoRegulerEditText agunanEtIsiSilinder;
    CustomFontLatoRegulerEditText agunanEtTahunPembuatan;
    CustomFontLatoRegulerEditText agunanEtNamaBpkb;
    CustomFontLatoRegulerEditText agunanEtWarna;
    CustomFontLatoRegulerEditText agunanEtKeterangan;
    private String agunanIdJenisKendraan;
    private List<String> jenisKendaraanList = new ArrayList<>();

    CustomFontLatoRegulerEditText etNamaUsaha;
    AppCompatSpinner spinnerBidangUsaha;
    CustomFontLatoRegulerEditText etLamaUsaha;
    AppCompatSpinner spinnerJenisTempatUsaha;
    AppCompatSpinner spinnerStatusTempatUsaha;

    private List<String> bidangUsahaList = new ArrayList<>();
    Map<String, String> dataMapBidangUsaha;
    private String idBidangUsaha;
    private List<String> jenistTempatUsahaList = new ArrayList<>();
    Map<String, String> dataJenisTempatUsaha;
    private String idTempatUsaha;
    private List<String> statusTempatUsahaList = new ArrayList<>();
    Map<String, String> dataStatusTempatUsaha;
    private String idStatusTempatUsaha;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configViewModel(viewModel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        configureToolbar(getString(R.string.amanah), getString(R.string.simulasi_amanah));
        viewPagerConfigurator();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    public void initFormAmanahNasabah01(
            CustomFontLatoRegulerEditText tvMaksimalPlafon,
            CustomFontLatoRegulerEditText tvAmanahUangMuka,
            CustomFontLatoRegulerEditText tvAmanahHargaPasar,
            int tipeKendaraan, int tipeNasabah
    ) {
        this.tvMaksimalPlafon = tvMaksimalPlafon;
        this.tvAmanahUangMuka = tvAmanahUangMuka;
        this.tvAmanahHargaPasar = tvAmanahHargaPasar;
        this.tipeKendaraan = tipeKendaraan;
        this.tipeNasabah = tipeNasabah;
    }
    public void initFormAgunan01(int kondisiKendaraan) {
        this.kondisiKendaraan = kondisiKendaraan;
    }
    public void initFormAgunanaKendaraan(
            int agunanTipeKendaraan,
            AppCompatSpinner spinnerMerekKendaraan,
            CustomFontLatoRegulerEditText etTipeKendaraan,
            CustomFontLatoRegulerEditText etIsiSilinderMesin,
            CustomFontLatoRegulerEditText etTahunPembuatanKendaraan,
            CustomFontLatoRegulerEditText etNamaBpkb,
            CustomFontLatoRegulerEditText etWarna,
            CustomFontLatoRegulerEditText etKeterangan
            ) {

        this.agunanTipeKendaraan = agunanTipeKendaraan;
        this.agunanSpinnerMerekKendaraan = spinnerMerekKendaraan;
        this.agunanEtTipKendaraan = etTipeKendaraan;
        this.agunanEtIsiSilinder = etIsiSilinderMesin;
        this.agunanEtTahunPembuatan = etTahunPembuatanKendaraan;
        this.agunanEtNamaBpkb = etNamaBpkb;
        this.agunanEtWarna = etWarna;
        this.agunanEtKeterangan = etKeterangan;
        viewModel.getJenisKendaraan(agunanTipeKendaraan);
    }
    public void initFormAmanahUsaha01(
            CustomFontLatoRegulerEditText etNamaUsaha,
            AppCompatSpinner spinnerBidangUsaha,
            CustomFontLatoRegulerEditText etLamaUsaha,
            AppCompatSpinner spinnerJenisTempatUsaha,
            AppCompatSpinner spinnerStatusTempatUsaha) {

        this.etNamaUsaha = etNamaUsaha;
        this.spinnerBidangUsaha = spinnerBidangUsaha;
        this.etLamaUsaha = etLamaUsaha;
        this.spinnerJenisTempatUsaha = spinnerJenisTempatUsaha;
        this.spinnerStatusTempatUsaha = spinnerStatusTempatUsaha;
        viewModel.getBidangUsaha();

    }
    public void updateAgunanTipeKendaraan(int tipeKendaraan) {
        this.agunanTipeKendaraan = tipeKendaraan;
        viewModel.getJenisKendaraan(this.agunanTipeKendaraan);
    }
    public void updateAgunanKondisiKendaraan(String kondisiKendaraan) {
        this.agunanKondisiKendaraan = kondisiKendaraan;
    }
    private void processResponse(Response response) {
         switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                Log.i(TAG, "Tag: "+ response.type);
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_JENIS_KENDARAAN.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    loadJenisKendaraan(response);
                }
                if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_BIDANG_USAHA.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    loadBidangUsaha(response);
                    viewModel.getStatusTempatUsaha();
                }
                if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_JENIS_TEMPAT_USAHA.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    loadJenisTempatUsaha(response);
                }
                if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_STATUS_TEMPAT_USAHA.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    loadStatusTempatUsaha(response);
                    viewModel.getJenisJenisTempatUsaha();
                }
                if (StringUtils.isNotBlank(response.type) &&
                        ConfigurationUtils.TAG_NASABAH_FOUND.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setResponseNasabah((ResponseNasabah) response.result);
                    showNasabahResult();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_AMANAH_ACTIVITY.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    pager.setCurrentItem(FORM_USAHA_01);
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_NASABAH_NOT_FOUND.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    showDialogError(response.result.toString());
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_AMANAH_ACTIVITY.equals(response.type)) {
                    nextFormUsaha01();
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UNAUTHORIZED.equals(response.type)) {
                    showDialogError(response.result.toString(), dialog -> {
                        dialog.dismiss();
                        viewModel.doLogoutAgent();
                    });
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }
    private void loadStatusTempatUsaha(Response response) {
        dataStatusTempatUsaha =(Map<String, String>) response.result;
        statusTempatUsahaList = new ArrayList<>(dataStatusTempatUsaha.values());
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idStatusTempatUsaha = MapHelper.getKeyFromValue(dataStatusTempatUsaha, statusTempatUsahaList.get(i)).toString();
                Log.i(TAG, "id Status Tempat Usaha   "+ idStatusTempatUsaha);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
        attachSpinner(spinnerStatusTempatUsaha, statusTempatUsahaList, listener);

    }
    private void loadJenisTempatUsaha(Response response) {
        dataJenisTempatUsaha = (Map<String, String>) response.result;
        jenistTempatUsahaList = new ArrayList<>(dataJenisTempatUsaha.values());
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idTempatUsaha = MapHelper.getKeyFromValue(dataJenisTempatUsaha, jenistTempatUsahaList.get(i)).toString();
                Log.i(TAG, "id tempat usaha   "+ idTempatUsaha);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        attachSpinner(spinnerJenisTempatUsaha, jenistTempatUsahaList, listener);

    }
    private void loadBidangUsaha(Response response) {
        Log.i(TAG, "bind bidang usaha to spinner:   "+ response.result);
        dataMapBidangUsaha = (Map<String, String>) response.result;
        bidangUsahaList = new ArrayList<>(dataMapBidangUsaha.values());
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idBidangUsaha = MapHelper.getKeyFromValue(dataMapBidangUsaha, bidangUsahaList.get(i)).toString();
                Log.i(TAG, "id bidang usaha   "+ idBidangUsaha);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        attachSpinner(spinnerBidangUsaha, bidangUsahaList, listener);

    }
    private void loadJenisKendaraan(Response response) {
        Map<String, String> dataMap = (Map<String, String>) response.result;
        jenisKendaraanList = new ArrayList<>(dataMap.values());
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                agunanIdJenisKendraan = MapHelper.getKeyFromValue(dataMap, jenisKendaraanList.get(i)).toString();
                Log.i(TAG, "agunanIdJenisKendraan   "+ agunanIdJenisKendraan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };
        attachSpinner(agunanSpinnerMerekKendaraan, jenisKendaraanList, listener);
    }

    private void handleOnBackPressed() {
        if (pager.getCurrentItem() == 0) {
            finishActivity();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }
    @Override
    public void onBackPressed() {
        handleOnBackPressed();
    }

    public void nextAmanahSimulasi() {
        dismissKeyboard();
        pager.setCurrentItem(FORM_SIMULASI_AMANAH_02);
    }

    public void nextAgunanForm() {
        dismissKeyboard();
        configureToolbar(getString(R.string.amanah), "Form Agunan");
        pager.setCurrentItem(FORM_AGUNAN_01);
    }
    public void nextAgunanForm02() {
        dismissKeyboard();
        configureToolbar(getString(R.string.amanah), "Form Agunan");
        pager.setCurrentItem(FORM_AGUNAN_02);
    }
    public void nextAgunanKendaraanForm() {
        dismissKeyboard();
        String strTipeKendaraan = "Kendaraan Baru";
        if(tipeKendaraan == FormAmanahAgunanFragment.TIPE_KENDARAAN_LAMA) {
            strTipeKendaraan = "Kendaraan Lama";
        }
        configureToolbar(getString(R.string.amanah), "Form Agunan "+ strTipeKendaraan);
        pager.setCurrentItem(FORM_AGUNAN_02);
    }
    public void nextDataNasabah() {
        dismissKeyboard();
        configureToolbar("Rahn (Gadai Nasabah)", "Data Nasabah");
        pager.setCurrentItem(FORM_DATA_NASABAH);

    }
    public void nextFormUsaha01() {
        dismissKeyboard();
        configureToolbar("Amanah", "Form ItemUsaha");
        pager.setCurrentItem(FORM_USAHA_02);
    }
    public void nextFormUsaha02() {
        dismissKeyboard();
        configureToolbar("Amanah", "Form ItemUsaha");
        pager.setCurrentItem(FORM_USAHA_02);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_amanah;
    }

    private void viewPagerConfigurator() {
//
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FormAmanahNasabah01Fragment.newInstance(this), "");
        adapter.addFragment(FormAmanahNasabah02Fragment.newInstance(this), "");
        adapter.addFragment(FormAmanahAgunanFragment.newInstance(this), "");
        adapter.addFragment(FormAmanahAgunanKendaraanFragment.newInstance(this), "");
        adapter.addFragment(CariPenggunaFragment.newInstance(this, ConfigurationUtils.TAG_AMANAH_ACTIVITY), "");
        adapter.addFragment(FormAmanahUsaha01Fragment.newInstance(this), "");
        adapter.addFragment(FormAmanahUsaha02Fragment.newInstance(this), "");

        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                progressBar.setProgress(position + 1);

                if (position == adapter.getCount() - 1) {
                    progressBar.setProgress(100);
                } else {
                    progressBar.setProgress(viewModel.getProgress(position, adapter.getCount()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void nextToOtp() {

    }

    @Override
    public void nextToSubmitGadai() {

    }

    @Override
    public void initFormGadaiPerhiasanTaksir(RecyclerView listPerhiasan, AppCompatSpinner jenisPerhiasan, EditText jumlahPerhiasan, AppCompatSpinner kadarEmas, EditText beratKotorPerhiasan, EditText beratBersihPerhiasan, EditText keteranganBarang, ImageView gambarPerhiasan, ProgressBar progressPicture, AppCompatButton buttonTambahPerhiasan, AppCompatButton buttonSelanjutnya, View fakeLine) {

    }

    @Override
    public void addItemGadai() {

    }

    @Override
    public void nextToGadaiSimulasi() {

    }
}