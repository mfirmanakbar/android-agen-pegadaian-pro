package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseGadaiActivity;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.custom.GlideRequestListener;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import co.pegadaian.syariah.agen.object.master.Domain;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.KonfirmasiGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiNonPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ResponeSimulasi;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.transaction.view.fragment.CariPenggunaFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.CetakBuktiTransaksiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormGadaiKendaraanTaksirKendaraan;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormGadaiSimulasiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.KonfirmasiGadaiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.OpenGadaiOtpFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.RekeningTujuanFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.SubmitGadaiFragment;
import co.pegadaian.syariah.agen.transaction.view.listener.GadaiKendaraanListener;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.share.ShareFileHelper;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Dell on 3/16/2018.
 */

public class KendaraanActivity extends BaseGadaiActivity implements GadaiKendaraanListener {

    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.button_simpan)
    AppCompatImageView buttonSimpan;
    private ViewPagerAdapter adapter;

    @Inject
    TransactionViewModelFactory viewModelFactory;
    TransactionViewModel viewModel;

    private EditText jumlahLogam;
    private AppCompatSpinner merekKendaraan;
    private String urlKtpImage;
    private List<String> jenisKendaraanList = new ArrayList<>();
    private AppCompatSpinner spinnerTennor;
    private CustomFontLatoRegulerEditText taksiranEditText;
    private CustomFontLatoBoldTextView batasMahrumBih;
    private CustomFontLatoRegulerEditText marhunBihEditText;
    private BigDecimal taksiran;
    private ResponeSimulasi simulasiGadai;
    private List<ItemGadai> itemGadaiList = new ArrayList<>();
    private boolean isHandlingOtp = false;
    private String vehicleType;
    private ImageView gambarDepanKendaraan;
    private ProgressBar progressDepan;
    private ImageView gambarSampingKendaraan;
    private ProgressBar progressSamping;
    private ImageView gambarBelakangKendaraan;
    private ProgressBar progressBelakang;
    private ImageView gambarBpkbStnk;
    private ProgressBar progressBpkbStnk;
    private AppCompatEditText mobilEditText;
    private AppCompatEditText silinderEditText;
    private AppCompatSpinner year;
    private AppCompatEditText policeNumberEditText;
    private AppCompatEditText bpkbEditTex;
    private AppCompatEditText bpkbOwnerEditText;
    private AppCompatEditText stnkNumberEditText;
    private AppCompatEditText nomorRangkaEditText;
    private AppCompatEditText warnaKendaraanEditText;
    private AppCompatEditText tipeMobilEditText;
    private String urlGambarDepan;
    private String urlGambarBelakang;
    private String urlGambarSamping;
    private String urlGambarBpkb;
    private AppCompatEditText hargaPasarEditText;
    private AppCompatEditText nomorMesinEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configViewModel(viewModel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        configureToolbar(getString(R.string.rahn_agen_gadai), getString(R.string.form_logam_mulia));
        configureViewPager();
        initAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setProgress(viewModel.getProgress(0, adapter.getCount()));
        viewModel.transactionBinding();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
        EasyImage.clearConfiguration(this);
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
//                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_RESULT_LIST_GADAI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    if (jenisKendaraanList.size() == 0)
                        loadKendaraan(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_SIMULASI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    simulasiGadai = (ResponeSimulasi) response.result;
                    String minPinjaman = StringHelper.getPriceInRp(simulasiGadai.getMinPinjaman());
                    String maxPinjaman = StringHelper.getPriceInRp(simulasiGadai.getMaxPinjaman());
                    batasMahrumBih.setText(StringHelper.getStringBuilderToString(minPinjaman,
                            " - ",
                            maxPinjaman));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    EasyImage.configuration(KendaraanActivity.this)
                            .setImagesFolderName(ConfigurationUtils.APP_NAME)
                            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                            .setCopyPickedImagesToPublicGalleryAppFolder(true)
                            .setAllowMultiplePickInGallery(true);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GO_TO_CARI_NASABAH.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    pager.setCurrentItem(2);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SCAN_CIF.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    getSearchNasabah().setText(response.result.toString());
                    checkKtpOrCif(getSearchNasabah().getText().toString(), ConfigurationUtils.TAG_CHECK_CIF);
                }
                if (StringUtils.isNotBlank(response.type) &&
                        ConfigurationUtils.TAG_NASABAH_FOUND.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setResponseNasabah((ResponseNasabah) response.result);
                    showNasabahResult();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GADAI_PERHIASAN.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    pager.setCurrentItem(3);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_LIST_OF_BANK.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogList(new ArrayList<>(getDataMap().values()), getResources().getString(R.string.pilih_bank_label),
                            getResources().getString(R.string.cari_bank_label),
                            ConfigurationUtils.TAG_GET_LIST_OF_BANK_SUCCESS, getFragmentManagerBankAccount());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_CHECK_BANK.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setKonfirmasiGadai(new KonfirmasiGadai(getResponseNasabah().getNamaNasabah(),
                            ConfigurationUtils.TAG_GADAI_PERHIASAN_LABEL,
                            taksiranEditText.getText().toString(),
                            marhunBihEditText.getText().toString(),
                            getTenor(),
                            getNamaBankEditText().getText().toString(),
                            getNomorRekeningEditText().getText().toString(),
                            getNamaPemilikEditText().getText().toString()));
                    pager.setCurrentItem(4);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_DIALOG.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    getSimpleAdapter().getFilter().filter((CharSequence) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlKtpImage = response.result != null ? ((File) response.result).getAbsolutePath() : null;
                    loadKtpImage(urlKtpImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    handleSetOtp(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleExpiredOtpCountDownTimer(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleSendOtpSuccess();
                    pager.setCurrentItem(5);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleSendOtpSuccess();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleVerifyOtpSuccess(viewModel.getRessetedTimer());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    ListGadai listGadai = (ListGadai) response.result;
                    getListItemAdapter().set(listGadai.getIndex(), listGadai);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_SUBMIT_GADAI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleOnSuccessSubmitGadai(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SHARE_DOWNLOAD_FILE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    ShareFileHelper.shareStruk((File) response.result, getString(R.string.share_struk_title),
                            getString(R.string.share_struk_message), this);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE.equals(response.type)) {
                    viewModel.uploadImage(((File) response.result), ConfigurationUtils.TAG_SUCCESS_UPLOAD_FRONT_IMAGE);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE.equals(response.type)) {
                    viewModel.uploadImage(((File) response.result), ConfigurationUtils.TAG_SUCCESS_UPLOAD_BACK_IMAGE);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_SIDE_IMAGE.equals(response.type)) {
                    viewModel.uploadImage(((File) response.result), ConfigurationUtils.TAG_SUCCESS_UPLOAD_SIDE_IMAGE);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_BPKB_IMAGE.equals(response.type)) {
                    viewModel.uploadImage(((File) response.result), ConfigurationUtils.TAG_SUCCESS_UPLOAD_BPKB_IMAGE);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_FRONT_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlGambarDepan = response.result.toString();
                    GlideApp.with(this)
                            .load(response.result.toString())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressDepan))
                            .transform(new CircleTransform(this))
                            .into(gambarDepanKendaraan);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_BACK_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlGambarBelakang = response.result.toString();
                    GlideApp.with(this)
                            .load(response.result.toString())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBelakang))
                            .transform(new CircleTransform(this))
                            .into(gambarBelakangKendaraan);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_SIDE_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlGambarSamping = response.result.toString();
                    GlideApp.with(this)
                            .load(response.result.toString())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressSamping))
                            .transform(new CircleTransform(this))
                            .into(gambarSampingKendaraan);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_BPKB_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlGambarBpkb = response.result.toString();
                    GlideApp.with(this)
                            .load(response.result.toString())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBpkbStnk))
                            .transform(new CircleTransform(this))
                            .into(gambarBpkbStnk);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    viewModel.clearData();
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UNAUTHORIZED.equals(response.type)) {
                    showDialogError(response.result.toString(), dialog -> {
                        dialog.dismiss();
                        viewModel.doLogoutAgent();
                    });
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_FAILED_GO_TO_CARI_NASABAH.equals(response.type)) {
                    showDialogError(getString(R.string.warning_input_mahrumbih));
                } else {
                    if (isHandlingOtp) {
                        handleFailedSendOtp();
                    }
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    private void handleOnSuccessSubmitGadai(Response response) {
        setDomain((Domain) response.result);
        loadBuktiTransaksi();
        showDialogSuccessNoCancel(getString(R.string.success_submit_gadai),
                dialog -> pager.setCurrentItem(7));
    }

    private void loadKendaraan(Response response) {
        jenisKendaraanList = new ArrayList<>();
        jenisKendaraanList.addAll((List<String>) response.result);
        attachSpinner(this.merekKendaraan, jenisKendaraanList);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_gadai_perhiasan;
    }

    private void configureViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FormGadaiKendaraanTaksirKendaraan.newInstance(this), "");
        adapter.addFragment(FormGadaiSimulasiFragment.newInstance(this), "");
        adapter.addFragment(CariPenggunaFragment.newInstance(this, ConfigurationUtils.TAG_GADAI_PERHIASAN),
                "");
        adapter.addFragment(RekeningTujuanFragment.newInstance(this),
                "");
        adapter.addFragment(KonfirmasiGadaiFragment.newInstance(this),
                "");
        adapter.addFragment(OpenGadaiOtpFragment.newInstance(this),
                "");
        adapter.addFragment(SubmitGadaiFragment.newInstance(this),
                "");
        adapter.addFragment(CetakBuktiTransaksiFragment.newInstance(this),
                "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(onPageChangeListener());
    }

    @NonNull
    private ViewPager.OnPageChangeListener onPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                progressBar.setProgress(position + 1);
                isHandlingOtp = false;
                buttonSimpan.setVisibility(View.GONE);
                if (position == 1) {
                    if (taksiranEditText != null)
                        taksiranEditText.setFocusableInTouchMode(true);
                    taksiranEditText.setFocusable(true);

                } else if (position == 4) {
                    isHandlingOtp = true;
                    initConfirmationValue();
                } else if (position == 5) {
                    isHandlingOtp = true;
                } else if (position == 7) {
                    buttonSimpan.setVisibility(View.VISIBLE);
                    startBluetoothService();
                }

                //handle progressbar
                if (position == adapter.getCount() - 1) {
                    progressBar.setProgress(100);
                } else {
                    progressBar.setProgress(viewModel.getProgress(position, adapter.getCount()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    public void addItemGadai() {
        //TODO maintain unused code
    }

    @Override
    public void nextToGadaiSimulasi() {
        dismissKeyboard();
        if (StringUtils.isNotBlank(urlGambarDepan) &&
                StringUtils.isNotBlank(urlGambarBelakang) &&
                StringUtils.isNotBlank(urlGambarSamping) &&
                StringUtils.isNotBlank(urlGambarBpkb)) {
            taksiranEditText.setFocusable(true);
            pager.setCurrentItem(1);
        } else {
            showDialogError(getString(R.string.warning_upload_image_photo));
        }
    }

    @Override
    public void initFormGadaiSimulasi(AppCompatSpinner spinnerTennor,
                                      CustomFontLatoRegulerEditText taksiranEditText,
                                      CustomFontLatoBoldTextView batasMahrumBih,
                                      CustomFontLatoRegulerEditText marhunBihEditText) {
        this.spinnerTennor = spinnerTennor;
        this.taksiranEditText = taksiranEditText;
        this.batasMahrumBih = batasMahrumBih;
        this.marhunBihEditText = marhunBihEditText;
    }

    @Override
    public void initSpinnerTenorOnSelectedListener(int index) {
        if (StringUtils.isNotBlank(taksiranEditText.getText().toString()) &&
                !ConfigurationUtils.TENOR[index].equals("-")) {
            setTenor(ConfigurationUtils.TENOR[index]);
            viewModel.simulasiKendaraan(new RequestSimulasiNonPerhiasan(Integer.valueOf(getTenor()),
                    (vehicleType.equals(ConfigurationUtils.TAG_MOBIL)) ?
                            ConfigurationUtils.TAG_KENDARAAN_MESIN_MOBIL : ConfigurationUtils.TAG_KENDARAAN_SEPEDA_MOTOR,
                    BigDecimal.valueOf(Double.valueOf(taksiranEditText.getText().toString()))));
        }
    }

    @Override
    public void nextToCariNasabah() {
        dismissKeyboard();
        if (StringUtils.isNotBlank(batasMahrumBih.getText().toString()))
            viewModel.nextToCariNasabah(BigDecimal.valueOf(Double.valueOf(marhunBihEditText.getText().toString())),
                    simulasiGadai.getMinPinjaman(),
                    simulasiGadai.getMaxPinjaman());
    }

    @Override
    public void nextToKonfirmasiTransaksi() {
        dismissKeyboard();
        RequestCheckBank requestCheckBank = new RequestCheckBank(getIdBank(), getNamaPemilik().toUpperCase(),
                getNorekPemilik());
        viewModel.checkBank(requestCheckBank);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleOnBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        handleOnBackPressed();
    }

    private void handleOnBackPressed() {
        if (pager.getCurrentItem() == 0) {
            finishActivity();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void nextToOtp() {
        if (!isAlreadySetTimer()) {
            circleProgressBar.hide();
            handleNextToOtp();
        } else {
            pager.setCurrentItem(5);
        }
    }

    @Override
    public void nextToSubmitGadai() {
        if (!isAlreadyVerified()) {
            handleVerifyOtp();
        } else {
            setRequestSubmitGadai();
            pager.setCurrentItem(6);
        }
    }

    @Override
    protected void setRequestSubmitGadai() {
        ListGadai listGadai = new ListGadai(BigDecimal.valueOf(Double.valueOf(hargaPasarEditText.getText().toString())),
                silinderEditText.getText().toString(),
                "",
                "",
                merekKendaraan.getSelectedItem().toString().toUpperCase(),
                tipeMobilEditText.getText().toString().toUpperCase(),
                bpkbOwnerEditText.getText().toString().toUpperCase(),
                bpkbEditTex.getText().toString().toUpperCase(),
                nomorMesinEditText.getText().toString().toUpperCase(),
                policeNumberEditText.getText().toString().toUpperCase(),
                nomorRangkaEditText.getText().toString().toUpperCase(),
                stnkNumberEditText.getText().toString().toUpperCase(),
                "",
                year.getSelectedItem().toString(),
                (vehicleType.equals(ConfigurationUtils.TAG_MOBIL)) ?
                        ConfigurationUtils.TAG_KENDARAAN_MESIN_MOBIL : ConfigurationUtils.TAG_KENDARAAN_SEPEDA_MOTOR,
                warnaKendaraanEditText.getText().toString().toUpperCase(),
                Integer.valueOf(taksiranEditText.getText().toString()),
                urlGambarDepan,
                urlGambarSamping,
                urlGambarBelakang,
                urlGambarBpkb);
        setRequestSubmitGadai(new RequestSubmitGadai(getResponseNasabah().getCif(),
                getResponseNasabah().getIbuKandung(),
                getResponseNasabah().getKelurahan().getId(),
                getResponseNasabah().getJalan(),
                getResponseNasabah().getJenisKelamin(),
                getResponseNasabah().getKewarganegaraan(),
                getNorekPemilik(),
                ConfigurationUtils.TAG_AGENT,
                getIdBank(),
                ConfigurationUtils.TAG_KODE_PRODUK_RAHN,
                getResponseNasabah().getNoIdentitas(),
                getResponseNasabah().getNamaNasabah().toUpperCase(),
                getKonfirmasiGadai().getNamaPemilikRekening(),
                getResponseNasabah().getNoHp(),
                "12345",
                "",
                getResponseNasabah().getStatusKawin(),
                0L,
                getResponseNasabah().getTglLahir(),
                System.currentTimeMillis(),
                getResponseNasabah().getTempatLahir(),
                "10",
                listGadai,
                Integer.valueOf(getTenor()),
                BigDecimal.valueOf(Double.valueOf(getKonfirmasiGadai().getMarhunBih())),
                getUrlKtp()));
        viewModel.setRequestSubmitGadai(getRequestSubmitGadai());
        viewModel.submitGadai();
    }

    @OnClick(R.id.button_simpan)
    public void onViewClicked() {
        viewModel.downloadFile(getDomain().getUrlPdf(), false);
    }

    @Override
    public void initGadaiKendaraanComponent(AppCompatSpinner merekKendaraan,
                                            ImageView gambarDepanKendaraan,
                                            ProgressBar progressDepan,
                                            ImageView gambarSampingKendaraan,
                                            ProgressBar progressSamping,
                                            ImageView gambarBelakangKendaraan,
                                            ProgressBar progressBelakang,
                                            ImageView gambarBpkbStnk,
                                            ProgressBar progressBpkbStnk,
                                            AppCompatEditText mobilEditText,
                                            AppCompatEditText silinderEditText,
                                            AppCompatSpinner year,
                                            AppCompatEditText policeNumberEditText,
                                            AppCompatEditText bpkbEditText,
                                            AppCompatEditText bpkbOwnerEditText,
                                            AppCompatEditText stnkNumberEditText,
                                            AppCompatEditText nomorRangkaEditText,
                                            AppCompatEditText nomorMesinEditText,
                                            AppCompatEditText warnaKendaraanEditText,
                                            AppCompatEditText hargaPasarEditText) {
        this.merekKendaraan = merekKendaraan;
        this.gambarDepanKendaraan = gambarDepanKendaraan;
        this.progressDepan = progressDepan;
        this.gambarSampingKendaraan = gambarSampingKendaraan;
        this.progressSamping = progressSamping;
        this.gambarBelakangKendaraan = gambarBelakangKendaraan;
        this.progressBelakang = progressBelakang;
        this.gambarBpkbStnk = gambarBpkbStnk;
        this.progressBpkbStnk = progressBpkbStnk;
        this.mobilEditText = mobilEditText;
        this.silinderEditText = silinderEditText;
        this.year = year;
        this.policeNumberEditText = policeNumberEditText;
        this.bpkbEditTex = bpkbEditText;
        this.bpkbOwnerEditText = bpkbOwnerEditText;
        this.stnkNumberEditText = stnkNumberEditText;
        this.nomorRangkaEditText = nomorRangkaEditText;
        this.nomorMesinEditText = nomorMesinEditText;
        this.warnaKendaraanEditText = warnaKendaraanEditText;
        this.hargaPasarEditText = hargaPasarEditText;
        viewModel.getKendaraan(vehicleType);
        attachSpinner(year, viewModel.getListOfYears());
        gambarDepanKendaraan.setOnClickListener(view -> EasyImage.openCamera(KendaraanActivity.this, 4));
        gambarSampingKendaraan.setOnClickListener(view -> EasyImage.openCamera(KendaraanActivity.this, 5));
        gambarBelakangKendaraan.setOnClickListener(view -> EasyImage.openCamera(KendaraanActivity.this, 6));
        gambarBpkbStnk.setOnClickListener(view -> EasyImage.openCamera(KendaraanActivity.this, 7));
    }

    @Override
    public void setVehicleType(String type) {
        vehicleType = type;
    }

    @Override
    public void loadKendaraan() {
        jenisKendaraanList = new ArrayList<>();
        viewModel.getKendaraan(vehicleType);
    }
}