package co.pegadaian.syariah.agen.object.userprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 11/03/2018.
 */

public class RequestChangePin {

    @SerializedName("new_pin")
    @Expose private String newPin;
    @SerializedName("old_pin")
    @Expose private String oldPin;
    @SerializedName("confirm_pin")
    @Expose private String confirmationPin;

    public RequestChangePin(String oldPin, String newPin, String confirmationPin) {
        this.oldPin = oldPin;
        this.newPin = newPin;
        this.confirmationPin = confirmationPin;
    }

    public String getNewPin() {
        return newPin;
    }

    public void setNewPin(String newPin) {
        this.newPin = newPin;
    }

    public String getOldPin() {
        return oldPin;
    }

    public void setOldPin(String oldPin) {
        this.oldPin = oldPin;
    }

    public String getConfirmationPin() {
        return confirmationPin;
    }

    public void setConfirmationPin(String confirmationPin) {
        this.confirmationPin = confirmationPin;
    }
}
