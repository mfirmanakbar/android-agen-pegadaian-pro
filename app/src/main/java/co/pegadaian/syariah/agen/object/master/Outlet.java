package co.pegadaian.syariah.agen.object.master;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/7/2018.
 */

public class Outlet extends AbstractItem<Outlet, Outlet.ViewHolder> {

    @SerializedName("name")
    @Expose
    private String nameOutlet;
    @SerializedName("address")
    @Expose
    private String addressOutlet;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("distance")
    @Expose
    private String distanceOutlet;
    @SerializedName("kode_cabang")
    @Expose
    private String kodeCabang;

    public Outlet() {
    }

    public Outlet(String nameOutlet,
                  String addressOutlet,
                  String distanceOutlet) {
        this.nameOutlet = nameOutlet;
        this.addressOutlet = addressOutlet;
        this.distanceOutlet = distanceOutlet;
    }

    public String getNameOutlet() {
        return nameOutlet;
    }

    public String getAddressOutlet() {
        return addressOutlet;
    }

    public String getDistanceOutlet() {
        return distanceOutlet;
    }

    public String getRoundedDistanceOutlet() {
        return StringHelper.getStringBuilderToString(String.valueOf(Math.round(Double.valueOf(getDistanceOutlet()))),
                " KM");
    }

    public void setNameOutlet(String nameOutlet) {
        this.nameOutlet = nameOutlet;
    }

    public void setAddressOutlet(String addressOutlet) {
        this.addressOutlet = addressOutlet;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setDistanceOutlet(String distanceOutlet) {
        this.distanceOutlet = distanceOutlet;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.outlet_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_outlet;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.location_name)
        AppCompatTextView locationName;
        @BindView(R.id.location_address)
        AppCompatTextView locationAddress;
        @BindView(R.id.distance_outlet_location)
        AppCompatTextView distanceOutletLocation;

        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.locationName.setText(getNameOutlet());
        holder.locationName.setTypeface(getLatoBold(holder));
        holder.locationAddress.setText(getAddressOutlet());
        holder.locationAddress.setTypeface(getLatoRegular(holder));
        holder.distanceOutletLocation.setText(getRoundedDistanceOutlet());
        holder.distanceOutletLocation.setTypeface(getLatoRegular(holder));
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.locationName.setText(null);
        holder.locationAddress.setText(null);
        holder.distanceOutletLocation.setText(null);
    }

    public Typeface getLatoRegular(ViewHolder holder) {
        return Typeface.createFromAsset(holder.context.getAssets(),
                "fonts/Lato-Regular.ttf");
    }

    public Typeface getLatoBold(ViewHolder holder) {
        return Typeface.createFromAsset(holder.context.getAssets(),
                "fonts/Lato-Bold.ttf");
    }


}