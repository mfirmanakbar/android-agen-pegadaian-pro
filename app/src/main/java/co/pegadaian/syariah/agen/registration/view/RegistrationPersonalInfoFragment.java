package co.pegadaian.syariah.agen.registration.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class RegistrationPersonalInfoFragment extends BaseFragment {

    @BindView(R.id.name_edit_label)
    AppCompatTextView nameEditLabel;
    @NotEmpty
    @BindView(R.id.name_edit_text)
    AppCompatEditText nameEditText;
    @BindView(R.id.front_image_text)
    AppCompatTextView frontImageText;
    @BindView(R.id.back_image_text)
    AppCompatTextView backImageText;
    @BindView(R.id.upload_image_text)
    AppCompatTextView uploadImageText;
    @BindView(R.id.front_image)
    AppCompatImageView frontImage;
    @BindView(R.id.front_image_progress_bar)
    ProgressBar frontImageProgressBar;
    @BindView(R.id.back_image)
    AppCompatImageView backImage;
    @BindView(R.id.back_image_progress_bar)
    ProgressBar backImageProgressBar;
    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.email_edit_label)
    AppCompatTextView emailEditLabel;
    @NotEmpty
    @Email
    @BindView(R.id.email_edit_text)
    AppCompatEditText emailEditText;
    @BindView(R.id.hp_edit_label)
    AppCompatTextView hpEditLabel;
    @NotEmpty
    @BindView(R.id.hp_edit_text)
    AppCompatEditText hpEditText;
    @BindView(R.id.ktp_edit_label)
    AppCompatTextView ktpEditLabel;
    @BindView(R.id.province_edit_label)
    AppCompatTextView provinceEditLabel;
    @NotEmpty
    @BindView(R.id.province_edit_text)
    AppCompatEditText provinceEditText;
    @NotEmpty
    @BindView(R.id.ktp_edit_text)
    AppCompatEditText ktpEditText;
    @BindView(R.id.city_edit_label)
    AppCompatTextView cityEditLabel;
    @NotEmpty
    @BindView(R.id.city_edit_text)
    AppCompatEditText cityEditText;
    @BindView(R.id.kecamatan_edit_label)
    AppCompatTextView kecamatanEditLabel;
    @NotEmpty
    @BindView(R.id.kecamatan_edit_text)
    AppCompatEditText kecamatanEditText;
    @BindView(R.id.kelurahan_edit_label)
    AppCompatTextView kelurahanEditLabel;
    @NotEmpty
    @BindView(R.id.kelurahan_edit_text)
    AppCompatEditText kelurahanEditText;
    @NotEmpty
    @BindView(R.id.address_edit_label)
    AppCompatTextView addressEditLabel;
    @BindView(R.id.address_edit_text)
    AppCompatEditText addressEditText;

    private Unbinder unbinder;
    private static RegistrationActivity registrationActivity;

    public static RegistrationPersonalInfoFragment newInstance(RegistrationActivity activity) {
        RegistrationPersonalInfoFragment registrationPersonalInfoFragment = new RegistrationPersonalInfoFragment();
        registrationActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_personal_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        handleOnTouchListener();
        return view;
    }

    private void handleOnTouchListener() {
        provinceEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showProvinceDialog();
                return true;
            }
            return false;
        });
        cityEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showCityDialog();
                return true;
            }
            return false;
        });
        kecamatanEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showKecamatanDialog();
                return true;
            }
            return false;
        });
        kelurahanEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showKelurahanDialog();
                return true;
            }
            return false;
        });
    }

    private void configureFont() {
        nameEditLabel.setTypeface(getLatoBold());
        nameEditText.setTypeface(getLatoRegular());
        frontImageText.setTypeface(getLatoRegular());
        backImageText.setTypeface(getLatoRegular());
        uploadImageText.setTypeface(getLatoRegular());
        goToNextButton.setTypeface(getRonniaRegular());
        emailEditLabel.setTypeface(getLatoBold());
        emailEditText.setTypeface(getLatoRegular());
        hpEditLabel.setTypeface(getLatoBold());
        hpEditText.setTypeface(getLatoRegular());
        ktpEditLabel.setTypeface(getLatoBold());
        ktpEditText.setTypeface(getLatoRegular());
        addressEditLabel.setTypeface(getLatoBold());
        addressEditText.setTypeface(getLatoRegular());
        provinceEditLabel.setTypeface(getLatoBold());
        provinceEditText.setTypeface(getLatoRegular());
        cityEditLabel.setTypeface(getLatoBold());
        cityEditText.setTypeface(getLatoRegular());
        kecamatanEditLabel.setTypeface(getLatoBold());
        kecamatanEditText.setTypeface(getLatoRegular());
        kelurahanEditLabel.setTypeface(getLatoBold());
        kelurahanEditText.setTypeface(getLatoRegular());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initKtpImagePersonalInfo(frontImage, frontImageProgressBar);
        registrationActivity.initSelfieImagePersonalInfo(backImage, backImageProgressBar);
        registrationActivity.initInputanPersonalInfo(provinceEditText, cityEditText, kecamatanEditText,
                kelurahanEditText, hpEditText, addressEditText, emailEditText, nameEditText, ktpEditText,
                getActivity().getFragmentManager());
    }

    @OnClick(R.id.go_to_next_button)
    public void onViewClicked() {
        getValidator().validate();
    }

    @OnClick({R.id.placeholder_front_image, R.id.front_image_text, R.id.front_image,
            R.id.placeholder_back_image, R.id.back_image_text, R.id.back_image,
            R.id.province_edit_text})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.placeholder_front_image:
                registrationActivity.onKtpImageClicked();
                break;
            case R.id.front_image_text:
                registrationActivity.onKtpImageClicked();
                break;
            case R.id.front_image:
                registrationActivity.onKtpImageClicked();
                break;
            case R.id.placeholder_back_image:
                registrationActivity.onSelfieImageClicked();
                break;
            case R.id.back_image_text:
                registrationActivity.onSelfieImageClicked();
                break;
            case R.id.back_image:
                registrationActivity.onSelfieImageClicked();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (ktpEditText.getText().toString().length() == 16) {
            registrationActivity.onNext();
        } else {
            ktpEditText.setError(getResources().getString(R.string.error_not_valid_ktp));
        }
    }
}