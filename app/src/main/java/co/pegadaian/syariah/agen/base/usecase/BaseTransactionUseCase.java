package co.pegadaian.syariah.agen.base.usecase;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.pegadaian.syariah.agen.base.repository.BaseTransactionRepository;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSendOtpOpenGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.transaction.domain.model.TransactionRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Dell on 3/18/2018.
 */

public class BaseTransactionUseCase extends BaseAgentUseCase {

    public BaseTransactionUseCase(BaseTransactionRepository repository,
                                  Compressor compressor) {
        super(repository, compressor);
    }

    public BaseTransactionUseCase(BaseTransactionRepository repository) {
        super(repository);
    }

    public boolean checkKtpOrCifLengt(String ktpOrCif) {
        return ktpOrCif.length() >= 10;
    }

    public Observable<Object> checkKtpOrCif(String ktpOrCif, String type) {
        if (ConfigurationUtils.TAG_CHECK_KTP.equals(type)) {
            return ((BaseTransactionRepository) getRepository()).getNasabahByKtp(ktpOrCif);
        } else if (ConfigurationUtils.TAG_CHECK_CIF.equals(type)) {
            return ((BaseTransactionRepository) getRepository()).getNasabahByCif(ktpOrCif);
        } else {
            return null;
        }
    }

    public boolean minimumAmount(String nominalAwal) {
        BigDecimal nominal = new BigDecimal(nominalAwal);
        return nominal.compareTo(ConfigurationUtils.TAG_MINIMUM_NOMINAL_AWAL) >= 0;
    }

    public Observable<Object> mulaiSimulasi(String nominalAwal, String type) {
        if (ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)) {
            return ((BaseTransactionRepository) getRepository()).mulaiSimulasi(new BigDecimal(nominalAwal));
        } else if (ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)) {
            return ((BaseTransactionRepository) getRepository()).validasiSaldo(new BigDecimal(nominalAwal));
        } else if (ConfigurationUtils.TAG_KONFIRMASI_PEMBUKAAN_TABUNGAN.equals(type)) {
            return ((BaseTransactionRepository) getRepository()).konfirmasiTabungan(new BigDecimal(nominalAwal));
        } else {
            return null;
        }
    }

    public Observable<Object> getStatusPernikahan() {
        return ((BaseTransactionRepository) getRepository()).getStatusPernikahan();
    }

    public boolean checkPin(String pin) {
        if (pin.length() == 6) {
            return true;
        } else {
            return false;
        }
    }

    public Observable<Object> validasiPin(String pin) {
        return ((BaseTransactionRepository) getRepository()).validasiPin(pin);
    }

    public Observable<Object> getListOfBank() {
        return ((BaseTransactionRepository) getRepository()).getListOfBank();
    }

    public boolean checkBatasMahrumBihValue(BigDecimal mahrumBihValue,
                                            BigDecimal batasMinMahrumBihValue,
                                            BigDecimal batasAtasMahrumBihValue) {
        return (mahrumBihValue.compareTo(batasMinMahrumBihValue) >= 0 &&
                mahrumBihValue.compareTo(batasAtasMahrumBihValue) <= 0);
    }

    public Observable<Object> checkBank(RequestCheckBank requestCheckBank) {
        return ((TransactionRepository) getRepository()).checkBank(requestCheckBank);
    }

    public Observable<Object> sendOtpOpenGadai(RequestSendOtpOpenGadai requestSendOtpOpenGadai) {
        return ((TransactionRepository) getRepository()).sendOtpOpenGadai(requestSendOtpOpenGadai);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return ((TransactionRepository) getRepository()).verifyOtp(requestVerifyOtp);
    }

    public Observable<Long> timer() {
        long count = getRepository().getPreferenceManager().getCurrentOtpCountdown() + 1;
        return Observable.interval(1, TimeUnit.SECONDS)
                .take(count)
                .map(aLong -> {
                    long time = count - aLong;
                    getRepository().getPreferenceManager().setCurrentOtpCountdown(time);
                    return time - 1;
                });
    }

    public void resetOtpCountDown() {
        ((TransactionRepository) getRepository()).resetOtpCountDown();
    }

    public Long getCurrentOtpCountDown() {
        return ((TransactionRepository) getRepository()).getCurrentOtpCountDown();
    }

    public Observable<Object> submitGadai(RequestSubmitGadai requestSubmitGadai) {
        if (ConfigurationUtils.TAG_KANTONG_PERHIASAN_PLG.equals(requestSubmitGadai.getListGadai().get(0).getTipeJaminan())) {
            return ((TransactionRepository) getRepository()).submitGadaiPerhiasan(requestSubmitGadai);
        } else if (ConfigurationUtils.TAG_KANTONG_LOGAM_MULIA.equals(requestSubmitGadai.getListGadai().get(0).getTipeJaminan())) {
            return ((TransactionRepository) getRepository()).submitGadaiLogamMulia(requestSubmitGadai);
        } else {
            return null;
        }
    }

    public Observable<Object> uploadImage(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        return ((TransactionRepository) getRepository()).uploadImageRegistration(body);
    }

    public Observable<Response<ResponseBody>> download(String url) {
        return ((TransactionRepository) getRepository()).download(url);
    }

    public List<ListGadai> getListItemGadai(FastItemAdapter<ItemGadaiAdapter> itemGadaiAdapter) {
        List<ListGadai> listGadai = new ArrayList<>();
        for (int index = 0; index < itemGadaiAdapter.getAdapterItems().size(); index++) {
            ItemGadai itemGadai = itemGadaiAdapter.getAdapterItem(index).getItemGadai();
            listGadai.add(new ListGadai(itemGadai.getKeterangan(),
                    itemGadai.getType(),
                    itemGadai.getBeratBersih(),
                    itemGadai.getBeratKotor(),
                    itemGadai.getCategory(),
                    itemGadai.getJumlah(),
                    itemGadai.getKarat(),
                    itemGadai.getImageFile(),
                    index));
        }
        return listGadai;
    }
}