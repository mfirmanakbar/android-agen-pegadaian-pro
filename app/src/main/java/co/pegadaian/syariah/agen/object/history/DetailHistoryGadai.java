package co.pegadaian.syariah.agen.object.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by T460s on 3/21/2018.
 */
@Parcel(Parcel.Serialization.BEAN)
public class DetailHistoryGadai implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("history-rahnId")
    @Expose
    private String historyRahnId;
    @SerializedName("tgl_transaksi")
    @Expose
    private Integer tglTransaksi;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("nomor_akad")
    @Expose
    private Integer nomorAkad;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("merk")
    @Expose
    private String merk;
    @SerializedName("market_price")
    @Expose
    private Integer marketPrice;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("tenor")
    @Expose
    private Integer tenor;
    @SerializedName("taksiran_price")
    @Expose
    private Integer taksiranPrice;
    @SerializedName("pinjaman_price")
    @Expose
    private Integer pinjamanPrice;
    @SerializedName("url_cetak")
    @Expose
    private String urlCetak;
    @SerializedName("url_pdf")
    @Expose
    private String urlPdf;
    @SerializedName("barang_jaminan")
    @Expose
    private String barangJaminan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHistoryRahnId() {
        return historyRahnId;
    }

    public void setHistoryRahnId(String historyRahnId) {
        this.historyRahnId = historyRahnId;
    }

    public Integer getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Integer tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public Integer getNomorAkad() {
        return nomorAkad;
    }

    public void setNomorAkad(Integer nomorAkad) {
        this.nomorAkad = nomorAkad;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public Integer getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Integer marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

    public Integer getTaksiranPrice() {
        return taksiranPrice;
    }

    public void setTaksiranPrice(Integer taksiranPrice) {
        this.taksiranPrice = taksiranPrice;
    }

    public Integer getPinjamanPrice() {
        return pinjamanPrice;
    }

    public void setPinjamanPrice(Integer pinjamanPrice) {
        this.pinjamanPrice = pinjamanPrice;
    }

    public String getUrlCetak() {
        return urlCetak;
    }

    public void setUrlCetak(String urlCetak) {
        this.urlCetak = urlCetak;
    }

    public String getUrlPdf() {
        return urlPdf;
    }

    public void setUrlPdf(String urlPdf) {
        this.urlPdf = urlPdf;
    }

    public String getBarangJaminan() {
        return barangJaminan;
    }

    public void setBarangJaminan(String barangJaminan) {
        this.barangJaminan = barangJaminan;
    }

}
