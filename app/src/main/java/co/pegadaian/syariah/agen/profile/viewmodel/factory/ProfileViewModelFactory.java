package co.pegadaian.syariah.agen.profile.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.profile.domain.interactor.AgentUseCase;
import co.pegadaian.syariah.agen.profile.viewmodel.AgentViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class ProfileViewModelFactory extends BaseViewModelFactory {

    private final AgentUseCase useCase;
    private final RealmConfigurator realmConfigurator;

    public ProfileViewModelFactory(SchedulersFacade schedulersFacade,
                                   JsonParser jsonParser,
                                   RxBus rxBus,
                                   PreferenceManager preferenceManager,
                                   AgentUseCase useCase,
                                   RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.useCase = useCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AgentViewModel.class)) {
            return (T) new AgentViewModel(getSchedulersFacade(),
                    getJsonParser(),
                    getRxBus(),
                    getPreferenceManager(),
                    useCase,
                    realmConfigurator);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
