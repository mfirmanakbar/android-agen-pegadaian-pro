package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;


public class FormAmanahAgunanFragment extends BaseFragment {

    public static final int TIPE_KENDARAAN_BARU = 1;
    public static final int TIPE_KENDARAAN_LAMA = 0;

    private static AmanahActivity amanahActivity;
    Unbinder unbinder;

    public static FormAmanahAgunanFragment newInstance(AmanahActivity activity) {
        FormAmanahAgunanFragment formAmanahAgunanFragment = new FormAmanahAgunanFragment();
        amanahActivity = activity;
        return formAmanahAgunanFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_amanah_agunan, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        amanahActivity.initFormAgunan01(TIPE_KENDARAAN_LAMA);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnClick({R.id.btn_agunan_bekas, R.id.btn_agunan_baru})
    public void onClickButton(View view) {
        switch (view.getId()) {
            case R.id.btn_agunan_baru:
                amanahActivity.initFormAgunan01(TIPE_KENDARAAN_BARU);
                amanahActivity.nextAgunanKendaraanForm();
                amanahActivity.updateAgunanKondisiKendaraan("BARU");
                break;
            case R.id.btn_agunan_bekas:
                amanahActivity.initFormAgunan01(TIPE_KENDARAAN_LAMA);
                amanahActivity.nextAgunanKendaraanForm();
                amanahActivity.updateAgunanKondisiKendaraan("LAMA");

                break;
        }

    }

    @Override
    public void onValidationSucceeded() {

    }
}
