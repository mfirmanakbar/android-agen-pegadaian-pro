package co.pegadaian.syariah.agen.tabungan.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.BayarRahnActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class KonfirmasiPembayaranFragment extends BaseFragment {


    @BindView(R.id.data_nasabah)
    RecyclerView dataNasabah;
    @BindView(R.id.data_transaksi)
    RecyclerView dataTransaksi;
    @BindView(R.id.label_total_transaksi)
    CustomFontLatoRegularTextView labelTotalTransaksi;
    @BindView(R.id.total_transaksi)
    CustomFontLatoBoldTextView totalTransaksi;
    private Unbinder unbinder;
    private static BayarRahnActivity bayarRahnActivity;

    public static KonfirmasiPembayaranFragment newInstance(BayarRahnActivity activity) {
        KonfirmasiPembayaranFragment konfirmasiPembayaranFragment = new KonfirmasiPembayaranFragment();
        bayarRahnActivity = activity;
        return konfirmasiPembayaranFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pembayaran_konfirmasi_transaksi, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        bayarRahnActivity.initComponentKonfirmasiPembayaran(dataNasabah, dataTransaksi,
                labelTotalTransaksi, totalTransaksi);
    }

    @Override
    public void onValidationSucceeded() {
    }

    @OnClick(R.id.button_konfirmasi)
    public void onViewClicked() {
        bayarRahnActivity.initDialogPin();
    }
}