package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.transaction.view.activity.ScanQrCodeActivity;
import co.pegadaian.syariah.agen.di.module.GadaiModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/16/2018.
 */

@Module
public abstract class ScanQrCodeBuilderModule {

    @ContributesAndroidInjector(modules = {GadaiModule.class})
    abstract ScanQrCodeActivity bindActivity();
}