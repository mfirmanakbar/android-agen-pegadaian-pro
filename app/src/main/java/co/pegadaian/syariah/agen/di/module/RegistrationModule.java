package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.registration.domain.interactors.RegistrationUseCase;
import co.pegadaian.syariah.agen.registration.domain.model.RegistrationRepository;
import co.pegadaian.syariah.agen.registration.viewmodel.factory.RegistrationViewModelFactory;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public class RegistrationModule {

    @Provides
    RegistrationRepository provideRegistrationRepository(PreferenceManager preferenceManager) {
        return new RegistrationRepository(preferenceManager, RestApiAdapter.getRestApi());
    }

    @Provides
    RegistrationViewModelFactory provideRegistrationViewModelFactory(RegistrationUseCase registrationUseCase,
                                                                     SchedulersFacade schedulersFacade,
                                                                     @Named("JsonParser") JsonParser jsonParser,
                                                                     @Named("RxBus") RxBus rxBus,
                                                                     PreferenceManager preferenceManager) {
        return new RegistrationViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, registrationUseCase);
    }
}
