package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.listeners.ClickEventHook;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseGadaiActivity;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.custom.GlideRequestListener;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import co.pegadaian.syariah.agen.object.master.Domain;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.KonfirmasiGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ListGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.ResponeSimulasi;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.transaction.view.fragment.CariPenggunaFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.CetakBuktiTransaksiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormGadaiLogamMuliaTaksirFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.FormGadaiSimulasiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.KonfirmasiGadaiFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.OpenGadaiOtpFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.RekeningTujuanFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.SubmitGadaiFragment;
import co.pegadaian.syariah.agen.transaction.view.listener.GadaiLogamMuliaListener;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.share.ShareFileHelper;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Dell on 3/16/2018.
 */

public class LogamMuliaActivity extends BaseGadaiActivity implements GadaiLogamMuliaListener {

    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.button_simpan)
    AppCompatImageView buttonSimpan;
    private ViewPagerAdapter adapter;

    @Inject
    TransactionViewModelFactory viewModelFactory;
    TransactionViewModel viewModel;

    private RecyclerView listKeping;
    private EditText jumlahLogam;
    private AppCompatSpinner kepingLogam;
    private EditText keteranganBarang;
    private ImageView gambarLogam;
    private ProgressBar progressPicture;
    private AppCompatButton buttonTambahkanLogam;
    private AppCompatButton buttonSelanjutnya;
    private View fakeLine;
    private File imageFile;
    private String urlKtpImage;
    private List<String> jenisKepingList = new ArrayList<>();
    private AppCompatSpinner spinnerTennor;
    private CustomFontLatoRegulerEditText taksiranEditText;
    private CustomFontLatoBoldTextView batasMahrumBih;
    private CustomFontLatoRegulerEditText marhunBihEditText;
    private BigDecimal taksiran;
    private ResponeSimulasi simulasiLogamMulia;
    private List<ItemGadai> itemGadaiList = new ArrayList<>();
    private boolean isHandlingOtp = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configViewModel(viewModel);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        configureToolbar(getString(R.string.rahn_agen_gadai), getString(R.string.form_logam_mulia));
        configureViewPager();
        configureItemAdapter(itemGadaiAdapter);
        initAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setProgress(viewModel.getProgress(0, adapter.getCount()));
        viewModel.transactionBinding();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
        EasyImage.clearConfiguration(this);
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
//                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_RESULT_LIST_GADAI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    if (jenisKepingList.size() == 0)
                        loadKepingLogam(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    imageFile = response.result != null ? ((File) response.result) : null;
                    GlideApp.with(this)
                            .load(imageFile.getAbsolutePath())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressPicture))
                            .transform(new CircleTransform(this))
                            .into(gambarLogam);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_TAKSIR_PERHIASAN.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    itemGadaiList.add(new ItemGadai(Double.valueOf(kepingLogam.getSelectedItem().toString()),
                            (BigDecimal) response.result,
                            Integer.valueOf(jumlahLogam.getText().toString()),
                            keteranganBarang.getText().toString(),
                            imageFile));
                    loadItemGadaiList();
                    clearGadai();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_SIMULASI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    simulasiLogamMulia = (ResponeSimulasi) response.result;
                    String minPinjaman = StringHelper.getPriceInRp(simulasiLogamMulia.getMinPinjaman());
                    String maxPinjaman = StringHelper.getPriceInRp(simulasiLogamMulia.getMaxPinjaman());
                    batasMahrumBih.setText(StringHelper.getStringBuilderToString(minPinjaman,
                            " - ",
                            maxPinjaman));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    EasyImage.configuration(LogamMuliaActivity.this)
                            .setImagesFolderName(ConfigurationUtils.APP_NAME)
                            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                            .setCopyPickedImagesToPublicGalleryAppFolder(true)
                            .setAllowMultiplePickInGallery(true);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GO_TO_CARI_NASABAH.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    pager.setCurrentItem(2);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SCAN_CIF.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    getSearchNasabah().setText(response.result.toString());
                    checkKtpOrCif(getSearchNasabah().getText().toString(), ConfigurationUtils.TAG_CHECK_CIF);
                }
                if (StringUtils.isNotBlank(response.type) &&
                        ConfigurationUtils.TAG_NASABAH_FOUND.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setResponseNasabah((ResponseNasabah) response.result);
                    showNasabahResult();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GADAI_PERHIASAN.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    pager.setCurrentItem(3);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_LIST_OF_BANK.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogList(new ArrayList<>(getDataMap().values()), getResources().getString(R.string.pilih_bank_label),
                            getResources().getString(R.string.cari_bank_label),
                            ConfigurationUtils.TAG_GET_LIST_OF_BANK_SUCCESS, getFragmentManagerBankAccount());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_CHECK_BANK.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setKonfirmasiGadai(new KonfirmasiGadai(getResponseNasabah().getNamaNasabah(),
                            ConfigurationUtils.TAG_GADAI_PERHIASAN_LABEL,
                            taksiranEditText.getText().toString(),
                            marhunBihEditText.getText().toString(),
                            getTenor(),
                            getNamaBankEditText().getText().toString(),
                            getNomorRekeningEditText().getText().toString(),
                            getNamaPemilikEditText().getText().toString()));
                    pager.setCurrentItem(4);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_DIALOG.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    getSimpleAdapter().getFilter().filter((CharSequence) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    urlKtpImage = response.result != null ? ((File) response.result).getAbsolutePath() : null;
                    loadKtpImage(urlKtpImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    handleSetOtp(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleExpiredOtpCountDownTimer(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleSendOtpSuccess();
                    pager.setCurrentItem(5);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleSendOtpSuccess();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleVerifyOtpSuccess(viewModel.getRessetedTimer());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    ListGadai listGadai = (ListGadai) response.result;
                    getListItemAdapter().set(listGadai.getIndex(), listGadai);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_SUBMIT_GADAI.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    handleOnSuccessSubmitGadai(response);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SHARE_DOWNLOAD_FILE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    ShareFileHelper.shareStruk((File) response.result, getString(R.string.share_struk_title),
                            getString(R.string.share_struk_message), this);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    viewModel.clearData();
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UNAUTHORIZED.equals(response.type)) {
                    showDialogError(response.result.toString(), dialog -> {
                        dialog.dismiss();
                        viewModel.doLogoutAgent();
                    });
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_FAILED_GO_TO_CARI_NASABAH.equals(response.type)) {
                    showDialogError(getString(R.string.warning_input_mahrumbih));
                } else {
                    if (isHandlingOtp) {
                        handleFailedSendOtp();
                    }
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    private void handleOnSuccessSubmitGadai(Response response) {
        setDomain((Domain) response.result);
        loadBuktiTransaksi();
        showDialogSuccessNoCancel(getString(R.string.success_submit_gadai),
                dialog -> pager.setCurrentItem(7));
    }

    private void loadItemGadaiList() {
        itemGadaiAdapter.clear();
        for (ItemGadai itemGadai : itemGadaiList)
            itemGadaiAdapter.add(new ItemGadaiAdapter(itemGadai, getString(R.string.jenis_logam_mulia),
                    getString(R.string.jumlah_logam)));
    }

    private void loadKepingLogam(Response response) {
        jenisKepingList = new ArrayList<>();
        jenisKepingList.addAll((List<String>) response.result);
        attachSpinner(this.kepingLogam, jenisKepingList);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_gadai_perhiasan;
    }

    private void configureViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FormGadaiLogamMuliaTaksirFragment.newInstance(this), "");
        adapter.addFragment(FormGadaiSimulasiFragment.newInstance(this), "");
        adapter.addFragment(CariPenggunaFragment.newInstance(this, ConfigurationUtils.TAG_GADAI_PERHIASAN),
                "");
        adapter.addFragment(RekeningTujuanFragment.newInstance(this),
                "");
        adapter.addFragment(KonfirmasiGadaiFragment.newInstance(this),
                "");
        adapter.addFragment(OpenGadaiOtpFragment.newInstance(this),
                "");
        adapter.addFragment(SubmitGadaiFragment.newInstance(this),
                "");
        adapter.addFragment(CetakBuktiTransaksiFragment.newInstance(this),
                "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(onPageChangeListener());
    }

    @NonNull
    private ViewPager.OnPageChangeListener onPageChangeListener() {
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                progressBar.setProgress(position + 1);
                isHandlingOtp = false;
                buttonSimpan.setVisibility(View.GONE);
                if (position == 1) {
                    if (taksiranEditText != null)
                        taksiranEditText.setText(StringHelper.getSimplePriceWithoutFormatter(taksiran.doubleValue()));

                } else if (position == 4) {
                    isHandlingOtp = true;
                    initConfirmationValue();
                } else if (position == 5) {
                    isHandlingOtp = true;
                } else if (position == 7) {
                    buttonSimpan.setVisibility(View.VISIBLE);
                    startBluetoothService();
                }

                //handle progressbar
                if (position == adapter.getCount() - 1) {
                    progressBar.setProgress(100);
                } else {
                    progressBar.setProgress(viewModel.getProgress(position, adapter.getCount()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    private void configureItemAdapter(FastItemAdapter<ItemGadaiAdapter> itemGadaiAdapter) {
        itemGadaiAdapter.withSelectable(true);
        itemGadaiAdapter.withEventHook(new ClickEventHook<ItemGadaiAdapter>() {

            @javax.annotation.Nullable
            @Override
            public View onBind(RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof ItemGadaiAdapter.ViewHolder) {
                    return ((ItemGadaiAdapter.ViewHolder) viewHolder).imageButton;
                }
                return null;
            }

            @Override
            public void onClick(View v, int position,
                                FastAdapter<ItemGadaiAdapter> fastAdapter, ItemGadaiAdapter item) {
                showPopupMenu(v, position);
            }
        });
    }

    @Override
    public void initGadaiLogamMuliaComponent(RecyclerView listKeping,
                                             AppCompatSpinner kepingLogam,
                                             EditText jumlahLogam,
                                             EditText keteranganBarang,
                                             ImageView gambarLogam,
                                             ProgressBar progressPicture,
                                             AppCompatButton buttonTambahkanLogam,
                                             AppCompatButton buttonSelanjutnya) {
        this.listKeping = listKeping;
        this.kepingLogam = kepingLogam;
        this.jumlahLogam = jumlahLogam;
        this.keteranganBarang = keteranganBarang;
        this.gambarLogam = gambarLogam;
        this.progressPicture = progressPicture;
        this.buttonTambahkanLogam = buttonTambahkanLogam;
        this.buttonSelanjutnya = buttonSelanjutnya;
        configureAdapter(itemGadaiAdapter, this.listKeping);
        viewModel.getKepingLogam();
        loadItemGadaiList();
        gambarLogam.setOnClickListener(view -> EasyImage.openCamera(LogamMuliaActivity.this, 0));
    }

    @Override
    public void addItemGadai() {
        if (StringUtils.isBlank(imageFile.getAbsolutePath())) {
            showDialogError(getString(R.string.warning_upload_image_photo));
        } else {
            resetSimulasi();
            ItemGadai itemGadai = new ItemGadai(Double.valueOf(Integer.valueOf(kepingLogam.getSelectedItem().toString())));
            viewModel.taksir(itemGadai, ConfigurationUtils.TAG_KANTONG_LOGAM_MULIA);
        }
    }

    @Override
    public void nextToGadaiSimulasi() {
        dismissKeyboard();
        if (itemGadaiAdapter.getAdapterItems().size() == 0) {
            showDialogError(getString(R.string.warning_taksir_perhiasan));
        } else {
            taksiran = viewModel.getTotalTaksiran(itemGadaiAdapter.getAdapterItems());
            pager.setCurrentItem(1);
        }
    }

    @Override
    public void initFormGadaiSimulasi(AppCompatSpinner spinnerTennor,
                                      CustomFontLatoRegulerEditText taksiranEditText,
                                      CustomFontLatoBoldTextView batasMahrumBih,
                                      CustomFontLatoRegulerEditText marhunBihEditText) {
        this.spinnerTennor = spinnerTennor;
        this.taksiranEditText = taksiranEditText;
        this.batasMahrumBih = batasMahrumBih;
        this.marhunBihEditText = marhunBihEditText;
        this.taksiranEditText.setFocusable(true);
    }

    @Override
    public void initSpinnerTenorOnSelectedListener(int index) {
        if (!ConfigurationUtils.TENOR[index].equals("-")) {
            setTenor(ConfigurationUtils.TENOR[index]);
            viewModel.simulasiLogamMulia(itemGadaiAdapter.getAdapterItems(),
                    Integer.parseInt(ConfigurationUtils.TENOR[index]));
        }
    }

    @Override
    public void nextToCariNasabah() {
        dismissKeyboard();
        if (StringUtils.isNotBlank(batasMahrumBih.getText().toString()))
            viewModel.nextToCariNasabah(BigDecimal.valueOf(Double.valueOf(marhunBihEditText.getText().toString())),
                    simulasiLogamMulia.getMinPinjaman(),
                    simulasiLogamMulia.getMaxPinjaman());
    }

    @Override
    public void nextToKonfirmasiTransaksi() {
        dismissKeyboard();
        RequestCheckBank requestCheckBank = new RequestCheckBank(getIdBank(), getNamaPemilik().toUpperCase(),
                getNorekPemilik());
        viewModel.checkBank(requestCheckBank);
    }

    private void showPopupMenu(View view, int index) {
        // inflate menu
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.adapter_item_gadai_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.edit_item_gadai:
                    loadGadaiFromAdapter(index);
                    itemGadaiList.remove(index);
                    itemGadaiAdapter.remove(index);
                    resetSimulasi();
                    return true;
                case R.id.delete_item_gadai:
                    itemGadaiList.remove(index);
                    itemGadaiAdapter.remove(index);
                    resetSimulasi();
                    return true;
                default:
            }
            return false;
        });
        popup.show();
    }

    private void resetSimulasi() {
        if (spinnerTennor != null)
            spinnerTennor.setSelection(0);
        if (batasMahrumBih != null)
            batasMahrumBih.setText("");
    }

    private void loadGadaiFromAdapter(int index) {
        String key = String.format("%.0f", itemGadaiAdapter.getAdapterItem(index).getItemGadai().getBeratBersih());
        kepingLogam.setSelection(viewModel.getValueFromSpinner(jenisKepingList,
                key));
        jumlahLogam.setText(String.valueOf(itemGadaiAdapter.getAdapterItem(index).getItemGadai().getJumlah()));
        keteranganBarang.setText(itemGadaiAdapter.getAdapterItem(index).getItemGadai().getKeterangan());
        GlideApp.with(this)
                .load(itemGadaiAdapter.getAdapterItem(index).getItemGadai().getImageFile().getAbsolutePath())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new GlideRequestListener(progressPicture))
                .placeholder(R.drawable.ic_placeholder)
                .transform(new CircleTransform(this))
                .into(gambarLogam);
    }

    private void clearGadai() {
        kepingLogam.setSelection(0);
        jumlahLogam.setText(null);
        keteranganBarang.setText(null);
        GlideApp.with(this)
                .load("")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new GlideRequestListener(progressPicture))
                .placeholder(R.drawable.ic_placeholder)
                .transform(new CircleTransform(this))
                .into(gambarLogam);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleOnBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        handleOnBackPressed();
    }

    private void handleOnBackPressed() {
        if (pager.getCurrentItem() == 0) {
            finishActivity();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void nextToOtp() {
        if (!isAlreadySetTimer()) {
            circleProgressBar.hide();
            handleNextToOtp();
        } else {
            pager.setCurrentItem(5);
        }
    }

    @Override
    public void nextToSubmitGadai() {
        if (!isAlreadyVerified()) {
            handleVerifyOtp();
        } else {
            setRequestSubmitGadai();
            pager.setCurrentItem(6);
        }
    }

    @OnClick(R.id.button_simpan)
    public void onViewClicked() {
        viewModel.downloadFile(getDomain().getUrlPdf(), false);
    }
}