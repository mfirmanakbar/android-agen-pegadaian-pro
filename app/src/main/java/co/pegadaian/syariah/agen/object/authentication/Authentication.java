package co.pegadaian.syariah.agen.object.authentication;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class Authentication {
    @SerializedName("username")
    private String email;
    private String password;

    public Authentication(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
