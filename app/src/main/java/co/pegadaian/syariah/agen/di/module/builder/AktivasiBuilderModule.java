package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.aktivasi.view.AktivasiActivity;
import co.pegadaian.syariah.agen.di.module.AktivasiModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public abstract class AktivasiBuilderModule {

    @ContributesAndroidInjector(modules = {AktivasiModule.class})
    abstract AktivasiActivity bindActivity();
}
