package co.pegadaian.syariah.agen.object.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.utils.time.TimeUtils;

/**
 * Created by T460s on 3/21/2018.
 */

public class HistoryEmas extends AbstractItem<ItemTransaksi, HistoryEmas.ViewHolder> {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("no_rekening")
    @Expose
    private String noRekening;
    @SerializedName("tgl_transaksi")
    @Expose
    private Integer tglTransaksi;
    @SerializedName("nominal_transaksi")
    @Expose
    private Integer nominalTransaksi;

    @SerializedName("detail-emas")
    @Expose
    private List<DetailHistoryEmas> detailEmas = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }

    public Integer getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Integer tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public Integer getNominalTransaksi() {
        return nominalTransaksi;
    }

    public void setNominalTransaksi(Integer nominalTransaksi) {
        this.nominalTransaksi = nominalTransaksi;
    }
    public List<DetailHistoryEmas> getDetailEmas() {
        return detailEmas;
    }

    public void setDetailEmas(List<DetailHistoryEmas> detailEmas) {
        this.detailEmas = detailEmas;
    }

    @NonNull
    @Override
    public HistoryEmas.ViewHolder getViewHolder(View v) {
        return new HistoryEmas.ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.card_transaksi_emas;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_jenis)
        TextView jenis;

        @BindView(R.id.tv_nama)
        TextView nama;

        @BindView(R.id.tv_no)
        TextView noNasabah;

        @BindView(R.id.tv_tanggal)
        TextView tanggal;

        @BindView(R.id.tv_nominal)
        TextView nominal;

        Context context;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(@NonNull HistoryEmas.ViewHolder holder, @NonNull List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.nama.setText(getNamaNasabah());
        holder.jenis.setText(getJenisTransaksi());
        holder.noNasabah.setText(getNoRekening());
        holder.tanggal.setText(TimeUtils.getDateFormated("dd/MM/yyyy",getTglTransaksi()));
        holder.nominal.setText(StringHelper.getPriceInRp(getNominalTransaksi().doubleValue()));
    }

    @Override
    public void unbindView(@NonNull HistoryEmas.ViewHolder holder) {
        super.unbindView(holder);
        holder.nama.setText(null);
        holder.jenis.setText(null);
        holder.noNasabah.setText(null);
        holder.tanggal.setText(null);
        holder.nominal.setText(null);
    }
}
