package co.pegadaian.syariah.agen.object.sample;

/**
 * Created by Dell on 3/1/2018.
 */

public class Request {

    private String authorization;
    private String grantType;
    private String userName;
    private String password;

    public Request(String authorization,
                   String grantType,
                   String userName,
                   String password) {
        this.authorization = authorization;
        this.grantType = grantType;
        this.userName = userName;
        this.password = password;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getGrantType() {
        return grantType;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
