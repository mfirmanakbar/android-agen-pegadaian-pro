package co.pegadaian.syariah.agen.object.userprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 11/03/2018.
 */

public class RequestChangePassword {

    @SerializedName("new_pass")
    @Expose
    private String newPassword;
    @SerializedName("old_pass")
    @Expose
    private String oldPassword;
    @SerializedName("confirm_pass")
    @Expose
    private String confirmationPassword;

    public RequestChangePassword(String oldPassword, String newPassword, String confirmationPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.confirmationPassword = confirmationPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }
}
