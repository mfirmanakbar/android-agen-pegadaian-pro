package co.pegadaian.syariah.agen.history.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.parceler.Parcels;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryEmas;
import co.pegadaian.syariah.agen.object.history.DetailHistoryPemasaran;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryDetailRiwayatPemasaranActivity extends BaseToolbarActivity {

    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private DetailHistoryPemasaran detailHistoryPemasaran;
    private TextView bankMitra, namaNasabah,jenisTransaksi,totTaksirJaminan,uangPinjaman,munahAwal,diskonMunah,munahNett
            ,angsBulanan;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
//        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
//        viewModel.setActivity(this);
//        viewModel.response().observe(this, this::processResponse);
        configureToolbar("Detail Riwayat");
        initComponent();
    }

    private void initComponent(){
        bankMitra = findViewById(R.id.tv_bank_mitra);
        namaNasabah = findViewById(R.id.tv_nama_nasabah);
        jenisTransaksi = findViewById(R.id.tv_jenis_transaksi);
        totTaksirJaminan = findViewById(R.id.tv_tot_taksir_jaminan);
        uangPinjaman = findViewById(R.id.tv_uang_pinjaman);
        munahAwal = findViewById(R.id.tv_munah_awal);
        diskonMunah = findViewById(R.id.tv_diskon_munah);
        munahNett = findViewById(R.id.tv_munah_nett);
        angsBulanan = findViewById(R.id.tv_angs_bulanana);

        detailHistoryPemasaran = Parcels.unwrap(getIntent().getParcelableExtra("detailHistoryPemasaran"));

        bankMitra.setText(String.valueOf(detailHistoryPemasaran.getBankMitra()));
        namaNasabah.setText(detailHistoryPemasaran.getNamaNasabah());
        jenisTransaksi.setText(String.valueOf(detailHistoryPemasaran.getJenisTransaksi()));
        totTaksirJaminan.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getTotalTransaksi().doubleValue()));
        uangPinjaman.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getUangPinjaman().doubleValue()));
        munahAwal.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getMunahAwal().doubleValue()));
        diskonMunah.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getDiskonMunah().doubleValue()));
        munahNett.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getMunahNett().doubleValue()));
        angsBulanan.setText(StringHelper.getPriceInRp(detailHistoryPemasaran.getAngsuran().doubleValue()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        viewModel.unBinding();
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_transaksi_detail_riwayat_pemasaran;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                break;
            case ERROR:
                break;
        }
    }


}
