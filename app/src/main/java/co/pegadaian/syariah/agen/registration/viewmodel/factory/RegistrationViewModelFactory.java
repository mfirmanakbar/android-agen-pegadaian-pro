package co.pegadaian.syariah.agen.registration.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.registration.domain.interactors.RegistrationUseCase;
import co.pegadaian.syariah.agen.registration.viewmodel.RegistrationViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by Dell on 3/6/2018.
 */

public class RegistrationViewModelFactory extends BaseViewModelFactory {

    private final RegistrationUseCase registrationUseCase;

    public RegistrationViewModelFactory(SchedulersFacade schedulersFacade,
                                        JsonParser jsonParser,
                                        RxBus rxBus,
                                        PreferenceManager preferenceManager,
                                        RegistrationUseCase registrationUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.registrationUseCase = registrationUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RegistrationViewModel.class)) {
            return (T) new RegistrationViewModel(getSchedulersFacade(), getJsonParser(), getRxBus(), getPreferenceManager(), registrationUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
