package co.pegadaian.syariah.agen.sample.samplemap.viewmodel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import co.pegadaian.syariah.agen.sample.samplemap.domain.interactors.SampleMapUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Dell on 3/4/2018.
 */

public class SampleMapViewModel extends BaseViewModel {

    private Observable<String> addressObservable;

    public SampleMapViewModel(SchedulersFacade schedulersFacade,
                              JsonParser jsonParser,
                              RxBus rxBus,
                              PreferenceManager preferenceManager,
                              SampleMapUseCase sampleMapUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, sampleMapUseCase);
    }

    public void updateLocationFromPicker(final LatLng latLang) {
        getDisposables().add(((SampleMapUseCase) getBaseUseCase())
                .getAddressObservable(getLocationProvider(), latLang.latitude, latLang.longitude)
                .doOnSubscribe(disposable -> {
                    if (response().hasActiveObservers())
                        response().postValue(Response.loading());
                })
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<String>() {
                    @Override
                    public void accept(String address) throws Exception {
                        super.accept(address);
                        if (response().hasActiveObservers())
                            response().postValue(Response.success(address, ConfigurationUtils.TAG_UPDATE_ADDRESS_FROM_PICKER));
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers())
                            response().postValue(Response.error(throwable));
                    }
                }));
    }

    @SuppressLint("MissingPermission")
    public void getAddress() {
        addressObservable = getLocationProvider().getUpdatedLocation(getLocationRequest())
                .doOnSubscribe(disposable -> {
                    if (response().hasActiveObservers())
                        response().postValue(Response.loading());
                })
                .flatMap(location -> ((SampleMapUseCase) getBaseUseCase()).getReverseGeocodeObservable(getLocationProvider(),
                        location.getLatitude(), location.getLongitude()))
                .map(((SampleMapUseCase) getBaseUseCase()).getListAddress())
                .map(((SampleMapUseCase) getBaseUseCase()).getAddress());
    }

    public void updateAddress() {
        getDisposables().add(addressObservable
                .doOnSubscribe(disposable -> {
                    if (response().hasActiveObservers())
                        response().postValue(Response.loading());
                })
                .subscribe(new ConsumerAdapter<String>() {
                    @Override
                    public void accept(String address) throws Exception {
                        super.accept(address);
                        if (response().hasActiveObservers())
                            response().postValue(Response.success(address, ConfigurationUtils.TAG_UPDATE_ADDRESS));
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers())
                            response().postValue(Response.error(throwable));
                    }
                }));
    }

    @Override
    public void updateCurrentLocation() {
        updateLocation();
        updateAddress();
    }
}