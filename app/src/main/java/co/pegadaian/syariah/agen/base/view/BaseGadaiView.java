package co.pegadaian.syariah.agen.base.view;

import android.support.v7.widget.AppCompatSpinner;

import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;

/**
 * Created by Dell on 3/22/2018.
 */

public interface BaseGadaiView {

    void initFormGadaiSimulasi(AppCompatSpinner spinnerTennor, CustomFontLatoRegulerEditText taksiranEditText,
                               CustomFontLatoBoldTextView batasMahrumBih, CustomFontLatoRegulerEditText marhunBihEditText);
    void initSpinnerTenorOnSelectedListener(int index);
    void nextToCariNasabah();
    void nextToKonfirmasiTransaksi();
}
