package co.pegadaian.syariah.agen.login.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.login.domain.interactors.LoginLocalUseCase;
import co.pegadaian.syariah.agen.login.domain.interactors.LoginRemoteUseCase;
import co.pegadaian.syariah.agen.login.viewmodel.LoginViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by Dell on 2/28/2018.
 */

public class LoginViewModelFactory extends BaseViewModelFactory {

    private final LoginRemoteUseCase loginRemoteUseCase;
    private final LoginLocalUseCase loginLocalUseCase;
    private final RealmConfigurator realmConfigurator;

    public LoginViewModelFactory(SchedulersFacade schedulersFacade,
                                 JsonParser jsonParser,
                                 RxBus rxBus,
                                 PreferenceManager preferenceManager,
                                 LoginRemoteUseCase loginRemoteUseCase,
                                 LoginLocalUseCase loginLocalUseCase,
                                 RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.loginRemoteUseCase = loginRemoteUseCase;
        this.loginLocalUseCase = loginLocalUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(loginRemoteUseCase,loginLocalUseCase,
                    realmConfigurator,getSchedulersFacade(),
                    getJsonParser(), getRxBus(), getPreferenceManager());
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
