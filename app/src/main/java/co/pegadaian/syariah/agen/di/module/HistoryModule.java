package co.pegadaian.syariah.agen.di.module;


import javax.inject.Named;

import co.pegadaian.syariah.agen.history.domain.interactor.HistoryUseCase;
import co.pegadaian.syariah.agen.history.domain.model.HistoryRepository;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public class HistoryModule {

    @Provides
    HistoryRepository provideHistoryRepository(PreferenceManager preferenceManager) {
        return new HistoryRepository(preferenceManager, RestApiAdapter.getMockRestApi());
    }

    @Provides
    HistoryViewModelFactory provideHistoryViewModelFactory(SchedulersFacade schedulersFacade,
                                                           @Named("JsonParser") JsonParser jsonParser,
                                                           @Named("RxBus") RxBus rxBus,
                                                           PreferenceManager preferenceManager,
                                                           HistoryUseCase berandaUseCase) {
        return new HistoryViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, berandaUseCase);
    }
}
