package co.pegadaian.syariah.agen.login.viewmodel;


import android.support.annotation.NonNull;

import org.apache.commons.lang3.StringUtils;

import co.pegadaian.syariah.agen.base.viewmodel.BaseAgentViewModel;
import co.pegadaian.syariah.agen.login.domain.interactors.LoginLocalUseCase;
import co.pegadaian.syariah.agen.login.domain.interactors.LoginRemoteUseCase;
import co.pegadaian.syariah.agen.object.authentication.Authentication;
import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Arloji on 3/05/2018.
 */

public class LoginViewModel extends BaseAgentViewModel {

    private final LoginRemoteUseCase loginRemoteUseCase;
    private final LoginLocalUseCase loginLocalUseCase;

    public LoginViewModel(LoginRemoteUseCase loginRemoteUseCase,
                          LoginLocalUseCase loginLocalUseCase,
                          RealmConfigurator realmConfigurator,
                          SchedulersFacade schedulersFacade,
                          JsonParser jsonParser,
                          RxBus rxBus,
                          PreferenceManager preferenceManager) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, loginLocalUseCase, realmConfigurator);
        this.loginRemoteUseCase = loginRemoteUseCase;
        this.loginLocalUseCase = loginLocalUseCase;
    }

    @NonNull
    private Disposable doLoginAgent(Authentication authentication) {
        return loginRemoteUseCase.login(authentication)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (LoginViewModel.this.response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = LoginViewModel.this.getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                SignInResponse signInResponse = LoginViewModel.this.getJsonParser().getObject(responseNostraAPI.getResult(),
                                        SignInResponse.class);
                                Disposable getUserProfile = getUserProfile(signInResponse);
                                if (getUserProfile == null) {
                                    response().postValue(Response.success(signInResponse.getAccessToken(),
                                            signInResponse.getStatusRegistrasi()));
                                } else {
                                    handleSetToken(signInResponse.getAccessToken(), signInResponse.getId(),
                                            signInResponse.getStatusRegistrasi());
                                    getDisposables().add(getUserProfile);
                                }
                            } else {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError());
    }

    public void handleLoginAgent(String email, String password) {
        Authentication authentication = loginLocalUseCase.checkEmailPassword(email, password);
        if (authentication != null) {
            getDisposables().add(doLoginAgent(authentication));
        } else {
            LoginViewModel.this.response().setValue(Response.error("", ConfigurationUtils.TAG_NO_USERNAME_OR_PASSWORD));
        }
    }

    private void handleSetToken(String accessToken, String id, String statusRegistrasi) {
        loginLocalUseCase.insertToken(accessToken, id, statusRegistrasi);
    }

    public Disposable getUserProfile(SignInResponse signInResponse) {
        Observable<Object> userProfileObservable = loginRemoteUseCase
                .getUserProfile(signInResponse);
        return (userProfileObservable == null) ? null : userProfileObservable
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                UserProfile userProfile = getJsonParser().getObject(responseNostraAPI.getResult(), UserProfile.class);
                                userProfile.setSignInResponse(signInResponse);
                                handleSetAgen(userProfile);
                            } else {
                                response().postValue(Response.error("", ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED));
                            }

                        }
                    }
                }, handleError());
    }
}