package co.pegadaian.syariah.agen.sample.sampledetail.restapi.adapter;

import co.pegadaian.syariah.agen.sample.sampledetail.restapi.SampleRestApi;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.restapi.BaseRestApiAdapter;

/**
 * Created by Dell on 2/27/2018.
 */

public class SampleRestApiAdapter extends BaseRestApiAdapter {

    public static SampleRestApi getSampleRestApi() {
        restAdapter = getRestAdapter(ConfigurationUtils.TAG_BASE_SAMPLE_URL);
        return restAdapter.create(SampleRestApi.class);
    }
}