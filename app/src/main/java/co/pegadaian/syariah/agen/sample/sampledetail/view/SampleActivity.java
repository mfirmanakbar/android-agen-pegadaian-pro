package co.pegadaian.syariah.agen.sample.sampledetail.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.sample.Request;
import co.pegadaian.syariah.agen.sample.sampledetail.viewmodel.SampleViewModel;
import co.pegadaian.syariah.agen.sample.sampledetail.viewmodel.factory.SampleViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.view.activity.BaseActivity;

/**
 * Created by Dell on 2/28/2018.
 */

public class SampleActivity extends BaseActivity {

    @BindView(R.id.result_value)
    TextView resultValue;

    private SampleViewModel viewModel;

    @Inject
    SampleViewModelFactory viewModelFactory;

    private Request request = new Request("Basic YXBsaWthc2lhZ2VudHN5YXJpYWg6YWdlblN5YXIxMjMh",
            "password",
            "9991",
            "agenSyar321!");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SampleViewModel.class);
        viewModel.response().observe(this, this::processResponse);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_sample;
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.handleGetToken(request);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                updateDisplay("Please Wait");
                break;

            case SUCCESS:
                updateDisplay(response.result.toString());
                break;

            case ERROR:
                String errorMessage = (response.error == null) ?
                        response.result.toString() : response.error.getMessage();
                updateDisplay(errorMessage);
                break;
        }
    }

    private void updateDisplay(String value) {
        resultValue.setText(value);
    }
}