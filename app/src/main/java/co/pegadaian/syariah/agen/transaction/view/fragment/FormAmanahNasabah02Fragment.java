package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class FormAmanahNasabah02Fragment extends BaseFragment {

    private static final String TAG = FormAmanahAgunanFragment.class.getSimpleName();

    private static AmanahActivity amanahActivity;
    Unbinder unbinder;

    @BindView(R.id.llSimulasi)
    LinearLayout llSimulasi;

    @BindView(R.id.llTenor)
    LinearLayout llTenor;


    public static FormAmanahNasabah02Fragment newInstance(AmanahActivity activity) {
        FormAmanahNasabah02Fragment formAmanahNasabah02Fragment = new FormAmanahNasabah02Fragment();
        amanahActivity = activity;
        return formAmanahNasabah02Fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_amanah_nasabah_tenor, container, false);
        unbinder = ButterKnife.bind(this, view);

        View child = getLayoutInflater().inflate(R.layout.item_simulasi_amanah, null);
        CustomFontLatoRegularTextView txtLabelSimulasi = child.findViewById(R.id.txtLabelSimulasi);
        CustomFontLatoRegularTextView txtValueSimulasi = child.findViewById(R.id.txtValueSimulasi);

        txtLabelSimulasi.setText("Jenis Kendaraan");
        txtValueSimulasi.setText("Motor");

        llSimulasi.addView(child);

        child = getLayoutInflater().inflate(R.layout.item_simulasi_amanah, null);
        txtLabelSimulasi = child.findViewById(R.id.txtLabelSimulasi);
        txtValueSimulasi = child.findViewById(R.id.txtValueSimulasi);
        txtLabelSimulasi.setText("Harga Kendaraan");
        txtValueSimulasi.setText(Html.fromHtml("<font color=\"black\"><small>Rp.</small></font> <b>50.000.000</b>"));
        llSimulasi.addView(child);

        child = getLayoutInflater().inflate(R.layout.item_simulasi_amanah, null);
        txtLabelSimulasi = child.findViewById(R.id.txtLabelSimulasi);
        txtValueSimulasi = child.findViewById(R.id.txtValueSimulasi);
        txtLabelSimulasi.setText("Uang Muka");
        txtValueSimulasi.setText(Html.fromHtml("<font color=\"black\"><small>Rp.</small></font> <b>50.000.000</b>"));
        llSimulasi.addView(child);

        child = getLayoutInflater().inflate(R.layout.item_simulasi_amanah, null);
        txtLabelSimulasi = child.findViewById(R.id.txtLabelSimulasi);
        txtValueSimulasi = child.findViewById(R.id.txtValueSimulasi);
        final View viewBorder = child.findViewById(R.id.viewBorderSimulasi);
        txtLabelSimulasi.setText("Maksimal Marhun Bih");
        txtValueSimulasi.setText(Html.fromHtml("<font color=\"black\"><small>Rp.</small></font> <b>50.000.000</b>"));
        viewBorder.setVisibility(View.GONE);
        llSimulasi.addView(child);

        for(int i = 10; i < 15; i++) {
            // dummy
            child = getLayoutInflater().inflate(R.layout.item_simulasi_tenor, null);
            final CustomFontLatoRegularTextView txtNumberOfCicilan = child.findViewById(R.id.txt_number_of_cicilan);
            final CustomFontLatoRegularTextView txtPrice = child.findViewById(R.id.txt_price);
            txtNumberOfCicilan.setText("12x");
            txtPrice.setText("1.245.0000");
            int finalI = i;
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i(TAG, "index: "+ finalI);
                    amanahActivity.nextAgunanForm();
                }
            });
            llTenor.addView(child);
        }

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {

    }
}
