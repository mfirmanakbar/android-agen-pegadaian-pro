package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.di.module.BerandaModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public abstract class BerandaBuilderModule {
    @ContributesAndroidInjector(modules = {BerandaModule.class})
    abstract BerandaActivity bindActivity();
}
