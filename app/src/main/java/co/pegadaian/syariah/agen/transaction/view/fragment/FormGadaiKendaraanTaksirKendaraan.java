package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.KendaraanActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/22/2018.
 */

public class FormGadaiKendaraanTaksirKendaraan extends BaseFragment {

    @BindView(R.id.merek_kendaraan)
    AppCompatSpinner merekKendaraan;
    @BindView(R.id.gambar_depan_kendaraan)
    ImageView gambarDepanKendaraan;
    @BindView(R.id.label_depan_kendaraan)
    TextView labelDepanKendaraan;
    @BindView(R.id.progressDepan)
    ProgressBar progressDepan;
    @BindView(R.id.area_depan_kendaraan)
    RelativeLayout areaDepanKendaraan;
    @BindView(R.id.gambar_samping_kendaraan)
    ImageView gambarSampingKendaraan;
    @BindView(R.id.label_samping_kendaraan)
    TextView labelSampingKendaraan;
    @BindView(R.id.progressSamping)
    ProgressBar progressSamping;
    @BindView(R.id.gambar_belakang_kendaraan)
    ImageView gambarBelakangKendaraan;
    @BindView(R.id.label_belakang_kendaraan)
    TextView labelBelakangKendaraan;
    @BindView(R.id.progressBelakang)
    ProgressBar progressBelakang;
    @BindView(R.id.area_belakang_kendaraan)
    RelativeLayout areaBelakangKendaraan;
    @BindView(R.id.gambar_bpkb_stnk)
    ImageView gambarBpkbStnk;
    @BindView(R.id.label_bpkb_stnk)
    TextView labelBpkbStnk;
    @BindView(R.id.progressBpkbStnk)
    ProgressBar progressBpkbStnk;
    @BindView(R.id.button_selanjutnya)
    AppCompatButton buttonSelanjutnya;
    @BindView(R.id.label_merk)
    CustomFontLatoRegularTextView labelMerk;
    @BindView(R.id.label_tipe)
    CustomFontLatoBoldTextView labelTipe;
    @BindView(R.id.tipe_mobil_edit_text)
    AppCompatEditText tipeMobilEditText;
    @BindView(R.id.button_motor)
    CustomFontLatoRegularTextView buttonMotor;
    @BindView(R.id.button_mobil)
    CustomFontLatoRegularTextView buttonMobil;
    @NotEmpty
    @BindView(R.id.silinder_edit_text)
    AppCompatEditText silinderEditText;
    @BindView(R.id.year)
    AppCompatSpinner year;
    @BindView(R.id.police_number_edit_text)
    AppCompatEditText policeNumberEditText;
    @NotEmpty
    @BindView(R.id.bpkb_edit_text)
    AppCompatEditText bpkbEditText;
    @NotEmpty
    @BindView(R.id.bpkb_owner_edit_text)
    AppCompatEditText bpkbOwnerEditText;
    @NotEmpty
    @BindView(R.id.stnk_number_edit_text)
    AppCompatEditText stnkNumberEditText;
    @NotEmpty
    @BindView(R.id.nomor_rangka_edit_text)
    AppCompatEditText nomorRangkaEditText;
    @BindView(R.id.label_nomor_mesin)
    CustomFontLatoBoldTextView labelNomorMesin;
    @NotEmpty
    @BindView(R.id.nomor_mesin_edit_text)
    AppCompatEditText nomorMesinEditText;
    @BindView(R.id.label_warna_kendaraan)
    CustomFontLatoBoldTextView labelWarnaKendaraan;
    @NotEmpty
    @BindView(R.id.warna_kendaraan_edit_text)
    AppCompatEditText warnaKendaraanEditText;
    @BindView(R.id.label_harga_pasar)
    CustomFontLatoBoldTextView labelHargaPasar;
    @NotEmpty
    @BindView(R.id.harga_pasar_edit_text)
    AppCompatEditText hargaPasarEditText;
    private Unbinder unbinder;
    private static KendaraanActivity kendaraanActivity;

    public static FormGadaiKendaraanTaksirKendaraan newInstance(KendaraanActivity activity) {
        FormGadaiKendaraanTaksirKendaraan formGadaiKendaraanTaksirKendaraan = new FormGadaiKendaraanTaksirKendaraan();
        kendaraanActivity = activity;
        return formGadaiKendaraanTaksirKendaraan;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_data_kendaraan, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        kendaraanActivity.setVehicleType(ConfigurationUtils.TAG_MOTOR);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        kendaraanActivity.initGadaiKendaraanComponent(merekKendaraan,
                gambarDepanKendaraan,
                progressDepan,
                gambarSampingKendaraan,
                progressSamping,
                gambarBelakangKendaraan,
                progressBelakang,
                gambarBpkbStnk,
                progressBpkbStnk,
                tipeMobilEditText,
                silinderEditText,
                year,
                policeNumberEditText,
                bpkbEditText,
                bpkbOwnerEditText,
                stnkNumberEditText,
                nomorRangkaEditText,
                nomorMesinEditText,
                warnaKendaraanEditText,
                hargaPasarEditText);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {
        kendaraanActivity.nextToGadaiSimulasi();
    }

    @OnClick({R.id.button_motor, R.id.button_mobil, R.id.button_selanjutnya})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_motor:
                changeViewMotor();
                break;
            case R.id.button_mobil:
                changeViewMobil();
                break;
            case R.id.button_selanjutnya:
                getValidator().validate();
                break;
        }
    }

    private void changeViewMotor() {
        labelMerk.setText(kendaraanActivity.getString(R.string.merek_motor));
        labelTipe.setText(kendaraanActivity.getString(R.string.tipe_motor));
        labelTipe.setHint(kendaraanActivity.getString(R.string.model_jenis_motor));
        kendaraanActivity.setVehicleType(ConfigurationUtils.TAG_MOTOR);
        buttonMotor.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
        buttonMobil.setBackgroundColor(getResources().getColor(R.color.baseBackground));
        buttonMotor.setTextColor(getResources().getColor(R.color.white));
        buttonMobil.setTextColor(getResources().getColor(R.color.grey));
        kendaraanActivity.loadKendaraan();
    }

    private void changeViewMobil() {
        labelMerk.setText(kendaraanActivity.getString(R.string.merek_mobil));
        labelTipe.setText(kendaraanActivity.getString(R.string.tipe_mobil));
        labelTipe.setHint(kendaraanActivity.getString(R.string.model_jenis_mobil));
        kendaraanActivity.setVehicleType(ConfigurationUtils.TAG_MOBIL);
        buttonMobil.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
        buttonMotor.setBackgroundColor(getResources().getColor(R.color.baseBackground));
        buttonMobil.setTextColor(getResources().getColor(R.color.white));
        buttonMotor.setTextColor(getResources().getColor(R.color.grey));
        kendaraanActivity.loadKendaraan();
    }
}
