package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by Dell on 3/17/2018.
 */

public class ResponeSimulasi {

    @SerializedName("minPinjaman")
    @Expose
    private BigDecimal minPinjaman;
    @SerializedName("maxPinjaman")
    @Expose
    private BigDecimal maxPinjaman;

    public BigDecimal getMinPinjaman() {
        return minPinjaman;
    }

    public void setMinPinjaman(BigDecimal minPinjaman) {
        this.minPinjaman = minPinjaman;
    }

    public BigDecimal getMaxPinjaman() {
        return maxPinjaman;
    }

    public void setMaxPinjaman(BigDecimal maxPinjaman) {
        this.maxPinjaman = maxPinjaman;
    }
}
