package co.pegadaian.syariah.agen.sample.samplehome.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.sample.samplehome.domain.interactors.HomeLocalUseCase;
import co.pegadaian.syariah.agen.sample.samplehome.viewmodel.HomeViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by Dell on 2/28/2018.
 */

public class HomeViewModelFactory extends BaseViewModelFactory {

    private final HomeLocalUseCase homeLocalUseCase;

    public HomeViewModelFactory(SchedulersFacade schedulersFacade,
                                JsonParser jsonParser,
                                RxBus rxBus,
                                PreferenceManager preferenceManager,
                                HomeLocalUseCase homeLocalUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.homeLocalUseCase = homeLocalUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(getSchedulersFacade(), getJsonParser(), getRxBus(), getPreferenceManager(), homeLocalUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
