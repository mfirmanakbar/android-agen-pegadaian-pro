package co.pegadaian.syariah.agen.history.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by T460s on 3/21/2018.
 */

public class HistoryDetailFragment extends BaseFragment {

    private Unbinder unbinder;
    private static HistoryActivity historyActivity;
    @BindView(R.id.list_transaksi_gadai)
    RecyclerView listTransaksiGadai;
    private static String titles;

    public static HistoryDetailFragment newInstance(HistoryActivity activity, String title) {
        HistoryDetailFragment registrationDataNasabahFragment = new HistoryDetailFragment();
        historyActivity = activity;
        titles = title;
        return registrationDataNasabahFragment;
    }

    @Override
    public void onValidationSucceeded() {

    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaksi_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        configureComponent();
        return view;
    }

    private void configureComponent() {

    }

    private void configureFont() {

    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
