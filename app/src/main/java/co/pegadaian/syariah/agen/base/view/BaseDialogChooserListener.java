package co.pegadaian.syariah.agen.base.view;

import android.support.v7.widget.AppCompatEditText;
import android.widget.ArrayAdapter;

/**
 * Created by Dell on 3/19/2018.
 */

public interface BaseDialogChooserListener {
    void handleOnItemSearchDialogClick(int index, String type);
    void initDialogChooserFragment(AppCompatEditText dialogSearchEditText, ArrayAdapter<String> adapter);
}
