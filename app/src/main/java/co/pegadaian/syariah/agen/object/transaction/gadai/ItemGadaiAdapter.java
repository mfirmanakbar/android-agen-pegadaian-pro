package co.pegadaian.syariah.agen.object.transaction.gadai;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;

/**
 * Created by Dell on 3/16/2018.
 */

public class ItemGadaiAdapter extends AbstractItem<ItemGadaiAdapter, ItemGadaiAdapter.ViewHolder> {

    private final ItemGadai itemGadai;
    private final String title;
    private final String label;

    public ItemGadaiAdapter(ItemGadai itemGadai, String title, String label) {
        this.itemGadai = itemGadai;
        this.title = title;
        this.label = label;
    }

    public ItemGadai getItemGadai() {
        return itemGadai;
    }

    public String getTitle() {
        return title;
    }

    @NonNull
    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.item_gadai_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.adapter_gadai;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category)
        CustomFontLatoBoldTextView category;
        @BindView(R.id.berat_bersih_text_view)
        CustomFontLatoBoldTextView beratBersihTextView;
        @BindView(R.id.imageButton)
        public AppCompatImageView imageButton;
        @BindView(R.id.taksiran_text_view)
        CustomFontLatoBoldTextView taksiranTextView;
        @BindView(R.id.item_label)
        CustomFontLatoRegularTextView itemLabel;

        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(ViewHolder holder, List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.category.setText(getTitle());
        holder.beratBersihTextView.setText(getItemGadai().getBeratBersihFormatted());
        holder.taksiranTextView.setText(getItemGadai().getTaksiranFormatted());
        holder.itemLabel.setText(this.label);
    }

    @Override
    public void unbindView(ViewHolder holder) {
        super.unbindView(holder);
        holder.category.setText(null);
        holder.beratBersihTextView.setText(null);
        holder.taksiranTextView.setText(null);
    }
}