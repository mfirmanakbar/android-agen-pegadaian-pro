package co.pegadaian.syariah.agen.object.master;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 3/8/2018.
 */

public class Region {

    @SerializedName("nama_wilayah")
    @Expose
    private String namaWilayah;
    @SerializedName("id_provinsi")
    @Expose
    private String idProvinsi;
    @SerializedName("id_kota")
    @Expose
    private String idKota;
    @SerializedName("id_kecamatan")
    @Expose
    private String idKecamatan;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;

    public String getNamaWilayah() {
        return namaWilayah;
    }

    public void setNamaWilayah(String namaWilayah) {
        this.namaWilayah = namaWilayah;
    }

    public String getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(String idProvinsi) {
        this.idProvinsi = idProvinsi;
    }

    public String getSortIdProvinsi() {
        return String.format("%02d", Integer.valueOf(getIdProvinsi()));
    }

    public String getSortIdKota() {
        return String.format("%04d", Integer.valueOf(getIdKota()));
    }

    public String getSortIdKecamatan() {
        return String.format("%06d", Integer.valueOf(getIdKecamatan()));
    }

    public String getSortIdKelurahan() {
        return String.format("%08d", Integer.valueOf(getIdKelurahan()));
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }
}