package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.AmanahActivity;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class FormAmanahAgunanKendaraanFragment extends BaseFragment {


    public static int TIPE_KENDARAAN_MOBIL = 1;
    public static int TIPE_KENDARAAN_MOTOR = 0;

    @BindView(R.id.spinner_merek_kendaraan)
    AppCompatSpinner spinnerMerekKendaraan;
    @BindView(R.id.btn_tipe_kendaraan_mobil)
    AppCompatButton btnTipeKendaraanMobil;
    @BindView(R.id.btn_tipe_kendaraan_motor)
    AppCompatButton btnTipeKendaraanMotor;
    @NotEmpty
    @BindView(R.id.et_tipe_kendaraan)
    CustomFontLatoRegulerEditText etTipeKendaraan;
    @NotEmpty
    @BindView(R.id.et_silinder_mesin)
    CustomFontLatoRegulerEditText etIsiSilinderMesin;
    @NotEmpty
    @BindView(R.id.et_pembuatan_kendaraan)
    CustomFontLatoRegulerEditText etTahunPembuatanKendaraan;
    @NotEmpty
    @BindView(R.id.et_warna)
    CustomFontLatoRegulerEditText etWarna;
    @NotEmpty
    @BindView(R.id.et_nama_bpkb)
    CustomFontLatoRegulerEditText etNamaBpkb;
    @NotEmpty
    @BindView(R.id.et_keterangan)
    CustomFontLatoRegulerEditText etKeterangan;

    @BindColor(R.color.transparent)
    int bgColorTransparent;
    @BindColor(R.color.grey)
    int colorGrey;
    @BindColor(R.color.white)
    int colorWhite;

    @BindDrawable(R.drawable.button_green_selector)
    Drawable bgGreenSelector;

    private int tipeKendaraan = TIPE_KENDARAAN_MOTOR;
    private static AmanahActivity amanahActivity;

    Unbinder unbinder;

    public static FormAmanahAgunanKendaraanFragment newInstance(AmanahActivity activity) {
        FormAmanahAgunanKendaraanFragment formAmanahAgunanKendaraanFragment = new FormAmanahAgunanKendaraanFragment();
        amanahActivity = activity;
        return formAmanahAgunanKendaraanFragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_form_amanah_agunan_kendaraan, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        amanahActivity.initFormAgunanaKendaraan(tipeKendaraan,
                spinnerMerekKendaraan,
                etTipeKendaraan,
                etIsiSilinderMesin,
                etTahunPembuatanKendaraan,
                etNamaBpkb,
                etWarna,
                etKeterangan);
    }

    @Override
    public void onValidationSucceeded() {
        amanahActivity.nextDataNasabah();
    }

    @OnClick({R.id.btn_tipe_kendaraan_mobil, R.id.btn_tipe_kendaraan_motor, R.id.button_next})
    public void onBtnKendaraanMotor(View view) {
        switch (view.getId()) {
            case R.id.btn_tipe_kendaraan_mobil:
                btnTipeKendaraanMotor.setBackgroundColor(bgColorTransparent);
                btnTipeKendaraanMotor.setTextColor(colorGrey);
                btnTipeKendaraanMobil.setBackground(bgGreenSelector);
                btnTipeKendaraanMobil.setTextColor(colorWhite);
                tipeKendaraan = TIPE_KENDARAAN_MOBIL;
                amanahActivity.updateAgunanTipeKendaraan(TIPE_KENDARAAN_MOBIL);
                break;
            case R.id.btn_tipe_kendaraan_motor:
                btnTipeKendaraanMobil.setBackgroundColor(bgColorTransparent);
                btnTipeKendaraanMobil.setTextColor(colorGrey);
                btnTipeKendaraanMotor.setBackground(bgGreenSelector);
                btnTipeKendaraanMotor.setTextColor(colorWhite);
                tipeKendaraan = TIPE_KENDARAAN_MOTOR;
                amanahActivity.updateAgunanTipeKendaraan(TIPE_KENDARAAN_MOTOR);
                break;
            case R.id.button_next:
                getValidator().validate();
                break;
        }

    }
}
