package co.pegadaian.syariah.agen.transaction.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.transaction.domain.interactors.TransactionUseCase;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by Dell on 3/16/2018.
 */

public class TransactionViewModelFactory extends BaseViewModelFactory {

    private final TransactionUseCase transactionUseCase;
    private final RealmConfigurator realmConfigurator;

    public TransactionViewModelFactory(SchedulersFacade schedulersFacade,
                                       JsonParser jsonParser,
                                       RxBus rxBus,
                                       PreferenceManager preferenceManager,
                                       TransactionUseCase transactionUseCase,
                                       RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.transactionUseCase = transactionUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(TransactionViewModel.class)) {
            return (T) new TransactionViewModel(getSchedulersFacade(), getJsonParser(), getRxBus(), getPreferenceManager(), transactionUseCase, realmConfigurator);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}