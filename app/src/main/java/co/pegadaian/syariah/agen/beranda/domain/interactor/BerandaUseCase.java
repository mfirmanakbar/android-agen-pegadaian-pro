package co.pegadaian.syariah.agen.beranda.domain.interactor;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.beranda.domain.model.BerandaRepository;
import co.pegadaian.syariah.agen.object.beranda.Dashboard;
import co.pegadaian.syariah.agen.object.beranda.Menu;
import co.pegadaian.syariah.agen.object.userprofile.PrivilegeList;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class BerandaUseCase extends BaseAgentUseCase {

    private final BerandaRepository repository;

    @Inject
    public BerandaUseCase(BerandaRepository repository,
                          Compressor compressor) {
        super(repository, compressor);
        this.repository = repository;
    }

    public boolean isLogin() {
        return StringUtils.isNotBlank(getRepository().getToken());
    }

    public Observable<PrivilegeList> getMenuList(UserProfile userProfile) {
        return Observable
                .fromIterable(userProfile.getPrivilegeList());
    }

    public Dashboard getDashboard(PrivilegeList privilegeList, Dashboard dashboard) {
        if (privilegeList.getKategori().toLowerCase().contains(ConfigurationUtils.TAG_RAHN.toLowerCase())) {
            dashboard.getRahn().add(new Menu(privilegeList.getDeskripsi()));
        } else if (privilegeList.getKategori().toLowerCase().contains(ConfigurationUtils.TAG_PEMBAYARAN.toLowerCase())) {
            if (privilegeList.getDeskripsi().toLowerCase().contains(ConfigurationUtils.TAG_EMAS.toLowerCase())) {
                dashboard.getEmas().add(new Menu(privilegeList.getDeskripsi()));
            } else {
                dashboard.getPembayaran().add(new Menu(privilegeList.getDeskripsi()));
            }
        } else if (privilegeList.getKategori().toLowerCase().contains(ConfigurationUtils.TAG_PEMASARAN.toLowerCase())) {
            dashboard.getPemasaran().add(new Menu(privilegeList.getDeskripsi()));
        }
        return dashboard;
    }


    public Observable<Object> getDataDashboard() {
        return ((BerandaRepository) getRepository()).getDataDashboard();
    }
}