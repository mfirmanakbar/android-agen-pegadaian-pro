package co.pegadaian.syariah.agen.history.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.parceler.Parcels;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryEmas;
import co.pegadaian.syariah.agen.object.history.DetailHistoryPembayaran;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryDetailRiwayatPembayaranActivity extends BaseToolbarActivity {

    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private DetailHistoryPembayaran detailHistoryPembayaran;
    private TextView noAkad, namaNasabah,jenisTransaksi,hariTariff,marhunBih,cicilan,munahAkad,munahAkadAsuransi,biayaAdmin
            ,total;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
//        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
//        viewModel.setActivity(this);
//        viewModel.response().observe(this, this::processResponse);
        configureToolbar("Detail Riwayat");
        initComponent();
    }

    private void initComponent(){
        noAkad = findViewById(R.id.tv_nomor_akad);
        namaNasabah = findViewById(R.id.tv_nama_nasabah);
        jenisTransaksi = findViewById(R.id.tv_jenis_transaksi);
        hariTariff = findViewById(R.id.tv_hari_tarrif);
        marhunBih = findViewById(R.id.tv_marhun_bih);
        cicilan = findViewById(R.id.tv_cicilan);
        munahAkad = findViewById(R.id.tv_munah_akad);
        munahAkadAsuransi = findViewById(R.id.tv_munah_akad_asuransi);
        biayaAdmin = findViewById(R.id.tv_biaya_administrasi);
        total = findViewById(R.id.tv_total);

        detailHistoryPembayaran = Parcels.unwrap(getIntent().getParcelableExtra("detailHistoryPembayaran"));

        noAkad.setText(String.valueOf(detailHistoryPembayaran.getNomorAkad()));
        namaNasabah.setText(detailHistoryPembayaran.getNamaNasabah());
        jenisTransaksi.setText(String.valueOf(detailHistoryPembayaran.getJenisTransaksi()));
        hariTariff.setText(String.valueOf(detailHistoryPembayaran.getHariTariff()));
        marhunBih.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getMarhubBih().doubleValue()));
        cicilan.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getCicilan().doubleValue()));
        munahAkad.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getMunah().doubleValue()));
        munahAkadAsuransi.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getMunahAsuransi().doubleValue()));
        biayaAdmin.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getBiayaAdministrasi().doubleValue()));
        total.setText(StringHelper.getPriceInRp(detailHistoryPembayaran.getTotal().doubleValue()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        viewModel.unBinding();
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_transaksi_detail_riwayat_pembayaran;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                break;
            case ERROR:
                break;
        }
    }


}
