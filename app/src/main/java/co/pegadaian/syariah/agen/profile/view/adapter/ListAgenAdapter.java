package co.pegadaian.syariah.agen.profile.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;

/**
 * Created by ArLoJi on 11/03/2018.
 */

public class ListAgenAdapter extends RecyclerView.Adapter<ListAgenAdapter.AgenViewHolder> {

    private List<String> dataList = new ArrayList<>();

    @Override
    public AgenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_tipe_agen_profile, parent, false);
        return new AgenViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AgenViewHolder holder, int position) {
        if (dataList.size() > 0) {
            holder.loadBind(position);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class AgenViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tipe_agen_profile)
        CustomFontLatoRegularTextView textTipe;

        AgenViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void loadBind(int position) {
            textTipe.setText(dataList.get(position));
        }
    }

    public void setDataList(List<String> listAgenType) {
        dataList = new ArrayList<>();
        dataList.addAll(listAgenType);
        notifyDataSetChanged();
    }
}