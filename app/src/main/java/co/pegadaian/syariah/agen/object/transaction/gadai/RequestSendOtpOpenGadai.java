package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by Dell on 3/20/2018.
 */

public class RequestSendOtpOpenGadai {

    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("tipe_otp")
    @Expose
    private String tipeOtp;
    @SerializedName("up")
    @Expose
    private BigDecimal up;

    public RequestSendOtpOpenGadai(String noHp,
                                   String tipeOtp,
                                   BigDecimal up) {
        this.noHp = noHp;
        this.tipeOtp = tipeOtp;
        this.up = up;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getTipeOtp() {
        return tipeOtp;
    }

    public void setTipeOtp(String tipeOtp) {
        this.tipeOtp = tipeOtp;
    }

    public BigDecimal getUp() {
        return up;
    }

    public void setUp(BigDecimal up) {
        this.up = up;
    }
}
