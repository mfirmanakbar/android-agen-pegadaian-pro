package co.pegadaian.syariah.agen.object.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.SignInResponseRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by ArLoJi on 05/03/2018.
 */

@RealmClass
@Parcel(implementations = {SignInResponseRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {SignInResponse.class})
public class SignInResponse extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("immediate_change_password")
    @Expose
    private Boolean immediateChangePassword;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("group")
    @Expose
    private Group group;
    @SerializedName("status_registrasi")
    @Expose
    private String statusRegistrasi;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Boolean getImmediateChangePassword() {
        return immediateChangePassword;
    }

    public void setImmediateChangePassword(Boolean immediateChangePassword) {
        this.immediateChangePassword = immediateChangePassword;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatusRegistrasi() {
        return statusRegistrasi;
    }

    public void setStatusRegistrasi(String statusRegistrasi) {
        this.statusRegistrasi = statusRegistrasi;
    }
}
