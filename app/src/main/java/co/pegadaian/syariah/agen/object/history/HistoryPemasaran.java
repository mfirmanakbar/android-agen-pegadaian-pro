package co.pegadaian.syariah.agen.object.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import id.app.pegadaian.library.utils.time.TimeUtils;

/**
 * Created by T460s on 3/21/2018.
 */

public class HistoryPemasaran extends AbstractItem<ItemTransaksi, HistoryPemasaran.ViewHolder> {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tgl_transaksi")
    @Expose
    private Integer tglTransaksi;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("kode_booking")
    @Expose
    private Integer kodeBooking;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("detail-pemasaran")
    @Expose
    private List<DetailHistoryPemasaran> detailPemasaran = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Integer tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public Integer getKodeBooking() {
        return kodeBooking;
    }

    public void setKodeBooking(Integer kodeBooking) {
        this.kodeBooking = kodeBooking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public List<DetailHistoryPemasaran> getDetailPemasaran() {
        return detailPemasaran;
    }

    public void setDetailPemasaran(List<DetailHistoryPemasaran> detailPemasaran) {
        this.detailPemasaran = detailPemasaran;
    }

    @NonNull
    @Override
    public HistoryPemasaran.ViewHolder getViewHolder(View v) {
        return new HistoryPemasaran.ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.card_transaksi_pembayaran;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_jenis)
        TextView jenis;

        @BindView(R.id.tv_nama)
        TextView nama;

        @BindView(R.id.tv_kode_booking)
        TextView kodeBooking;

        @BindView(R.id.tv_status)
        TextView status;

        @BindView(R.id.tv_tanggal)
        TextView tanggal;



        Context context;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(@NonNull HistoryPemasaran.ViewHolder holder, @NonNull List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.nama.setText(getNamaNasabah());
        holder.jenis.setText(getJenisTransaksi());
        holder.kodeBooking.setText(String.valueOf(getKodeBooking()));
        holder.tanggal.setText(TimeUtils.getDateFormated("dd/MM/yyyy",getTglTransaksi()));
        holder.status.setText(getStatus());
    }

    @Override
    public void unbindView(@NonNull HistoryPemasaran.ViewHolder holder) {
        super.unbindView(holder);
        holder.nama.setText(null);
        holder.jenis.setText(null);
        holder.kodeBooking.setText(null);
        holder.tanggal.setText(null);
        holder.status.setText(null);
    }
}
