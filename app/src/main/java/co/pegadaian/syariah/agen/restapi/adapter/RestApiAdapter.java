package co.pegadaian.syariah.agen.restapi.adapter;

import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.restapi.BaseRestApiAdapter;

/**
 * Created by Dell on 2/27/2018.
 */

public class RestApiAdapter extends BaseRestApiAdapter {

    public static RestApi getRestApi() {
        restAdapter = getRestAdapter(ConfigurationUtils.TAG_BASE_URL);
        return restAdapter.create(RestApi.class);
    }

    public static LogoutRestApi getLogoutRestApi() {
        restLogoutAdapter = getRestAdapter(ConfigurationUtils.TAG_BASE_URL);
        return restLogoutAdapter.create(LogoutRestApi.class);
    }

    //for mockapi
    public static RestApi getMockRestApi() {
        restAdapter = getRestAdapter("http://5aabf3ef0deaa20014f92ba2.mockapi.io/");
        return restAdapter.create(RestApi.class);
    }
}