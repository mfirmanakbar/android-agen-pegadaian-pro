package co.pegadaian.syariah.agen.history.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.history.adapter.DataAdapter;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryGadai;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.utils.time.TimeUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryDetailRiwayatActivity extends BaseToolbarActivity {

    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private DetailHistoryGadai detailHistoryGadai;
    private TextView statusTransaksi, namaNasabah, nomorAkad,tglTransaksi,barangJaminan,jenisElektronik
            ,merk,hargaPasaran,tenor,taksiran,pinjaman;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
//        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
//        viewModel.setActivity(this);
//        viewModel.response().observe(this, this::processResponse);
        configureToolbar("Detail Riwayat");
        initComponent();
    }

    private void initComponent(){
        statusTransaksi = findViewById(R.id.tv_status_transaksi);
        namaNasabah = findViewById(R.id.tv_nama_nasabah);
        nomorAkad = findViewById(R.id.tv_nomor_akad);
        tglTransaksi = findViewById(R.id.tv_tgl_transaksi);
        barangJaminan = findViewById(R.id.tv_barang_jaminan);
        jenisElektronik = findViewById(R.id.tv_jenis_elektronik);
        merk = findViewById(R.id.tv_merk);
        hargaPasaran = findViewById(R.id.tv_harga_pasaran);
        tenor = findViewById(R.id.tv_tenor);
        taksiran = findViewById(R.id.tv_taksiran);
        pinjaman = findViewById(R.id.tv_pinjaman);

        detailHistoryGadai = Parcels.unwrap(getIntent().getParcelableExtra("detailHistoryGadai"));

        statusTransaksi.setText(detailHistoryGadai.getStatus());
        namaNasabah.setText(detailHistoryGadai.getNamaNasabah());
        nomorAkad.setText(String.valueOf(detailHistoryGadai.getNomorAkad()));
        tglTransaksi.setText(TimeUtils.getDateFormated("dd/MM/yyyy",detailHistoryGadai.getTglTransaksi()));
        barangJaminan.setText(detailHistoryGadai.getBarangJaminan());
        jenisElektronik.setText(detailHistoryGadai.getType());
        merk.setText(detailHistoryGadai.getMerk());
        hargaPasaran.setText(StringHelper.getPriceInRp(detailHistoryGadai.getMarketPrice().doubleValue()));
        tenor.setText(StringHelper.getStringBuilderToString(detailHistoryGadai.getTenor().toString()," hari"));
        taksiran.setText(StringHelper.getPriceInRp(detailHistoryGadai.getTaksiranPrice().doubleValue()));
        pinjaman.setText(StringHelper.getPriceInRp(detailHistoryGadai.getPinjamanPrice().doubleValue()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        viewModel.unBinding();
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_transaksi_detail_riwayat;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                break;
            case ERROR:
                break;
        }
    }


}
