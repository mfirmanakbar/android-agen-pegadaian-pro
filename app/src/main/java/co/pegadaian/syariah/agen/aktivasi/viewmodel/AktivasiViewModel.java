package co.pegadaian.syariah.agen.aktivasi.viewmodel;

import org.apache.commons.lang3.StringUtils;

import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiRemoteUseCase;
import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiUseCase;
import co.pegadaian.syariah.agen.base.viewmodel.BaseAgentViewModel;
import co.pegadaian.syariah.agen.object.authentication.RequestChangePIN;
import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.utils.time.TimeUtils;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by ArLoJi on 07/03/2018.
 */

public class AktivasiViewModel extends BaseAgentViewModel {

    private Disposable timerDisposable;
    private final AktivasiRemoteUseCase aktivasiRemoteUseCase;

    public AktivasiViewModel(SchedulersFacade schedulersFacade,
                             JsonParser jsonParser,
                             RxBus rxBus,
                             PreferenceManager preferenceManager,
                             AktivasiUseCase aktivasiUseCase,
                             AktivasiRemoteUseCase aktivasiRemoteUseCase, RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, aktivasiUseCase, realmConfigurator);
        this.aktivasiRemoteUseCase = aktivasiRemoteUseCase;
    }

    private Disposable sendOtp(RequestSendSms requestSendSms, String type) {
        return aktivasiRemoteUseCase.sendOtp(requestSendSms)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   if (response().hasActiveObservers()) {
                                       if (ConfigurationUtils.TAG_SEND_OTP.equals(type)) {
                                           response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_OTP_SUCCESS));
                                       } else if (ConfigurationUtils.TAG_SEND_RETRY_OTP.equals(type)) {
                                           response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS));
                                       }
                                   }
                               }
                           },
                        handleError());
    }

    public void agenAktivasi(String phone, String type) {
        if (((AktivasiUseCase) getBaseUseCase()).checkPhone(phone) != null) {
            getDisposables().add(sendOtp(new RequestSendSms(phone, ConfigurationUtils.ACTIVATION_AGEN), type));
        }
    }

    public void timer() {
        timerDisposable = ((AktivasiUseCase) getBaseUseCase()).timer()
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        String formattedTimer = null;
                        if (aLong == 0) {
                            ((AktivasiUseCase) getBaseUseCase()).resetOtpCountDown();
                            if (response().hasActiveObservers()) {
                                Long currentOtpCountDown = ((AktivasiUseCase) getBaseUseCase())
                                        .getCurrentOtpCountDown();
                                formattedTimer = TimeUtils.getFormattedTime(currentOtpCountDown / 1000);
                                if (response().hasActiveObservers())
                                    response().postValue(Response.success(formattedTimer,
                                            ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER));
                            }
                        } else {
                            long parkingTime = (aLong * 1000);
                            formattedTimer = TimeUtils.getFormattedTime(parkingTime / 1000);
                            if (response().hasActiveObservers())
                                response().postValue(Response.success(formattedTimer,
                                        ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER));
                        }

                    }
                });
        getDisposables().add(timerDisposable);
    }

    public void verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        getDisposables().add(aktivasiRemoteUseCase
                .verifyOtp(requestVerifyOtp)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                response().postValue(Response.success(null, ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                        ConfigurationUtils.TAG_VERIFY_OTP_FAILED));
                            }
                        }
                    }
                }, handleError()));
    }

    public void resetOtpCountDown() {
        if (timerDisposable != null)
            timerDisposable.dispose();

        ((AktivasiUseCase) getBaseUseCase()).resetOtpCountDown();
    }

    public String getRessetedTimer() {
        return TimeUtils.getFormattedTime(ConfigurationUtils.TAG_OTP_COUNTDOWN / 1000);
    }

    public Disposable getUserProfile(String accessToken) {
        Observable<Object> userProfileObservable = aktivasiRemoteUseCase
                .getUserProfile(accessToken);
        return userProfileObservable
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                UserProfile userProfile = getJsonParser().getObject(responseNostraAPI.getResult(), UserProfile.class);
                                handleSetAgen(userProfile);
                            } else {
                                response().postValue(Response.error("", ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED));
                            }

                        }
                    }
                }, handleError());
    }

    public void changePIN(RequestChangePIN requestChangePIN, String accessToken) {
        getDisposables().add(aktivasiRemoteUseCase
                .changePIN(accessToken, requestChangePIN)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                getBaseUseCase().insertToken(accessToken);
                                response().postValue(Response.success(accessToken, ConfigurationUtils.TAG_CHANGE_PIN_SUCCESS));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                        ConfigurationUtils.TAG_CHANGE_PIN_FAILED));
                            }
                        }
                    }
                }, handleError()));
    }

    @Override
    protected void handleAfterAcceptSetAgentToLocal(UserProfile agen1) {
        response().postValue(Response.success("",
                ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED));
    }
}
