package co.pegadaian.syariah.agen.registration.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class RegistrationDataUsahaFragment extends BaseFragment {

    @BindView(R.id.front_image_text)
    AppCompatTextView frontImageText;
    @BindView(R.id.back_image_text)
    AppCompatTextView backImageText;
    @BindView(R.id.upload_image_text)
    AppCompatTextView uploadImageText;
    @BindView(R.id.front_image)
    AppCompatImageView frontImage;
    @BindView(R.id.front_image_progress_bar)
    ProgressBar frontImageProgressBar;
    @BindView(R.id.back_image)
    AppCompatImageView backImage;
    @BindView(R.id.back_image_progress_bar)
    ProgressBar backImageProgressBar;
    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.province_edit_label)
    AppCompatTextView provinceEditLabel;
    @NotEmpty
    @BindView(R.id.province_edit_text)
    AppCompatEditText provinceEditText;
    @BindView(R.id.city_edit_label)
    AppCompatTextView cityEditLabel;
    @NotEmpty
    @BindView(R.id.city_edit_text)
    AppCompatEditText cityEditText;
    @BindView(R.id.kecamatan_edit_label)
    AppCompatTextView kecamatanEditLabel;
    @NotEmpty
    @BindView(R.id.kecamatan_edit_text)
    AppCompatEditText kecamatanEditText;
    @BindView(R.id.kelurahan_edit_label)
    AppCompatTextView kelurahanEditLabel;
    @NotEmpty
    @BindView(R.id.kelurahan_edit_text)
    AppCompatEditText kelurahanEditText;
    @BindView(R.id.nama_usaha_edit_label)
    AppCompatTextView namaUsahaEditLabel;
    @NotEmpty
    @BindView(R.id.nama_usaha_edit_text)
    AppCompatEditText namaUsahaEditText;
    @BindView(R.id.jenis_usaha_edit_label)
    AppCompatTextView jenisUsahaEditLabel;
    @NotEmpty
    @BindView(R.id.jenis_usaha_edit_text)
    AppCompatEditText jenisUsahaEditText;
    @BindView(R.id.alamat_usaha_edit_label)
    AppCompatTextView alamatUsahaEditLabel;
    @NotEmpty
    @BindView(R.id.alamat_usaha_edit_text)
    AppCompatEditText alamatUsahaEditText;

    private Unbinder unbinder;
    private static RegistrationActivity registrationActivity;

    public static RegistrationDataUsahaFragment newInstance(RegistrationActivity activity) {
        RegistrationDataUsahaFragment registrationPersonalInfoFragment = new RegistrationDataUsahaFragment();
        registrationActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_data_usaha, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        handleOnTouchListener();
        return view;
    }

    private void configureFont() {
        namaUsahaEditLabel.setTypeface(getLatoBold());
        namaUsahaEditText.setTypeface(getLatoRegular());
        jenisUsahaEditLabel.setTypeface(getLatoBold());
        jenisUsahaEditText.setTypeface(getLatoRegular());
        alamatUsahaEditLabel.setTypeface(getLatoBold());
        alamatUsahaEditText.setTypeface(getLatoRegular());
        frontImageText.setTypeface(getLatoRegular());
        backImageText.setTypeface(getLatoRegular());
        uploadImageText.setTypeface(getLatoRegular());
        goToNextButton.setTypeface(getRonniaRegular());
        provinceEditLabel.setTypeface(getLatoBold());
        provinceEditText.setTypeface(getLatoRegular());
        cityEditLabel.setTypeface(getLatoBold());
        cityEditText.setTypeface(getLatoRegular());
        kecamatanEditLabel.setTypeface(getLatoBold());
        kecamatanEditText.setTypeface(getLatoRegular());
        kelurahanEditLabel.setTypeface(getLatoBold());
        kelurahanEditText.setTypeface(getLatoRegular());
    }

    private void handleOnTouchListener() {
        jenisUsahaEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showBadanUsahaDialog();
                return true;
            }
            return false;
        });
        provinceEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showProvinceDataUsahaDialog();
                return true;
            }
            return false;
        });
        cityEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showCityDataUsahaDialog();
                return true;
            }
            return false;
        });
        kecamatanEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showKecamatanDataUsahaDialog();
                return true;
            }
            return false;
        });
        kelurahanEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showKelurahanDataUsahaDialog();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initFrontImageBadanUsaha(frontImage, frontImageProgressBar);
        registrationActivity.initBackImageBadanUsaha(backImage, backImageProgressBar);
        registrationActivity.initInputanDataUsaha(jenisUsahaEditText, provinceEditText, cityEditText,
                kecamatanEditText, kelurahanEditText, alamatUsahaEditText, namaUsahaEditText,
                getActivity().getFragmentManager());
    }

    @OnClick(R.id.go_to_next_button)
    public void onViewClicked() {
        getValidator().validate();
    }

    @OnClick({R.id.placeholder_front_image, R.id.front_image_text, R.id.front_image,
            R.id.placeholder_back_image, R.id.back_image_text, R.id.back_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.placeholder_front_image:
                registrationActivity.onFrontImageClicked();
                break;
            case R.id.front_image_text:
                registrationActivity.onFrontImageClicked();
                break;
            case R.id.front_image:
                registrationActivity.onFrontImageClicked();
                break;
            case R.id.placeholder_back_image:
                registrationActivity.onBackImageClicked();
                break;
            case R.id.back_image_text:
                registrationActivity.onBackImageClicked();
                break;
            case R.id.back_image:
                registrationActivity.onBackImageClicked();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        registrationActivity.onNext();
    }
}