package co.pegadaian.syariah.agen.sample.samplehome.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.sample.sampledetail.view.SampleActivity;
import co.pegadaian.syariah.agen.sample.samplehome.viewmodel.HomeViewModel;
import co.pegadaian.syariah.agen.sample.samplehome.viewmodel.factory.HomeViewModelFactory;
import co.pegadaian.syariah.agen.sample.samplemap.view.SampleMapsActivity;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseActivity;

/**
 * Created by Dell on 2/28/2018.
 */

public class HomeActivity extends BaseActivity {

    @BindView(R.id.result_value)
    TextView resultValue;
    @BindView(R.id.button_go_to_next)
    Button buttonGoToNext;

    private HomeViewModel viewModel;
    Integer temp = 0;

    @Inject
    HomeViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);
        viewModel.response().observe(this, this::processResponse);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_home;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                updateDisplay("Please Wait");
                break;

            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHANGE_BUTTON_VALUE_TYPE.equals(response.type)) {
                    temp = (Integer) response.result;
                    buttonGoToNext.setText("Tekan : " + temp);
                } else {
                    updateDisplay(response.result.toString());
                }
                break;

            case ERROR:
                String errorMessage = (response.error == null) ?
                        response.result.toString() : response.error.getMessage();
                updateDisplay((StringUtils.isNotBlank(errorMessage)) ? errorMessage : response.error.toString());
                break;
        }
    }

    private void updateDisplay(String value) {
        resultValue.setText(value);
    }

    @OnClick(R.id.button_go_to_next)
    public void onButtonGoToCalcClicked() {
        viewModel.handleSetValue(temp);
        showActivity(new Intent(this, SampleActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.homeBinding();
        viewModel.changeButtonTextBinding();
        viewModel.getValue();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @OnClick(R.id.button_go_to_map)
    public void onButtonGoToMapClicked() {
        showActivity(new Intent(this, SampleMapsActivity.class));
    }
}
