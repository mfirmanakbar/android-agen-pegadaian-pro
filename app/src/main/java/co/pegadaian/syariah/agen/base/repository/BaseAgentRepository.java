package co.pegadaian.syariah.agen.base.repository;

import java.io.File;

import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import io.realm.Realm;
import okhttp3.MultipartBody;

/**
 * Created by Dell on 3/15/2018.
 */

public class BaseAgentRepository extends BaseRepository {

    private RestApi restApi;

    public BaseAgentRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }

    public BaseAgentRepository(PreferenceManager preferenceManager,
                               RestApi restApi,
                               LogoutRestApi logoutRestApi) {
        super(preferenceManager, logoutRestApi);
        this.restApi = restApi;
    }

    public BaseAgentRepository(PreferenceManager preferenceManager,
                               LogoutRestApi logoutRestApi) {
        super(preferenceManager, logoutRestApi);
    }

    public UserProfile getUserProfile(Realm realm) {
        return realm.where(UserProfile.class).findFirst();
    }

    public UserProfile setUserProfile(Realm realm, UserProfile agen) {
        return realm.copyToRealmOrUpdate(agen);
    }

    public RestApi getRestApi() {
        return restApi;
    }

    public Observable<Object> uploadImage(MultipartBody.Part image) {
        return restApi.uploadImage(image);
    }

    public Observable<Object> getRegion() {
        return restApi.getRegion();
    }

    public Observable<Object> getKota(String idProvinsi) {
        return restApi.getKota(idProvinsi);
    }

    public Observable<Object> getKecamatan(String idKota) {
        return restApi.getKecamatan(idKota);
    }

    public Observable<Object> getKelurahan(String idKecamatan) {
        return restApi.getKelurahan(idKecamatan);
    }
}