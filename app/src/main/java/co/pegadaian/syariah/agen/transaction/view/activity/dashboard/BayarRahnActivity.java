package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.widget.EditText;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.object.Agen;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import co.pegadaian.syariah.agen.object.tabungan.KonfirmasiPembayaran;
import co.pegadaian.syariah.agen.object.tabungan.ResponseSimulasiTopUp;
import co.pegadaian.syariah.agen.object.transaction.tabungan.DataPembayaran;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.tabungan.view.fragment.KonfirmasiPembayaranFragment;
import co.pegadaian.syariah.agen.tabungan.view.fragment.PilihJenisPembayaranFragment;
import co.pegadaian.syariah.agen.tabungan.viewmodel.TabunganViewModel;
import co.pegadaian.syariah.agen.tabungan.viewmodel.factory.TabunganViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import id.app.pegadaian.library.widget.colordialog.PromptDialog;
import id.app.pegadaian.library.widget.dialog.DialogPin;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by ArLoJi on 18/03/2018.
 */

public class BayarRahnActivity extends BaseToolbarActivity {
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    @Inject
    TabunganViewModelFactory viewModelFactory;
    TabunganViewModel viewModel;

    private ViewPager.OnPageChangeListener pageChangeListener;
    private ViewPagerAdapter adapter;
    private DialogPin dialogPin;
    private String titleTopUp;
    private String deskripsiPembayaran;
    private int tempPage = 0;
    private UserProfile agen;
    private KonfirmasiPembayaran pembayaran;
    private DataPembayaran dataPembayaran;

    private String nomorAkad;

    //init komponen konfirmasi
    RecyclerView dataNasabah;
    RecyclerView dataTransaksi;
    CustomFontLatoRegularTextView labelTotalTransaksi;
    CustomFontLatoBoldTextView totalTransaksi;
    private FastItemAdapter<ItemTransaksi> listDataNasabah = new FastItemAdapter<>();
    private FastItemAdapter<ItemTransaksi> listDataTransaksi = new FastItemAdapter<>();
    private String jenisTransaksi;

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_tabungan;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TabunganViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        ButterKnife.bind(this);
        configureTitle();
        configureToolbar(titleTopUp, deskripsiPembayaran);
        initListener();
        viewPagerConfigurator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.handleGetUserProfile();
    }

    private void configureTitle() {
        titleTopUp = "Pembayaran";
        deskripsiPembayaran = "Form Pembayaran Gadai";
    }

    private void configureItemAdapter() {
        configureMenuAdapter(new LinearLayoutManager(this), listDataNasabah, dataNasabah);
        configureMenuAdapter(new LinearLayoutManager(this), listDataTransaksi, dataTransaksi);
    }

    private void configureMenuAdapter(LayoutManager linearLayout,
                                      FastItemAdapter<ItemTransaksi> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void viewPagerConfigurator() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(PilihJenisPembayaranFragment.newInstance(this), "");
        adapter.addFragment(KonfirmasiPembayaranFragment.newInstance(this), "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(pageChangeListener);
        progressBar.setProgress(viewModel.getProgress(0, adapter.getCount()));
    }

    private void initListener() {
        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //handle action@page
                if (position == ConfigurationUtils.TAG_PAGE_PERTAMA) {

                } else if (position == ConfigurationUtils.TAG_PAGE_KEDUA) {
                    initHasilKonfirmasi();
                }

                //handle progressbar
                progressBar.setProgress(viewModel.getProgress(position, adapter.getCount()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        handleOnBack();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    public void handleOnNext() {
        if (tempPage < adapter.getCount() - 1) {
            tempPage = tempPage + 1;
            pager.setCurrentItem(tempPage);
        }
    }

    public void handleOnBack() {
        if (tempPage > 0) {
            tempPage = tempPage - 1;
            pager.setCurrentItem(tempPage);
        } else {
            finishActivity();
        }
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_USER_PROFILE.equals(response.type)) {
                    agen = (UserProfile) response.result;
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_JENIS_LAYANAN_SUCCESS.equals(response.type)){
                    pembayaran = (KonfirmasiPembayaran) response.result;
                    dataPembayaran.setReffSwitching(pembayaran != null ? pembayaran.getReffSwitching() : null);
                    dataPembayaran.setMinimumCicil(pembayaran.getMinimalUpCicil());
                    dataPembayaran.setCicilan(pembayaran.getTotalKewajiban());
                    initHasilKonfirmasi();
                    handleOnNext();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_VALID.equals(response.type)){
                    submitKonfirmasiPembayaran();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_KONFIRMASI_PEMBAYARAN_BERHASIL.equals(response.type)){
                    showDialogSuccessNoCancel("Pembayaran Selesai!", dialog -> {
                        showActivityAndFinishCurrentActivity(this, BerandaActivity.class);
                    });
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)){
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                }
                circleProgressBar.smoothToHide();
                break;
            case ERROR:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_INVALID.equals(response.type)) {
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_MINIMAL_PIN_INVALID.equals(response.type)) {
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)) {
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else {
                    showDialogError(response.result.toString());
                }
                circleProgressBar.smoothToHide();
                break;
        }
    }

    private void submitKonfirmasiPembayaran() {
        circleProgressBar.smoothToShow();
        viewModel.submitKonfirmasiPembayaran(dataPembayaran);
    }

    private void initHasilKonfirmasi(){
        listDataNasabah.clear();
        listDataTransaksi.clear();
        listDataNasabah.add(new ItemTransaksi("Nama Nasabah",
                pembayaran.getNamaNasabah()));
        listDataNasabah.add(new ItemTransaksi("Nomor Akad",
                nomorAkad));
        listDataTransaksi.add(new ItemTransaksi("Jenis Transaksi",
                jenisTransaksi));
        listDataTransaksi.add(new ItemTransaksi("Hari Tarif",
                pembayaran.getJumlahHariTarif()));
        listDataTransaksi.add(new ItemTransaksi("Mahrum Bih",
                pembayaran.getUp()));
        listDataTransaksi.add(new ItemTransaksi("Cicilan",
                pembayaran.getNilaiTransaksi()));
        listDataTransaksi.add(new ItemTransaksi("Mu'nah Akad",
                pembayaran.getSewaModal()));
        listDataTransaksi.add(new ItemTransaksi("Mu'nah Akad + Asuransi",
                pembayaran.getAdministrasi()));
        listDataTransaksi.add(new ItemTransaksi("Biaya Administrasi",
                pembayaran.getSurcharge()));
        labelTotalTransaksi.setText(getResources().getString(R.string.total));
        totalTransaksi.setText(StringHelper.getSimplePriceFormatter(pembayaran.getTotalKewajiban()));
    }

    public void initDialogPin() {
        dialogPin = DialogPin.newInstance();
        dialogPin.setPositiveListener(view -> {
            BayarRahnActivity.this.validasiPin(dialogPin.getPinTransaksi());
            dialogPin.dismissDialog();
        });
        dialogPin.show(getSupportFragmentManager(), "");
    }

    private void validasiPin(String pin) {
        circleProgressBar.smoothToShow();
        viewModel.validasiPin(pin);
    }

    public void mulaiPembayaran(String nomorAkad, String nominalCicil, String type) {
        circleProgressBar.smoothToShow();
        this.nomorAkad = nomorAkad;
        this.jenisTransaksi = type;
        dataPembayaran = new DataPembayaran(agen.getId(), type,
                nomorAkad, agen.getNoHp(), "AGENT", nominalCicil);
        viewModel.mulaiPembayaran(dataPembayaran);
    }

    public void initComponentKonfirmasiPembayaran(RecyclerView dataNasabah,
                                                  RecyclerView dataTransaksi,
                                                  CustomFontLatoRegularTextView labelTotalTransaksi,
                                                  CustomFontLatoBoldTextView totalTransaksi) {
        this.dataNasabah = dataNasabah;
        this.dataTransaksi = dataTransaksi;
        this.labelTotalTransaksi = labelTotalTransaksi;
        this.totalTransaksi = totalTransaksi;
        configureItemAdapter();
    }
}
