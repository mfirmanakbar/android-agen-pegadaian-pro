package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseGadaiActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/19/2018.
 */

public class RekeningTujuanFragment extends BaseFragment {

    @BindView(R.id.choose_nasabah)
    CustomFontLatoBoldTextView chooseNasabah;
    @BindView(R.id.choose_agent)
    CustomFontLatoBoldTextView chooseAgent;
    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.nama_bank_edit_label)
    AppCompatTextView namaBankEditLabel;
    @NotEmpty
    @BindView(R.id.nama_bank_edit_text)
    AppCompatEditText namaBankEditText;
    @BindView(R.id.nomor_rekening_edit_label)
    AppCompatTextView nomorRekeningEditLabel;
    @NotEmpty
    @BindView(R.id.nomor_rekening_edit_text)
    AppCompatEditText nomorRekeningEditText;
    @BindView(R.id.nama_pemilik_edit_label)
    AppCompatTextView namaPemilikEditLabel;
    @NotEmpty
    @BindView(R.id.nama_pemilik_edit_text)
    AppCompatEditText namaPemilikEditText;

    private Unbinder unbinder;
    private static BaseGadaiActivity baseTransactionActivity;

    public static RekeningTujuanFragment newInstance(BaseGadaiActivity activity) {
        RekeningTujuanFragment rekeningTujuanFragment = new RekeningTujuanFragment();
        RekeningTujuanFragment.baseTransactionActivity = activity;
        return rekeningTujuanFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rekening_tujuan, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        handleOnTouchListener();
        return view;
    }

    private void handleOnTouchListener() {
        namaBankEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                baseTransactionActivity.showListOfBankDialog();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        baseTransactionActivity.initComponentRekeningTujuan(namaBankEditText, namaPemilikEditText, nomorRekeningEditText,
                getActivity().getFragmentManager());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {
        baseTransactionActivity.nextToKonfirmasiTransaksi();
    }

    @OnClick(R.id.go_to_next_button)
    public void onViewClicked() {
        getValidator().validate();
    }

    @OnClick({R.id.choose_nasabah, R.id.choose_agent})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.choose_nasabah:
                changeNasabahView();
                break;
            case R.id.choose_agent:
                changeAgenView();
                break;
        }
    }

    private void changeNasabahView() {
        chooseAgent.setTextColor(getResources().getColor(R.color.grey));
        chooseNasabah.setTextColor(getResources().getColor(R.color.white));
        chooseAgent.setBackgroundColor(getResources().getColor(R.color.white));
        chooseNasabah.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }

    private void changeAgenView() {
        chooseNasabah.setTextColor(getResources().getColor(R.color.grey));
        chooseAgent.setTextColor(getResources().getColor(R.color.white));
        chooseNasabah.setBackgroundColor(getResources().getColor(R.color.white));
        chooseAgent.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }
}