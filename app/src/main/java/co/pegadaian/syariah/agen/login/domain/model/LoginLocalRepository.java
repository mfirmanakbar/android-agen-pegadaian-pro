package co.pegadaian.syariah.agen.login.domain.model;

import co.pegadaian.syariah.agen.base.repository.BaseAgentRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/3/2018.
 */

public class LoginLocalRepository extends BaseAgentRepository implements LoginLocalModel {

    public LoginLocalRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }
}