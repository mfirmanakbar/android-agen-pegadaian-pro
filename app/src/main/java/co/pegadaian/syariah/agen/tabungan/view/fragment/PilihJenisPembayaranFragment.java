package co.pegadaian.syariah.agen.tabungan.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.BayarRahnActivity;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class PilihJenisPembayaranFragment extends BaseFragment {

    @NotEmpty
    @BindView(R.id.nomor_akad)
    EditText nomorAkad;
    @BindView(R.id.nominal_cicil)
    EditText nominalCicil;
    @BindView(R.id.area_cicilan)
    RelativeLayout areaCicilan;
    @BindView(R.id.button_ulang)
    CustomFontLatoBoldTextView buttonUlang;
    @BindView(R.id.button_cicil)
    CustomFontLatoBoldTextView buttonCicil;
    @BindView(R.id.button_tebus)
    CustomFontLatoBoldTextView buttonTebus;
    private Unbinder unbinder;
    private static BayarRahnActivity bayarRahnActivity;
    private String type = ConfigurationUtils.TAG_PERPANJANGAN_PEMBAYARAN;

    public static PilihJenisPembayaranFragment newInstance(BayarRahnActivity activity) {
        PilihJenisPembayaranFragment jenisPembayaranFragment = new PilihJenisPembayaranFragment();
        bayarRahnActivity = activity;
        return jenisPembayaranFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pembayaran_jenis_transaksi, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        return view;
    }

    private void configureFont() {
        nomorAkad.setTypeface(getLatoRegular());
        nominalCicil.setTypeface(getLatoRegular());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        initValidator();
    }

    @Override
    public void onValidationSucceeded() {
        if (nomorAkad.length() == ConfigurationUtils.TAG_MINIMUM_NOMOR_AKAD) {
            bayarRahnActivity.mulaiPembayaran(nomorAkad.getText().toString(),
                    nominalCicil.getText().toString(), type);
        }
    }

    private void changeViewToGrey(CustomFontLatoBoldTextView passive) {
        passive.setTextColor(getResources().getColor(R.color.grey));
        passive.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void changeViewToGreen(CustomFontLatoBoldTextView aktif, String type) {
        this.type = type;
        aktif.setTextColor(getResources().getColor(R.color.white));
        aktif.setBackground(getResources().getDrawable(R.drawable.background_little_corner_green));
    }


    @OnClick({R.id.button_ulang, R.id.button_cicil, R.id.button_tebus, R.id.button_selanjutnya})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_ulang:
                changeViewToGreen(buttonUlang, ConfigurationUtils.TAG_PERPANJANGAN_PEMBAYARAN);
                changeViewToGrey(buttonCicil);
                changeViewToGrey(buttonTebus);
                areaCicilan.setVisibility(View.GONE);
                break;
            case R.id.button_cicil:
                changeViewToGreen(buttonCicil, ConfigurationUtils.TAG_CICIL_PEMBAYARAN);
                changeViewToGrey(buttonUlang);
                changeViewToGrey(buttonTebus);
                areaCicilan.setVisibility(View.VISIBLE);
                break;
            case R.id.button_tebus:
                changeViewToGreen(buttonTebus, ConfigurationUtils.TAG_TEBUS_PEMBAYARAN);
                changeViewToGrey(buttonUlang);
                changeViewToGrey(buttonCicil);
                areaCicilan.setVisibility(View.GONE);
                break;
            case R.id.button_selanjutnya:
                bayarRahnActivity.mulaiPembayaran(nomorAkad.getText().toString(),
                        nominalCicil.getText().toString(),
                        type);
                break;
        }
    }
}