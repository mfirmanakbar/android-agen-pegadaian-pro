package co.pegadaian.syariah.agen.profile.viewmodel;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.viewmodel.BaseAgentViewModel;
import co.pegadaian.syariah.agen.object.imageslider.DataDashboard;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangeProfile;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.profile.domain.interactor.AgentUseCase;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class AgentViewModel extends BaseAgentViewModel {

    private final AgentUseCase useCase;

    public AgentViewModel(SchedulersFacade schedulersFacade,
                          JsonParser jsonParser,
                          RxBus rxBus,
                          PreferenceManager preferenceManager,
                          AgentUseCase useCase,
                          RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, useCase, realmConfigurator);
        this.useCase = useCase;
    }

    public void getPinOrPassword() {
        response().postValue(Response.success("", useCase.getPinOrPassword()));
    }

    public void setPinOrPassword(String pirOrPassword) {
        useCase.setPinOrPassword(pirOrPassword);
    }

    public void changePinOrPassword(String lama, String baru, String confirm) {
        getDisposables().add(useCase.changePinOrPassword(lama, baru, confirm)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                            if (ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                response().postValue(Response.success("",
                                        ConfigurationUtils.TAG_CHANGE_PIN_PASSWORD_SUCCESS));
                            } else {
                                response().postValue(Response.error("",
                                        ConfigurationUtils.TAG_CHANGE_PIN_PASSWORD_FAILED));
                            }

                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (StringUtils.isNotBlank(throwable.getMessage())) {
                            response().postValue(Response.error(throwable.getMessage()));
                        }
                    }
                }));
    }

    public void getUserProfile() {
        ((AgentUseCase) getBaseUseCase())
                .getUserProfile()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                UserProfile userProfile = getJsonParser().getObject(responseNostraAPI.getResult(), UserProfile.class);
                                response().postValue(Response.success(userProfile, ConfigurationUtils.TAG_LOAD_USER_PROFILE));
                            } else {
                                response().postValue(Response.error("", ConfigurationUtils.TAG_LOAD_USER_PROFILE_FAILED));
                            }

                        }
                    }
                }, handleError());
    }

    public void compressFile(File file, int type) {
        getDisposables().add(getBaseUseCase()
                .compressFile(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<File>() {
                    @Override
                    public void accept(File file) throws Exception {
                        super.accept(file);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_COMPRESS_FILE_USER_PROFILE));
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                    ConfigurationUtils.TAG_FAILED_COMPRESS_FILE_USER_PROFILE));
                        }
                    }
                }));
    }

    public void uploadImageChangeProfile(File file) {
        getDisposables().add(((AgentUseCase) getBaseUseCase())
                .uploadImageChangeProfile(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {

                            if (response().hasActiveObservers()) {
                                response().setValue(Response.success(responseNostraAPI.getResult().toString(),
                                        ConfigurationUtils.TAG_SUCCESS_UPLOAD_ALL_IMAGE));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image)));
                        }
                    }
                }));
    }

    public void changeProfile(UserProfile userProfile) {
        getDisposables().add(((AgentUseCase) getBaseUseCase())
                .changeProfile(new RequestChangeProfile(userProfile.getName(), userProfile.getUrlSelfie()))
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (response().hasActiveObservers()
                                && StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
//                            response().postValue(Response.success("", ConfigurationUtils.TAG_SUCCESS_CHANGE_PROFILE));
                            handleSetAgen(userProfile);
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error("", ConfigurationUtils.TAG_FAILED_CHANGE_PROFILE));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getSaldoProfile() {
        ((AgentUseCase) getBaseUseCase())
                .getSaldoProfile()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                DataDashboard dataDashboard = getJsonParser().getObject(responseNostraAPI.getResult(), DataDashboard.class);
                                response().postValue(Response.success(dataDashboard, ConfigurationUtils.TAG_SUCCES_DATA_SALDO));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(), ConfigurationUtils.TAG_FAILED_DATA_SALDO));
                            }

                        }
                    }
                }, handleError());
    }
}