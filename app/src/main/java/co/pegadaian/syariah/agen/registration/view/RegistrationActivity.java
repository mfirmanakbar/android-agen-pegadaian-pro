package co.pegadaian.syariah.agen.registration.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseDialogChooserListener;
import co.pegadaian.syariah.agen.custom.CircleTransform;
import co.pegadaian.syariah.agen.custom.GlideRequestListener;
import co.pegadaian.syariah.agen.login.view.LoginActivity;
import co.pegadaian.syariah.agen.module.glide.GlideApp;
import co.pegadaian.syariah.agen.object.master.Outlet;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.registration.RequestRegistration;
import co.pegadaian.syariah.agen.object.registration.ResultCheckKTP;
import co.pegadaian.syariah.agen.registration.viewmodel.RegistrationViewModel;
import co.pegadaian.syariah.agen.registration.viewmodel.factory.RegistrationViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.easyimage.DefaultCallback;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.receiver.OtpView;
import id.app.pegadaian.library.receiver.SmsVerificationReceiver;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.map.MapHelper;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class RegistrationActivity extends BaseToolbarActivity implements RegistrationListener, OtpView,
        BaseDialogChooserListener {

    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private String registrationTitle;
    private String personalInfo;

    @Inject
    RegistrationViewModelFactory viewModelFactory;
    RegistrationViewModel viewModel;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private View.OnClickListener retryClickListener;
    private int tempPage = 0;
    private ViewPagerAdapter adapter;
    private AppCompatImageView frontImage;
    private AppCompatImageView backImage;
    private ProgressBar progressBarFrontImage;
    private ProgressBar progressBarBackImage;
    private AppCompatTextView otpCountDownText;
    private LinearLayout retryArea;
    private AppCompatEditText otpEditText;
    private Animation slideUp;
    private Animation slideDown;
    private SmsVerificationReceiver smsVerificationReceiver;
    private GoogleMap map;
    private Marker marker;
    private List<Marker> markers = new ArrayList<>();
    private String positionLabel;
    private BottomSheetDialog bottomSheetDialog;
    private View bottomSheetView;
    private String kodeOtpLabel;
    private String dataUsahaLabel;
    private String lokasiOutletLabel;
    private String akunBankLabel;
    private String passwordAkunLabel;
    private String pilihProvinsiLabel;
    private String cariProvinsiLabel;
    private AppCompatImageView ktpImage;
    private AppCompatImageView selfieImage;
    private ProgressBar progressBarKtpImage;
    private ProgressBar progressBarSelfieImage;
    private AppCompatEditText outletSearchEditText;
    private AppCompatEditText provinceEditText;
    private ArrayAdapter<String> simpleAdapter;
    private DialogChooserFragment dialogChooserFragment;
    private FragmentManager fragmentManagerPersonalInfo;
    private String pilihKotaLabel;
    private String cariKotaLabel;
    private String pilihKecamatanLabel;
    private String cariKecamatanLabel;
    private String pilihKelurahanLabel;
    private String cariKelurahanLabel;
    private String pilihBidangUsahaLabel;
    private String cariBidangUsahaLabel;
    private Map<String, String> dataMap;
    private String idProvinsi;
    private String idKota;
    private String idKecamatan;
    private String idKelurahan;
    private String idBidangUsaha;
    private AppCompatEditText cityEditText;
    private AppCompatEditText kecamatanEditText;
    private AppCompatEditText kelurahanEditText;
    private AppCompatEditText hpEditText;
    private AppCompatEditText dialogSearchEditText;
    private AppCompatEditText jenisUsahaEditText;
    private AppCompatEditText provinceDataUsahaEditText;
    private AppCompatEditText cityDataUsahaEditText;
    private AppCompatEditText kecamatanDataUsahaEditText;
    private AppCompatEditText kelurahanDataUsahaEditText;
    private FragmentManager fragmentManagerDataUsaha;
    private String idProvinsiBidangUsaha;
    private String idKotaBidangUsaha;
    private String idKecamatanBidangUsaha;
    private String idKelurahanBidangUsaha;
    private boolean isAlreadySetTimer = false;
    private boolean isAlreadyVerified = false;
    private LatLng latLang;
    private FastItemAdapter<Outlet> outletAdapter;
    private List<Outlet> outlets = new ArrayList<>();
    private AppCompatEditText namaBankEditText;
    private FragmentManager fragmentManagerBankAccount;
    private String pilihBankLabel;
    private String cariBankLabel;
    private String idBank;
    private String kodeCabang;
    private String latitude;
    private String longitude;
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();
    private HashMap<String, File> requestFiles = new HashMap<>();
    private HashMap<String, String> requestFilesUploaded = new HashMap<>();
    private String warningUploadImagePhoto;
    private AppCompatEditText addressEditText;
    private AppCompatEditText alamatUsahaEditText;
    private TextInputEditText confirmPasswordEditText;
    private AppCompatEditText emailEditText;
    private AppCompatEditText nameEditText;
    private AppCompatEditText namaPemilikEditText;
    private AppCompatEditText namaUsahaEditText;
    private AppCompatEditText ktpEditText;
    private AppCompatEditText nomorRekeningEditText;
    private TextInputEditText passwordEditText;
    private String cif = "";
    private String ibuKandung = "";
    private String jenisKelamin = "";
    private String statusKawin = "";
    private Long tanggalLahir = Long.valueOf(0);
    private ResultCheckKTP resultCheckKTP;
    private AppCompatButton finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegistrationViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        initResources();
        initAnimation();
        initListener();
        initResultCheckKTP();
        configureToolbar(registrationTitle, personalInfo);
        viewPagerConfigurator();
        getCurrentLocation();
    }

    private void initResources() {
        registrationTitle = getResources().getString(R.string.title_activity_registration);
        personalInfo = getResources().getString(R.string.title_activity_registration_personal_info);
        positionLabel = getResources().getString(R.string.your_position_label);
        kodeOtpLabel = getResources().getString(R.string.kode_otp_label);
        dataUsahaLabel = getResources().getString(R.string.data_usaha_label);
        lokasiOutletLabel = getResources().getString(R.string.lokasi_outlet_label);
        akunBankLabel = getResources().getString(R.string.akun_bank_label);
        passwordAkunLabel = getResources().getString(R.string.password_akun_label);
        pilihProvinsiLabel = getResources().getString(R.string.pilih_provinsi_label);
        cariProvinsiLabel = getResources().getString(R.string.cari_provinsi_label);
        pilihKotaLabel = getResources().getString(R.string.pilih_kota_kabupaten_label);
        cariKotaLabel = getResources().getString(R.string.cari_kota_kabupaten_label);
        pilihKecamatanLabel = getResources().getString(R.string.pilih_kecamatan_label);
        cariKecamatanLabel = getResources().getString(R.string.cari_kecamatan_label);
        pilihKelurahanLabel = getResources().getString(R.string.pilih_kelurahan_label);
        cariKelurahanLabel = getResources().getString(R.string.cari_kelurahan_label);
        pilihBidangUsahaLabel = getResources().getString(R.string.pilih_bidang_usaha_label);
        cariBidangUsahaLabel = getResources().getString(R.string.cari_bidang_usaha_label);
        pilihBankLabel = getResources().getString(R.string.pilih_bank_label);
        cariBankLabel = getResources().getString(R.string.cari_bank_label);
        warningUploadImagePhoto = getResources().getString(R.string.warning_upload_image_photo);
    }

    private void initAnimation() {
        slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
    }

    private void initListener() {
        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //handle action@page
                if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_PERSONAL_INFO) {
                    configureToolbar(registrationTitle, personalInfo);
                } else if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_OTP) {
                    configureToolbar(registrationTitle, kodeOtpLabel);
                    if (retryArea != null)
                        retryArea.setVisibility(View.GONE);

                } else if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_DATA_USAHA) {
                    configureToolbar(registrationTitle, dataUsahaLabel);
                } else if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_MAP) {
                    configureToolbar(registrationTitle, lokasiOutletLabel);
                    viewModel.updateCurrentLocation();
                } else if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_BANK_ACCOUNT) {
                    configureToolbar(registrationTitle, akunBankLabel);
                } else if (position == ConfigurationUtils.TAG_PAGE_REGISTRATION_PASSWORD) {
                    configureToolbar(registrationTitle, passwordAkunLabel);
                }

                //handle progressbar
                if (position == adapter.getCount() - 1) {
                    viewModel.setFullProgress();
                } else {
                    viewModel.setProgress(position, adapter.getCount());
                }

                //handleloadimage
                loadImage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        retryClickListener = view -> {
            retryArea.setVisibility(View.GONE);
            retryArea.startAnimation(slideDown);
            isAlreadySetTimer = true;
            isAlreadyVerified = false;
            viewModel.sendOtp(new RequestSendSms(hpEditText.getText().toString(), ConfigurationUtils.REGISTRATION_AGEN),
                    ConfigurationUtils.TAG_SEND_RETRY_OTP);
        };
    }

    private void initResultCheckKTP() {
        resultCheckKTP = Parcels.unwrap(getIntent().getParcelableExtra("resultCheckKTP"));
        if (resultCheckKTP != null) {
            initValueRegistration();
        }
    }

    private void initValueRegistration() {
        cif = resultCheckKTP.getCif();
        ibuKandung = resultCheckKTP.getIbuKandung();
        jenisKelamin = resultCheckKTP.getJenisKelamin();
        statusKawin = resultCheckKTP.getStatusKawin();
        tanggalLahir = resultCheckKTP.getTglLahir();
        idProvinsi = resultCheckKTP.getProvince().getId();
        idKota = resultCheckKTP.getCity().getId();
        idKecamatan = resultCheckKTP.getKecamatan().getId();
        idKelurahan = resultCheckKTP.getKelurahan().getId();
        idBidangUsaha = resultCheckKTP.getKodeCabang().getId();
    }

    private void viewPagerConfigurator() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RegistrationPersonalInfoFragment.newInstance(this), "");
        adapter.addFragment(RegistrationOtpFragment.newInstance(this), "");
        adapter.addFragment(RegistrationDataUsahaFragment.newInstance(this), "");
        adapter.addFragment(RegistrationMapsFragment.newInstance(this), "");
        adapter.addFragment(RegistrationBankAccountFragment.newInstance(this), "");
        adapter.addFragment(RegistrationPasswordFragment.newInstance(this), "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(pageChangeListener);
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOADING.equals(response.type)) {
                    circleProgressBar.smoothToShow();

                    if (finishButton != null)
                        finishButton.setEnabled(false);
                }
                break;
            case SUCCESS:
                if (finishButton != null)
                    finishButton.setEnabled(true);

                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UPDATE_PROGRESS.equals(response.type)) {
                    if (response.result != null)
                        progressBar.setProgress((Integer) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED.equals(response.type)) {
                    EasyImage.configuration(RegistrationActivity.this)
                            .setImagesFolderName(ConfigurationUtils.APP_NAME)
                            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                            .setCopyPickedImagesToPublicGalleryAppFolder(true)
                            .setAllowMultiplePickInGallery(true);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE.equals(response.type)) {
                    requestFiles.put(response.type, (File) response.result);
                    GlideApp.with(this)
                            .load(response.result != null ? ((File) response.result).getAbsolutePath() : null)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBarFrontImage))
                            .transform(new CircleTransform(this))
                            .into(frontImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE.equals(response.type)) {
                    requestFiles.put(response.type, (File) response.result);
                    GlideApp.with(this)
                            .load(response.result != null ? ((File) response.result).getAbsolutePath() : null)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBarBackImage))
                            .transform(new CircleTransform(this))
                            .into(backImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE.equals(response.type)) {
                    requestFiles.put(response.type, (File) response.result);
                    GlideApp.with(this)
                            .load(response.result != null ? ((File) response.result).getAbsolutePath() : null)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBarKtpImage))
                            .transform(new CircleTransform(this))
                            .into(ktpImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_SELFIE_IMAGE.equals(response.type)) {
                    requestFiles.put(response.type, (File) response.result);
                    GlideApp.with(this)
                            .load(response.result != null ? ((File) response.result).getAbsolutePath() : null)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .listener(new GlideRequestListener(progressBarSelfieImage))
                            .transform(new CircleTransform(this))
                            .into(selfieImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    otpCountDownText.setText(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    isAlreadySetTimer = false;
                    isAlreadyVerified = false;
                    otpCountDownText.setText(String.valueOf(response.result));
                    retryArea.setVisibility(View.VISIBLE);
                    retryArea.startAnimation(slideUp);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UPDATE_LOCATION.equals(response.type)) {
                    updateLocation((Location) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_DIALOG.equals(response.type)) {
                    simpleAdapter.getFilter().filter((CharSequence) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihProvinsiLabel, cariProvinsiLabel,
                            ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KOTA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKotaLabel, cariKotaLabel,
                            ConfigurationUtils.TAG_GET_KOTA_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKecamatanLabel, cariKecamatanLabel,
                            ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKelurahanLabel, cariKelurahanLabel,
                            ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_OTP_SUCCESS.equals(response.type)) {
                    viewModel.timer();
                    circleProgressBar.smoothToHide();
                    goToNextPage();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS.equals(response.type)) {
                    viewModel.timer();
                    circleProgressBar.smoothToHide();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS.equals(response.type)) {
                    isAlreadySetTimer = false;
                    isAlreadyVerified = true;
                    viewModel.resetOtpCountDown();
                    otpCountDownText.setText(viewModel.getRessetedTimer());
                    circleProgressBar.smoothToHide();
                    goToNextPage();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_BIDANG_USAHA.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihBidangUsahaLabel, cariBidangUsahaLabel,
                            ConfigurationUtils.TAG_GET_BIDANG_USAHA_SUCCESS, fragmentManagerDataUsaha);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_PROVINCE_USAHA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihProvinsiLabel, cariProvinsiLabel,
                            ConfigurationUtils.TAG_GET_PROVINCE_USAHA_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KOTA_USAHA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKotaLabel, cariKotaLabel,
                            ConfigurationUtils.TAG_GET_KOTA_USAHA_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KECAMATAN_USAHA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKecamatanLabel, cariKecamatanLabel,
                            ConfigurationUtils.TAG_GET_KECAMATAN_USAHA_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KELURAHAN_USAHA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihKelurahanLabel, cariKelurahanLabel,
                            ConfigurationUtils.TAG_GET_KELURAHAN_USAHA_SUCCESS, fragmentManagerPersonalInfo);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_OUTLET_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    this.outlets = new ArrayList<>();
                    this.outlets.addAll((List<Outlet>) response.result);
                    showOutletsDialog(this.outlets);
                    viewModel.loadMarkerOutlets(outlets);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_OUTLET.equals(response.type)) {
                    viewModel.filterOutlet((CharSequence) response.result, outlets);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_OUTLET_RESULTS.equals(response.type)) {
                    dismissKeyboard(outletSearchEditText);
                    showOutletsDialog((List<Outlet>) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_LIST_OF_BANK.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    dataMap = (Map<String, String>) response.result;
                    showDialogList(new ArrayList<>(dataMap.values()), pilihBankLabel, cariBankLabel,
                            ConfigurationUtils.TAG_GET_LIST_OF_BANK_SUCCESS, fragmentManagerBankAccount);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_MARKER_OUTLET.equals(response.type)) {
                    loadMarkerOutlet((Outlet) response.result, builder);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_ALL_IMAGE.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    requestFilesUploaded = (HashMap<String, String>) response.result;
                    setAgent();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_REGISTRATION_SUCCESS.equals(response.type)) {
                    handleRegistrationSuccess();
                }
                break;

            case ERROR:
                if (finishButton != null)
                    finishButton.setEnabled(true);

                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PERMISSION_DENIED.equals(response.type)) {
                    finishActivity();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_ERROR.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    showSnackBar(this, response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_VERIFY_OTP_FAILED.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    showSnackBar(this, response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GATEWAY_TIMEOUT.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    showSnackBar(this, response.result.toString());
                } else {
                    if (ConfigurationUtils.TAG_PAGE_REGISTRATION_OTP == tempPage) {
                        isAlreadySetTimer = false;
                        isAlreadyVerified = false;
                    }
                    circleProgressBar.smoothToHide();
                    showSnackBar(this, response.result.toString());
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.setProgress(0, adapter.getCount());
    }

    private void loadImage() {
        if (frontImage != null && progressBarFrontImage != null)
            GlideApp.with(this)
                    .load(requestFiles.get(ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new GlideRequestListener(progressBarFrontImage))
                    .transform(new CircleTransform(this))
                    .into(frontImage);
        if (backImage != null && progressBarBackImage != null)
            GlideApp.with(this)
                    .load(requestFiles.get(ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new GlideRequestListener(progressBarBackImage))
                    .transform(new CircleTransform(this))
                    .into(backImage);
        if (ktpImage != null && progressBarKtpImage != null)
            GlideApp.with(this)
                    .load(requestFiles.get(ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new GlideRequestListener(progressBarKtpImage))
                    .transform(new CircleTransform(this))
                    .into(ktpImage);
        if (selfieImage != null && progressBarSelfieImage != null)
            GlideApp.with(this)
                    .load(requestFiles.get(ConfigurationUtils.TAG_SUCCESS_ADD_SELFIE_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new GlideRequestListener(progressBarSelfieImage))
                    .transform(new CircleTransform(this))
                    .into(selfieImage);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart(Manifest.permission.CAMERA, Manifest.permission.RECEIVE_SMS, Manifest.permission.ACCESS_FINE_LOCATION);
        registerReceiverPresenter();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(smsVerificationReceiver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
        EasyImage.clearConfiguration(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if (type == 0) {
                    progressBarFrontImage.setVisibility(View.VISIBLE);
                } else if (type == 1) {
                    progressBarBackImage.setVisibility(View.VISIBLE);
                } else if (type == 2) {
                    progressBarKtpImage.setVisibility(View.VISIBLE);
                } else if (type == 3) {
                    progressBarSelfieImage.setVisibility(View.VISIBLE);
                }
                viewModel.compressFile(imageFiles.get(0), type);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(RegistrationActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_registration;
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleOnBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        handleOnBackPressed();
    }

    private void handleOnBackPressed() {
        if (tempPage > 0) {
            tempPage = tempPage - 1;
            pager.setCurrentItem(tempPage);
        } else {
            finishActivity();
        }
    }

    @Override
    public void onNext() {
        //for testing
        dismissKeyboard();
        if (tempPage <= adapter.getCount() - 1) {
            if (tempPage == ConfigurationUtils.TAG_PAGE_REGISTRATION_PERSONAL_INFO) {
                if (requestFiles.size() >= 2) {
                    if (!isAlreadySetTimer) {
                        isAlreadySetTimer = true;
                        isAlreadyVerified = false;
                        viewModel.resetOtpCountDown();
                        otpCountDownText.setText(viewModel.getRessetedTimer());
                        viewModel.sendOtp(new RequestSendSms(hpEditText.getText().toString(), ConfigurationUtils.REGISTRATION_AGEN),
                                ConfigurationUtils.TAG_SEND_OTP);
                    } else {
                        goToNextPage();
                    }
                } else {
                    showSnackBar(this, warningUploadImagePhoto);
                }
            } else if (tempPage == ConfigurationUtils.TAG_PAGE_REGISTRATION_OTP) {
                if (!isAlreadyVerified) {
                    viewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                            hpEditText.getText().toString()));
                } else {
                    goToNextPage();
                }
            } else if (tempPage == ConfigurationUtils.TAG_PAGE_REGISTRATION_DATA_USAHA) {
                if (requestFiles.size() < 4) {
                    showSnackBar(this, warningUploadImagePhoto);
                } else {
                    goToNextPage();
                }
            } else if (tempPage == ConfigurationUtils.TAG_PAGE_REGISTRATION_PASSWORD) {
                if (requestFiles.size() == requestFilesUploaded.size()) {
                    setAgent();
                } else {
                    viewModel.handleUploadImageRegistration(requestFiles, requestFilesUploaded);
                }
            } else {
                goToNextPage();
            }
        }
    }

    private void goToNextPage() {
        tempPage = tempPage + 1;
        pager.setCurrentItem(tempPage);
    }

    @Override
    public void onFrontImageClicked() {
        EasyImage.openCamera(RegistrationActivity.this, 0);
    }

    @Override
    public void onBackImageClicked() {
        EasyImage.openCamera(RegistrationActivity.this, 1);
    }

    @Override
    public void onKtpImageClicked() {
        EasyImage.openCamera(RegistrationActivity.this, 2);
    }

    @Override
    public void onSelfieImageClicked() {
        EasyImage.openCamera(RegistrationActivity.this, 3);
    }

    @Override
    public void initKtpImagePersonalInfo(AppCompatImageView ktpImage,
                                         ProgressBar progressBar) {
        this.ktpImage = ktpImage;
        this.progressBarKtpImage = progressBar;

    }

    @Override
    public void initSelfieImagePersonalInfo(AppCompatImageView selfieImage,
                                            ProgressBar progressBar) {
        this.selfieImage = selfieImage;
        this.progressBarSelfieImage = progressBar;

    }

    @Override
    public void initFrontImageBadanUsaha(AppCompatImageView frontImage,
                                         ProgressBar progressBar) {
        this.frontImage = frontImage;
        this.progressBarFrontImage = progressBar;
    }

    @Override
    public void initBackImageBadanUsaha(AppCompatImageView backImage,
                                        ProgressBar progressBar) {
        this.backImage = backImage;
        this.progressBarBackImage = progressBar;
    }

    @Override
    public void initOtpCountDownFragment(AppCompatTextView textView,
                                         LinearLayout retryArea,
                                         AppCompatButton retryButton,
                                         AppCompatEditText otpEditText) {
        this.otpCountDownText = textView;
        this.retryArea = retryArea;
        this.otpEditText = otpEditText;
        retryButton.setOnClickListener(retryClickListener);
    }

    @Override
    public void initMapsFragment(AppCompatEditText outletSearchEditText) {
        this.outletSearchEditText = outletSearchEditText;
        viewModel.bindingSearchEditText(this.outletSearchEditText, ConfigurationUtils.TAG_SEARCH_OUTLET);
    }

    @Override
    public void initLocation(Location location) {
        updateLocation(location);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void initMap(GoogleMap map) {
        this.map = map;
        this.map.setOnMarkerClickListener(marker -> {
            if (marker.getTag() != null) {
                Outlet outlet = (Outlet) marker.getTag();
                showOutletsDialog(outlet);
            }
            return false;
        });
    }

    @Override
    public void showProvinceDialog() {
        viewModel.handleGetProvince(ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS);
    }

    @Override
    public void showCityDialog() {
        if (StringUtils.isNotBlank(idProvinsi))
            viewModel.getKota(idProvinsi, ConfigurationUtils.TAG_GET_KOTA_SUCCESS);
    }

    @Override
    public void showKecamatanDialog() {
        if (StringUtils.isNotBlank(idKota))
            viewModel.getKecamatan(idKota, ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS);
    }

    @Override
    public void showKelurahanDialog() {
        if (StringUtils.isNotBlank(idKecamatan))
            viewModel.getKelurahan(idKecamatan, ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS);
    }

    @Override
    public void showProvinceDataUsahaDialog() {
        viewModel.handleGetProvince(ConfigurationUtils.TAG_GET_PROVINCE_USAHA_SUCCESS);
    }

    @Override
    public void showCityDataUsahaDialog() {
        if (StringUtils.isNotBlank(idProvinsiBidangUsaha))
            viewModel.getKota(idProvinsiBidangUsaha, ConfigurationUtils.TAG_GET_KOTA_USAHA_SUCCESS);
    }

    @Override
    public void showKecamatanDataUsahaDialog() {
        if (StringUtils.isNotBlank(idKotaBidangUsaha))
            viewModel.getKecamatan(idKotaBidangUsaha, ConfigurationUtils.TAG_GET_KECAMATAN_USAHA_SUCCESS);
    }

    @Override
    public void showKelurahanDataUsahaDialog() {
        if (StringUtils.isNotBlank(idKecamatanBidangUsaha))
            viewModel.getKelurahan(idKecamatanBidangUsaha, ConfigurationUtils.TAG_GET_KELURAHAN_USAHA_SUCCESS);
    }

    @Override
    public void initDialogChooserFragment(AppCompatEditText dialogSearchEditText, ArrayAdapter<String> adapter) {
        simpleAdapter = adapter;
        this.dialogSearchEditText = dialogSearchEditText;
        viewModel.bindingSearchEditText(dialogSearchEditText, ConfigurationUtils.TAG_SEARCH_DIALOG);
    }

    @Override
    public void handleOnItemSearchDialogClick(int index, String type) {
        dialogChooserFragment.dismiss();
        dialogChooserFragment = null;
        String item = simpleAdapter.getItem(index);
        String id = MapHelper.getKeyFromValue(dataMap, item).toString();
        if (ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS.equals(type)) {
            idProvinsi = id;
            idKota = null;
            idKecamatan = null;
            idKelurahan = null;
            provinceEditText.setText(item);
            cityEditText.setText(null);
            kecamatanEditText.setText(null);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KOTA_SUCCESS.equals(type)) {
            idKota = id;
            idKecamatan = null;
            idKelurahan = null;
            cityEditText.setText(item);
            kecamatanEditText.setText(null);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS.equals(type)) {
            idKecamatan = id;
            idKelurahan = null;
            kecamatanEditText.setText(item);
            kelurahanEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS.equals(type)) {
            idKelurahan = id;
            kelurahanEditText.setText(item);
        } else if (ConfigurationUtils.TAG_GET_BIDANG_USAHA_SUCCESS.equals(type)) {
            idBidangUsaha = id;
            jenisUsahaEditText.setText(item);
            if (ConfigurationUtils.TAG_INDEX_NON_USAHA.equals(id)) {
                namaUsahaEditText.setText(null);
                namaUsahaEditText.setEnabled(false);
                idProvinsiBidangUsaha = idProvinsi;
                idKotaBidangUsaha = idKota;
                idKecamatanBidangUsaha = idKecamatan;
                idKelurahanBidangUsaha = idKelurahan;
                provinceDataUsahaEditText.setText(provinceEditText.getText().toString());
                cityDataUsahaEditText.setText(cityEditText.getText().toString());
                kecamatanDataUsahaEditText.setText(kecamatanEditText.getText().toString());
                kelurahanDataUsahaEditText.setText(kelurahanEditText.getText().toString());
                alamatUsahaEditText.setText(addressEditText.getText().toString());
            } else {
                namaUsahaEditText.setEnabled(true);
                idProvinsiBidangUsaha = null;
                idKotaBidangUsaha = null;
                idKecamatanBidangUsaha = null;
                idKelurahanBidangUsaha = null;
                provinceDataUsahaEditText.setText(null);
                cityDataUsahaEditText.setText(null);
                kecamatanDataUsahaEditText.setText(null);
                kelurahanDataUsahaEditText.setText(null);
                alamatUsahaEditText.setText(null);
            }

        } else if (ConfigurationUtils.TAG_GET_PROVINCE_USAHA_SUCCESS.equals(type)) {
            idProvinsiBidangUsaha = id;
            idKotaBidangUsaha = null;
            idKecamatanBidangUsaha = null;
            idKelurahanBidangUsaha = null;
            provinceDataUsahaEditText.setText(item);
            cityDataUsahaEditText.setText(null);
            kecamatanDataUsahaEditText.setText(null);
            kelurahanDataUsahaEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KOTA_USAHA_SUCCESS.equals(type)) {
            idKotaBidangUsaha = id;
            idKecamatanBidangUsaha = null;
            idKelurahanBidangUsaha = null;
            cityDataUsahaEditText.setText(item);
            kecamatanDataUsahaEditText.setText(null);
            kelurahanDataUsahaEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KECAMATAN_USAHA_SUCCESS.equals(type)) {
            idKecamatanBidangUsaha = id;
            idKelurahanBidangUsaha = null;
            kecamatanDataUsahaEditText.setText(item);
            kelurahanDataUsahaEditText.setText(null);
        } else if (ConfigurationUtils.TAG_GET_KELURAHAN_USAHA_SUCCESS.equals(type)) {
            idKelurahanBidangUsaha = id;
            kelurahanDataUsahaEditText.setText(item);
        } else if (ConfigurationUtils.TAG_GET_LIST_OF_BANK_SUCCESS.equals(type)) {
            idBank = id;
            namaBankEditText.setText(item);
        }
        dismissKeyboard();
    }

    @Override
    public void initInputanPersonalInfo(AppCompatEditText provinceEditText,
                                        AppCompatEditText cityEditText,
                                        AppCompatEditText kecamatanEditText,
                                        AppCompatEditText kelurahanEditText,
                                        AppCompatEditText hpEditText,
                                        AppCompatEditText addressEditText,
                                        AppCompatEditText emailEditText,
                                        AppCompatEditText nameEditText,
                                        AppCompatEditText ktpEditText, FragmentManager fragmentManager) {
        this.provinceEditText = provinceEditText;
        this.cityEditText = cityEditText;
        this.kecamatanEditText = kecamatanEditText;
        this.kelurahanEditText = kelurahanEditText;
        this.hpEditText = hpEditText;
        this.addressEditText = addressEditText;
        this.emailEditText = emailEditText;
        this.nameEditText = nameEditText;
        this.ktpEditText = ktpEditText;
        this.fragmentManagerPersonalInfo = fragmentManager;
        initPersonalnfo();
    }

    @Override
    public void showBadanUsahaDialog() {
        viewModel.getBidangUsaha();
    }

    @Override
    public void initInputanDataUsaha(AppCompatEditText jenisUsahaEditText,
                                     AppCompatEditText provinceEditText,
                                     AppCompatEditText cityEditText,
                                     AppCompatEditText kecamatanEditText,
                                     AppCompatEditText kelurahanEditText,
                                     AppCompatEditText alamatUsahaEditText,
                                     AppCompatEditText namaUsahaEditText,
                                     FragmentManager fragmentManager) {
        this.jenisUsahaEditText = jenisUsahaEditText;
        this.provinceDataUsahaEditText = provinceEditText;
        this.cityDataUsahaEditText = cityEditText;
        this.kecamatanDataUsahaEditText = kecamatanEditText;
        this.kelurahanDataUsahaEditText = kelurahanEditText;
        this.alamatUsahaEditText = alamatUsahaEditText;
        this.namaUsahaEditText = namaUsahaEditText;
        this.fragmentManagerDataUsaha = fragmentManager;
        if (ConfigurationUtils.TAG_INDEX_NON_USAHA.equals(idBidangUsaha)) {
            if (this.namaUsahaEditText != null) {
                this.namaUsahaEditText.setText(null);
                this.namaUsahaEditText.setEnabled(false);
            }
        } else {
            if (this.namaUsahaEditText != null)
                this.namaUsahaEditText.setEnabled(true);
        }
    }

    @Override
    public void initInputanBankAccount(AppCompatEditText namaBankEditText,
                                       AppCompatEditText namaPemilikEditText,
                                       AppCompatEditText nomorRekeningEditText,
                                       FragmentManager fragmentManager) {
        this.namaBankEditText = namaBankEditText;
        this.namaPemilikEditText = namaPemilikEditText;
        this.nomorRekeningEditText = nomorRekeningEditText;
        this.fragmentManagerBankAccount = fragmentManager;
    }

    @Override
    public void showListOfBankDialog() {
        viewModel.getListOfBank();
    }

    @Override
    public void initInputanPassword(TextInputEditText confirmPasswordEditText,
                                    TextInputEditText passwordEditText,
                                    AppCompatButton finishButton) {
        this.confirmPasswordEditText = confirmPasswordEditText;
        this.passwordEditText = passwordEditText;
        this.finishButton = finishButton;
    }

    @Override
    public void setSmsVerificationCode(String code) {
        otpEditText.setText(code);
        otpEditText.setSelection(code.length());
        if (code.length() == 6)
            viewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                    hpEditText.getText().toString()));
    }

    private void registerReceiverPresenter() {
        smsVerificationReceiver = new SmsVerificationReceiver(this);
        registerReceiver(smsVerificationReceiver, new IntentFilter(SmsVerificationReceiver.SMS_RECEIVED_INTENT));
    }

    private void updateLocation(Location location) {
        latLang = new LatLng(location.getLatitude(), location.getLongitude());
        if (latLang != null && tempPage == ConfigurationUtils.TAG_PAGE_REGISTRATION_MAP) {
            latitude = String.valueOf(latLang.latitude);
            longitude = String.valueOf(latLang.longitude);
            viewModel.getOutlet(latLang);
        }
        updateLocation(latLang);
    }

    private void updateLocation(LatLng latLang) {
        if (map != null) {

            if (marker != null)
                marker.remove();

            marker = map.addMarker(new MarkerOptions().position(latLang).title(positionLabel)
                    .draggable(true));
            marker.setPosition(latLang);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLang, 17));
        }
    }

    private void getCurrentLocation() {
        viewModel.getCurrentLocation();
    }

    private void showOutletsDialog(List<Outlet> outlets) {
        configureRecyclerView(outlets);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetDialog.setOnDismissListener(dialog -> bottomSheetDialog = null);

        if (!bottomSheetDialog.isShowing())
            bottomSheetDialog.show();
    }

    private void showOutletsDialog(Outlet outlet) {
        List<Outlet> outlets = new ArrayList<>();
        outlets.add(outlet);
        showOutletsDialog(outlets);
    }

    private void configureRecyclerView(List<Outlet> outlets) {

        if (bottomSheetDialog != null && bottomSheetDialog.isShowing())
            bottomSheetDialog.dismiss();

        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetView = getLayoutInflater().inflate(R.layout.bottom_sheet_outlet_, null);
        RecyclerView recyclerView = bottomSheetView.findViewById(R.id.recyclerView);
        AppCompatTextView title = bottomSheetView.findViewById(R.id.dialog_title);
        title.setTypeface(getLatoBold());
        outletAdapter = new FastItemAdapter<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(outletAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        outletAdapter.add(outlets);
        outletAdapter.withOnClickListener((v, adapter, item, position) -> {
            if (bottomSheetDialog != null && bottomSheetDialog.isShowing())
                bottomSheetDialog.dismiss();

            kodeCabang = item.getKodeCabang();
            RegistrationActivity.this.outletSearchEditText.setText(null);
            RegistrationActivity.this.onNext();
            return true;
        });
    }

    private void showDialogList(ArrayList<String> values, String title, String hint, String type,
                                FragmentManager fragmentManager) {
        dismissKeyboard();
        if (dialogChooserFragment != null) {
            dialogChooserFragment.dismiss();
            dialogChooserFragment = null;
        }
        dialogChooserFragment = DialogChooserFragment.newInstance(this, values, title,
                hint, type, this);
        dialogChooserFragment.show(fragmentManager, DialogChooserFragment.TAG_SHOW_LIST);
    }

    public void loadMarkerOutlet(Outlet outlet,
                                 LatLngBounds.Builder builder) {
        LatLng latLng = new LatLng(Double.valueOf(outlet.getLatitude()),
                Double.valueOf(outlet.getLongitude()));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.flat(true);
        markerOptions.position(latLng);
        Marker marker = map.addMarker(markerOptions);
        marker.setTag(outlet);
        markers.add(marker);
        builder.include(latLng);
    }

    private void setAgent() {
        RequestRegistration requestRegistration =
                new RequestRegistration(addressEditText.getText().toString().toUpperCase(),
                        alamatUsahaEditText.getText().toString().toUpperCase(),
                        cif,
                        confirmPasswordEditText.getText().toString(),
                        emailEditText.getText().toString(),
                        Long.valueOf(0),
                        ibuKandung,
                        "",
                        idKecamatan.toUpperCase(),
                        idKelurahan.toUpperCase(),
                        idKota.toUpperCase(),
                        idProvinsi.toUpperCase(),
                        jenisKelamin,
                        idBidangUsaha.toUpperCase(),
                        idKecamatanBidangUsaha.toUpperCase(),
                        idKelurahanBidangUsaha.toUpperCase(),
                        idBank.toUpperCase(),
                        kodeCabang.toUpperCase(),
                        idKotaBidangUsaha.toUpperCase(),
                        latitude.toUpperCase(),
                        longitude.toUpperCase(),
                        namaBankEditText.getText().toString().toUpperCase(),
                        nameEditText.getText().toString().toUpperCase(),
                        namaPemilikEditText.getText().toString().toUpperCase(),
                        namaUsahaEditText.getText().toString().toUpperCase(),
                        StringHelper.getStringBuilderToString("0", hpEditText.getText().toString()),
                        ktpEditText.getText().toString().toUpperCase(),
                        nomorRekeningEditText.getText().toString().toUpperCase(),
                        passwordEditText.getText().toString(),
                        requestFilesUploaded.get(ConfigurationUtils.TAG_SUCCESS_UPLOAD_BACK_IMAGE),
                        requestFilesUploaded.get(ConfigurationUtils.TAG_SUCCESS_UPLOAD_FRONT_IMAGE),
                        requestFilesUploaded.get(ConfigurationUtils.TAG_SUCCESS_UPLOAD_KTP_IMAGE),
                        requestFilesUploaded.get(ConfigurationUtils.TAG_SUCCESS_UPLOAD_SELFIE_IMAGE),
                        idProvinsiBidangUsaha,
                        statusKawin,
                        tanggalLahir,
                        "");
        viewModel.registerAgent(requestRegistration);
    }

    private void handleRegistrationSuccess() {
        showDialogSuccess(getResources().getString(R.string.register_success_label), dialog -> {
            circleProgressBar.smoothToHide();
            dialog.dismiss();
            showActivityAndFinishCurrentActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
        });
    }

    private void initPersonalnfo() {
        if (resultCheckKTP != null) {
            if (nameEditText != null && StringUtils.isBlank(nameEditText.getText().toString()))
                nameEditText.setText(resultCheckKTP.getNamaNasabah());
            if (hpEditText != null && StringUtils.isBlank(hpEditText.getText().toString()))
                hpEditText.setText(resultCheckKTP.getNoHp().replaceFirst("0", ""));
            if (ktpEditText != null && StringUtils.isBlank(ktpEditText.getText().toString()))
                ktpEditText.setText(resultCheckKTP.getNoIdentitas());
            if (addressEditText != null && StringUtils.isBlank(addressEditText.getText().toString()))
                addressEditText.setText(resultCheckKTP.getJalan());
            if (provinceEditText != null && StringUtils.isBlank(provinceEditText.getText().toString()))
                provinceEditText.setText(resultCheckKTP.getProvince().getNama());
            if (cityEditText != null && StringUtils.isBlank(cityEditText.getText().toString()))
                cityEditText.setText(resultCheckKTP.getCity().getNama());
            if (kecamatanEditText != null && StringUtils.isBlank(kecamatanEditText.getText().toString()))
                kecamatanEditText.setText(resultCheckKTP.getKecamatan().getNama());
            if (kelurahanEditText != null && StringUtils.isBlank(kelurahanEditText.getText().toString()))
                kelurahanEditText.setText(resultCheckKTP.getKelurahan().getNama());
        }
    }
}