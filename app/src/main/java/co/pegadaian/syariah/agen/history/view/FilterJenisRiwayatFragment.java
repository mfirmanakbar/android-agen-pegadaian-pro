package co.pegadaian.syariah.agen.history.view;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.annotation.Nullable;

import co.pegadaian.syariah.agen.R;

/**
 * Created by T460s on 3/18/2018.
 */

public class FilterJenisRiwayatFragment extends BottomSheetDialogFragment {
    public static FilterJenisRiwayatFragment newInstance() {
        return new FilterJenisRiwayatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_filter_jenis_riwayat, container,
                false);

        // get the views and attach the listener

        return view;

    }
}
