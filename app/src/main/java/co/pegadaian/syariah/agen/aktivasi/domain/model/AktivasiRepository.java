package co.pegadaian.syariah.agen.aktivasi.domain.model;

import co.pegadaian.syariah.agen.base.repository.BaseAgentRepository;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 07/03/2018.
 */

public class AktivasiRepository extends BaseAgentRepository {

    public AktivasiRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }

    public void resetOtpCountDown() {
        getPreferenceManager().setCurrentOtpCountdown(ConfigurationUtils.TAG_OTP_COUNTDOWN);
    }

    public Long getCurrentOtpCountDown() {
        return getPreferenceManager().getCurrentOtpCountdown();
    }
}
