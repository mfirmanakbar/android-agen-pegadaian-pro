package co.pegadaian.syariah.agen.aktivasi.view;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.AktivasiViewModel;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.factory.AktivasiViewModelFactory;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.receiver.OtpView;
import id.app.pegadaian.library.receiver.SmsVerificationReceiver;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseValidatorActivity;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class AktivasiOtpActivity extends BaseValidatorActivity implements OtpView {

    @Inject
    AktivasiViewModelFactory viewModelFactory;
    AktivasiViewModel viewModel;

    @BindView(R.id.otp_text)
    AppCompatTextView otpText;
    @BindView(R.id.timer_text)
    AppCompatTextView timerText;
    @NotEmpty
    @Digits(integer = 6)
    @BindView(R.id.otp_edit_text)
    AppCompatEditText otpEditText;
    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.retry_text)
    AppCompatTextView retryText;
    @BindView(R.id.retry_button)
    AppCompatButton retryButton;
    @BindView(R.id.retry_area)
    LinearLayout retryArea;
    @BindView(R.id.toolbar_title)
    AppCompatTextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private Animation slideUp;
    private Animation slideDown;
    private SmsVerificationReceiver smsVerificationReceiver;
    private String editNomorHp;
    private String accessToken_Id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AktivasiViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        editNomorHp = getIntent().getStringExtra("editNomorHp");
        accessToken_Id = getIntent().getStringExtra("accessToken_Id");
        configureFont();
        initAnimation();
        configureToolbar(getString(R.string.aktivasi_akun));
    }

    @Override
    protected void onStart() {
        super.onStart();
        aktivasiReceiverPresenter();
        initTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(smsVerificationReceiver);
        super.onStop();
        viewModel.resetOtpCountDown();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    private void configureFont() {
        toolbarTitle.setTypeface(getRonniaBold());
        goToNextButton.setTypeface(getRonniaRegular());
        retryButton.setTypeface(getRonniaRegular());
        otpText.setTypeface(getLatoRegular());
        timerText.setTypeface(getLatoRegular());
        otpEditText.setTypeface(getLatoRegular());
        retryText.setTypeface(getLatoRegular());
    }

    private void initTimer() {
        viewModel.timer();
        if (retryArea != null)
            retryArea.setVisibility(View.GONE);
    }

    private void initAnimation() {
        slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                circleProgressBar.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    timerText.setText(String.valueOf(response.result));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER.equals(response.type)) {
                    timerText.setText(String.valueOf(response.result));
                    retryArea.setVisibility(View.VISIBLE);
                    retryArea.startAnimation(slideUp);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS.equals(response.type)) {
                    viewModel.resetOtpCountDown();
                    timerText.setText(viewModel.getRessetedTimer());
                    Intent intent = getIntent(AktivasiOtpActivity.this, SetupPinActivity.class);
                    intent.putExtra("accessToken_Id", accessToken_Id);
                    showActivityAndFinishCurrentActivity(intent);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS.equals(response.type)) {
                    initTimer();
                }
                break;

            case ERROR:
                circleProgressBar.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PERMISSION_DENIED.equals(response.type)) {
                    finishActivity();
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_aktivasi_akun_2;
    }

    @OnClick({R.id.go_to_next_button, R.id.retry_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.go_to_next_button:
                getValidator().validate();
                break;
            case R.id.retry_button:
                retryArea.setVisibility(View.GONE);
                retryArea.startAnimation(slideDown);
                viewModel.agenAktivasi(editNomorHp, ConfigurationUtils.TAG_SEND_RETRY_OTP);
                break;
        }
    }

    @Override
    public void setSmsVerificationCode(String code) {
        otpEditText.setText(code);
        otpEditText.setSelection(code.length());
        if (code.length() == 6)
            viewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                    editNomorHp));
    }

    private void aktivasiReceiverPresenter() {
        smsVerificationReceiver = new SmsVerificationReceiver(this);
        registerReceiver(smsVerificationReceiver, new IntentFilter(smsVerificationReceiver.SMS_RECEIVED_INTENT));
    }

    @Override
    public void onValidationSucceeded() {
        viewModel.verifyOtp(new RequestVerifyOtp(otpEditText.getText().toString(),
                editNomorHp));
    }
}