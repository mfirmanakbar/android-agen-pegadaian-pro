package co.pegadaian.syariah.agen.registration.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class RegistrationBankAccountFragment extends BaseFragment {

    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.nama_bank_edit_label)
    AppCompatTextView namaBankEditLabel;
    @NotEmpty
    @BindView(R.id.nama_bank_edit_text)
    AppCompatEditText namaBankEditText;
    @BindView(R.id.nomor_rekening_edit_label)
    AppCompatTextView nomorRekeningEditLabel;
    @NotEmpty
    @BindView(R.id.nomor_rekening_edit_text)
    AppCompatEditText nomorRekeningEditText;
    @BindView(R.id.nama_pemilik_edit_label)
    AppCompatTextView namaPemilikEditLabel;
    @NotEmpty
    @BindView(R.id.nama_pemilik_edit_text)
    AppCompatEditText namaPemilikEditText;

    private Unbinder unbinder;
    private static RegistrationActivity registrationActivity;

    public static RegistrationBankAccountFragment newInstance(RegistrationActivity activity) {
        RegistrationBankAccountFragment registrationPersonalInfoFragment = new RegistrationBankAccountFragment();
        registrationActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_bank_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        handleOnTouchListener();
        return view;
    }

    private void configureFont() {
        goToNextButton.setTypeface(getRonniaRegular());
        namaBankEditLabel.setTypeface(getLatoBold());
        namaBankEditText.setTypeface(getLatoRegular());
        nomorRekeningEditLabel.setTypeface(getLatoBold());
        nomorRekeningEditText.setTypeface(getLatoRegular());
        namaPemilikEditLabel.setTypeface(getLatoBold());
        namaPemilikEditText.setTypeface(getLatoRegular());
    }

    private void handleOnTouchListener() {
        namaBankEditText.setOnTouchListener((view1, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                dismissKeyboard();
                registrationActivity.showListOfBankDialog();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initInputanBankAccount(namaBankEditText, namaPemilikEditText, nomorRekeningEditText,
                getActivity().getFragmentManager());
    }

    @OnClick(R.id.go_to_next_button)
    public void onViewClicked() {
        getValidator().validate();
    }


    @Override
    public void onValidationSucceeded() {
        registrationActivity.onNext();
    }
}