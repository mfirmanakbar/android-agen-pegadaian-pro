package co.pegadaian.syariah.agen.history.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import co.pegadaian.syariah.agen.MainActivity;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.history.adapter.DataAdapter;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryGadai;
import co.pegadaian.syariah.agen.object.history.HistoryGadai;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryDetailActivity extends BaseToolbarActivity {

    @BindView(R.id.list_transaksi_gadai)
    RecyclerView listTransaksiGadai;
    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;

    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    List<HistoryGadai> historyGadaiList;

    private FastItemAdapter<HistoryGadai> listDataHistoryGadai = new FastItemAdapter<>();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        listTransaksiGadai = (RecyclerView) findViewById(R.id.list_transaksi_gadai);
        circleProgressBar = (AVLoadingIndicatorView) findViewById(R.id.circle_progress_bar);
        configureToolbar("Riwayat Gadai");
        viewModel.getLastHistoryGadai();
        configureItemAdapter();
    }

    private void configureItemAdapter(){
        configureMenuAdapter(new LinearLayoutManager(this), listDataHistoryGadai, listTransaksiGadai);
    }

    private void configureMenuAdapter(RecyclerView.LayoutManager linearLayout,
                                      FastItemAdapter<HistoryGadai> itemAdapter,
                                      RecyclerView recyclerView) {
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setNestedScrollingEnabled(false);
//        itemAdapter.withSelectable(true);
        itemAdapter.withOnClickListener((v, adapter, item, position) -> {
            HistoryGadai historyGadai = historyGadaiList.get(position);
            List<DetailHistoryGadai> detailHistoryGadai = historyGadai.getDetailHistory();
            if(null != detailHistoryGadai && detailHistoryGadai.size() > 0){
                Intent intent = new Intent(HistoryDetailActivity.this, HistoryDetailRiwayatActivity.class);
                intent.putExtra("detailHistoryGadai", Parcels.wrap(detailHistoryGadai.get(0)));
                showActivity(intent);
            } else {
                Toast.makeText(this, "Detail History tidak ditemukan", Toast.LENGTH_SHORT).show();
            }

            return true;
        });
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_transaksi_detail;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                circleProgressBar.smoothToShow();
                break;
            case SUCCESS:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_LIST_GADAI_TERAKHIR.equals(response.type)) {
                    historyGadaiList = (List<HistoryGadai>) response.result;
                    listDataHistoryGadai.clear();
                    listDataHistoryGadai.add(historyGadaiList);
                }
                break;
            case ERROR:
                showDialogError(response.result.toString());
                break;
        }
    }


    public void onClickFilterJenis(View v){
        FilterJenisRiwayatFragment filterJenisRiwayatFragment =
                FilterJenisRiwayatFragment.newInstance();
        filterJenisRiwayatFragment.show(getSupportFragmentManager(),
                "filter_jenis_riwayat");
    }

    public void onClickFilterUrutkan(View v){
        FilterUrutkanRiwayatFragment filterUrutkanRiwayatFragment =
                FilterUrutkanRiwayatFragment.newInstance();
        filterUrutkanRiwayatFragment.show(getSupportFragmentManager(),
                "filter_urutan_riwayat");
    }


}
