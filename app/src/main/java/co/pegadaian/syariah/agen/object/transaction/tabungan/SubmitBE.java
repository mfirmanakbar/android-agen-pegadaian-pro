package co.pegadaian.syariah.agen.object.transaction.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public class SubmitBE {
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("amount")
    @Expose
    private BigDecimal amount;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @SerializedName("id_kecamatan")
    @Expose
    private String idKecamatan;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;
    @SerializedName("id_kota")
    @Expose
    private String idKota;
    @SerializedName("id_provinsi")
    @Expose
    private String idProvinsi;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("reff_switching")
    @Expose
    private String reffSwitching;
    @SerializedName("tanggal_lahir")
    @Expose
    private Long tanggalLahir;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("url_ktp")
    @Expose
    private String urlKtp;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("tanggal_expired")
    @Expose
    private Long tanggalExpired;

    public SubmitBE(String alamat, BigDecimal amount, String ibuKandung, String idKecamatan, String idKelurahan,
                    String idKota, String idProvinsi, String jenisKelamin, String namaNasabah,
                    String noHp, String noIdentitas, String reffSwitching, String statusKawin,
                    Long tanggalLahir, String tempatLahir, String urlKtp, Long tanggalExpired) {
        this.alamat = alamat;
        this.amount = amount;
        this.ibuKandung = ibuKandung;
        this.idKecamatan = idKecamatan;
        this.idKelurahan = idKelurahan;
        this.idKota = idKota;
        this.idProvinsi = idProvinsi;
        this.jenisKelamin = jenisKelamin;
        this.namaNasabah = namaNasabah;
        this.noHp = noHp;
        this.noIdentitas = noIdentitas;
        this.reffSwitching = reffSwitching;
        this.statusKawin = statusKawin;
        this.tanggalLahir = tanggalLahir;
        this.tempatLahir = tempatLahir;
        this.urlKtp = urlKtp;
        this.tanggalExpired = tanggalExpired;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(String idProvinsi) {
        this.idProvinsi = idProvinsi;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getReffSwitching() {
        return reffSwitching;
    }

    public void setReffSwitching(String reffSwitching) {
        this.reffSwitching = reffSwitching;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public Long getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(Long tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }

    public Long getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Long tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getUrlKtp() {
        return urlKtp;
    }

    public void setUrlKtp(String urlKtp) {
        this.urlKtp = urlKtp;
    }
}
