package co.pegadaian.syariah.agen.beranda.viewmodel;

import org.apache.commons.lang3.StringUtils;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.viewmodel.BaseAgentViewModel;
import co.pegadaian.syariah.agen.beranda.domain.interactor.BerandaUseCase;
import co.pegadaian.syariah.agen.object.beranda.Dashboard;
import co.pegadaian.syariah.agen.object.imageslider.DataDashboard;
import co.pegadaian.syariah.agen.object.userprofile.PrivilegeList;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.profile.domain.interactor.AgentUseCase;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class BerandaViewModel extends BaseAgentViewModel {

    public BerandaViewModel(SchedulersFacade schedulersFacade,
                            JsonParser jsonParser,
                            RxBus rxBus,
                            PreferenceManager preferenceManager,
                            BerandaUseCase berandaUseCase,
                            RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, berandaUseCase, realmConfigurator);
    }

    public void routingFlowAuthentication() {
        String flow = (((BerandaUseCase) getBaseUseCase()).isLogin()) ? ConfigurationUtils.TAG_IS_LOGIN : ConfigurationUtils.TAG_NOT_LOGIN;
        if (response().hasActiveObservers())
            response().setValue(Response.success("", flow));
    }

    public void getMenuList(UserProfile userProfile) {
        final Dashboard[] dashboard = {new Dashboard()};
        ((BerandaUseCase) getBaseUseCase()).getMenuList(userProfile)
                .subscribe(new Observer<PrivilegeList>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(PrivilegeList privilegeList) {
                        dashboard[0] = ((BerandaUseCase) getBaseUseCase()).getDashboard(privilegeList, dashboard[0]);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (response().hasActiveObservers())
                            response().setValue(Response.error(getActivity().getResources().getString(R.string.failed_to_load_dashboard),
                                    ConfigurationUtils.TAG_FAILED_LOAD_DASHBOARD));
                    }

                    @Override
                    public void onComplete() {
                        if (response().hasActiveObservers())
                            response().setValue(Response.success(dashboard[0],
                                    ConfigurationUtils.TAG_LOAD_DASHBOARD));
                    }
                });
    }

    public void handleDataDashboard() {
        ((BerandaUseCase) getBaseUseCase())
                .getDataDashboard()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                    ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                DataDashboard dataDashboard = getJsonParser().getObject(responseNostraAPI.getResult(), DataDashboard.class);
                                response().postValue(Response.success(dataDashboard, ConfigurationUtils.TAG_SUCCES_DATA_DASHBOARD));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(), ConfigurationUtils.TAG_FAILED_DATA_DASHBOARD));
                            }

                        }
                    }
                }, handleError());
    }

}