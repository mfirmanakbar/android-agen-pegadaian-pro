package co.pegadaian.syariah.agen.profile.domain.interactor;

import java.io.File;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangeProfile;
import co.pegadaian.syariah.agen.profile.domain.model.AgentRepository;
import id.app.pegadaian.library.compressor.Compressor;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class AgentUseCase extends BaseAgentUseCase {

    @Inject
    public AgentUseCase(AgentRepository repository,
                        Compressor compressor) {
        super(repository, compressor);
    }

    public String getPinOrPassword() {
        return ((AgentRepository) getRepository()).getPinOrPassword();
    }

    public void setPinOrPassword(String pinOrPassword) {
        ((AgentRepository) getRepository()).setPinOrPassword(pinOrPassword);
    }

    public Observable<Object> changePinOrPassword(String lama, String baru, String confirm) {
        return ((AgentRepository) getRepository()).changePinOrPassword(lama, baru, confirm);
    }

    public Observable<Object> getUserProfile() {
        return ((AgentRepository) getRepository()).getUserProfile(getToken());
    }

    public Observable<Object> changeProfile(RequestChangeProfile requestChangeProfile) {
        return ((AgentRepository) getRepository()).changeProfile(getToken(), requestChangeProfile);
    }

    public Observable<Object> uploadImageChangeProfile(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        return ((AgentRepository) getRepository()).uploadImageChangeProfile(body);
    }

    public Observable<Object> getSaldoProfile(){
        return ((AgentRepository) getRepository()).getSaldoProfile(getToken());
    }
}