package co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.object.sample.Request;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.model.SampleRemoteRepository;
import io.reactivex.Observable;

/**
 * Created by Dell on 2/27/2018.
 */

public class SampleRemoteUseCase {

    private final SampleRemoteRepository repository;

    @Inject
    public SampleRemoteUseCase(SampleRemoteRepository repository) {
        this.repository = repository;
    }

    public Observable<Object> getToken(Request request) {
        return repository.getToken(request);
    }
}