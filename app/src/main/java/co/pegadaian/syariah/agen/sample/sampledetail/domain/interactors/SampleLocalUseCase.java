package co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.object.sample.Token;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.model.SampleLocalRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by Dell on 3/2/2018.
 */

public class SampleLocalUseCase extends BaseUseCase {

    @Inject
    public SampleLocalUseCase(SampleLocalRepository repository,
                              Compressor compressor) {
        super(repository, compressor);
    }

    public Flowable<Token> getToken(Realm realm) {
        return (((SampleLocalRepository) getRepository()).getToken(realm) == null) ?
                null : ((SampleLocalRepository) getRepository()).getToken(realm).asFlowable();
    }

    public Observable<Token> insertToken(Realm realm, Token token) {
        return Observable.create(emitter -> {
            emitter.onNext(((SampleLocalRepository) getRepository()).insertToken(realm, token));
            emitter.onComplete();
        });
    }
}