package co.pegadaian.syariah.agen.history.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.history.domain.interactor.HistoryUseCase;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class HistoryViewModelFactory extends BaseViewModelFactory {

    private final HistoryUseCase historyUseCase;

    public HistoryViewModelFactory(SchedulersFacade schedulersFacade,
                                   JsonParser jsonParser,
                                   RxBus rxBus,
                                   PreferenceManager preferenceManager,
                                   HistoryUseCase historyUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.historyUseCase = historyUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HistoryViewModel.class)) {
            return (T) new HistoryViewModel(getSchedulersFacade(),
                    getJsonParser(),
                    getRxBus(),
                    getPreferenceManager(),
                    historyUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
