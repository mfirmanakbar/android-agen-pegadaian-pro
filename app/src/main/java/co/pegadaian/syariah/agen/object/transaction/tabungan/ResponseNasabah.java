package co.pegadaian.syariah.agen.object.transaction.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.pegadaian.syariah.agen.object.master.Domain;

/**
 * Created by ArLoJi on 17/03/2018.
 */

public class ResponseNasabah {

    @SerializedName("cif")
    @Expose
    private String cif;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("kode_cabang")
    @Expose
    private Domain kodeCabang;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("tgl_lahir")
    @Expose
    private Long tglLahir;
    @SerializedName("tgl_kyc")
    @Expose
    private Long tglKyc;
    @SerializedName("status_kyc")
    @Expose
    private String statusKyc;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("kewarganegaraan")
    @Expose
    private String kewarganegaraan;
    @SerializedName("tipe_identitas")
    @Expose
    private String tipeIdentitas;
    @SerializedName("jalan")
    @Expose
    private String jalan;
    @SerializedName("kelurahan")
    @Expose
    private Domain kelurahan;
    @SerializedName("kecamatan")
    @Expose
    private Domain kecamatan;
    @SerializedName("city")
    @Expose
    private Domain city;
    @SerializedName("province")
    @Expose
    private Domain province;
    @SerializedName("tempat_lahir")
    @Expose
    private String tempatLahir;
    @SerializedName("url_ktp")
    @Expose
    private String urlKtp;

    public ResponseNasabah(String noHp, String ibuKandung, String jenisKelamin, String namaNasabah,
                           String noIdentitas, Long tglLahir, String statusKawin, String jalan,
                           Domain kelurahan, Domain kecamatan, Domain city, Domain province,
                           String tempatLahir, String urlKtp) {
        this.noHp = noHp;
        this.ibuKandung = ibuKandung;
        this.jenisKelamin = jenisKelamin;
        this.namaNasabah = namaNasabah;
        this.noIdentitas = noIdentitas;
        this.tglLahir = tglLahir;
        this.statusKawin = statusKawin;
        this.jalan = jalan;
        this.kelurahan = kelurahan;
        this.kecamatan = kecamatan;
        this.city = city;
        this.province = province;
        this.tempatLahir = tempatLahir;
        this.urlKtp = urlKtp;
    }

    public ResponseNasabah(String cif, String noHp, String ibuKandung, String jenisKelamin,
                           Domain kodeCabang, String namaNasabah, String noIdentitas, String telp,
                           Long tglLahir, Long tglKyc, String statusKyc,
                           String statusKawin, String kewarganegaraan, String tipeIdentitas,
                           String jalan, Domain kelurahan, Domain kecamatan, Domain city, Domain province,
                           String tempatLahir, String urlKtp) {
        this(noHp, ibuKandung, jenisKelamin, namaNasabah, noIdentitas, tglLahir, statusKawin, jalan,
                 kelurahan, kecamatan, city, province, tempatLahir, urlKtp);
        this.cif = cif;
        this.kodeCabang = kodeCabang;
        this.telp = telp;
        this.tglKyc = tglKyc;
        this.statusKyc = statusKyc;
        this.kewarganegaraan = kewarganegaraan;
        this.tipeIdentitas = tipeIdentitas;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public Domain getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(Domain kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public Long getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Long tglLahir) {
        this.tglLahir = tglLahir;
    }

    public Long getTglKyc() {
        return tglKyc;
    }

    public void setTglKyc(Long tglKyc) {
        this.tglKyc = tglKyc;
    }

    public String getStatusKyc() {
        return statusKyc;
    }

    public void setStatusKyc(String statusKyc) {
        this.statusKyc = statusKyc;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }

    public String getTipeIdentitas() {
        return tipeIdentitas;
    }

    public void setTipeIdentitas(String tipeIdentitas) {
        this.tipeIdentitas = tipeIdentitas;
    }

    public String getJalan() {
        return jalan;
    }

    public void setJalan(String jalan) {
        this.jalan = jalan;
    }

    public Domain getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(Domain kelurahan) {
        this.kelurahan = kelurahan;
    }

    public Domain getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Domain kecamatan) {
        this.kecamatan = kecamatan;
    }

    public Domain getCity() {
        return city;
    }

    public void setCity(Domain city) {
        this.city = city;
    }

    public Domain getProvince() {
        return province;
    }

    public void setProvince(Domain province) {
        this.province = province;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getUrlKtp() {
        return urlKtp;
    }

    public void setUrlKtp(String urlKtp) {
        this.urlKtp = urlKtp;
    }
}
