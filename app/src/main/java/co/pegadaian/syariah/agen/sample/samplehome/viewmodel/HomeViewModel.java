package co.pegadaian.syariah.agen.sample.samplehome.viewmodel;

import android.support.annotation.NonNull;

import id.app.pegadaian.library.common.ErrorContent;
import co.pegadaian.syariah.agen.object.event.ThrowableEvent;
import co.pegadaian.syariah.agen.object.sample.Token;
import co.pegadaian.syariah.agen.sample.samplehome.domain.interactors.HomeLocalUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.Observable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Dell on 2/28/2018.
 */

public class HomeViewModel extends BaseViewModel {

    private final HomeLocalUseCase homeLocalUseCase;
    private final BehaviorSubject<Integer> valueSubject = BehaviorSubject.create();

    public HomeViewModel(SchedulersFacade schedulersFacade,
                         JsonParser jsonParser,
                         RxBus rxBus,
                         PreferenceManager preferenceManager,
                         HomeLocalUseCase homeLocalUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, homeLocalUseCase);
        this.homeLocalUseCase = ((HomeLocalUseCase)getBaseUseCase());
    }

    public void homeBinding() {
        ConnectableFlowable<Object> tapEventEmitter = getRxBus().asFlowable().publish();
        getDisposables().add(tapEventEmitter
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object event) throws Exception {
                                   if (event instanceof Token.TokenEventRemote) {
                                       Object result = ((Token.TokenEventRemote) event).getResult();
                                       Token token = HomeViewModel.this.getJsonParser().getObject(result, Token.class);
                                       HomeViewModel.this.response().postValue(Response.success(token.getAccessToken()));
                                   } else if (event instanceof Token.TokenEventLocal) {
                                       Token result = ((Token.TokenEventLocal) event).getResult();
                                       HomeViewModel.this.response().postValue(Response.success(result.getAccessToken()));
                                   } else if (event instanceof ErrorContent.ErrorContentEvent) {
                                       Object result = ((ErrorContent.ErrorContentEvent) event).getResult();
                                       ErrorContent error = HomeViewModel.this.getJsonParser().getObject(result, ErrorContent.class);
                                       HomeViewModel.this.response().postValue(Response.error(error.getErrorDescription()));
                                   } else if (event instanceof ThrowableEvent) {
                                       HomeViewModel.this.response().postValue(Response.error(((ThrowableEvent) event).getResult()));
                                   }
                               }
                           },
                        new ConsumerThrowableAdapter() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                HomeViewModel.this.response().setValue(Response.error(throwable));
                            }
                        })
        );
        getDisposables().add(tapEventEmitter.connect());
    }

    public void handleSetValue(Integer value) {
        valueSubject.onNext(value);
    }

    public void changeButtonTextBinding() {
        getDisposables().add(changeButtonText()
                .observeOn(getSchedulersFacade().computation())
                .subscribeOn(getSchedulersFacade().ui())
                .doOnSubscribe(subscription -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        super.accept(aBoolean);
                        getValue();
                    }
                }));
    }

    @NonNull
    private Observable<Boolean> changeButtonText() {
        return valueSubject
                .observeOn(getSchedulersFacade().computation())
                .subscribeOn(getSchedulersFacade().ui())
                .map(value -> value)
                .flatMap(value -> {
                    Observable<Boolean> observableResult = homeLocalUseCase.insertValue(value);
                    return observableResult;
                });
    }

    public void getValue() {
        if (response().hasActiveObservers())
            response().postValue(Response.success(homeLocalUseCase.getValue(), ConfigurationUtils.TAG_CHANGE_BUTTON_VALUE_TYPE));
    }

}
