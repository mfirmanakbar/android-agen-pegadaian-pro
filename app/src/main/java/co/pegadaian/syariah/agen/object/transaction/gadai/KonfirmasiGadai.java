package co.pegadaian.syariah.agen.object.transaction.gadai;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/19/2018.
 */

public class KonfirmasiGadai {

    private String namaNasabah;
    private String barangJaminan;
    private String nilaiTaksiran;
    private String marhunBih;
    private String tenor;
    private String namaBank;
    private String norekRekening;
    private String namaPemilikRekening;

    public KonfirmasiGadai(String namaNasabah,
                           String barangJaminan,
                           String nilaiTaksiran,
                           String marhunBih,
                           String tenor,
                           String namaBank,
                           String norekRekening,
                           String namaPemilikRekening) {
        this.namaNasabah = namaNasabah;
        this.barangJaminan = barangJaminan;
        this.nilaiTaksiran = nilaiTaksiran;
        this.marhunBih = marhunBih;
        this.tenor = tenor;
        this.namaBank = namaBank;
        this.norekRekening = norekRekening;
        this.namaPemilikRekening = namaPemilikRekening;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public String getBarangJaminan() {
        return barangJaminan;
    }

    public String getNilaiTaksiran() {
        return nilaiTaksiran;
    }

    public String getNilaiTaksiranFormatted() {
        return StringHelper.getSimplePriceFormatter(getNilaiTaksiran());
    }

    public String getMarhunBih() {
        return marhunBih;
    }

    public String getMarhunBihFormatted() {
        return StringHelper.getSimplePriceFormatter(getMarhunBih());
    }

    public String getTenor() {
        return tenor;
    }

    public String getNamaBank() {
        return namaBank;
    }

    public String getNorekRekening() {
        return norekRekening;
    }

    public String getNamaPemilikRekening() {
        return namaPemilikRekening;
    }
}
