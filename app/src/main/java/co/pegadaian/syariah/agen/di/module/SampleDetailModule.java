package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleLocalUseCase;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleRemoteUseCase;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.model.SampleLocalRepository;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.model.SampleRemoteRepository;
import co.pegadaian.syariah.agen.sample.sampledetail.restapi.adapter.SampleRestApiAdapter;
import co.pegadaian.syariah.agen.sample.sampledetail.viewmodel.factory.SampleViewModelFactory;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 2/27/2018.
 */

@Module
public class SampleDetailModule {

    @Provides
    SampleRemoteRepository provideSampleRepository() {
        return new SampleRemoteRepository(SampleRestApiAdapter.getSampleRestApi());
    }

    @Provides
    SampleLocalRepository provideSampleLocalRepository(PreferenceManager preferenceManager) {
        return new SampleLocalRepository(preferenceManager);
    }

    @Provides
    SampleViewModelFactory provideSampleViewModelFactory(SampleRemoteUseCase sampleRemoteUseCase,
                                                         SampleLocalUseCase sampleLocalUseCase,
                                                         SchedulersFacade schedulersFacade,
                                                         RealmConfigurator realmConfigurator,
                                                         @Named("JsonParser") JsonParser jsonParser,
                                                         @Named("RxBus") RxBus rxBus,
                                                         PreferenceManager preferenceManager) {
        return new SampleViewModelFactory(sampleRemoteUseCase, sampleLocalUseCase, schedulersFacade,
                realmConfigurator, jsonParser, rxBus, preferenceManager);
    }
}
