package co.pegadaian.syariah.agen.tabungan.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.beranda.domain.interactor.BerandaUseCase;
import co.pegadaian.syariah.agen.beranda.viewmodel.BerandaViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.tabungan.domain.interactors.TabunganUseCase;
import co.pegadaian.syariah.agen.tabungan.viewmodel.TabunganViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class TabunganViewModelFactory extends BaseViewModelFactory {

    private final TabunganUseCase tabunganUseCase;
    private final RealmConfigurator realmConfigurator;

    public TabunganViewModelFactory(SchedulersFacade schedulersFacade,
                                    JsonParser jsonParser,
                                    RxBus rxBus,
                                    PreferenceManager preferenceManager,
                                    TabunganUseCase tabunganUseCase,
                                    RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.tabunganUseCase = tabunganUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(TabunganViewModel.class)) {
            return (T) new TabunganViewModel(getSchedulersFacade(),
                    getJsonParser(),
                    getRxBus(),
                    getPreferenceManager(),
                    tabunganUseCase,
                    realmConfigurator);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
