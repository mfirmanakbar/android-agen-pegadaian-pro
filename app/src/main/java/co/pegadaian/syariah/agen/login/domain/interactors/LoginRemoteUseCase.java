package co.pegadaian.syariah.agen.login.domain.interactors;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.login.domain.model.LoginRemoteRepository;
import co.pegadaian.syariah.agen.object.authentication.Authentication;
import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/3/2018.
 */

public class LoginRemoteUseCase {

    private final LoginRemoteRepository repository;

    @Inject
    public LoginRemoteUseCase(LoginRemoteRepository repository) {
        this.repository = repository;
    }

    public Observable<Object> login(Authentication authentication) {
        return repository.login(authentication);
    }

    public Observable<Object> getUserProfile(SignInResponse signInResponse) {
        return (ConfigurationUtils.TAG_STATUS_AGEN_ACTIVATED.equals(signInResponse.getStatusRegistrasi())) ?
                repository.getUserProfile(signInResponse.getAccessToken()) : null;
    }
}