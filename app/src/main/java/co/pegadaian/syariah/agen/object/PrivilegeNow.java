package co.pegadaian.syariah.agen.object;

import com.google.gson.annotations.Expose;

/**
 * Created by ArLoJi on 07/03/2018.
 */

public class PrivilegeNow {

    @Expose private String name;

    public PrivilegeNow(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
