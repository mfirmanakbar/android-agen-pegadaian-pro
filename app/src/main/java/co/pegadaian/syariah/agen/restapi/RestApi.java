package co.pegadaian.syariah.agen.restapi;

import co.pegadaian.syariah.agen.object.SingleRequest;
import co.pegadaian.syariah.agen.object.authentication.Authentication;
import co.pegadaian.syariah.agen.object.authentication.RequestChangePIN;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.registration.RequestCheckKTP;
import co.pegadaian.syariah.agen.object.registration.RequestRegistration;
import co.pegadaian.syariah.agen.object.tabungan.RequestTopUp;
import co.pegadaian.syariah.agen.object.tabungan.SubmitBeTopUp;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestCheckBank;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSendOtpOpenGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiLogamMulia;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiNonPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSubmitGadai;
import co.pegadaian.syariah.agen.object.transaction.tabungan.DataPembayaran;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangePassword;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangePin;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangeProfile;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Dell on 2/27/2018.
 */

public interface RestApi {

    @Headers("Content-Type: application/json")
    @POST("api/auth/login")
    Observable<Object> login(@Body Authentication authentication);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/region/province")
    Observable<Object> getRegion();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/region/kota")
    Observable<Object> getKota(@Query("province_id") String provinceId);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/region/kecamatan")
    Observable<Object> getKecamatan(@Query("city_id") String cityId);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/region/kelurahan")
    Observable<Object> getKelurahan(@Query("district_id") String districtId);

    @Headers("Content-Type: application/json")
    @POST("api/otp/send-otp")
    Observable<Object> sendOtp(@Body RequestSendSms requestSendSms);

    @Headers("Content-Type: application/json")
    @POST("api/otp/verify-otp")
    Observable<Object> verifyOtp(@Body RequestVerifyOtp requestVerifyOtp);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/bidang-usaha")
    Observable<Object> getBidangUsaha();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/jenis-tempat-usaha")
    Observable<Object> getJenisTempatUsaha();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/status-tempat-usaha")
    Observable<Object> getStatusTempatUsaha();

    @Headers("Content-Type: application/json")
    @GET("api/branch/get-list-by-distance")
    Observable<Object> getOutlet(@Query("latitude") String latitude,
                                 @Query("longitude") String longitude);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/bank-code")
    Observable<Object> getListOfBank();

    @Headers("Content-Type: application/json")
    @POST("api/agent-registration/submit-agen")
    Observable<Object> setAgent(@Body RequestRegistration requestRegistration);

    @Multipart
    @POST("api/image/upload")
    Observable<Object> uploadImage(@Part MultipartBody.Part image);

    @Headers("Content-Type: application/json")
    @POST("api/agent-registration/check-ktp")
    Observable<Object> checkKtp(@Body RequestCheckKTP requestCheckKTP);

    @Headers("Content-Type: application/json")
    @POST("api/agent-registration/change-pin")
    Observable<Object> changePIN(@Query("access_token") String accessToken,
                                 @Body RequestChangePIN requestChangePIN);

    @Headers("Content-Type: application/json")
    @POST("api/agent/ganti-password")
    Observable<Object> changePassword(@Query("access_token") String accessToken,
                                      @Body RequestChangePassword changePassword);

    @Headers("Content-Type: application/json")
    @POST("api/agent/ganti-pin")
    Observable<Object> changePin(@Query("access_token") String accessToken,
                                 @Body RequestChangePin changePin);

    @Headers("Content-Type: application/json")
    @GET("api/agent/get-profile")
    Observable<Object> getUserProfile(@Query("access_token") String id);

    @Headers("Content-Type: application/json")
    @POST("api/agent/update-profile")
    Observable<Object> changeProfile(@Query("access_token") String accessToken,
                                     @Body RequestChangeProfile requestChangeProfile);


    @Headers("Content-Type: application/json")
    @POST("api/tabungan-emas/simulasi")
    Observable<Object> mulaiSimulasi(@Query("access_token") String accessToken,
                                     @Body SingleRequest requestSimulasi);

    @Headers("Content-Type: application/json")
    @POST("api/tabungan-emas/validasi-saldo")
    Observable<Object> validasiSaldo(@Query("access_token") String accessToken,
                                     @Body SingleRequest requestSimulasi);

    @Headers("Content-Type: application/json")
    @POST("api/tabungan-emas/konfirmasi")
    Observable<Object> konfirmasiPembukaanTabunganEmas(@Query("access_token") String accessToken,
                                                       @Body SingleRequest requestSimulasi);

    @Headers("Content-Type: application/json")
    @GET("api/nasabah/get-by-cif")
    Observable<Object> getNasabahByCif(@Query("access_token") String accessToken,
                                       @Query("cif") String cif);

    @Headers("Content-Type: application/json")
    @GET("api/nasabah/get-by-ktp")
    Observable<Object> getNasabahByKtp(@Query("access_token") String accessToken,
                                       @Query("no_ktp") String ktp);

    @Headers("Content-Type: application/json")
    @POST("api/tabungan-emas/submit")
    Observable<Object> submitBukaTabungan(@Query("access_token") String accessToken,
                                          @Body SubmitBE submitBE);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-jenis-perhiasan")
    Observable<Object> getJenisPerhiasan();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-kendaraan-motor")
    Observable<Object> getKendaraanMotor();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-kendaraan-mobil")
    Observable<Object> getKendaraanMobil();

    @Headers("Content-Type: application/json")
    @POST("api/rahn/perhiasan/taksir")
    Observable<Object> taksirPerhiasan(@Query("access_token") String accessToken,
                                       @Body ItemGadai itemGadai);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/perhiasan/simulasi")
    Observable<Object> simulasiPerhiasan(@Query("access_token") String accessToken,
                                         @Body RequestSimulasiPerhiasan requestSimulasiPerhiasan);

    @Headers("Content-Type: application/json")
    @POST("api/agent/cek-pin-agen")
    Observable<Object> validasiPin(@Query("access_token") String accessToken,
                                   @Body SingleRequest requestPin);

    @Headers("Content-Type: application/json")
    @POST("api/tabungan-emas/topuptabunganemas")
    Observable<Object> sendValidasiTopUp(@Query("access_token") String token,
                                         @Body RequestTopUp requestTopUp);

    @Headers("Content-Type: application/json")
    @PUT("api/tabungan-emas/edit-topuptabunganemas/{secure_id}")
    Observable<Object> submitTopUp(@Path("secure_id") String idAgen,
                                   @Query("access_token") String token,
                                   @Body SubmitBeTopUp submitBeTopUp);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/inquiry")
    Observable<Object> mulaiPembayaran(@Query("access_token") String token,
                                       @Body DataPembayaran pembayaran);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/payment")
    Observable<Object> submitKonfirmasiPembayaran(@Query("access_token") String token,
                                                  @Body DataPembayaran pembayaran);

    @Headers("Content-Type: application/json")
    @GET("/api/agent/dashboard")
    Observable<Object> dataDashboard(@Query("access_token") String token);

    @POST("api/validation/cek-bank")
    Observable<Object> checkBank(@Query("access_token") String token,
                                 @Body RequestCheckBank requestCheckBank);

    @Headers("Content-Type: application/json")
    @GET("api/history-rahn")
    Observable<Object> getLastHistoryGadai();

    @Headers("Content-Type: application/json")
    @GET("api/history-tabungan-emas")
    Observable<Object> getLastHistoryEmas();

    @Headers("Content-Type: application/json")
    @POST("api/rahn/perhiasan/send-otp")
    Observable<Object> sendOtpOpenGadai(@Query("access_token") String token,
                                        @Body RequestSendOtpOpenGadai requestSendOtpOpenGadai);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/perhiasan/submit")
    Observable<Object> submitGadaiPerhiasan(@Query("access_token") String token,
                                            @Body RequestSubmitGadai requestSubmitGadai);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/status-kawin")
    Observable<Object> getStatusPernikahan();

    @Streaming
    @GET
    Observable<Response<ResponseBody>> downloadFileByUrl(@Url String fileUrl);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-parameter-keping")
    Observable<Object> getKepingLogam();

    @Headers("Content-Type: application/json")
    @POST("api/rahn/logam-mulia/taksir")
    Observable<Object> taksirLogamMulia(@Query("access_token") String accessToken,
                                        @Body ItemGadai itemGadai);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/logam-mulia/simulasi")
    Observable<Object> simulasiLogamMulia(@Query("access_token") String accessToken,
                                          @Body RequestSimulasiLogamMulia requestSimulasiLogamMulia);

    @Headers("Content-Type: application/json")
    @POST("api/rahn/logam-mulia/submit")
    Observable<Object> submitGadaiLogamMulia(@Query("access_token") String token,
                                             @Body RequestSubmitGadai requestSubmitGadai);

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-kendaraan-mobil")
    Observable<Object> getMobil();

    @Headers("Content-Type: application/json")
    @GET("api/parameters/get-kendaraan-motor")
    Observable<Object> getMotor();

    @Headers("Content-Type: application/json")
    @POST("api/rahn/kendaraan/simulasi")
    Observable<Object> simulasiKendaraan(@Query("access_token") String accessToken,
                                          @Body RequestSimulasiNonPerhiasan requestSimulasiNonPerhiasan);

    @Headers("Content-Type: application/json")
    @GET("api/pemasaran")
    Observable<Object> getLastHistoryPemasaran();

    @Headers("Content-Type: application/json")
    @GET("api/pembayaran")
    Observable<Object> getLastHistorypembayaran();
}