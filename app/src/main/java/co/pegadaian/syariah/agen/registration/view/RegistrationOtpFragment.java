package co.pegadaian.syariah.agen.registration.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class RegistrationOtpFragment extends BaseFragment {

    @BindView(R.id.otp_text)
    AppCompatTextView otpText;
    @BindView(R.id.timer_text)
    AppCompatTextView timerText;
    @NotEmpty
    @Digits(integer = 6)
    @BindView(R.id.otp_edit_text)
    AppCompatEditText otpEditText;
    @BindView(R.id.go_to_next_button)
    AppCompatButton goToNextButton;
    @BindView(R.id.retry_text)
    AppCompatTextView retryText;
    @BindView(R.id.retry_button)
    AppCompatButton retryButton;
    @BindView(R.id.retry_area)
    LinearLayout retryArea;
    private Unbinder unbinder;
    private static RegistrationActivity registrationActivity;

    public static RegistrationOtpFragment newInstance(RegistrationActivity activity) {
        RegistrationOtpFragment registrationOtpFragment = new RegistrationOtpFragment();
        registrationActivity = activity;
        return registrationOtpFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_otp, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        return view;
    }

    private void configureFont() {
        goToNextButton.setTypeface(getRonniaRegular());
        retryButton.setTypeface(getRonniaRegular());
        otpText.setTypeface(getLatoRegular());
        timerText.setTypeface(getLatoRegular());
        otpEditText.setTypeface(getLatoRegular());
        retryText.setTypeface(getLatoRegular());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initOtpCountDownFragment(timerText, retryArea, retryButton, otpEditText);
    }

    @OnClick({R.id.go_to_next_button, R.id.retry_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.go_to_next_button:
                dismissKeyboard();
                getValidator().validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        registrationActivity.onNext();
    }
}