package co.pegadaian.syariah.agen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.history.view.HistoryActivity;
import co.pegadaian.syariah.agen.notification.view.NotificationActivity;
import co.pegadaian.syariah.agen.profile.view.ProfileActivity;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public abstract class MainActivity extends BaseToolbarActivity {

    @BindView(R.id.bottom_bar)
    BottomNavigationViewEx bottomBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        configureToolbarNoBack(getTitleToolbarMain());
        bottomBar.setTextVisibility(false);
        bottomBar.enableAnimation(false);
        bottomBar.enableItemShiftingMode(false);
        bottomBar.enableShiftingMode(false);
        bottomBar.setSelectedItemId(getItemSelected());
        bottomBar.setOnNavigationItemSelectedListener(item -> {
            Class<?> cls = null;
            switch (item.getItemId()){
                case R.id.bottom_beranda:
                    cls = BerandaActivity.class;
                    break;
                case R.id.bottom_riwayat:
                    cls = HistoryActivity.class;
                    break;
                case R.id.bottom_notification:
                    cls = NotificationActivity.class;
                    break;
                case R.id.bottom_profile:
                    cls = ProfileActivity.class;
                    break;
            }
            showActivityNoAnimation(getApplicationContext(),cls);
            return false;
        });

    }

    @Override
    public int getContentViewId() {
        return getContentId();
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return (AppCompatTextView) findViewById(R.id.toolbar_title);
    }

    protected abstract int getContentId();
    protected abstract int getItemSelected();
    protected abstract String getTitleToolbarMain();
}
