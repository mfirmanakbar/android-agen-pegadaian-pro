package co.pegadaian.syariah.agen.sample.samplehome.domain.model;

/**
 * Created by Dell on 3/3/2018.
 */

public interface HomeLocalModel {
    Integer getValue();
    boolean setValue(Integer value);
}
