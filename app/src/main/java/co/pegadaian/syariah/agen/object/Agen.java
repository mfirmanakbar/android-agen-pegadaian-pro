package co.pegadaian.syariah.agen.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by ArLoJi on 05/03/2018.
 */

@RealmClass
public class Agen extends RealmObject{
    @Expose
    private String email;
    @SerializedName("fullName")
    @Expose
    private String fullname;
    @SerializedName("no_identitas")
    @Expose
    private String noIdentitas;
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("bank_customer_name")
    @Expose
    private String bankCustomerName;
    @Expose
    private String cif;
    @Expose
    private String flag;
    @SerializedName("ibu_kandung")
    @Expose
    private String ibuKandung;
    @PrimaryKey
    @SerializedName("id_agen")
    @Expose
    private String idAgen;
    @SerializedName("id_kelurahan")
    @Expose
    private String idKelurahan;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("kode_cabang")
    @Expose
    private String kodeCabang;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("no_rekening")
    @Expose
    private String noRekening;
    @SerializedName("status_kawin")
    @Expose
    private String statusKawin;
    @SerializedName("tipe_agen")
    @Expose
    private String tipeAgen;
    @SerializedName("foto_agen")
    @Expose
    private String fotoAgen;
    @SerializedName("status_agen")
    @Expose
    private String statusAgen;
    @SerializedName("status_registrasi")
    @Expose
    private String statusRegistrasi;
    @SerializedName("status_transaksi")
    @Expose
    private String statusTransaksi;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullname;
    }

    public void setFullName(String fullname) {
        this.fullname = fullname;
    }

    public String getNoIdentitas() {
        return noIdentitas;
    }

    public void setNoIdentitas(String noIdentitas) {
        this.noIdentitas = noIdentitas;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankCustomerName() {
        return bankCustomerName;
    }

    public void setBankCustomerName(String bankCustomerName) {
        this.bankCustomerName = bankCustomerName;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIbuKandung() {
        return ibuKandung;
    }

    public void setIbuKandung(String ibuKandung) {
        this.ibuKandung = ibuKandung;
    }

    public String getIdAgen() {
        return idAgen;
    }

    public void setIdAgen(String idAgen) {
        this.idAgen = idAgen;
    }

    public String getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(String idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getKodeCabang() {
        return kodeCabang;
    }

    public void setKodeCabang(String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }

    public String getStatusKawin() {
        return statusKawin;
    }

    public void setStatusKawin(String statusKawin) {
        this.statusKawin = statusKawin;
    }

    public String getTipeAgen() {
        return tipeAgen;
    }

    public void setTipeAgen(String tipeAgen) {
        this.tipeAgen = tipeAgen;
    }

    public String getFotoAgen() {
        return fotoAgen;
    }

    public void setFotoAgen(String fotoAgen) {
        this.fotoAgen = fotoAgen;
    }

    public String getStatusAgen() {
        return statusAgen;
    }

    public void setStatusAgen(String statusAgen) {
        this.statusAgen = statusAgen;
    }

    public String getStatusRegistrasi() {
        return statusRegistrasi;
    }

    public void setStatusRegistrasi(String statusRegistrasi) {
        this.statusRegistrasi = statusRegistrasi;
    }

    public String getStatusTransaksi() {
        return statusTransaksi;
    }

    public void setStatusTransaksi(String statusTransaksi) {
        this.statusTransaksi = statusTransaksi;
    }
}
