package co.pegadaian.syariah.agen.registration.viewmodel;

import android.support.v7.widget.AppCompatEditText;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.reflect.TypeToken;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.master.MasterData;
import co.pegadaian.syariah.agen.object.master.Outlet;
import co.pegadaian.syariah.agen.object.master.Region;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.registration.RequestCheckKTP;
import co.pegadaian.syariah.agen.object.registration.RequestRegistration;
import co.pegadaian.syariah.agen.object.registration.ResultCheckKTP;
import co.pegadaian.syariah.agen.registration.domain.interactors.RegistrationUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.callback.ConsumerThrowableAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.utils.time.TimeUtils;
import id.app.pegadaian.library.viewmodel.BaseViewModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Dell on 3/6/2018.
 */

public class RegistrationViewModel extends BaseViewModel {

    private Disposable timerDisposable;

    public RegistrationViewModel(SchedulersFacade schedulersFacade,
                                 JsonParser jsonParser,
                                 RxBus rxBus,
                                 PreferenceManager preferenceManager,
                                 RegistrationUseCase useCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, useCase);
    }

    public void setProgress(int page, int totalPage) {
        if (response().hasActiveObservers())
            response().postValue(Response.success(((RegistrationUseCase) getBaseUseCase()).getProgress(page, totalPage),
                    ConfigurationUtils.TAG_UPDATE_PROGRESS));
    }

    public void setFullProgress() {
        if (response().hasActiveObservers())
            response().postValue(Response.success(100, ConfigurationUtils.TAG_UPDATE_PROGRESS));
    }

    public void compressFile(File file, int type) {
        getDisposables().add(getBaseUseCase()
                .compressFile(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<File>() {
                    @Override
                    public void accept(File file) throws Exception {
                        super.accept(file);
                        if (response().hasActiveObservers()) {
                            if (type == 0) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE));
                            } else if (type == 1) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE));
                            } else if (type == 2) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE));
                            } else if (type == 3) {
                                response().postValue(Response.success(file, ConfigurationUtils.TAG_SUCCESS_ADD_SELFIE_IMAGE));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            if (type == 0) {
                                response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                        ConfigurationUtils.TAG_FAILED_ADD_FRONT_IMAGE));
                            } else if (type == 1) {
                                response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                        ConfigurationUtils.TAG_FAILED_ADD_BACK_IMAGE));
                            } else if (type == 2) {
                                response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                        ConfigurationUtils.TAG_FAILED_ADD_KTP_IMAGE));
                            } else if (type == 3) {
                                response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image),
                                        ConfigurationUtils.TAG_FAILED_ADD_SELFIE_IMAGE));
                            }
                        }
                    }
                }));
    }

    @Override
    public void onCameraPermissionGranted() {
        response().postValue(Response.success(true, ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED));
    }

    public void timer() {
        timerDisposable = ((RegistrationUseCase) getBaseUseCase()).timer()
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        String formattedTimer;
                        if (aLong == 0) {
                            ((RegistrationUseCase) getBaseUseCase()).resetOtpCountDown();
                            if (response().hasActiveObservers()) {
                                Long currentOtpCountDown = ((RegistrationUseCase) getBaseUseCase())
                                        .getCurrentOtpCountDown();
                                formattedTimer = TimeUtils.getFormattedTime(currentOtpCountDown / 1000);
                                if (response().hasActiveObservers())
                                    response().postValue(Response.success(formattedTimer,
                                            ConfigurationUtils.TAG_EXPIRED_OTP_COUNTDOWN_TIMER));
                            }
                        } else {
                            long parkingTime = (aLong * 1000);
                            formattedTimer = TimeUtils.getFormattedTime(parkingTime / 1000);
                            if (response().hasActiveObservers())
                                response().postValue(Response.success(formattedTimer,
                                        ConfigurationUtils.TAG_OTP_COUNTDOWN_TIMER));
                        }

                    }
                });
        getDisposables().add(timerDisposable);
    }

    public void resetOtpCountDown() {
        if (timerDisposable != null)
            timerDisposable.dispose();

        ((RegistrationUseCase) getBaseUseCase()).resetOtpCountDown();
    }

    public String getRessetedTimer() {
        return TimeUtils.getFormattedTime(ConfigurationUtils.TAG_OTP_COUNTDOWN / 1000);
    }

    @Override
    public void updateCurrentLocation() {
        updateLocation();
    }

    public void bindingSearchEditText(AppCompatEditText searchEditText, String type) {
        getDisposables().add(RxTextView.textChanges(searchEditText)
                .debounce(ConfigurationUtils.SEARCH_DELAY, TimeUnit.SECONDS)
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {
                        super.accept(charSequence);
                        if (response().hasActiveObservers() && StringUtils.isNotBlank(charSequence))
                            response().postValue(Response.success(charSequence, type));
                    }
                }));
    }

    public void bindingSearchEditText(AppCompatEditText searchEditText, String type, int delay) {
        getDisposables().add(RxTextView.textChanges(searchEditText)
                .debounce(delay, TimeUnit.SECONDS)
                .subscribeOn(getSchedulersFacade().io())
                .observeOn(getSchedulersFacade().ui())
                .subscribe(new ConsumerAdapter<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {
                        super.accept(charSequence);
                        if (response().hasActiveObservers() && StringUtils.isNotBlank(charSequence))
                            response().postValue(Response.success(charSequence, type));
                    }
                }));
    }

    public void handleGetProvince(String type) {
        getDisposables().add(getProvince(type));
    }

    private Disposable getProvince(String provinceType) {
        return ((RegistrationUseCase) getBaseUseCase())
                .getRegion()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdProvinsi(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, provinceType));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }

                    }
                }, handleError());
    }

    public void getKota(String idProvinsi, String cityType) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getKota(idProvinsi)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKota(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, cityType));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getKecamatan(String idKota, String tipeKecamatan) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getKecamatan(idKota)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKecamatan(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, tipeKecamatan));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }

                    }
                }, handleError()));
    }

    public void getKelurahan(String idKecamatan, String tipeKelurahan) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getKelurahan(idKecamatan)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Region>>() {
                            }.getType();
                            List<Region> regions = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (Region region : regions) {
                                regionsMap.put(region.getSortIdKelurahan(), region.getNamaWilayah());
                            }

                            Map<String, String> regionsTreeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            regionsTreeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(regionsTreeMap, tipeKelurahan));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void sendOtp(RequestSendSms requestSendSms, String type) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .sendOtp(requestSendSms)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   if (response().hasActiveObservers()) {
                                       if (ConfigurationUtils.TAG_SEND_OTP.equals(type)) {
                                           response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_OTP_SUCCESS));
                                       } else if (ConfigurationUtils.TAG_SEND_RETRY_OTP.equals(type)) {
                                           response().postValue(Response.success(null, ConfigurationUtils.TAG_SEND_RETRY_OTP_SUCCESS));
                                       }
                                   }
                               }
                           },
                        handleError()));
    }

    public void verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .verifyOtp(requestVerifyOtp)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        if (response().hasActiveObservers()) {
                            ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                            if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                    && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                response().postValue(Response.success(null, ConfigurationUtils.TAG_VERIFY_OTP_SUCCESS));
                            } else {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                        ConfigurationUtils.TAG_VERIFY_OTP_FAILED));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getBidangUsaha() {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getBidangUsaha()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<MasterData>>() {
                            }.getType();
                            List<MasterData> masterDataList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (MasterData masterData : masterDataList) {
                                regionsMap.put(masterData.getSortIdBidangUsaha(), masterData.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap, ConfigurationUtils.TAG_GET_BIDANG_USAHA));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void getOutlet(LatLng latLang) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getOutlet(latLang)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<Outlet>>() {
                            }.getType();
                            List<Outlet> outlets = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            if (response().hasActiveObservers())
                                response().postValue(Response.success(outlets, ConfigurationUtils.TAG_GET_OUTLET_SUCCESS));
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void filterOutlet(CharSequence filter, List<Outlet> outlets) {
        List<Outlet> outletList = new ArrayList<>();
        ((RegistrationUseCase) getBaseUseCase())
                .getFilteredOutlets(filter, outlets)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new Observer<Outlet>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Outlet outlet) {
                        outletList.add(outlet);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        if (response().hasActiveObservers())
                            response().setValue(Response.success(outletList, ConfigurationUtils.TAG_SEARCH_OUTLET_RESULTS));
                    }
                });
    }

    public void getListOfBank() {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .getListOfBank()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<MasterData>>() {
                            }.getType();
                            List<MasterData> masterDataList = getJsonParser().getObjects(responseNostraAPI.getResult(), type);
                            HashMap regionsMap = new HashMap();

                            for (MasterData masterData : masterDataList) {
                                regionsMap.put(masterData.getSortIdBank(), masterData.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(regionsMap);
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap, ConfigurationUtils.TAG_GET_LIST_OF_BANK));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void loadMarkerOutlets(List<Outlet> outlets) {
        getDisposables().add(Observable.fromIterable(outlets)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Outlet>() {
                    @Override
                    public void accept(Outlet outlet) throws Exception {
                        super.accept(outlet);
                        if (response().hasActiveObservers())
                            response().setValue(Response.success(outlet, ConfigurationUtils.TAG_LOAD_MARKER_OUTLET));
                    }
                }));
    }

    public void uploadImageRegistration(File file,
                                        String type,
                                        HashMap<String, File> requestFiles,
                                        HashMap<String, String> requestFilesUploaded) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .uploadImageRegistration(file)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (ConfigurationUtils.TAG_SUCCESS_ADD_FRONT_IMAGE.equals(type)) {
                                requestFilesUploaded.put(ConfigurationUtils.TAG_SUCCESS_UPLOAD_FRONT_IMAGE,
                                        responseNostraAPI.getResult().toString());
                            } else if (ConfigurationUtils.TAG_SUCCESS_ADD_BACK_IMAGE.equals(type)) {
                                requestFilesUploaded.put(ConfigurationUtils.TAG_SUCCESS_UPLOAD_BACK_IMAGE,
                                        responseNostraAPI.getResult().toString());
                            } else if (ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE.equals(type)) {
                                requestFilesUploaded.put(ConfigurationUtils.TAG_SUCCESS_UPLOAD_KTP_IMAGE,
                                        responseNostraAPI.getResult().toString());
                            } else if (ConfigurationUtils.TAG_SUCCESS_ADD_SELFIE_IMAGE.equals(type)) {
                                requestFilesUploaded.put(ConfigurationUtils.TAG_SUCCESS_UPLOAD_SELFIE_IMAGE,
                                        responseNostraAPI.getResult().toString());
                            }

                            if (response().hasActiveObservers() && requestFiles.size() == requestFilesUploaded.size()) {
                                response().setValue(Response.success(requestFilesUploaded, ConfigurationUtils.TAG_SUCCESS_UPLOAD_ALL_IMAGE));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().setValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, new ConsumerThrowableAdapter() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        super.accept(throwable);
                        if (response().hasActiveObservers()) {
                            response().postValue(Response.error(getActivity().getResources().getString(R.string.failed_to_upload_image)));
                        }
                    }
                }));
    }

    public void handleUploadImageRegistration(HashMap<String, File> requestFiles,
                                              HashMap<String, String> requestFilesUploaded) {
        for (Map.Entry<String, File> file : requestFiles.entrySet()) {
            uploadImageRegistration(file.getValue(), file.getKey(), requestFiles, requestFilesUploaded);
        }
    }

    public void registerAgent(RequestRegistration requestRegistration) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .setAgent(requestRegistration)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                           ResponseNostraAPI.class);
                                   if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                           && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                       if (response().hasActiveObservers())
                                           response().postValue(Response.success(null, ConfigurationUtils.TAG_REGISTRATION_SUCCESS));
                                   } else {
                                       response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                               ConfigurationUtils.TAG_REGISTRATION_FAILED));
                                   }
                               }
                           },
                        handleError()));
    }

    public void checkKTP(RequestCheckKTP requestCheckKTP) {
        getDisposables().add(((RegistrationUseCase) getBaseUseCase())
                .checkKTP(requestCheckKTP)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading(ConfigurationUtils.TAG_LOADING)))
                .subscribe(new ConsumerAdapter<Object>() {
                               @Override
                               public void accept(Object object) throws Exception {
                                   super.accept(object);
                                   if (response().hasActiveObservers()) {
                                       ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                               ResponseNostraAPI.class);
                                       if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                               && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                                           ResultCheckKTP resultCheckKTP = getJsonParser().getObject(responseNostraAPI.getResult(),
                                                   ResultCheckKTP.class);
                                           response().postValue(Response.success(resultCheckKTP, ConfigurationUtils.TAG_CHECK_KTP_SUCCESS));
                                       } else {
                                           response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                                   ConfigurationUtils.TAG_CHECK_KTP_FAILED));
                                       }
                                   }
                               }
                           },
                        handleError()));
    }
}