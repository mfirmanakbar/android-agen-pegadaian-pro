package co.pegadaian.syariah.agen.object.userprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import co.pegadaian.syariah.agen.realm.RealmListParcelConverter;
import id.app.pegadaian.library.utils.string.StringHelper;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.UserProfileRealmProxy;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Dell on 3/12/2018.
 */

@RealmClass
@Parcel(implementations = {UserProfileRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {UserProfile.class})
public class UserProfile extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("privilege_list")
    @Expose
    private RealmList<PrivilegeList> privilegeList = new RealmList<>();
    @SerializedName("nama_outlet")
    @Expose
    private String namaOutlet;
    @SerializedName("telp_outlet")
    @Expose
    private String telpOutlet;
    @SerializedName("alamat_outlet")
    @Expose
    private String alamatOutlet;
    @SerializedName("norek_wallet")
    @Expose
    private String norekWallet;
    @SerializedName("deposit")
    @Expose
    private String deposit;
    @SerializedName("deposit_blokir")
    @Expose
    private String depositBlokir;
    @SerializedName("nilai_jaminan")
    @Expose
    private String nilaiJaminan;
    @SerializedName("blokir_jaminan")
    @Expose
    private String blokirJaminan;
    @SerializedName("saldo")
    @Expose
    private String saldo;
    @SerializedName("url_selfie")
    @Expose
    private String urlSelfie;
    @SerializedName("kode_outlet")
    @Expose
    private String kodeOutlet;
    @Expose
    private SignInResponse signInResponse;




    public UserProfile() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RealmList<PrivilegeList> getPrivilegeList() {
        return privilegeList;
    }

    @ParcelPropertyConverter(RealmListParcelConverter.class)
    public void setPrivilegeList(RealmList<PrivilegeList> privilegeList) {
        this.privilegeList = privilegeList;
    }

    public String getNamaOutlet() {
        return namaOutlet;
    }

    public void setNamaOutlet(String namaOutlet) {
        this.namaOutlet = namaOutlet;
    }

    public String getTelpOutlet() {
        return telpOutlet;
    }

    public void setTelpOutlet(String telpOutlet) {
        this.telpOutlet = telpOutlet;
    }

    public String getAlamatOutlet() {
        return alamatOutlet;
    }

    public void setAlamatOutlet(String alamatOutlet) {
        this.alamatOutlet = alamatOutlet;
    }

    public String getNorekWallet() {
        return norekWallet;
    }

    public void setNorekWallet(String norekWallet) {
        this.norekWallet = norekWallet;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getDepositBlokir() {
        return depositBlokir;
    }

    public void setDepositBlokir(String depositBlokir) {
        this.depositBlokir = depositBlokir;
    }

    public String getNilaiJaminan() {
        return nilaiJaminan;
    }

    public void setNilaiJaminan(String nilaiJaminan) {
        this.nilaiJaminan = nilaiJaminan;
    }

    public String getBlokirJaminan() {
        return blokirJaminan;
    }

    public void setBlokirJaminan(String blokirJaminan) {
        this.blokirJaminan = blokirJaminan;
    }

    public String getSaldo() {
        return saldo;
    }

    public String getSaldoFormatted() {
        return StringHelper.getPriceInRp(Double.valueOf(getSaldo()));
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getUrlSelfie() {
        return urlSelfie;
    }

    public void setUrlSelfie(String urlSelfie) {
        this.urlSelfie = urlSelfie;
    }

    public String getKodeOutlet() {
        return kodeOutlet;
    }

    public void setKodeOutlet(String kodeOutlet) {
        this.kodeOutlet = kodeOutlet;
    }

    public SignInResponse getSignInResponse() {
        return signInResponse;
    }

    public void setSignInResponse(SignInResponse signInResponse) {
        this.signInResponse = signInResponse;
    }
}