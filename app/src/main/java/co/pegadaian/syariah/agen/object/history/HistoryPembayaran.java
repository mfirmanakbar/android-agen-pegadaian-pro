package co.pegadaian.syariah.agen.object.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.tabungan.ItemTransaksi;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.utils.time.TimeUtils;

/**
 * Created by T460s on 3/21/2018.
 */

public class HistoryPembayaran extends AbstractItem<ItemTransaksi, HistoryPembayaran.ViewHolder> {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tgl_transaksi")
    @Expose
    private Integer tglTransaksi;
    @SerializedName("nama_nasabah")
    @Expose
    private String namaNasabah;
    @SerializedName("nomor_akad")
    @Expose
    private Integer nomorAkad;
    @SerializedName("nominal")
    @Expose
    private Integer nominal;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("detail_pembayaran")
    @Expose
    private List<DetailHistoryPembayaran> detailPembayaran = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Integer tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public Integer getNomorAkad() {
        return nomorAkad;
    }

    public void setNomorAkad(Integer nomorAkad) {
        this.nomorAkad = nomorAkad;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public List<DetailHistoryPembayaran> getDetailPembayaran() {
        return detailPembayaran;
    }

    public void setDetailPembayaran(List<DetailHistoryPembayaran> detailPembayaran) {
        this.detailPembayaran = detailPembayaran;
    }

    @NonNull
    @Override
    public HistoryPembayaran.ViewHolder getViewHolder(View v) {
        return new HistoryPembayaran.ViewHolder(v);
    }

    @Override
    public int getType() {
        return R.id.menu_id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.card_transaksi_emas;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_jenis)
        TextView jenis;

        @BindView(R.id.tv_nama)
        TextView nama;

        @BindView(R.id.tv_no)
        TextView noNasabah;

        @BindView(R.id.tv_tanggal)
        TextView tanggal;

        @BindView(R.id.tv_nominal)
        TextView nominal;

        Context context;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            context = itemView.getContext();
        }
    }

    @Override
    public void bindView(@NonNull HistoryPembayaran.ViewHolder holder, @NonNull List<Object> payloads) {
        super.bindView(holder, payloads);
        holder.nama.setText(getNamaNasabah());
        holder.jenis.setText(getJenisTransaksi());
        holder.noNasabah.setText(String.valueOf(getNomorAkad()));
        holder.tanggal.setText(TimeUtils.getDateFormated("dd/MM/yyyy",getTglTransaksi()));
        holder.nominal.setText(StringHelper.getPriceInRp(getNominal().doubleValue()));
    }

    @Override
    public void unbindView(@NonNull HistoryPembayaran.ViewHolder holder) {
        super.unbindView(holder);
        holder.nama.setText(null);
        holder.jenis.setText(null);
        holder.noNasabah.setText(null);
        holder.tanggal.setText(null);
        holder.nominal.setText(null);
    }
}
