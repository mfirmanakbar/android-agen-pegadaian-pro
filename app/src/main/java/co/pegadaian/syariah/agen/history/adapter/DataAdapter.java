package co.pegadaian.syariah.agen.history.adapter;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.history.HistoryGadai;

/**
 * Created by T460s on 3/17/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private List<HistoryGadai> historyGadaiList;

    public DataAdapter(List<HistoryGadai> historyGadaiList) {
        this.historyGadaiList = historyGadaiList;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_transaksi, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {
        HistoryGadai historyGadai = historyGadaiList.get(i);
        Log.d("nama", historyGadai.getNamaNasabah());
        viewHolder.nama.setText(historyGadai.getNamaNasabah());
    }

    @Override
    public int getItemCount() {
        return historyGadaiList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nama;
        public ViewHolder(View view) {
            super(view);

            nama = (TextView)view.findViewById(R.id.tv_nama);
        }
    }

}
