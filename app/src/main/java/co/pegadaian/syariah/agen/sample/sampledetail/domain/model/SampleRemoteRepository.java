package co.pegadaian.syariah.agen.sample.sampledetail.domain.model;

import co.pegadaian.syariah.agen.object.sample.Request;
import co.pegadaian.syariah.agen.sample.sampledetail.restapi.SampleRestApi;
import io.reactivex.Observable;

/**
 * Created by Dell on 2/27/2018.
 */

public class SampleRemoteRepository implements SampleRemoteModel {

    private SampleRestApi restApi;

    public SampleRemoteRepository(SampleRestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<Object> getToken(Request request) {
        return restApi.getToken(request.getAuthorization(),
                request.getGrantType(),
                request.getUserName(),
                request.getPassword());
    }
}