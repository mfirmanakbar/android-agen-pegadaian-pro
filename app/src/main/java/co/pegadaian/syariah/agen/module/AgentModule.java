package co.pegadaian.syariah.agen.module;

import co.pegadaian.syariah.agen.object.Agen;
import co.pegadaian.syariah.agen.object.authentication.Group;
import co.pegadaian.syariah.agen.object.authentication.PrivilegeListSignIn;
import co.pegadaian.syariah.agen.object.authentication.SignInResponse;
import co.pegadaian.syariah.agen.object.sample.Token;
import co.pegadaian.syariah.agen.object.userprofile.PrivilegeList;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import io.realm.annotations.RealmModule;

/**
 * Created by Dell on 3/2/2018.
 */

@RealmModule(classes = {Token.class, Agen.class, UserProfile.class, PrivilegeList.class, Group.class,
        PrivilegeListSignIn.class, PrivilegeList.class, SignInResponse.class})
public class AgentModule {
}
