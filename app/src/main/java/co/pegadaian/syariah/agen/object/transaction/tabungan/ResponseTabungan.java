package co.pegadaian.syariah.agen.object.transaction.tabungan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by ArLoJi on 16/03/2018.
 */

public class ResponseTabungan {
    @SerializedName("nominal_pembukaan")
    @Expose
    private BigDecimal nominalPembukaan;
    @SerializedName("biaya_titip")
    @Expose
    private BigDecimal biayaTitip;
    @SerializedName("biaya_administrasi")
    @Expose
    private BigDecimal biayaAdministrasi;
    @SerializedName("biaya_transaksi")
    @Expose
    private BigDecimal biayaTransaksi;
    @SerializedName("saldo_awal_nominal")
    @Expose
    private BigDecimal saldoAwalNominal;
    @SerializedName("saldo_awal_gram")
    @Expose
    private BigDecimal saldoAwalGram;
    @SerializedName("reffCore")
    @Expose
    private String reffCore;
    @SerializedName("reffSwitching")
    @Expose
    private String reffSwitching;
    @SerializedName("hak_nasabah")
    @Expose
    private String hakNasabah;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("saldo_emas")
    @Expose
    private String saldoEmas;
    @SerializedName("saldo_nominal")
    @Expose
    private String saldoNominal;
    @SerializedName("satuan")
    @Expose
    private String satuan;
    @SerializedName("tanggal_buka")
    @Expose
    private String tanggalBuka;
    @SerializedName("jenis_transaksi")
    @Expose
    private String jenisTransaksi;
    @SerializedName("nama_produk")
    @Expose
    private String namaProduk;
    @SerializedName("tglTransaksi")
    @Expose
    private String tglTransaksi;

    public ResponseTabungan() {
    }

    public ResponseTabungan(BigDecimal nominalPembukaan, BigDecimal biayaTitip,
                            BigDecimal biayaAdministrasi, BigDecimal biayaTransaksi,
                            BigDecimal saldoAwalNominal, BigDecimal saldoAwalGram, String reffSwitching) {
        this.nominalPembukaan = nominalPembukaan;
        this.biayaTitip = biayaTitip;
        this.biayaAdministrasi = biayaAdministrasi;
        this.biayaTransaksi = biayaTransaksi;
        this.saldoAwalNominal = saldoAwalNominal;
        this.saldoAwalGram = saldoAwalGram;
        this.reffSwitching = reffSwitching;
    }

    public BigDecimal getNominalPembukaan() {
        return nominalPembukaan;
    }

    public void setNominalPembukaan(BigDecimal nominalPembukaan) {
        this.nominalPembukaan = nominalPembukaan;
    }

    public BigDecimal getBiayaTitip() {
        return biayaTitip;
    }

    public void setBiayaTitip(BigDecimal biayaTitip) {
        this.biayaTitip = biayaTitip;
    }

    public BigDecimal getBiayaAdministrasi() {
        return biayaAdministrasi;
    }

    public void setBiayaAdministrasi(BigDecimal biayaAdministrasi) {
        this.biayaAdministrasi = biayaAdministrasi;
    }

    public BigDecimal getBiayaTransaksi() {
        return biayaTransaksi;
    }

    public void setBiayaTransaksi(BigDecimal biayaTransaksi) {
        this.biayaTransaksi = biayaTransaksi;
    }

    public BigDecimal getSaldoAwalNominal() {
        return saldoAwalNominal;
    }

    public void setSaldoAwalNominal(BigDecimal saldoAwalNominal) {
        this.saldoAwalNominal = saldoAwalNominal;
    }

    public BigDecimal getSaldoAwalGram() {
        return saldoAwalGram;
    }

    public void setSaldoAwalGram(BigDecimal saldoAwalGram) {
        this.saldoAwalGram = saldoAwalGram;
    }

    public String getReffSwitching() {
        return reffSwitching;
    }

    public void setReffSwitching(String reffSwitching) {
        this.reffSwitching = reffSwitching;
    }
}
