package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.login.domain.interactors.LoginLocalUseCase;
import co.pegadaian.syariah.agen.login.domain.interactors.LoginRemoteUseCase;
import co.pegadaian.syariah.agen.login.domain.model.LoginLocalRepository;
import co.pegadaian.syariah.agen.login.domain.model.LoginRemoteRepository;
import co.pegadaian.syariah.agen.login.viewmodel.factory.LoginViewModelFactory;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 2/28/2018.
 */

@Module
public class LoginModule {

    @Provides
    LoginLocalRepository provideLoginLocalRepository(PreferenceManager preferenceManager) {
        return new LoginLocalRepository(preferenceManager);
    }

    @Provides
    LoginRemoteRepository provideSampleRepository() {
        return new LoginRemoteRepository(RestApiAdapter.getRestApi());
    }

    @Provides
    LoginViewModelFactory provideLoginViewModelFactory(SchedulersFacade schedulersFacade,
                                                       @Named("JsonParser") JsonParser jsonParser,
                                                       @Named("RxBus") RxBus rxBus,
                                                       PreferenceManager preferenceManager,
                                                       LoginRemoteUseCase loginRemoteUseCase,
                                                       LoginLocalUseCase loginLocalUseCase,
                                                       RealmConfigurator realmConfigurator) {
        return new LoginViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, loginRemoteUseCase,loginLocalUseCase,realmConfigurator);
    }
}
