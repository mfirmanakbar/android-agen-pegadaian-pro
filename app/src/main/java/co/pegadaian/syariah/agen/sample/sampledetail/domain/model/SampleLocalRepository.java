package co.pegadaian.syariah.agen.sample.sampledetail.domain.model;

import co.pegadaian.syariah.agen.object.sample.Token;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.realm.Realm;

/**
 * Created by Dell on 3/2/2018.
 */

public class SampleLocalRepository extends BaseRepository implements SampleLocalModel {

    public SampleLocalRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }

    @Override
    public Token getToken(Realm realm) {
        return realm.where(Token.class).findFirst();
    }

    @Override
    public Token insertToken(Realm realm, Token token) {
        return realm.copyToRealmOrUpdate(token);
    }
}