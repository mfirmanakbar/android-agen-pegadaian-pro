package co.pegadaian.syariah.agen.base.usecase;

import java.io.File;

import co.pegadaian.syariah.agen.base.repository.BaseAgentRepository;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.domain.model.BaseRepository;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Dell on 3/15/2018.
 */

public class BaseAgentUseCase extends BaseUseCase {

    public BaseAgentUseCase(BaseAgentRepository repository,
                            Compressor compressor) {
        super(repository, compressor);
    }

    public BaseAgentUseCase(BaseRepository repository) {
        super(repository);
    }

    public Flowable<UserProfile> getUserProfile(Realm realm) {
        UserProfile userProfile = ((BaseAgentRepository) getRepository()).getUserProfile(realm);
        return (userProfile == null) ?
                null : userProfile.asFlowable();
    }

    public Observable<UserProfile> insertAgent(Realm realm, UserProfile agen) {
        return Observable.create(emitter -> {
            emitter.onNext(((BaseAgentRepository) getRepository()).setUserProfile(realm, agen));
            emitter.onComplete();
        });
    }

    public Observable<Object> uploadImage(File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        return ((BaseAgentRepository) getRepository()).uploadImage(body);
    }

    public Observable<Object> getRegion() {
        return ((BaseAgentRepository) getRepository()).getRegion();
    }

    public Observable<Object> getKota(String idProvinsi) {
        return ((BaseAgentRepository) getRepository()).getKota(idProvinsi);
    }

    public Observable<Object> getKecamatan(String idKota) {
        return ((BaseAgentRepository) getRepository()).getKecamatan(idKota);
    }

    public Observable<Object> getKelurahan(String idKecamatan) {
        return ((BaseAgentRepository) getRepository()).getKelurahan(idKecamatan);
    }
}