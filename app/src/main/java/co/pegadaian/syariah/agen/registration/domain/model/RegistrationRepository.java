package co.pegadaian.syariah.agen.registration.domain.model;

import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.object.registration.RequestCheckKTP;
import co.pegadaian.syariah.agen.object.registration.RequestRegistration;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import okhttp3.MultipartBody;

/**
 * Created by Dell on 3/6/2018.
 */

public class RegistrationRepository extends BaseRepository {

    private final RestApi restApi;

    public RegistrationRepository(PreferenceManager preferenceManager,
                                  RestApi restApi) {
        super(preferenceManager);
        this.restApi = restApi;
    }

    public void resetOtpCountDown() {
        getPreferenceManager().setCurrentOtpCountdown(ConfigurationUtils.TAG_OTP_COUNTDOWN);
    }

    public Long getCurrentOtpCountDown() {
        return getPreferenceManager().getCurrentOtpCountdown();
    }

    public Observable<Object> getRegion() {
        return restApi.getRegion();
    }

    public Observable<Object> getKota(String idProvinsi) {
        return restApi.getKota(idProvinsi);
    }

    public Observable<Object> getKecamatan(String idKota) {
        return restApi.getKecamatan(idKota);
    }

    public Observable<Object> getKelurahan(String idKecamatan) {
        return restApi.getKelurahan(idKecamatan);
    }

    public Observable<Object> sendOtp(RequestSendSms requestSendSms) {
        return restApi.sendOtp(requestSendSms);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return restApi.verifyOtp(requestVerifyOtp);
    }

    public Observable<Object> getBidangUsaha() {
        return restApi.getBidangUsaha();
    }

    public Observable<Object> getOutlet(String latitude,
                                        String longitude) {
        return restApi.getOutlet(latitude, longitude);
    }

    public Observable<Object> getListOfBank() {
        return restApi.getListOfBank();
    }

    public Observable<Object> setAgent(RequestRegistration requestRegistration) {
        return restApi.setAgent(requestRegistration);
    }

    public Observable<Object> uploadImageRegistration(MultipartBody.Part image) {
        return restApi.uploadImage(image);
    }

    public Observable<Object> checkKTP(RequestCheckKTP requestCheckKTP) {
        return restApi.checkKtp(requestCheckKTP);
    }
}