package co.pegadaian.syariah.agen.profile.domain.model;

import co.pegadaian.syariah.agen.base.repository.BaseAgentRepository;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangePassword;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangePin;
import co.pegadaian.syariah.agen.object.userprofile.RequestChangeProfile;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;
import okhttp3.MultipartBody;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class AgentRepository extends BaseAgentRepository {

    private final RestApi restApi;

    public AgentRepository(PreferenceManager preferenceManager,
                           RestApi restApi,
                           LogoutRestApi logoutRestApi) {
        super(preferenceManager, logoutRestApi);
        this.restApi = restApi;
    }

    public String getPinOrPassword() {
        return getPreferenceManager().getPinOrPassword();
    }

    public void setPinOrPassword(String pinOrPassword) {
        getPreferenceManager().setPinOrPassword(pinOrPassword);
    }

    public Observable<Object> changePinOrPassword(String lama, String baru, String confirm) {
        if (ConfigurationUtils.TAG_IS_PASSWORD.equals(getPreferenceManager().getPinOrPassword())) {
            return restApi.changePassword(getToken(), new RequestChangePassword(lama, baru, confirm));
        } else {
            return restApi.changePin(getToken(), new RequestChangePin(lama, baru, confirm));
        }
    }

    public Observable<Object> getUserProfile(String accessToken) {
        return restApi.getUserProfile(accessToken);
    }

    public Observable<Object> changeProfile(String accessToken,
                                            RequestChangeProfile requestChangeProfile) {
        return restApi.changeProfile(accessToken, requestChangeProfile);
    }

    public Observable<Object> uploadImageChangeProfile(MultipartBody.Part image) {
        return restApi.uploadImage(image);
    }

    public Observable<Object> getSaldoProfile(String accessToken){
        return restApi.dataDashboard(accessToken);
    }

}