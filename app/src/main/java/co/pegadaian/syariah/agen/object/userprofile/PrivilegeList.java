package co.pegadaian.syariah.agen.object.userprofile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.PrivilegeListRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Dell on 3/13/2018.
 */

@RealmClass
@Parcel(implementations = {PrivilegeListRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {PrivilegeList.class})
public class PrivilegeList extends RealmObject {

    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("kategori")
    @Expose
    private String kategori;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

}
