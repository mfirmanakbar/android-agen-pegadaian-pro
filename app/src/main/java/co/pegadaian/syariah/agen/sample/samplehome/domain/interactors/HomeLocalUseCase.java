package co.pegadaian.syariah.agen.sample.samplehome.domain.interactors;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.sample.samplehome.domain.model.HomeLocalRepository;
import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import io.reactivex.Observable;

/**
 * Created by Dell on 3/3/2018.
 */

public class HomeLocalUseCase extends BaseUseCase {

    @Inject
    public HomeLocalUseCase(HomeLocalRepository repository,
                            Compressor compressor) {
        super(repository, compressor);
    }

    public Observable<Boolean> insertValue(Integer value) {
        return Observable.create(emitter -> {
            emitter.onNext(((HomeLocalRepository) getRepository()).setValue(value + 1));
            emitter.onComplete();
        });
    }

    public Integer getValue() {
        return ((HomeLocalRepository) getRepository()).getValue();
    }
}