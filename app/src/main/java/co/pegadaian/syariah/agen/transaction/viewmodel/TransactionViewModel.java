package co.pegadaian.syariah.agen.transaction.viewmodel;

import android.util.Log;

import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import co.pegadaian.syariah.agen.base.viewmodel.BaseTransactionViewModel;
import co.pegadaian.syariah.agen.object.response.MasterSingleResponse;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemUsaha;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadai;
import co.pegadaian.syariah.agen.object.transaction.gadai.ItemGadaiAdapter;
import co.pegadaian.syariah.agen.object.transaction.gadai.JenisKendaraan;
import co.pegadaian.syariah.agen.object.transaction.gadai.JenisPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiLogamMulia;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiNonPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.RequestSimulasiPerhiasan;
import co.pegadaian.syariah.agen.object.transaction.gadai.ResponeSimulasi;
import co.pegadaian.syariah.agen.object.transaction.gadai.ResultListGadai;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.transaction.domain.interactors.TransactionUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.disposables.Disposable;

/**
 * Created by Dell on 3/16/2018.
 */

public class TransactionViewModel extends BaseTransactionViewModel {

    private static final String TAG = TransactionViewModel.class.getSimpleName();

    public TransactionViewModel(SchedulersFacade schedulersFacade,
                                JsonParser jsonParser,
                                RxBus rxBus,
                                PreferenceManager preferenceManager,
                                TransactionUseCase transactionUseCase,
                                RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, transactionUseCase, realmConfigurator);
    }

    public List<String> getValueFromSpinner() {
        return ((TransactionUseCase) getBaseUseCase()).getKarat();
    }

    public int getProgress(int page, int totalPage) {
        return ((TransactionUseCase) getBaseUseCase()).getProgress(page, totalPage);
    }
    public void getStatusTempatUsaha() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getStatusTempatUsaha()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<ItemUsaha>>() {
                            }.getType();
                            List<ItemUsaha> bidangUsahaList = getJsonParser()
                                    .getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (ItemUsaha bidangUsaha : bidangUsahaList) {
                                map.put(bidangUsaha.getValue(), bidangUsaha.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_STATUS_TEMPAT_USAHA));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));


    }
    public void getJenisJenisTempatUsaha() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getJenisTempatUsaha()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<ItemUsaha>>() {
                            }.getType();
                            List<ItemUsaha> bidangUsahaList = getJsonParser()
                                    .getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (ItemUsaha bidangUsaha : bidangUsahaList) {
                                map.put(bidangUsaha.getValue(), bidangUsaha.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_JENIS_TEMPAT_USAHA));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));


    }
    public void getBidangUsaha() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .geBidangUsaha()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<ItemUsaha>>() {
                            }.getType();
                            List<ItemUsaha> bidangUsahaList = getJsonParser()
                                    .getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (ItemUsaha bidangUsaha : bidangUsahaList) {
                                map.put(bidangUsaha.getValue(), bidangUsaha.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);
                            Log.i(TAG, "hasActiveObservers: "+ response().hasActiveObservers());
                            Log.i(TAG, "TREEAMP size:"+ treeMap.size());
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_BIDANG_USAHA));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));

    }

    public void getJenisPerhiasan() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getJenisPerhiasan()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<JenisPerhiasan>>() {
                            }.getType();
                            List<JenisPerhiasan> jenisPerhiasanList = getJsonParser()
                                    .getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (JenisPerhiasan jenisPerhiasan : jenisPerhiasanList) {
                                map.put(jenisPerhiasan.getValue(), jenisPerhiasan.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_JENIS_PERHIASAN));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }

    public void submitBukaTabunganEmas(SubmitBE submitBE) {
        getDisposables().add(submitTabunganToBe(submitBE));
    }

    private Disposable submitTabunganToBe(SubmitBE submitBE) {
        return ((TransactionUseCase) getBaseUseCase()).submitBukaTabunganToBE(submitBE)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object, ResponseNostraAPI.class);
                        if (response().hasActiveObservers()) {
                            if (ConfigurationUtils.TAG_OK.equals(respon.getMessage())) {
                                response().postValue(Response.success(""
                                        , ConfigurationUtils.TAG_BUKA_TABUNGAN_EMAS_BERHASIL));
                            } else {
                                response().postValue(Response.error(respon.getResult()));
                            }
                        }
                    }
                }, handleError());
    }

    public int getValueFromSpinner(List<String> spinner, String key) {
        return ((TransactionUseCase) getBaseUseCase()).getKarat(spinner, key);
    }

    public void taksir(ItemGadai itemGadai, String type) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .taksir(itemGadai, type)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (response().hasActiveObservers()) {
                                MasterSingleResponse masterSingleResponse = getJsonParser().getObject(responseNostraAPI.getResult(),
                                        MasterSingleResponse.class);
                                response().postValue(Response.success(masterSingleResponse.getTaksiran(),
                                        ConfigurationUtils.TAG_SUCCESS_TAKSIR_PERHIASAN));
                            }
                        } else {
                            response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                    ConfigurationUtils.TAG_FAILED_TAKSIR_PERHIASAN));
                        }
                    }
                }, handleError()));
    }

    public void simulasiPerhiasan(List<ItemGadaiAdapter> itemGadaiAdapters, int tenor) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .simulasiPerhiasan(getRequestSimulasiPerhiasan(itemGadaiAdapters, tenor,
                        ConfigurationUtils.TAG_KANTONG_PERHIASAN_PLG))
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (response().hasActiveObservers()) {
                                ResponeSimulasi responeSimulasi = getJsonParser().getObject(responseNostraAPI.getResult(),
                                        ResponeSimulasi.class);
                                response().postValue(Response.success(responeSimulasi,
                                        ConfigurationUtils.TAG_SUCCESS_SIMULASI));
                            }
                        } else {
                            response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                    ConfigurationUtils.TAG_FAILED_SIMULASI));
                        }
                    }
                }, handleError()));
    }

    public void simulasiLogamMulia(List<ItemGadaiAdapter> itemGadaiAdapters, int tenor) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .simulasiLogamMulia(getRequestSimulasiLogamMulia(itemGadaiAdapters, tenor,
                        ConfigurationUtils.TAG_KANTONG_LOGAM_MULIA))
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (response().hasActiveObservers()) {
                                ResponeSimulasi responeSimulasi = getJsonParser().getObject(responseNostraAPI.getResult(),
                                        ResponeSimulasi.class);
                                response().postValue(Response.success(responeSimulasi,
                                        ConfigurationUtils.TAG_SUCCESS_SIMULASI));
                            }
                        } else {
                            response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                    ConfigurationUtils.TAG_FAILED_SIMULASI));
                        }
                    }
                }, handleError()));
    }

    public void simulasiKendaraan(RequestSimulasiNonPerhiasan requestSimulasiNonPerhiasan) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .simulasiKendaraan(requestSimulasiNonPerhiasan)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            if (response().hasActiveObservers()) {
                                ResponeSimulasi responeSimulasi = getJsonParser().getObject(responseNostraAPI.getResult(),
                                        ResponeSimulasi.class);
                                response().postValue(Response.success(responeSimulasi,
                                        ConfigurationUtils.TAG_SUCCESS_SIMULASI));
                            }
                        } else {
                            response().setValue(Response.error(responseNostraAPI.getResult().toString(),
                                    ConfigurationUtils.TAG_FAILED_SIMULASI));
                        }
                    }
                }, handleError()));
    }

    public BigDecimal getTotalTaksiran(List<ItemGadaiAdapter> itemGadaiAdapters) {
        return ((TransactionUseCase) getBaseUseCase()).getTotalTaksiran(itemGadaiAdapters);
    }

    public RequestSimulasiPerhiasan getRequestSimulasiPerhiasan(List<ItemGadaiAdapter> itemGadaiAdapters, int tenor,
                                                                String type) {
        return ((TransactionUseCase) getBaseUseCase()).getRequestSimulasiPerhiasan(itemGadaiAdapters, tenor, type);
    }

    @Override
    public void onCameraPermissionGranted() {
        response().postValue(Response.success(true, ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED));
    }

    public void nextToCariNasabah(BigDecimal mahrumBihValue,
                                  BigDecimal batasMinMahrumBihValue,
                                  BigDecimal batasAtasMahrumBihValue) {
        if (response().hasActiveObservers()) {
            if (((TransactionUseCase) getBaseUseCase()).checkBatasMahrumBihValue(mahrumBihValue, batasMinMahrumBihValue, batasAtasMahrumBihValue)) {
                response().postValue(Response.success("", ConfigurationUtils.TAG_GO_TO_CARI_NASABAH));
            } else {
                response().postValue(Response.error("", ConfigurationUtils.TAG_FAILED_GO_TO_CARI_NASABAH));
            }
        }
    }

    public void getKepingLogam() {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getKepingLogam()
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        handleResponseResultListGadai(object);
                    }
                }, handleError()));
    }
    public void getJenisKendaraan(int tipeKendaraan) {
        getDisposables().add(((TransactionUseCase) getBaseUseCase())
                .getJenisKendaraan(tipeKendaraan)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                                ResponseNostraAPI.class);
                        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
                            Type type = new TypeToken<ArrayList<JenisKendaraan>>() {
                            }.getType();
                            List<JenisKendaraan> jenisKendaraanList = getJsonParser()
                                    .getObjects(responseNostraAPI.getResult(), type);

                            HashMap map = new HashMap();

                            for (JenisKendaraan jenisKendaraan : jenisKendaraanList) {
                                map.put(jenisKendaraan.getValue(), jenisKendaraan.getDescription());
                            }

                            Map<String, String> treeMap = new TreeMap<>(
                                    (o1, o2) -> o1.compareTo(o2));
                            treeMap.putAll(map);

                            if (response().hasActiveObservers()) {
                                response().postValue(Response.success(treeMap,
                                        ConfigurationUtils.TAG_LOAD_JENIS_KENDARAAN));
                            }
                        } else {
                            if (response().hasActiveObservers()) {
                                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
                            }
                        }
                    }
                }, handleError()));
    }


    public RequestSimulasiLogamMulia getRequestSimulasiLogamMulia(List<ItemGadaiAdapter> itemGadaiAdapters, int tenor,
                                                                  String type) {
        return ((TransactionUseCase) getBaseUseCase()).getRequestSimulasiLogamMulia(itemGadaiAdapters, tenor, type);
    }

    public void getKendaraan(String type) {
        if (StringUtils.isNotBlank(type))
            getDisposables().add(((TransactionUseCase) getBaseUseCase())
                    .getKendaraan(type)
                    .observeOn(getSchedulersFacade().ui())
                    .subscribeOn(getSchedulersFacade().io())
                    .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                    .subscribe(new ConsumerAdapter<Object>() {
                        @Override
                        public void accept(Object object) throws Exception {
                            super.accept(object);
                            handleResponseResultListGadai(object);
                        }
                    }, handleError()));
    }

    private void handleResponseResultListGadai(Object object) {
        ResponseNostraAPI responseNostraAPI = getJsonParser().getObject(object,
                ResponseNostraAPI.class);
        if (StringUtils.isNotBlank(responseNostraAPI.getMessage())
                && ConfigurationUtils.TAG_OK.equals(responseNostraAPI.getMessage())) {
            Type type = new TypeToken<ArrayList<ResultListGadai>>() {
            }.getType();
            List<ResultListGadai> resultListGadaiList = getJsonParser()
                    .getObjects(responseNostraAPI.getResult(), type);
            List<String> kepingList = new ArrayList<>();
            for (ResultListGadai resultListGadai : resultListGadaiList) {
                kepingList.add(resultListGadai.getValue());
            }
            if (response().hasActiveObservers()) {
                response().postValue(Response.success(kepingList,
                        ConfigurationUtils.TAG_LOAD_RESULT_LIST_GADAI));
            }
        } else {
            if (response().hasActiveObservers()) {
                response().postValue(Response.error(responseNostraAPI.getResult().toString()));
            }
        }
    }

    public List<String> getListOfYears() {
        return ((TransactionUseCase) getBaseUseCase()).getListOfYears();
    }
}