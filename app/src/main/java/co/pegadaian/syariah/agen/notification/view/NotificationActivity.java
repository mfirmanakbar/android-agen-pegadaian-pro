package co.pegadaian.syariah.agen.notification.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import co.pegadaian.syariah.agen.MainActivity;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class NotificationActivity extends MainActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_beranda;
    }

    @Override
    protected int getItemSelected() {
        return R.id.bottom_notification;
    }

    @Override
    protected String getTitleToolbarMain() {
        return "Notifikasi";
    }


    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                break;
            case ERROR:
                break;
        }
    }
}
