package co.pegadaian.syariah.agen.sample.sampledetail.domain.model;

import co.pegadaian.syariah.agen.object.sample.Token;
import io.realm.Realm;

/**
 * Created by Dell on 3/2/2018.
 */

public interface SampleLocalModel {
    Token getToken(Realm realm);
    Token insertToken(Realm realm, Token token);
}