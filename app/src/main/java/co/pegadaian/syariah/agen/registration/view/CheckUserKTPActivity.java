package co.pegadaian.syariah.agen.registration.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.object.registration.RequestCheckKTP;
import co.pegadaian.syariah.agen.object.registration.ResultCheckKTP;
import co.pegadaian.syariah.agen.registration.viewmodel.RegistrationViewModel;
import co.pegadaian.syariah.agen.registration.viewmodel.factory.RegistrationViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.widget.colordialog.PromptDialog;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.view.activity.BaseValidatorActivity;

public class CheckUserKTPActivity extends BaseValidatorActivity {

    @NotEmpty
    @BindView(R.id.ktp_search_edit_text)
    AppCompatEditText ktpSearchEditText;
    @BindView(R.id.hasil_pencarian_text_view)
    AppCompatTextView hasilPencarianTextView;
    @BindView(R.id.user_name)
    AppCompatTextView userName;
    @BindView(R.id.user_id)
    AppCompatTextView userId;
    @BindView(R.id.hp_label)
    AppCompatTextView hpLabel;
    @BindView(R.id.hp_text_view)
    AppCompatTextView hpTextView;
    @BindView(R.id.ktp_label)
    AppCompatTextView ktpLabel;
    @BindView(R.id.ktp_text_view)
    AppCompatTextView ktpTextView;
    @BindView(R.id.register_button)
    AppCompatButton registerButton;
    @BindView(R.id.search_result_area)
    LinearLayout searchResultArea;
    @BindView(R.id.btn_search)
    AppCompatButton btnSearch;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private String cariNasabahLabel;

    @Inject
    RegistrationViewModelFactory viewModelFactory;
    RegistrationViewModel viewModel;
    private ResultCheckKTP resultCheckKTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RegistrationViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        searchResultArea.setVisibility(View.GONE);
        initResources();
        configureToolbar(cariNasabahLabel);
        configureFont();
        ktpSearchEditText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                getValidator().validate();
                return true;
            }
            return false;
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValidator().validate();
            }
        });

    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOADING.equals(response.type)) {
                    circleProgressBar.smoothToShow();
                }
                break;
            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHECK_KTP.equals(response.type)) {
                    CharSequence result = (CharSequence) response.result;
                    RequestCheckKTP checkKTP = new RequestCheckKTP(result.toString());
                    viewModel.checkKTP(checkKTP);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHECK_KTP_SUCCESS.equals(response.type)) {
                    resultCheckKTP = (ResultCheckKTP) response.result;
                    circleProgressBar.smoothToHide();
                    dismissKeyboard();
                    searchResultArea.setVisibility(View.VISIBLE);
                    userName.setText(resultCheckKTP.getNamaNasabah());
                    userId.setText(resultCheckKTP.getCif());
                    ktpTextView.setText(resultCheckKTP.getNoIdentitas());
                    hpTextView.setText(resultCheckKTP.getNoHp());
                }
                break;
            case ERROR:
                circleProgressBar.smoothToHide();
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CHECK_KTP_FAILED.equals(response.type)) {
                    searchResultArea.setVisibility(View.GONE);
                    showDialogError(getResources().getString(R.string.check_ktp_not_found),
                            getResources().getString(R.string.check_ktp_not_found_info),
                            (PromptDialog.OnPositiveListener) dialog -> {
                                dialog.dismiss();
                                CheckUserKTPActivity.this.showActivity(new Intent(CheckUserKTPActivity.this,
                                        RegistrationActivity.class));
                            });
                } else {
                    showDialogError(response.result.toString());
                }
                break;
        }
    }

    private void configureFont() {
        ktpSearchEditText.setTypeface(getLatoRegular());
        hasilPencarianTextView.setTypeface(getLatoRegular());
        userName.setTypeface(getLatoBold());
        userId.setTypeface(getLatoBold());
        hpLabel.setTypeface(getLatoRegular());
        hpTextView.setTypeface(getLatoRegular());
        ktpLabel.setTypeface(getLatoRegular());
        ktpTextView.setTypeface(getLatoRegular());
        registerButton.setTypeface(getLatoRegular());

    }

    private void initResources() {
        cariNasabahLabel = getResources().getString(R.string.cari_nasabah_label);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_check_user_ktp;
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        viewModel.bindingSearchEditText(ktpSearchEditText, ConfigurationUtils.TAG_CHECK_KTP, 2);
    }

    @OnClick(R.id.register_button)
    public void onViewClicked() {
        Intent intent = getIntent(CheckUserKTPActivity.this, RegistrationActivity.class);
        intent.putExtra("resultCheckKTP", Parcels.wrap(resultCheckKTP));
        showActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        if (ktpSearchEditText.getText().toString().length() == 16) {
            RequestCheckKTP checkKTP = new RequestCheckKTP(ktpSearchEditText.getText().toString());
            viewModel.checkKTP(checkKTP);
        } else {
            ktpSearchEditText.setError(getResources().getString(R.string.error_not_valid_ktp));
        }
    }
}
