package co.pegadaian.syariah.agen.aktivasi.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiRemoteUseCase;
import co.pegadaian.syariah.agen.aktivasi.domain.interactor.AktivasiUseCase;
import co.pegadaian.syariah.agen.aktivasi.viewmodel.AktivasiViewModel;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by ArLoJi on 07/03/2018.
 */

public class AktivasiViewModelFactory extends BaseViewModelFactory {

    private final AktivasiUseCase aktivasiUseCase;
    private final AktivasiRemoteUseCase aktivasiRemoteUseCase;
    private final RealmConfigurator realmConfigurator;

    public AktivasiViewModelFactory(SchedulersFacade schedulersFacade,
                                    JsonParser jsonParser,
                                    RxBus rxBus,
                                    PreferenceManager preferenceManager,
                                    AktivasiUseCase aktivasiUseCase,
                                    AktivasiRemoteUseCase aktivasiRemoteUseCase,
                                    RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.aktivasiUseCase = aktivasiUseCase;
        this.aktivasiRemoteUseCase = aktivasiRemoteUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AktivasiViewModel.class)) {
            return (T) new AktivasiViewModel(getSchedulersFacade(), getJsonParser(),
                    getRxBus(), getPreferenceManager(),
                    aktivasiUseCase, aktivasiRemoteUseCase, realmConfigurator);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
