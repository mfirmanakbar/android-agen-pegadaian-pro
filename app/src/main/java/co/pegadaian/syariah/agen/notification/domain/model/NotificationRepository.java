package co.pegadaian.syariah.agen.notification.domain.model;

import id.app.pegadaian.library.domain.model.BaseRepository;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class NotificationRepository extends BaseRepository {

    public NotificationRepository(PreferenceManager preferenceManager) {
        super(preferenceManager);
    }
}
