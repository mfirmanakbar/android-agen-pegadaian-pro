package co.pegadaian.syariah.agen.object.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.PrivilegeListSignInRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Dell on 3/15/2018.
 */

@RealmClass
@Parcel(implementations = {PrivilegeListSignInRealmProxy.class},
        value = Parcel.Serialization.BEAN,
        analyze = {PrivilegeListSignIn.class})
public class PrivilegeListSignIn extends RealmObject {

    @PrimaryKey
    @SerializedName("name")
    @Expose
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
