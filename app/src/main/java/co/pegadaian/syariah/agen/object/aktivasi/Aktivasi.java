package co.pegadaian.syariah.agen.object.aktivasi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ArLoJi on 08/03/2018.
 */

public class Aktivasi {
    @SerializedName("no_hp")
    @Expose
    private String nomorHp;
    @SerializedName("kode_otp")
    @Expose
    private String kodeOtp;
    @Expose
    private String id;

    public Aktivasi(String nomorHp, String kodeOtp, String id) {
        this.nomorHp = nomorHp;
        this.kodeOtp = kodeOtp;
        this.id = id;
    }

    public String getNomorHp() {
        return nomorHp;
    }

    public void setNomorHp(String nomorHp) {
        this.nomorHp = nomorHp;
    }

    public String getKodeOtp() {
        return kodeOtp;
    }

    public void setKodeOtp(String kodeOtp) {
        this.kodeOtp = kodeOtp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
