package co.pegadaian.syariah.agen.transaction.view.activity.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.base.view.BaseTransactionActivity;
import co.pegadaian.syariah.agen.beranda.view.BerandaActivity;
import co.pegadaian.syariah.agen.landing.view.LandingActivity;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseTabungan;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.transaction.view.fragment.CariPenggunaFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.KonfirmasiPembukaanTabunganFragment;
import co.pegadaian.syariah.agen.transaction.view.fragment.SimulasiPembukaanTabunganFragment;
import co.pegadaian.syariah.agen.tabungan.viewmodel.TabunganViewModel;
import co.pegadaian.syariah.agen.tabungan.viewmodel.factory.TabunganViewModelFactory;
import co.pegadaian.syariah.agen.transaction.view.fragment.TambahNasabahFragment;
import co.pegadaian.syariah.agen.transaction.viewmodel.TransactionViewModel;
import co.pegadaian.syariah.agen.transaction.viewmodel.factory.TransactionViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.easyimage.EasyImage;
import id.app.pegadaian.library.custom.edittext.CustomFontLatoRegulerEditText;
import id.app.pegadaian.library.widget.colordialog.PromptDialog;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.custom.textview.CustomFontLatoBoldTextView;
import id.app.pegadaian.library.custom.textview.CustomFontLatoRegularTextView;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.CustomViewPager;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;
import id.app.pegadaian.library.widget.dialog.DialogPin;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by ArLoJi on 16/03/2018.
 */

public class PembukaanTabunganEmasActivity extends BaseTransactionActivity {

    @Inject
    TransactionViewModelFactory viewModelFactory;
    TransactionViewModel viewModel;

    @BindView(R.id.progress_bar)
    MaterialProgressBar progressBar;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.circle_progress_bar)
    AVLoadingIndicatorView circleProgressBar;

    private ViewPager.OnPageChangeListener pageChangeListener;
    private ViewPagerAdapter adapter;
    private DialogPin dialogPin;

    private String titleBukaTabungan;
    private String deskripsiSimulasi;
    private String deskripsiDataNasabah;
    private String deskripsiKonfirmasiPembukaan;
    private int tempPage = 1;
    private ResponseTabungan responseTabungan;
    private String urlKtpImage;

    //init pembukaan tabungan
    private LinearLayout areaSimulasi;

    //init konfirmasi pembukaan tabungan emas
    CustomFontLatoBoldTextView namaNasabah;
    CustomFontLatoBoldTextView nomorKtp;
    CustomFontLatoBoldTextView nominalPembukaan;
    CustomFontLatoBoldTextView biayaTitip;
    CustomFontLatoBoldTextView biayaAdministrasi;
    CustomFontLatoBoldTextView biayaTransaksi;
    CustomFontLatoBoldTextView saldoAwal;
    CustomFontLatoBoldTextView saldoAwalEmas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        configViewModel(viewModel);
        ButterKnife.bind(this);
        configureTitle();
        configureToolbar(titleBukaTabungan, deskripsiSimulasi);
        initListener();
        viewPagerConfigurator();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        handleOnBack();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }

    private void configureTitle(){
        titleBukaTabungan = getString(R.string.emas);
        deskripsiSimulasi = getString(R.string.simulasi_pembukaan_tabungan);
        deskripsiDataNasabah = getString(R.string.data_nasabah);
        deskripsiKonfirmasiPembukaan = getString(R.string.konfirmasi_pembukaan);
    }

    private void showSimulationResult(ResponseTabungan simulasi){
        nominalPembukaan.setText(StringHelper.getPriceInRp(simulasi.getNominalPembukaan()));
        biayaTitip.setText(StringHelper.getPriceInRp(simulasi.getBiayaTitip()));
        biayaAdministrasi.setText(StringHelper.getPriceInRp(simulasi.getBiayaAdministrasi()));
        biayaTransaksi.setText(StringHelper.getPriceInRp(simulasi.getBiayaTransaksi()));
        saldoAwal.setText(StringHelper.getPriceInRp(simulasi.getSaldoAwalNominal()));
        saldoAwalEmas.setText(StringHelper.getStringBuilderToString(simulasi.getSaldoAwalGram().toString()," gr"));
    }

    private void showHasilKonfirmasi(ResponseTabungan tabungan){
        namaNasabah.setText(getResponseNasabah().getNamaNasabah());
        nomorKtp.setText(getResponseNasabah().getNoIdentitas());
        nominalPembukaan.setText(StringHelper.getPriceInRp(tabungan.getNominalPembukaan()));
        biayaTitip.setText(StringHelper.getPriceInRp(tabungan.getBiayaTitip()));
        biayaAdministrasi.setText(StringHelper.getPriceInRp(tabungan.getBiayaAdministrasi()));
        biayaTransaksi.setText(StringHelper.getPriceInRp(tabungan.getBiayaTransaksi()));
        saldoAwal.setText(StringHelper.getPriceInRp(tabungan.getSaldoAwalNominal()));
        saldoAwalEmas.setText(StringHelper.getStringBuilderToString(tabungan.getSaldoAwalGram().toString()," gr"));
    }

    private void viewPagerConfigurator() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(TambahNasabahFragment.newInstance(this, ConfigurationUtils.TAG_PEMBUKAAN_TABUNGAN_ACTIVITY), "");
        adapter.addFragment(SimulasiPembukaanTabunganFragment.newInstance(this), "");
        adapter.addFragment(CariPenggunaFragment.newInstance(this, ConfigurationUtils.TAG_PEMBUKAAN_TABUNGAN_ACTIVITY), "");
        adapter.addFragment(KonfirmasiPembukaanTabunganFragment.newInstance(this), "");
        pager.setAdapter(adapter);
        pager.disableScroll(true);
        pager.addOnPageChangeListener(pageChangeListener);
        pager.setCurrentItem(1);
        progressBar.setProgress(viewModel.getProgress(0,adapter.getCount()));
    }

    public void initComponentSimulasi(CustomFontLatoBoldTextView nominalPembukaan,
                                      CustomFontLatoBoldTextView biayaTitip,
                                      CustomFontLatoBoldTextView biayaAdministrasi,
                                      CustomFontLatoBoldTextView biayaTransaksi,
                                      CustomFontLatoBoldTextView saldoAwal,
                                      CustomFontLatoBoldTextView saldoAwalEmas,
                                      LinearLayout areaSimulasi){
        this.nominalPembukaan = nominalPembukaan;
        this.biayaTitip = biayaTitip;
        this.biayaAdministrasi = biayaAdministrasi;
        this.biayaTransaksi = biayaTransaksi;
        this.saldoAwal = saldoAwal;
        this.saldoAwalEmas = saldoAwalEmas;
        this.areaSimulasi = areaSimulasi;
    }

    public void initComponentKonfirmasiPembukaan(CustomFontLatoBoldTextView namaNasabah,
                                                 CustomFontLatoBoldTextView nomorKtp,
                                                 CustomFontLatoBoldTextView nominalPembukaan,
                                                 CustomFontLatoBoldTextView biayaTitip,
                                                 CustomFontLatoBoldTextView biayaAdministrasi,
                                                 CustomFontLatoBoldTextView biayaTransaksi,
                                                 CustomFontLatoBoldTextView saldoAwal,
                                                 CustomFontLatoBoldTextView saldoAwalEmas) {
        this.namaNasabah = namaNasabah;
        this.nomorKtp = nomorKtp;
        this.nominalPembukaan = nominalPembukaan;
        this.biayaTitip = biayaTitip;
        this.biayaAdministrasi = biayaAdministrasi;
        this.biayaTransaksi = biayaTransaksi;
        this.saldoAwal = saldoAwal;
        this.saldoAwalEmas = saldoAwalEmas;
    }

    private void initListener() {
        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //handle action@page
                if (position == ConfigurationUtils.TAG_PAGE_PERTAMA) {
                    configureToolbar(titleBukaTabungan, deskripsiSimulasi);
                } else if (position == ConfigurationUtils.TAG_PAGE_KEDUA){
                    configureToolbar(titleBukaTabungan,deskripsiDataNasabah);
                } else if (position == ConfigurationUtils.TAG_PAGE_KETIGA){
                    configureToolbar(titleBukaTabungan,deskripsiKonfirmasiPembukaan);
                }

                //handle progressbar
                if (position>0){
                    progressBar.setProgress(viewModel.getProgress(position-1,adapter.getCount()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
    }

    //Init function from fragment
    public void initTabungan(String nominalAwal, String type){
        circleProgressBar.smoothToShow();
        viewModel.checkAmount(nominalAwal, type);
    }
    public void checkKtpOrCif(String ktpOrCif, String type){
        setImageSearch(true);
        circleProgressBar.smoothToShow();
        viewModel.checkKtpOrCif(ktpOrCif, type);
    }

    @Override
    public void nextToOtp() {

    }

    @Override
    public void nextToSubmitGadai() {

    }

    public void sendKonfirmasiTabungan(String type){
        circleProgressBar.smoothToShow();
        viewModel.checkAmount(responseTabungan.getNominalPembukaan().toString(), type);
    }

    public void submitBukaTabunganEmas(){
        if(responseTabungan != null && getResponseNasabah() != null){
            SubmitBE submitBE = new SubmitBE(
                    getResponseNasabah().getJalan(),
                    responseTabungan.getNominalPembukaan(),
                    getResponseNasabah().getIbuKandung(),
                    getResponseNasabah().getKecamatan().getId(),
                    getResponseNasabah().getKelurahan().getId(),
                    getResponseNasabah().getCity().getId(),
                    getResponseNasabah().getProvince().getId(),
                    getResponseNasabah().getJenisKelamin(),
                    getResponseNasabah().getNamaNasabah(),
                    getResponseNasabah().getNoHp(),
                    getResponseNasabah().getNoIdentitas(),
                    responseTabungan.getReffSwitching(),
                    getResponseNasabah().getStatusKawin(),
                    getResponseNasabah().getTglLahir(),
                    getResponseNasabah().getTempatLahir(),
                    getResponseNasabah().getUrlKtp(), 0L
            );
            viewModel.submitBukaTabunganEmas(submitBE);
        }
    }

    public void initDialogPin(){
        dialogPin = DialogPin.newInstance();
        dialogPin.setPositiveListener(view -> {
            PembukaanTabunganEmasActivity.this.validasiPin(dialogPin.getPinTransaksi());
            dialogPin.dismissDialog();
        });
        dialogPin.show(getSupportFragmentManager(), "");
    }

    private void validasiPin(String pin){
        circleProgressBar.smoothToShow();
        viewModel.validasiPin(pin);
    }

    public void handleOnNext(){
        if (tempPage < adapter.getCount()-1) {
            tempPage = tempPage + 1;
            pager.setCurrentItem(tempPage);
        }
    }

    public void handleOnBack(){
        if (tempPage > 1) {
            tempPage = tempPage - 1;
            pager.setCurrentItem(tempPage);
        } else {
            finishActivity();
        }
    }

    @Nullable
    @Override
    protected Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    protected AppCompatTextView getToolbarDescription() {
        return findViewById(R.id.toolbar_description);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_tabungan;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SIMULASI_BERHASIL.equals(response.type)){
                    areaSimulasi.setVisibility(View.VISIBLE);
                    responseTabungan = (ResponseTabungan) response.result;
                    showSimulationResult(responseTabungan);
                } else if (StringUtils.isNotBlank(response.type) &&
                        ConfigurationUtils.TAG_SALDO_SESUAI.equals(response.type)){
                    handleOnNext();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_ADD_KTP_IMAGE.equals(response.type)) {
                    viewModel.uploadImage((File) response.result);
                } else if (StringUtils.isNotBlank(response.type) &&
                        ConfigurationUtils.TAG_NASABAH_FOUND.equals(response.type)){
                    setNasabahFound();
                    setResponseNasabah((ResponseNasabah) response.result);
                    showNasabahResult();
                    setImageSearch(true);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PEMBUKAAN_TABUNGAN_ACTIVITY.equals(response.type)) {
                    sendKonfirmasiTabungan(ConfigurationUtils.TAG_KONFIRMASI_PEMBUKAAN_TABUNGAN);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_KONFIRMASI_BERHASIL.equals(response.type)){
                    handleOnNext();
                    responseTabungan = (ResponseTabungan) response.result;
                    showHasilKonfirmasi(responseTabungan);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_VALID.equals(response.type)){
                    submitBukaTabunganEmas();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_BUKA_TABUNGAN_EMAS_BERHASIL.equals(response.type)){
                    showDialogSuccessNoCancel("Berhasil Buka Tabungan Emas", dialog -> {
                        dialog.dismiss();
                        showActivityAndFinishCurrentActivity(this, BerandaActivity.class);
                    });
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT.equals(response.type)){
                    showActivityAndFinishCurrentActivity(this, LandingActivity.class);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SEARCH_DIALOG.equals(response.type)) {
                    getSimpleAdapter().getFilter().filter((CharSequence) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_PROVINCE_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogProvince(new ArrayList<>(getDataMap().values()));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KOTA_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogCity(new ArrayList<>(getDataMap().values()));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KECAMATAN_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogKecamatan(new ArrayList<>(getDataMap().values()));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_GET_KELURAHAN_SUCCESS.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    setDataMap((Map<String, String>) response.result);
                    showDialogKelurahan(new ArrayList<>(getDataMap().values()));
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_CAMERA_PERMISSION_GRANTED.equals(response.type)) {
                    circleProgressBar.smoothToHide();
                    EasyImage.configuration(PembukaanTabunganEmasActivity.this)
                            .setImagesFolderName(ConfigurationUtils.APP_NAME)
                            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                            .setCopyPickedImagesToPublicGalleryAppFolder(true)
                            .setAllowMultiplePickInGallery(true);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_SUCCESS_UPLOAD_IMAGE.equals(response.type)) {
                    urlKtpImage = (String) response.result;
                    loadKtpImageCircle(urlKtpImage);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_TAMBAH_NASABAH.equals(response.type)){
                    initlabelAlamat();
                    getStatusPernikahan();
                    pager.setCurrentItem(0);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOAD_STATUS_NIKAH.equals(response.type)){
                    circleProgressBar.smoothToHide();
                    loadStatusPernikahan(response);
                }
                circleProgressBar.smoothToHide();
                break;
            case ERROR:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_ERROR_SALDO.equals(response.type)){
                    showDialogError((String) response.result);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_NOMINAL_SALAH.equals(response.type)){
                    showDialogError(getString(R.string.minimal_buka_tabungan), PromptDialog::dismiss);
                } else if(StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_NASABAH_NOT_FOUND.equals(response.type)){
                    showDialogError((String) response.result, PromptDialog::dismiss);
                    setImageSearch(true);
                    setNasabahNotFound();
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_PIN_INVALID.equals(response.type)){
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_MINIMAL_PIN_INVALID.equals(response.type)){
                    showDialogError((String) response.result, PromptDialog::dismiss);
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_LOGOUT_FAILED.equals(response.type)) {
                    showDialogError(response.result.toString());
                } else {
                    showDialogError(response.result.toString());
                }
                circleProgressBar.smoothToHide();
                break;
        }
    }
}
