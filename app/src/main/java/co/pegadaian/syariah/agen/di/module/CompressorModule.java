package co.pegadaian.syariah.agen.di.module;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.compressor.Compressor;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public class CompressorModule {

    @Provides
    @Singleton
    Compressor provideCompressor(@Named("ApplicationContext") Context context) {
        return new Compressor(context);
    }
}
