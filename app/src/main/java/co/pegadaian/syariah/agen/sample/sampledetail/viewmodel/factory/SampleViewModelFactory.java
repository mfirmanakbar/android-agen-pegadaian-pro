package co.pegadaian.syariah.agen.sample.sampledetail.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleLocalUseCase;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;
import co.pegadaian.syariah.agen.sample.sampledetail.domain.interactors.SampleRemoteUseCase;
import co.pegadaian.syariah.agen.sample.sampledetail.viewmodel.SampleViewModel;

/**
 * Created by Dell on 2/27/2018.
 */

public class SampleViewModelFactory extends BaseViewModelFactory {

    private final SampleRemoteUseCase sampleRemoteUseCase;
    private final SampleLocalUseCase sampleLocalUseCase;
    private final RealmConfigurator realmConfigurator;

    public SampleViewModelFactory(SampleRemoteUseCase sampleRemoteUseCase,
                                  SampleLocalUseCase sampleLocalUseCase,
                                  SchedulersFacade schedulersFacade,
                                  RealmConfigurator realmConfigurator,
                                  JsonParser jsonParser,
                                  RxBus rxBus,
                                  PreferenceManager preferenceManager) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.sampleRemoteUseCase = sampleRemoteUseCase;
        this.sampleLocalUseCase = sampleLocalUseCase;
        this.realmConfigurator = realmConfigurator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SampleViewModel.class)) {
            return (T) new SampleViewModel(sampleRemoteUseCase, sampleLocalUseCase, realmConfigurator,
                    getSchedulersFacade(), getJsonParser(), getRxBus(), getPreferenceManager());
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}