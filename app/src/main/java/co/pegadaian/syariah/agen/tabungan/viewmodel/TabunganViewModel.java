package co.pegadaian.syariah.agen.tabungan.viewmodel;

import co.pegadaian.syariah.agen.base.viewmodel.BaseTransactionViewModel;
import co.pegadaian.syariah.agen.object.tabungan.KonfirmasiPembayaran;
import co.pegadaian.syariah.agen.object.tabungan.ResponseSimulasiTopUp;
import co.pegadaian.syariah.agen.object.tabungan.SubmitBeTopUp;
import co.pegadaian.syariah.agen.object.transaction.tabungan.DataPembayaran;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseNasabah;
import co.pegadaian.syariah.agen.object.transaction.tabungan.ResponseTabungan;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.tabungan.domain.interactors.TabunganUseCase;
import id.app.pegadaian.library.callback.ConsumerAdapter;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.common.ResponseNostraAPI;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.disposables.Disposable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class TabunganViewModel extends BaseTransactionViewModel {

    private final TabunganUseCase tabunganUseCase;

    public TabunganViewModel(SchedulersFacade schedulersFacade,
                             JsonParser jsonParser,
                             RxBus rxBus,
                             PreferenceManager preferenceManager,
                             TabunganUseCase tabunganUseCase,
                             RealmConfigurator realmConfigurator) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager, tabunganUseCase,realmConfigurator);
        this.tabunganUseCase = tabunganUseCase;
    }

    public int getProgress(int page, int totalPage){
        return tabunganUseCase.getProgress(page, totalPage);
    }

    public void checkAmount(String nominalAwal, String type){
        if(tabunganUseCase.minimumAmount(nominalAwal)){
            getDisposables().add(mulaiSimulasi(nominalAwal, type));
        } else {
            response().postValue(Response.error("",ConfigurationUtils.TAG_NOMINAL_SALAH));
        }
    }

    private Disposable mulaiSimulasi(String nominalAwal, String type){
        return tabunganUseCase.mulaiSimulasi(nominalAwal, type)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                        if (response().hasActiveObservers()){
                            if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                if(ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)){
                                    ResponseTabungan responseTabungan = getJsonParser()
                                            .getObject(respon.getResult(),ResponseTabungan.class);
                                    response().postValue(Response.success(responseTabungan,
                                            ConfigurationUtils.TAG_SIMULASI_BERHASIL));
                                } else if(ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)){
                                    response().postValue(Response.success("",
                                            ConfigurationUtils.TAG_SALDO_SESUAI));
                                } else if(ConfigurationUtils.TAG_KONFIRMASI_PEMBUKAAN_TABUNGAN.equals(type)){
                                    ResponseTabungan responseTabungan = getJsonParser()
                                            .getObject(respon.getResult(),ResponseTabungan.class);
                                    response().postValue(Response.success(responseTabungan,
                                            ConfigurationUtils.TAG_KONFIRMASI_BERHASIL));
                                }
                            } else {
                                if(ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)){
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                } else if(ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)){
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                }
                            }
                        }
                    }
                }, handleError());
    }

    public void checkKtpOrCif(String ktpOrCif, String type){
        if(tabunganUseCase.checkKtpOrCifLengt(ktpOrCif)){
            getDisposables().add(checkKtpOrCifRemote(ktpOrCif,type));
        } else{
            response().postValue(Response.error("Minimum input 10 digit",
                    ConfigurationUtils.TAG_INVALID_LENGTH));
        }

    }

    private Disposable checkKtpOrCifRemote(String ktpOrCif, String type) {
        return tabunganUseCase.checkKtpOrCif(ktpOrCif, type)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .doOnSubscribe(disposable -> response().postValue(Response.loading()))
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                        if (response().hasActiveObservers()){
                            if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                ResponseNasabah responseNasabah = getJsonParser().getObject(
                                        respon.getResult(),ResponseNasabah.class);
                                response().postValue(Response.success(responseNasabah,
                                        ConfigurationUtils.TAG_NASABAH_FOUND));
                            } else {
                                if(ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)){
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_CEK_SALDO_FAILED));
                                } else if(ConfigurationUtils.TAG_CHECK_CIF.equals(type)
                                        || ConfigurationUtils.TAG_CHECK_KTP.equals(type)){
                                    response().postValue(Response.error(respon.getResult(),
                                            ConfigurationUtils.TAG_NASABAH_NOT_FOUND));
                                }
                            }
                        }
                    }
                }, handleError());
    }

    public void validasiPin(String pin) {
        if(tabunganUseCase.checkPin(pin)){
            getDisposables().add(validasiRemotePin(pin));
        } else {
            response().postValue(Response.error("Minimal PIN 6 Digit", ConfigurationUtils.TAG_MINIMAL_PIN_INVALID));
        }
    }

    private Disposable validasiRemotePin(String pin) {
        return tabunganUseCase.validasiPin(pin)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                        if (response().hasActiveObservers()){
                            if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                response().postValue(Response.success("",ConfigurationUtils.TAG_PIN_VALID));
                            } else {
                                response().postValue(Response.error(respon.getResult(),ConfigurationUtils.TAG_PIN_INVALID));
                            }
                        }
                    }
                }, handleError());
    }

    public void sendSimulasiTopUp(String rekening, String nominal) {
        getDisposables().add(sendSimulasiTopUpRemote(rekening,nominal));
    }

    private Disposable sendSimulasiTopUpRemote(String rekening, String nominal) {
        return tabunganUseCase.sendSimulasiTopUp(rekening, nominal)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                .subscribe(new ConsumerAdapter<Object>() {
                    @Override
                    public void accept(Object object) throws Exception {
                        super.accept(object);
                        ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                        if (response().hasActiveObservers()){
                            if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                ResponseSimulasiTopUp response = getJsonParser().getObject(respon.getResult(),
                                        ResponseSimulasiTopUp.class);
                                response().postValue(Response.success(response,
                                        ConfigurationUtils.TAG_SIMULASI_TOPUP_SUCCESS));
                            } else {
                                response().postValue(Response.error(respon.getResult(),ConfigurationUtils.TAG_PIN_INVALID));
                            }
                        }
                    }
                }, handleError());
    }

    public void submitTopUp(String idAgen, SubmitBeTopUp submitBeTopUp) {
        getDisposables().add(
                tabunganUseCase.submitTopUp(idAgen,submitBeTopUp)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                        .subscribe(new ConsumerAdapter<Object>() {
                            @Override
                            public void accept(Object object) throws Exception {
                                super.accept(object);
                                ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                                if (response().hasActiveObservers()){
                                    if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                        response().postValue(Response.success("",
                                                ConfigurationUtils.TAG_TOP_UP_BERHASIL));
                                    } else {
                                        response().postValue(Response.error(respon.getResult(),ConfigurationUtils.TAG_PIN_INVALID));
                                    }
                                }
                            }
                        }, handleError())
        );
    }

    public void mulaiPembayaran(DataPembayaran dataPembayaran) {
        getDisposables().add(
                tabunganUseCase.mulaiPembayaran(dataPembayaran)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                        .subscribeOn(getSchedulersFacade().io())
                        .subscribe(new ConsumerAdapter<Object>() {
                            @Override
                            public void accept(Object object) throws Exception {
                                super.accept(object);
                                ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                                if (response().hasActiveObservers()){
                                    if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                        KonfirmasiPembayaran pembayaran = getJsonParser()
                                                .getObject(respon.getResult(), KonfirmasiPembayaran.class);
                                        response().postValue(Response.success(pembayaran,
                                                ConfigurationUtils.TAG_JENIS_LAYANAN_SUCCESS));
                                    } else {
                                        response().postValue(Response.error(respon.getResult(),ConfigurationUtils.TAG_PIN_INVALID));
                                    }
                                }
                            }
                        }, handleError())
        );
    }

    public void submitKonfirmasiPembayaran(DataPembayaran dataPembayaran) {
        getDisposables().add(
                tabunganUseCase.submitKonfirmasiPembayaran(dataPembayaran)
                .observeOn(getSchedulersFacade().ui())
                .subscribeOn(getSchedulersFacade().io())
                        .subscribeOn(getSchedulersFacade().io())
                        .subscribeOn(getSchedulersFacade().io())
                        .subscribe(new ConsumerAdapter<Object>() {
                            @Override
                            public void accept(Object object) throws Exception {
                                super.accept(object);
                                ResponseNostraAPI respon = getJsonParser().getObject(object,ResponseNostraAPI.class);
                                if (response().hasActiveObservers()){
                                    if(ConfigurationUtils.TAG_OK.equals(respon.getMessage())){
                                        response().postValue(Response.success("",
                                                ConfigurationUtils.TAG_KONFIRMASI_PEMBAYARAN_BERHASIL));
                                    } else {
                                        response().postValue(Response.error(respon.getResult(),ConfigurationUtils.TAG_PIN_INVALID));
                                    }
                                }
                            }
                        }, handleError())
        );
    }
}