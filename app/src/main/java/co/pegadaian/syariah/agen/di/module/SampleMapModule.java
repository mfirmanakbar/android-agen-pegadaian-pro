package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.sample.samplemap.domain.interactors.SampleMapUseCase;
import co.pegadaian.syariah.agen.sample.samplemap.domain.model.SampleMapRepository;
import co.pegadaian.syariah.agen.sample.samplemap.viewmodel.factory.SampleMapViewModelFactory;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by Dell on 3/4/2018.
 */

@Module
public class SampleMapModule {

    @Provides
    SampleMapRepository provideSampleMapRepository(PreferenceManager preferenceManager) {
        return new SampleMapRepository(preferenceManager);
    }

    @Provides
    SampleMapViewModelFactory provideSampleMapViewModelFactory(SampleMapUseCase sampleMapUseCase,
                                                               SchedulersFacade schedulersFacade,
                                                               @Named("JsonParser") JsonParser jsonParser,
                                                               @Named("RxBus") RxBus rxBus,
                                                               PreferenceManager preferenceManager) {
        return new SampleMapViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, sampleMapUseCase);
    }
}
