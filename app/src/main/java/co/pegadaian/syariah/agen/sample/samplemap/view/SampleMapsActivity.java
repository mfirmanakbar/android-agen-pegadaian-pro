package co.pegadaian.syariah.agen.sample.samplemap.view;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatTextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.sample.samplemap.viewmodel.SampleMapViewModel;
import co.pegadaian.syariah.agen.sample.samplemap.viewmodel.factory.SampleMapViewModelFactory;
import dagger.android.AndroidInjection;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import id.app.pegadaian.library.utils.string.StringHelper;

public class SampleMapsActivity extends FragmentActivity implements OnMapReadyCallback,
        LocationSource.OnLocationChangedListener {

    @BindView(R.id.current_location)
    AppCompatTextView currentLocation;
    @BindView(R.id.current_address)
    AppCompatTextView currentAddress;

    @Inject
    SampleMapViewModelFactory viewModelFactory;

    private GoogleMap map;
    private LatLng latLang;
    private Marker marker;
    private String latitude;
    private String longitude;
    private String positionLabel;
    private GoogleMap.OnMarkerDragListener markerDragListener;
    private SampleMapViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_maps);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SampleMapViewModel.class);
        viewModel.setActivity(this);
        viewModel.response().observe(this, this::processResponse);
        initResources();
        configureMap();
        initListener();
        getCurrentLocation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart(Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                updateDisplay("Please Wait!");
                updateAddress("Please Wait!");
                break;

            case SUCCESS:
                if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UPDATE_ADDRESS.equals(response.type)) {
                    updateAddress(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UPDATE_ADDRESS_FROM_PICKER.equals(response.type)) {
                    updateLocation(latLang);
                    updateAddress(response.result.toString());
                } else if (StringUtils.isNotBlank(response.type)
                        && ConfigurationUtils.TAG_UPDATE_LOCATION.equals(response.type)) {
                    updateLocation((Location) response.result);
                }
                break;

            case ERROR:
                updateDisplay(response.error.toString());
                break;
        }
    }

    private void updateDisplay(String value) {
        currentLocation.setText(value);
    }

    private void initResources() {
        positionLabel = getResources().getString(R.string.your_position_label);
    }

    private void configureMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initListener() {
        markerDragListener = new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                latLang = marker.getPosition();
                viewModel.updateLocationFromPicker(latLang);
            }
        };
    }

    private void getCurrentLocation() {
        viewModel.getCurrentLocation();
        viewModel.getAddress();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        googleMap.setOnMarkerDragListener(markerDragListener);
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    private void updateLocation(Location location) {
        latLang = new LatLng(location.getLatitude(), location.getLongitude());
        updateLocation(latLang);
    }

    private void updateAddress(String address) {
        currentAddress.setText(address);
    }

    private void updateLocation(LatLng latLang) {
        if (marker != null) {
            marker.remove();
        }
        latitude = String.valueOf(latLang.latitude);
        longitude = String.valueOf(latLang.longitude);
        marker = map.addMarker(new MarkerOptions().position(latLang).title(positionLabel)
                .draggable(true));
        marker.setPosition(latLang);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLang, 17));
        updateDisplay(StringHelper.getStringBuilderToString(latitude, ", ", longitude));
    }

    @OnClick(R.id.show_current_location)
    public void onShowCurrentLocationClicked() {
        viewModel.updateCurrentLocation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.unBinding();
    }
}
