package co.pegadaian.syariah.agen.sample.sampledetail.restapi;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Dell on 2/27/2018.
 */

public interface SampleRestApi {

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("token")
    Observable<Object> getToken(@Header("Authorization") String authorization,
                                @Field("grant_type") String grantType,
                                @Field("username") String userName,
                                @Field("password") String password);
}
