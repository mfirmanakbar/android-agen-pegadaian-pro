package co.pegadaian.syariah.agen.aktivasi.domain.model;

import co.pegadaian.syariah.agen.object.authentication.RequestChangePIN;
import co.pegadaian.syariah.agen.object.otp.RequestSendSms;
import co.pegadaian.syariah.agen.object.otp.RequestVerifyOtp;
import co.pegadaian.syariah.agen.restapi.RestApi;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 08/03/2018.
 */

public class AktivasiRemoteRepository {

    private final RestApi restApi;

    public AktivasiRemoteRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    public Observable<Object> sendOtp(RequestSendSms requestSendSms) {
        return restApi.sendOtp(requestSendSms);
    }

    public Observable<Object> verifyOtp(RequestVerifyOtp requestVerifyOtp) {
        return restApi.verifyOtp(requestVerifyOtp);
    }

    public Observable<Object> changePIN(String accessToken, RequestChangePIN requestChangePIN) {
        return restApi.changePIN(accessToken, requestChangePIN);
    }

    public Observable<Object> getUserProfile(String accessToken) {
        return restApi.getUserProfile(accessToken);
    }
}
