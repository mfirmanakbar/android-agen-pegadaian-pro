package co.pegadaian.syariah.agen.tabungan.domain.interactors;

import java.math.BigDecimal;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.base.usecase.BaseTransactionUseCase;
import co.pegadaian.syariah.agen.object.transaction.tabungan.DataPembayaran;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.base.usecase.BaseAgentUseCase;
import co.pegadaian.syariah.agen.beranda.domain.model.BerandaRepository;
import co.pegadaian.syariah.agen.object.beranda.Dashboard;
import co.pegadaian.syariah.agen.object.beranda.Menu;
import co.pegadaian.syariah.agen.object.tabungan.SubmitBeTopUp;
import co.pegadaian.syariah.agen.object.transaction.tabungan.SubmitBE;
import co.pegadaian.syariah.agen.object.userprofile.PrivilegeList;
import co.pegadaian.syariah.agen.object.userprofile.UserProfile;
import co.pegadaian.syariah.agen.tabungan.domain.model.TabunganRepository;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.utils.ConfigurationUtils;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class TabunganUseCase extends BaseTransactionUseCase {

    private final TabunganRepository repository;

    @Inject
    public TabunganUseCase(TabunganRepository repository) {
        super(repository);
        this.repository = repository;
    }

    public int getProgress(int page, int totalPage) {
        return (100 / totalPage) * (page + 1);
    }

    public boolean minimumAmount(String nominalAwal){
        BigDecimal nominal = new BigDecimal(nominalAwal);
        return nominal.compareTo(ConfigurationUtils.TAG_MINIMUM_NOMINAL_AWAL) >= 0;
    }

    public boolean checkKtpOrCifLengt(String ktpOrCif){
        return ktpOrCif.length()>=10;
    }

    public Observable<Object> mulaiSimulasi(String nominalAwal, String type){
        if (ConfigurationUtils.TAG_SIMULASI_BUKA_TABUNGAN.equals(type)){
            return repository.mulaiSimulasi(new BigDecimal(nominalAwal));
        } else if(ConfigurationUtils.TAG_CEK_SALDO_AGEN.equals(type)){
            return repository.validasiSaldo(new BigDecimal(nominalAwal));
        } else if(ConfigurationUtils.TAG_KONFIRMASI_PEMBUKAAN_TABUNGAN.equals(type)){
            return repository.konfirmasiTabungan(new BigDecimal(nominalAwal));
        } else {
            return null;
        }
    }

    public Observable<Object> checkKtpOrCif(String ktpOrCif, String type){
        if (ConfigurationUtils.TAG_CHECK_KTP.equals(type)){
            return repository.getNasabahByKtp(ktpOrCif);
        } else if(ConfigurationUtils.TAG_CHECK_CIF.equals(type)){
            return repository.getNasabahByCif(ktpOrCif);
        } else {
            return null;
        }
    }

    public boolean checkPin(String pin){
        if(pin.length() == 6){
            return true;
        } else {
            return false;
        }
    }

    public Observable<Object> validasiPin(String pin) {
        return repository.validasiPin(pin);
    }

    public Observable<Object> sendSimulasiTopUp(String rekening, String nominal) {
        return repository.sendSimulasiTopUp(rekening, nominal);
    }

    public Observable<Object> submitTopUp(String idAgen, SubmitBeTopUp submitBeTopUp) {
        return repository.submitTopUp(idAgen,submitBeTopUp);
    }

    public Observable<Object> mulaiPembayaran(DataPembayaran dataPembayaran) {
        return repository.mulaiPembayaran(dataPembayaran);
    }

    public Observable<Object> submitKonfirmasiPembayaran(DataPembayaran dataPembayaran) {
        return repository.submitKonfirmasiPembayaran(dataPembayaran);
    }
}