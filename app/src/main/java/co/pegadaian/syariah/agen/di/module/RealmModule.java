package co.pegadaian.syariah.agen.di.module;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;

/**
 * Created by Dell on 9/14/2017.
 */

@Module
public class RealmModule {

    @Provides
    @Singleton
    public RealmConfigurator providesRealmConfigurator() {
        return new RealmConfigurator();
    }
}