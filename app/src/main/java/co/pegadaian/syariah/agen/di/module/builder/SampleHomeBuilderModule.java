package co.pegadaian.syariah.agen.di.module.builder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import co.pegadaian.syariah.agen.di.module.SampleHomeModule;
import co.pegadaian.syariah.agen.sample.samplehome.view.HomeActivity;

/**
 * Created by Dell on 2/28/2018.
 */

@Module
public abstract class SampleHomeBuilderModule {

    @ContributesAndroidInjector(modules = {SampleHomeModule.class})
    abstract HomeActivity bindActivity();
}
