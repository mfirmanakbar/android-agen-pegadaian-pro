package co.pegadaian.syariah.agen.registration.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import id.app.pegadaian.library.view.fragment.BaseFragment;

public class RegistrationPasswordFragment extends BaseFragment {


    @BindView(R.id.password_edit_label)
    AppCompatTextView passwordEditLabel;
    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC)
    @BindView(R.id.password_edit_text)
    TextInputEditText passwordEditText;
    @NotEmpty
    @ConfirmPassword
    @BindView(R.id.confirm_password_edit_text)
    TextInputEditText confirmPasswordEditText;
    @BindView(R.id.finish_button)
    AppCompatButton finishButton;

    private Unbinder unbinder;
    private static RegistrationActivity registrationActivity;

    public static RegistrationPasswordFragment newInstance(RegistrationActivity activity) {
        RegistrationPasswordFragment registrationPersonalInfoFragment = new RegistrationPasswordFragment();
        registrationActivity = activity;
        return registrationPersonalInfoFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        configureFont();
        initValidator();
        return view;
    }

    private void configureFont() {
        passwordEditLabel.setTypeface(getLatoBold());
        passwordEditText.setTypeface(getLatoRegular());
        confirmPasswordEditText.setTypeface(getLatoRegular());
        finishButton.setTypeface(getLatoRegular());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        registrationActivity.initInputanPassword(confirmPasswordEditText, passwordEditText, finishButton);
    }

    @OnClick(R.id.finish_button)
    public void onViewClicked() {
        getValidator().validate();
    }


    @Override
    public void onValidationSucceeded() {
        registrationActivity.onNext();
    }
}