package co.pegadaian.syariah.agen.di.module;


import javax.inject.Named;

import co.pegadaian.syariah.agen.history.domain.interactor.HistoryUseCase;
import co.pegadaian.syariah.agen.history.domain.model.HistoryRepository;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.notification.domain.interactor.NotificationUseCase;
import co.pegadaian.syariah.agen.notification.domain.model.NotificationRepository;
import co.pegadaian.syariah.agen.notification.viewmodel.factory.NotificationViewModelFactory;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public class NotificationModule {

    @Provides
    NotificationRepository provideNotificationRepository(PreferenceManager preferenceManager) {
        return new NotificationRepository(preferenceManager);
    }

    @Provides
    NotificationViewModelFactory provideNotificationViewModelFactory(SchedulersFacade schedulersFacade,
                                                                     @Named("JsonParser") JsonParser jsonParser,
                                                                     @Named("RxBus") RxBus rxBus,
                                                                     PreferenceManager preferenceManager,
                                                                     NotificationUseCase notificationUseCase) {
        return new NotificationViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, notificationUseCase);
    }
}
