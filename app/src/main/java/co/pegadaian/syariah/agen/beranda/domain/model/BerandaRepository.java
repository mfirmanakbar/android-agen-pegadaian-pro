package co.pegadaian.syariah.agen.beranda.domain.model;

import co.pegadaian.syariah.agen.base.repository.BaseAgentRepository;
import co.pegadaian.syariah.agen.restapi.RestApi;
import id.app.pegadaian.library.restapi.LogoutRestApi;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import io.reactivex.Observable;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class BerandaRepository extends BaseAgentRepository {

    private final RestApi restApi;

    public BerandaRepository(PreferenceManager preferenceManager,
                             LogoutRestApi logoutRestApi, RestApi restApi) {
        super(preferenceManager, logoutRestApi);
        this.restApi = restApi;
    }

    public Observable<Object> getDataDashboard() {
        return restApi.dataDashboard(getToken());
    }
}
