package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.aktivasi.view.SetupPinActivity;
import co.pegadaian.syariah.agen.di.module.AktivasiModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Dell on 3/6/2018.
 */

@Module
public abstract class SetupPinBuilderModule {

    @ContributesAndroidInjector(modules = {AktivasiModule.class})
    abstract SetupPinActivity bindActivity();
}
