package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.math.BigDecimal;

import id.app.pegadaian.library.utils.string.StringHelper;

/**
 * Created by Dell on 3/16/2018.
 */

public class ItemGadai {

    private String category;
    @SerializedName("berat")
    @Expose
    private Double beratBersih;
    private BigDecimal taksiran;
    private int jumlah;
    @SerializedName("karat")
    @Expose
    private int karat;
    private Double beratKotor;
    private String keterangan;
    private File imageFile;
    @SerializedName("stl")
    @Expose
    private String stl;
    @SerializedName("type")
    @Expose
    private String type;

    public ItemGadai() {
    }

    public ItemGadai(String category, Double beratBersih, BigDecimal taksiran, int jumlah, int karat,
                     Double beratKotor, String keterangan, File imageFile, String stl) {
        this.category = category;
        this.beratBersih = beratBersih;
        this.taksiran = taksiran;
        this.jumlah = jumlah;
        this.karat = karat;
        this.beratKotor = beratKotor;
        this.keterangan = keterangan;
        this.imageFile = imageFile;
        this.stl = stl;
    }

    public ItemGadai(Double beratBersih, int karat, String stl) {
        this.beratBersih = beratBersih;
        this.karat = karat;
        this.stl = stl;
    }

    public ItemGadai(Double beratBersih,
                     BigDecimal taksiran,
                     int jumlah,
                     String keterangan,
                     File imageFile) {
        this.beratBersih = beratBersih;
        this.taksiran = taksiran;
        this.jumlah = jumlah;
        this.keterangan = keterangan;
        this.imageFile = imageFile;
    }

    public ItemGadai(Double beratBersih) {
        this.beratBersih = beratBersih;
    }

    public String getCategoryFormatted() {
        return StringHelper.getStringBuilderToString("[", getCategory(), "]");
    }

    public String getCategory() {
        return category;
    }

    public Double getBeratBersih() {
        return beratBersih;
    }

    public String getBeratBersihFormatted() {
        return StringHelper.getStringBuilderToString(String.valueOf(beratBersih), " gram");
    }

    public BigDecimal getTaksiran() {
        return taksiran;
    }

    public BigDecimal getTaksiranRoundingUp() {
        return taksiran.setScale(0, BigDecimal.ROUND_UP);
    }

    public String getTaksiranFormatted() {
        return StringHelper.getPriceInRp(getTaksiran());
    }

    public int getJumlah() {
        return jumlah;
    }

    public int getKarat() {
        return karat;
    }

    public Double getBeratKotor() {
        return beratKotor;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public File getImageFile() {
        return imageFile;
    }

    public String getStl() {
        return stl;
    }

    public void setStl(String stl) {
        this.stl = stl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}