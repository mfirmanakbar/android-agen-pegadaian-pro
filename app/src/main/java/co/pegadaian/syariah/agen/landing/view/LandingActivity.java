package co.pegadaian.syariah.agen.landing.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.login.view.LoginActivity;
import co.pegadaian.syariah.agen.registration.view.CheckUserKTPActivity;
import id.app.pegadaian.library.view.activity.BaseActivity;

/**
 * Created by ArLoJi on 05/03/2018.
 */

public class LandingActivity extends BaseActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_landing;
    }

    @OnClick({R.id.button_daftar, R.id.button_masuk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_daftar:
                showActivity(this, CheckUserKTPActivity.class);
                break;
            case R.id.button_masuk:
                showActivityAndFinishCurrentActivity(this, LoginActivity.class);
                break;
        }
    }
}
