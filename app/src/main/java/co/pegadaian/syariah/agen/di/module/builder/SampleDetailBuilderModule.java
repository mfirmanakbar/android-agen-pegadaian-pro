package co.pegadaian.syariah.agen.di.module.builder;

import co.pegadaian.syariah.agen.di.module.SampleDetailModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import co.pegadaian.syariah.agen.sample.sampledetail.view.SampleActivity;

/**
 * Created by Dell on 2/28/2018.
 */

@Module
public abstract class SampleDetailBuilderModule {

    @ContributesAndroidInjector(modules = {SampleDetailModule.class})
    abstract SampleActivity bindActivity();
}
