package co.pegadaian.syariah.agen.object;

import com.google.gson.annotations.Expose;

/**
 * Created by ArLoJi on 06/03/2018.
 */

public class Privilege {

    @Expose
    private String nama;
    @Expose
    private String id;
    @Expose
    private String versi;
    @Expose
    private String kategori;

    public Privilege(String nama, String id, String versi, String kategori) {
        this.nama = nama;
        this.id = id;
        this.versi = versi;
        this.kategori = kategori;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersi() {
        return versi;
    }

    public void setVersi(String versi) {
        this.versi = versi;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}
