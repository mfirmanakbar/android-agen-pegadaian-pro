package co.pegadaian.syariah.agen.di.module;

import javax.inject.Named;

import co.pegadaian.syariah.agen.beranda.domain.interactor.BerandaUseCase;
import co.pegadaian.syariah.agen.beranda.domain.model.BerandaRepository;
import co.pegadaian.syariah.agen.beranda.viewmodel.factory.BerandaViewModelFactory;
import co.pegadaian.syariah.agen.realm.RealmConfigurator;
import co.pegadaian.syariah.agen.restapi.RestApi;
import co.pegadaian.syariah.agen.restapi.adapter.RestApiAdapter;
import dagger.Module;
import dagger.Provides;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;

/**
 * Created by ArLoJi on 08/03/2018.
 */

@Module
public class BerandaModule {

    @Provides
    BerandaRepository provideBerandaRepository(PreferenceManager preferenceManager) {
        return new BerandaRepository(preferenceManager,
                RestApiAdapter.getLogoutRestApi(), RestApiAdapter.getRestApi());
    }

    @Provides
    BerandaViewModelFactory provideBerandaViewModelFactory(BerandaUseCase berandaUseCase,
                                                           SchedulersFacade schedulersFacade,
                                                           @Named("JsonParser") JsonParser jsonParser,
                                                           @Named("RxBus") RxBus rxBus,
                                                           PreferenceManager preferenceManager,
                                                           RealmConfigurator realmConfigurator) {
        return new BerandaViewModelFactory(schedulersFacade, jsonParser, rxBus, preferenceManager, berandaUseCase, realmConfigurator);
    }
}
