package co.pegadaian.syariah.agen.history.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.parceler.Parcels;

import javax.inject.Inject;

import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.history.viewmodel.HistoryViewModel;
import co.pegadaian.syariah.agen.history.viewmodel.factory.HistoryViewModelFactory;
import co.pegadaian.syariah.agen.object.history.DetailHistoryEmas;
import co.pegadaian.syariah.agen.object.history.DetailHistoryGadai;
import id.app.pegadaian.library.common.Response;
import id.app.pegadaian.library.utils.string.StringHelper;
import id.app.pegadaian.library.utils.time.TimeUtils;
import id.app.pegadaian.library.view.activity.BaseToolbarActivity;
import id.app.pegadaian.library.view.viewpager.ViewPagerAdapter;

/**
 * Created by T460s on 3/17/2018.
 */

public class HistoryDetailRiwayatEmasActivity extends BaseToolbarActivity {

    @Inject
    HistoryViewModelFactory viewModelFactory;
    HistoryViewModel viewModel;
    private ViewPagerAdapter adapter;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private DetailHistoryEmas detailHistoryEmas;
    private TextView noRekening, namaNasabah,jenisTransaksi,nominalTopup,biayaAdmin
            ,nilaToupRp,nilaToupGr;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
//        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HistoryViewModel.class);
//        viewModel.setActivity(this);
//        viewModel.response().observe(this, this::processResponse);
        configureToolbar("Detail Riwayat");
        initComponent();
    }

    private void initComponent(){
        noRekening = findViewById(R.id.tv_nomor_rekening);
        namaNasabah = findViewById(R.id.tv_nama_nasabah);
        jenisTransaksi = findViewById(R.id.tv_jenis_transaksi);
        nominalTopup = findViewById(R.id.tv_nominal_topup);
        biayaAdmin = findViewById(R.id.tv_biaya_administrasi);
        nilaToupRp = findViewById(R.id.tv_nilai_topup_rp);
        nilaToupGr = findViewById(R.id.tv_nilai_topup_gr);

        detailHistoryEmas = Parcels.unwrap(getIntent().getParcelableExtra("detailHistoryEmas"));

        noRekening.setText(String.valueOf(detailHistoryEmas.getNomorRekeningEmas()));
        namaNasabah.setText(detailHistoryEmas.getNamaNasabah());
        jenisTransaksi.setText(String.valueOf(detailHistoryEmas.getJenisTransaksi()));
        nominalTopup.setText(StringHelper.getPriceInRp(detailHistoryEmas.getNominal().doubleValue()));
        biayaAdmin.setText(StringHelper.getPriceInRp(detailHistoryEmas.getBiayaAdministrasi().doubleValue()));
        nilaToupGr.setText(StringHelper.getStringBuilderToString(detailHistoryEmas.getSaldoGr().toString()," gram"));
        nilaToupRp.setText(StringHelper.getPriceInRp(detailHistoryEmas.getSaldoRp().doubleValue()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        viewModel.unBinding();
    }

    @Override
    public AppCompatTextView getToolbarTitle() {
        return findViewById(R.id.toolbar_title);
    }

    @Override
    public Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_transaksi_detail_riwayat_emas;
    }

    private void processResponse(Response response) {
        switch (response.status) {
            case LOADING:
                break;
            case SUCCESS:
                break;
            case ERROR:
                break;
        }
    }


}
