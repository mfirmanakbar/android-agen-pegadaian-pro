package co.pegadaian.syariah.agen.notification.domain.interactor;

import javax.inject.Inject;

import id.app.pegadaian.library.compressor.Compressor;
import id.app.pegadaian.library.domain.interactors.BaseUseCase;
import id.app.pegadaian.library.domain.model.BaseRepository;

/**
 * Created by ArLoJi on 09/03/2018.
 */

public class NotificationUseCase extends BaseUseCase {

    @Inject
    public NotificationUseCase(BaseRepository repository,
                               Compressor compressor) {
        super(repository, compressor);
    }
}
