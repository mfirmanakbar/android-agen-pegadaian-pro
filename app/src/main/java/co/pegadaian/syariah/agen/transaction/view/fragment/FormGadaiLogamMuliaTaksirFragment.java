package co.pegadaian.syariah.agen.transaction.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.pegadaian.syariah.agen.R;
import co.pegadaian.syariah.agen.transaction.view.activity.dashboard.LogamMuliaActivity;
import id.app.pegadaian.library.view.fragment.BaseFragment;

/**
 * Created by Dell on 3/16/2018.
 */

public class FormGadaiLogamMuliaTaksirFragment extends BaseFragment {

    @BindView(R.id.list_keping)
    RecyclerView listKeping;
    @BindView(R.id.keping_logam)
    AppCompatSpinner kepingLogam;
    @NotEmpty
    @Length(max = 4)
    @BindView(R.id.jumlah_logam)
    EditText jumlahLogam;
    @NotEmpty
    @BindView(R.id.keterangan_barang)
    EditText keteranganBarang;
    @BindView(R.id.gambar_logam)
    ImageView gambarLogam;
    @BindView(R.id.label_gambar_logam)
    TextView labelGambarLogam;
    @BindView(R.id.progressPicture)
    ProgressBar progressPicture;
    @BindView(R.id.button_selanjutnya)
    AppCompatButton buttonSelanjutnya;
    @BindView(R.id.button_tambahkan_logam)
    AppCompatButton buttonTambahkanLogam;
    private Unbinder unbinder;
    private static LogamMuliaActivity logamMuliaActivity;

    public static FormGadaiLogamMuliaTaksirFragment newInstance(LogamMuliaActivity activity) {
        FormGadaiLogamMuliaTaksirFragment formGadaiPerhiasanTaksirFragment = new FormGadaiLogamMuliaTaksirFragment();
        logamMuliaActivity = activity;
        return formGadaiPerhiasanTaksirFragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gadai_logam_mulia, container, false);
        unbinder = ButterKnife.bind(this, view);
        initValidator();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        logamMuliaActivity.initGadaiLogamMuliaComponent(listKeping,
                kepingLogam,
                jumlahLogam,
                keteranganBarang,
                gambarLogam,
                progressPicture,
                buttonTambahkanLogam,
                buttonSelanjutnya);
    }

    @Override
    public void onValidationSucceeded() {
        logamMuliaActivity.addItemGadai();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_tambahkan_logam, R.id.button_selanjutnya})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_tambahkan_logam:
                getValidator().validate();
                break;
            case R.id.button_selanjutnya:
                logamMuliaActivity.nextToGadaiSimulasi();
                break;
        }
    }
}
