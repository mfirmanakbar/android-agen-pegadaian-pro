package co.pegadaian.syariah.agen.object.transaction.gadai;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dell on 3/17/2018.
 */

public class RequestSimulasiLogamMulia {

    @SerializedName("list_logam_mulia")
    @Expose
    private List<ItemGadai> logamMulia = null;
    @SerializedName("tenor")
    @Expose
    private Integer tenor;

    public RequestSimulasiLogamMulia(List<ItemGadai> logamMulia, Integer tenor) {
        this.logamMulia = logamMulia;
        this.tenor = tenor;
    }

    public List<ItemGadai> getLogamMulia() {
        return logamMulia;
    }

    public void setLogamMulia(List<ItemGadai> logamMulia) {
        this.logamMulia = logamMulia;
    }

    public Integer getTenor() {
        return tenor;
    }

    public void setTenor(Integer tenor) {
        this.tenor = tenor;
    }

}
