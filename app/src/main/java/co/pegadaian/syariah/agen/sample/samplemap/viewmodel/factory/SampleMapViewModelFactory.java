package co.pegadaian.syariah.agen.sample.samplemap.viewmodel.factory;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import co.pegadaian.syariah.agen.sample.samplemap.domain.interactors.SampleMapUseCase;
import co.pegadaian.syariah.agen.sample.samplemap.viewmodel.SampleMapViewModel;
import id.app.pegadaian.library.rx.SchedulersFacade;
import id.app.pegadaian.library.rx.bus.RxBus;
import id.app.pegadaian.library.utils.parser.JsonParser;
import id.app.pegadaian.library.utils.sharedprefences.PreferenceManager;
import id.app.pegadaian.library.viewmodel.factory.BaseViewModelFactory;

/**
 * Created by Dell on 3/4/2018.
 */

public class SampleMapViewModelFactory extends BaseViewModelFactory {

    private final SampleMapUseCase sampleMapUseCase;

    public SampleMapViewModelFactory(SchedulersFacade schedulersFacade,
                                     JsonParser jsonParser,
                                     RxBus rxBus,
                                     PreferenceManager preferenceManager,
                                     SampleMapUseCase sampleMapUseCase) {
        super(schedulersFacade, jsonParser, rxBus, preferenceManager);
        this.sampleMapUseCase = sampleMapUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SampleMapViewModel.class)) {
            return (T) new SampleMapViewModel(getSchedulersFacade(), getJsonParser(), getRxBus(), getPreferenceManager(), sampleMapUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
